<?php

require_once "lib/class.session.inc.php";

/** DB sessions  * */
$sessions = new Sessions();
session_write_close();
session_set_save_handler(
    array($sessions, "on_session_start"), array($sessions, "on_session_end"), array($sessions, "on_session_read"), array($sessions, "on_session_write"), array($sessions, "on_session_destroy"), array($sessions, "on_session_gc"));
$timeout = __SESSION_LIFE__;
ini_set('session.gc_maxlifetime', $timeout);

session_start();

if (!isset($_SESSION['user']['member_type'])) {
    $_SESSION['user']['member_type'] = 'visitor';
}
if (!isset($_SESSION['user']['lang'])) {
    $_SESSION['user']['lang'] = 'en';
}

//for local host test
// singapore
$adresse_ip = '118.200.158.116';

//thailand
//$adresse_ip = '203.151.232.114';
//bangkok
//$adresse_ip = '58.97.45.121';
//hong kong
//$adresse_ip = '117.18.1.1';
//Indo surabaya
//$adresse_ip = '117.18.23.23';

$root_path = explode(':', get_include_path());
$validCountry = array("SG", "TH", "HK");
$validCity = array("Singapore", "Phuket", "Bangkok", "Hong Kong", "Hong-Kong", "HongKong", "Hongkong");

if (empty($_SESSION['user']['search_country']) && !(empty($_SESSION['user']['country']) && in_array($_SESSION['user']['search_country'], $validCountry))) {

    // $reader = new Reader($root_path[count($root_path) - 1] . '/ressources/db_location/GeoLite2-City.mmdb');
    // $record = $reader->city($adresse_ip);

    $_SESSION['user']['country'] = $_SESSION['user']['search_country'] = 'SG';
    $_SESSION['user']['city'] = $_SESSION['user']['search_city'] = 'Singapore';
    $_SESSION['user']['latitude'] = '111';
    $_SESSION['user']['longitude'] = '11';

// When setting VPN to NYC, cityname = "Warner Robins !!!".

    if (!in_array($_SESSION['user']['search_city'], $validCity)) {
        if (!in_array($_SESSION['user']['search_country'], $validCountry)) {
            $_SESSION['user']['search_city'] = "Singapore";
        } else {
            switch ($_SESSION['user']['search_country']) {
                case 'TH':
                    $_SESSION['user']['search_city'] = "Bangkok";
                    break;
                case 'HK':
                    $_SESSION['user']['search_city'] = "Hong Kong";
                    break;
                default:
                    $_SESSION['user']['search_country'] = "SG";
                    break;
            }
        }
    }

    if (!in_array($_SESSION['user']['search_country'], $validCountry)) {
        $_SESSION['user']['search_country'] = "SG";
    }
}

///  Define var for auto login on Website.
if ($_SESSION['user']['member_type'] == 'visitor') {
    $platform = (isset($_REQUEST['platform'])) ? $_REQUEST['platform'] : '';
}
///  Define var for auto login on Website.
