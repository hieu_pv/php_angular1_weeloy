<?php

define("__SERVERNAME__", $_SERVER['SERVER_NAME']);

define("__ROOTDIR__", "");
define("__SERVERROOT__", $_SERVER['DOCUMENT_ROOT']);


define("__TMPDIR__",  $_SERVER['DOCUMENT_ROOT'] . __ROOTDIR__ . "tmp/");
define("__SHOWDIR__", __ROOTDIR__ . "/weeloy_media/upload/restaurant/");
define("__SHOWDIRUSER__", __ROOTDIR__ . "/weeloy_media/upload/user/");
define("__MEDIADIR__", __ROOTDIR__ . "/weeloy_media/");
define("__UPLOADDIR__", $_SERVER['DOCUMENT_ROOT'] . __SHOWDIR__);
define("__UPLOADDIRUSER__", $_SERVER['DOCUMENT_ROOT'] . __SHOWDIRUSER__);

define("AWS", strstr($_SERVER['HTTP_HOST'], 'localhost') === false);
define("__S3HOST__", "https://media.weeloy.com/");
define("__S3HOST1__", "https://media1.weeloy.com/");
define("__S3HOST2__", "https://media2.weeloy.com/");
define("__S3HOST3__", "https://media3.weeloy.com/");
define("__S3HOST4__", "https://media4.weeloy.com/");
define("__S3HOST5__", "https://media5.weeloy.com/");
define("__S3DIR__", "upload/restaurant/");
define("__S3DIRUSER__", "upload/user/");

define("MAX_FILE_SIZE", 1500000);	// max size in bytes for each uploaded file

define("__SESSION_LIFE__", 5400);
define("__SESSION_LIFE_MINUTE__", 90);

//Facebook Booking form app
define('BOOKING_APP_ID', '1566905370260628');
define('BOOKING_APP_SECRET', 'cc494eee79bce533707e4423877006ea');

//Facebook website app app dev
define('WEBSITE_FB_APP_ID', '1590587811210971');
define('WEBSITE_FB_APP_SECRET', '535d344eb1f08a575ca5556bd37fd159');

/** useful vars **/
$curPage = '';if (isset($_REQUEST['page'])) {
	$curPage = $_REQUEST['page'];
}

if (!defined('PHP_VERSION_ID')) {
	$version = explode('.', PHP_VERSION);

	define('PHP_VERSION_ID', ($version[0] * 10000 + $version[1] * 100 + $version[2]));
}

if (strstr($_SERVER['HTTP_HOST'], 'localhost:8888')) {
	set_include_path(get_include_path() . ':/Users/philippebenedetti/Sites/weeloy.com/');
	define("HTTP", "http://localhost:8888/weeloy_ext1/");
} else {

	//set_include_path(get_include_path() . ':/var/www/vhosts/dev.weeloy.com/current/');
	set_include_path(get_include_path() . ':/var/www/vhosts/dev.weeloy.com/current/');
	define("HTTP", "http://$_[SERVERNAME]/weeloy.com/"); //must do it for dev and prod

	//show errors on dev
	//error_reporting(E_ALL);
	//ini_set('error_reporting', E_ALL);
	//ini_set("display_errors", 1);

}

define("PROTOCOL", "https");

if (strstr($_SERVER['HTTP_HOST'], 'localhost:8888')) {
	$Conf['TemplateDir'] = "templates/";
} else {
	$Conf['TemplateDir'] = "templates/";
}
