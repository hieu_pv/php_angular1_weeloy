<!DOCTYPE html>
<html lang="en" ng-app="WeeloyApp">

<head>
    <meta charset="UTF-8">
    <title ng-bind="title"></title>
    <meta property="og:title" content="Book Best Restaurants in Singapore and Get Rewarded with Weeloy" />
    <meta property="og:type" content="website" />
    <meta property="og:description" content="Book Best Restaurants in Singapore with Weeloy and discover exclusive deals and promotions.Book your table and spin the wheel at your restaurant in Singapore" />
    <meta property="og:locale" content="en_US" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="owner" content="weeloy.com">
    <meta name="apple-itunes-app" content="app-id=973030193">
    <meta name="description" content="Book Best Restaurants in Singapore with Weeloy and discover exclusive deals and promotions.Book your table and spin the wheel at your restaurant in Singapore">
    <meta name="keywords" content="restaurants in Singapore, where to eat in Singapore, fine dining Singapore, best food in Singapore" />
    <base href="//<?php echo $_SERVER['HTTP_HOST'] ?>/weeloy_ext1/">
    <meta name="google-site-verification" content="iHu3Km_-ufs5DlCTZxayDBqSxOG8p2u26vWFqCfTY98" />
    <meta name="msvalidate.01" content="90FBBB685EF1AC19990BADECF586BE25" />
    <link rel="shortcut icon" href="favicon.ico" title="favoris icone">
    <script type="text/javascript">
        var BASE_URL = "http://<?php echo $_SERVER['HTTP_HOST'] ?>/weeloy_ext1";
        var FB_ID = '1590587811210971';
        <?php
            require_once("lib/wpdo.inc.php");
            require_once("conf/conf.session.inc.php");
            if(isset($_SESSION['email'])){
                $loggedin = 'true';
                $user = $_SESSION['email'];
            } else {
                $loggedin = 'false';
                $user = null;
            };
        ?>
        var loggedin = <?php echo $loggedin; ?>;
        var user = <?php echo ($user != null) ? json_encode($user) : 'null'; ?>;
    </script>
    <script src="client/bower_components/jquery/dist/jquery.min.js"></script>
    <script src="client/bower_components/angular/angular.min.js"></script>
    <script src="client/bower_components/angular-route/angular-route.min.js"></script>
    <script src="client/bower_components/angular-cookie/angular-cookie.min.js"></script>
    <script src="client/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="client/bower_components/typeahead.js/dist/typeahead.jquery.min.js"></script>
    <script src="client/bower_components/typeahead.js/dist/bloodhound.min.js"></script>
    <script src="client/bower_components/jRating/jquery/jRating.jquery.min.js"></script>
    <script src="client/assets/js/fresco/fresco.js"></script>
    <script src="client/weeloy.js"></script>
    <link rel="stylesheet" href="client/bower_components/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="client/bower_components/bootstrap/dist/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="client/bower_components/fontawesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="client/assets/css/fresco/fresco.css">
    <link rel="stylesheet" href="css/jRating.jquery.css">
    <link rel="stylesheet" href="client/assets/css/weeloy.css">
</head>

<body ng-controller="RootCtrl">
    <header>
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/weeloy_ext1/angular-client"><img src="images/logo_w_t_small.png"></a>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right" ng-show="loggedin == false">
                        <li>
                            <a href="#" data-toggle="modal" data-target="#loginModal">Login</a>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right loggedin-menu" ng-show="loggedin">
                        <li>
                            <a class="account" id="user-profile-btn" ng-click="showLoggedin($event)"><img src="images/ico_user-256.png" alt="weeloy-member-section"></a>
                        </li>
                        <div class="submenu" ng-show="showLoggedinBox">
                            <ul class="root">
                                <li><a href="angular-client/mybookings">Bookings</a></li>
                                <li><a href="angular-client/myreviews">Reviews</a></li>
                                <li><a href="angular-client/myaccount">Account</a></li>
                                <li><a href="#" ng-click="logout()">Logout</a></li>
                            </ul>
                        </div>
                    </ul>
                </div>
            </div>
        </nav>
        <div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="myLoginModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-content">
                        <!-- Include meta tag to ensure proper rendering and touch zooming -->
                        <meta name="viewport" content="width=device-width, initial-scale=1">
                        <!-- Include bootstrap stylesheets -->
                        <div class="container_modal">
                            <div class="mainbox">
                                <div class="panel panel-info">
                                    <div class="panel-body">
                                        <h1 id="title">Welcome</h1>
                                        <div class="loginbox">
                                            <div id="facebook" class="login-form">
                                                <a class="btn btn-lg btn-social btn-facebook" id="loginBtn" ng-click="LoginWithFacebook()" title="Log in using your Facebook account">
                                                    <i class="fa fa-facebook"></i>Login with Facebook</a>
                                                <div id="status_fb"></div>
                                            </div>
                                            <div class="or-spacer">
                                                <div class="mask"></div>
                                                <span><i>or</i></span>
                                            </div>
                                        </div>
                                        <!-- LoginForm -->
                                        <div ng-if="showForm == 'loginForm'" ng-include="'client/app/shared/partial/_login_form.html'"></div>
                                        <!-- LoginForm -->
                                        <!-- SignUpForm -->
                                        <div ng-if="showForm == 'signupForm'" ng-include="'client/app/shared/partial/_signup_form.html'"></div>
                                        <!-- SignUpForm -->
                                        <!-- Forgot Password Form -->
                                        <div ng-if="showForm == 'forgotPasswordForm'" ng-include="'client/app/shared/partial/_forgot_password_form.html'"></div>
                                        <!-- Forgot Password Form -->
                                        <div style="margin-bottom: 20px;" class="form-inline logmenu">
                                            <p ng-show="showForm == 'loginForm' || showForm == 'signupFrom'"><a href="#" ng-click="showForm = 'forgotPasswordForm'">Forgot password?</a></p>
                                            <p ng-show="showForm != 'loginForm'"><a href="#" ng-click="showForm = 'loginForm'">You prefer to Login?</a></p>
                                        </div>
                                        <div id="registerTag" ng-show="showForm != 'signupForm'">
                                            <div style="border-top: 1px solid#888; padding-top:15px; font-size:85%">
                                            Don't have an account! <a href="#" ng-click="showForm = 'signupForm'" id="register"> Register Here </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-2"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <div class="wrapper">
        <div id="system_msg" ng-show="showSystemMsg">
            <p ng-bind="systemMsg"></p>
            <i class="fa fa-close" ng-click="hideSystemMsg()"></i>
        </div>
        <div ng-view></div>
    </div>
    <footer>
        <div class="follow-us">
            <div class="container">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <h3>Find us on</h3>
                    <ul>
                        <li><a target="_blank" href="https://www.facebook.com/weeloy.sg" id='social-facebook'><span class="fa fa-facebook"></span></a></li>
                        <li><a target="_blank" href="https://twitter.com/weeloyasia" id='social-twitter'><span class="fa fa-twitter"></span></a></li>
                        <li><a target="_blank" href="https://www.linkedin.com/company/weeloy-pte-ltd" id='social-linkedin'><span class="fa fa-linkedin"></span></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="footer-bg">
            <div class="container">
                <div>
                    <h3>INFORMATION</h3>
                    <ul class="footer-col-content">
                        <li><a href="https://weeloy.com/blog">Food blog</a></li>
                        <li><a href="angular-client/how-it-works">How It Works</a></li>
                        <li><a href="angular-client/info-contact">Contact Us</a></li>
                        <li><a href="angular-client/info-partner">Partner</a></li>
                        <li><a href="angular-client/info-faq">FAQ's</a></li>
                        <li><a target="_blank" href="https://www.weeloy.com/templates/tnc/termsservices.php" id='info-tns'>Terms and Conditions of Service</a></li>
                        <li><a target="_blank" href="https://www.weeloy.com/templates/tnc/termsconditions.php" id='info-tnc'>Privacy policy and Terms</a></li>
                    </ul>
                </div>
                <div>
                    <h3>OUR LOCATIONS</h3>
                    <ul class="footer-col-content">
                        <li><a href="#">Singapore</a></li>
                        <li><a href="#">Malaysia</a></li>
                        <li><a href="#">Thailand</a></li>
                    </ul>
                </div>
                <div>
                    <h3>MOBILE</h3>
                    <div class="footer-col-content">
                        <p>
                            <a class="mobile-picture-div" target="_blank" href="http://itunes.apple.com/app/id973030193">
                                <img width="196" height="60" src="images/home_picture/app-store-badge_en.png" alt="Available on the App Store">
                            </a>
                        </p>
                        <p>
                            <a class="mobile-picture-div" target="_blank" href="https://play.google.com/store/apps/details?id=com.weeloy.client">
                                <img width="196" height="60" src="images/home_picture/play-store-badge_en.png" alt="Get it on Google Play">
                            </a>
                        </p>
                    </div>
                </div>
                <div>
                    <h3>NEWSLETTER</h3>
                    <div class="footer-col-content">
                        <form method="POST" ng-submit="SubmitNewletter(newletter_email)" id="newsletterform" name="newsletterform">
                            <input id="email" name="newletter_email" ng-model="newletter_email" type="email" placeholder="e-mail address" required>
                            <input id="submit-follow" type="submit" value="Join" />
                        </form>
                    </div>
                </div>
            </div>
            <div class="copyright">
                <p class="text-center">Copyright &copy; 2015 Weeloy. All Rights Reserved.</p>
            </div>
        </div>
    </footer>
</body>

</html>
