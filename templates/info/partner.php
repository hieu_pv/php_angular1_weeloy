<script type="text/javascript"><!--
                            function gen_mail_to_link(lhs, rhs, subject)
    {
        document.write("<A HREF=\"mailto");
        document.write(":" + lhs + "@");
        document.write(rhs + "?subject=" + subject + "\">" + lhs + "@" + rhs + "<\/A>");
    }
    // --> </script>

<script type="text/javascript">

    $(document).ready(function () {
        $('#contact-us-partner-btn').on("click", function () {
            $('#partner-contact-section').show();
            location.hash = "#contact-us-partner-btn";
        });
    });

</script>

<div id="partner-page">
    <div class="contact-us-section">
        <div class="container">
            <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                <h1 class="text-center">Your technology and eMarketing partner</h1>
                <div class="info">
                    <p class="text-center">Increase your market reach and online visibility,</p>
                    <p class="text-center">gain new and repeat customers and</p>
                    <p class="text-center">protect your brand with our very innovative & efficient solutions,</p>
                    <p class="text-center">......continuoulsy enhanced. </p>
                </div>
                <div class="contact-us-btn text-center">
                    <button id="contact-us-partner-btn">Contact us</button>
                </div>
            </div>
        </div>
    </div>

    <div class="partner-contact-section" id="partner-contact-section">
        <div class="container">
            <div class="solution-text-3 col-lg-12 col-sm-12 col-md-12 col-xs-12">
                <p class="text-center">Weeloy Pte Ltd</p>
                <p class="text-center">Lian Huat Building, Level 5</p>
                <p class="text-center">163 Tras Street</p>
                <p class="text-center">079024 Singapore</p>
            </div>

            <div class="solution-text-1 col-lg-12 col-sm-12 col-md-12 col-xs-12">
                <p class="text-center">

                    <script LANGUAGE="JavaScript" type="text/javascript"><!-- 
                      gen_mail_to_link('partner', 'weeloy.com', 'I am interested in your partner solution');
                        // --> </script>
                    <noscript>
                    <em>Email address protected by JavaScript. Activate javascript to see the email.</em>
                    </noscript>
                </p>
            </div>
            <div class="solution-text-3 col-lg-12 col-sm-12 col-md-12 col-xs-12">
                <p class="text-center">Singapore: +65 9025 3065</p>
                <p class="text-center">Thailand: +66 81 8943467</p>
                <p class="text-center">Hong Kong: +85 2 90350906</p>
            </div>

        </div>
    </div>

    <div class="solution-section">
        <div class="container">
            <div class="solution-text-1 col-lg-12 col-sm-12 col-md-12 col-xs-12">
                <p class="text-center">Our proprietary and integrated solutions include  a web and mobile Portal,</p>
                <p class="text-center">a White Label Reservation System featuring 14 languages, a robust Call Center tool and a unique</p>
                <p class="text-center">Walk-in system to let you build customer data, reviews and ratings.</p>
            </div>
        </div>
        <div class="solution-image">
            <div class="container">
                <div class="text-center col-lg-8 col-md-8 col-sm-10 col-xs-12 col-lg-offset-2 col-md-offset-2 col-sm-offset-1">
                    <img src="images/partner_page/partner-solution-mac.png">
                </div>
            </div>
        </div>
        <div class="solution-text-2">
            <div class="container">
                <p class="text-center">All our solutions are centralised into a powerful CRM for business reports,</p>
                <p class="text-center">data analysis and marketing operations.</p>
            </div>
        </div>
        <div class="solution-text-3">
            <div class="container">
                <p class="text-center">And beyond products and technology, we believe in exceptional client</p>
                <p class="text-center">service. You will be supported by  a professional team responsive to your </p>
                <p class="text-center">needs and insightful about the challenges you face.</p>
            </div>
        </div>
    </div>
    <div class="consumer-section">
        <div class="container">
            <div class="consumer-title">
                <p class="text-center">A very innovative and Powerful reservation </p>
                <p class="text-center">portal and mobile app for your guests</p>
            </div>
            <div class="consumer-text">
                <p class="text-center">Our consumer portal, Weeloy.com is a dynamic on-line reservation portal </p>
                <p class="text-center">and consumer mobile app providing consumers a fun dining experience and </p>
                <p class="text-center">an easy way to search and book while giving an innovative and powerful tool to optimize your </p>
                <p class="text-center">margin, generate additional business and enhance the customer value </p>
                <p class="text-center">proposition.</p>
            </div>
        </div>
    </div>
    <div class="feature-section">
        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 feature-title">
                <p class="text-center">Your dedicated restaurant page on weeloy.com</p>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 feature-image text-center">
                <img src="images/partner_page/ipad-feature-section.png" alt="">
            </div>
            <div class="feature-text col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <p class="text-center feature-text-title">feature:</p>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <p><span class="fa fa-check-circle"></span>Rich content as a video of your signature dishes</p>
                    <p><span class="fa fa-check-circle"></span>High resolution photo gallery</p>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <p><span class="fa fa-check-circle"></span>A clear menu proposition</p>
                    <p><span class="fa fa-check-circle"></span>A description of your main events and</p>
                </div>
                <div class="feature-text-last col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <p class="text-center">all services provided by your restaurant. </p>
                </div>
            </div>
        </div>
    </div>
</div>