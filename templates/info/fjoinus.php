
<div class="container">
    <div class="row">
        <div class="col-md-10 left-sec">

			<p style='font-family:Roboto;margin: 40px 0 0 0;font-size:24px;line-height:32px'>
			<strong>WEELOY.Dining is a unique Food and Beverage e-marketing platform and e-distribution channel,</strong></p>
            <p style='font-family:Roboto;margin: 0 0 0 40px;font-size:16px;line-height:32px'>
 			<br />offering WEELOY’s members dining privileges at your restaurant.
            <br />We are a Singapore-based company, developing our own technology.
            <br />Initially, WEELOY.Dining is introduced in Thailand and Singapore.
            <br />Our intention is to expand to new markets, across Asia Pacific, and eventually globally,
            <br />providing WEELOY members a worldwide selection of restaurants.
 			<br />
         
            If you are interested to be part of the expansion program, please contact us at...
 			<br />
 			<br />
 			<strong>Please complete this form and we will contact you - You can also call us at +65 9651 7408</strong>
 			<br />
            </p>

<form class="form-horizontal"  action='registercontact.php' id="contactform" name="contactform" method="post">
<input type='hidden' name='formcontact' value='fjoinus'>            
            
    <div class="row">
            
    <div id="login-overlay" class="modal-dialog" >
      <div class="modal-content">
          <div class="modal-header">
              <h4 class="modal-title text-primary" id="myModalLabel">Join us</h4>
          </div>
          <div class="modal-body">
			<fieldset>
				<div class="col-md-2"></div>
				<div class="col-md-8">
				
				<div class='row' style='margin-bottom:15px;'>
				<div class="input-group" id="div-fname">
						<span class="input-group-addon"><i class="glyphicon glyphicon-user icon-2x  text-primary"></i></span>
						<input type="text" class="form-control" value="" id='fname' name='fname' placeholder="Firstname">                                        
					</div>
					</div>

				<div class='row' style='margin-bottom:15px;'>
				<div class="input-group" id="div-lname">
						<span class="input-group-addon"><i class="glyphicon glyphicon-user  text-primary"></i></span>
						<input type="text" class="form-control" value="" id='lname' name='lname' placeholder="Lastname">                                        
					</div>
					</div>

				<div class='row' style='margin-bottom:15px;'>
				<div class="input-group" id="div-email">
						<span class="input-group-addon"><i class="glyphicon glyphicon-envelope  text-primary"></i></span>
						<input type="text" class="form-control" value="" id='email' name='email' placeholder="Email Address">                                        
					</div>
					</div>
				
				<div class='row' style='margin-bottom:15px;'>
				<div class="input-group" id="div-restaurant">
						<span class="input-group-addon"><i class="glyphicon glyphicon-cutlery  text-primary"></i></span>
						<input type="text" class="form-control" value="" id='restaurant' name='restaurant' placeholder="Your Restaurant">                                        
					</div>
					</div>
				
				<div class='row' style='margin-bottom:15px;'>
				<div class="input-group" id="div-city">
						<span class="input-group-addon"><i class="glyphicon glyphicon-tower  text-primary"></i></span>
						<input type="text" class="form-control" value="" id='city' name='city' placeholder="City">                                        
					</div>
					</div>
				
				<div class='row' style='margin-bottom:15px;'>

				<select id="countries_phone1" class="form-control bfh-countries" data-name="country"  data-country="SG" data-available="SG,TH,MY,HK,CN,JP,IN,ID,TW,AU,PH,NZ,KR,KH,VN,LA" ></select>
				</div>

				<div class='row' style='margin-bottom:15px;'>
				<div class="input-group" id="div-phone">
						<span class="input-group-addon"><i class="glyphicon glyphicon-earphone  text-primary"></i></span>
						<input type="text" class="form-control bfh-phone" id='tmp_phone' name='tmp_phone' data-name="phone"  data-country="countries_phone1" placeholder="Mobile Phone">                            
				</div>
				</div>

			   <div class="form-group" id="div-message">
				<textarea class="form-control" id="message" name="message" placeholder="Enter your message here. We will get back to you within 2 business days." rows="7"></textarea>
				</div>
				
				<div class="form-group required">
				<div class="col-md-12 text-center">
					<button type="button" onclick="verifForm(contactform);" class="btn btn-primary btn-lg">Submit</button>
				</div>
			</div>
			</div>
			</fieldset>
           	</div>
           	</div>
           	</div>
           	</div>
    </div>
</div>
</form>
</div>
</div>
</div>

<script type="text/javascript">
    function verifForm(formulaire)
    {
        var res = true;
        $("#div-lname").attr("class", "input-group");
        $("#div-fname").attr("class", "input-group");
        $("#div-email").attr("class", "input-group");
        $("#div-message").attr("class", "input-group");


        if ($('#fname').val().length < 3)
        {
            $("#div-fname").attr("class", "input-group has-error");
            res = false;
        }
        else jQuery("#div-fname").removeClass("has-error");

        if ($('#lname').val().length < 3)
        {
            $('#div-lname').attr("class", "input-group has-error");
            res = false;
        }
        else $('#div-lname').removeClass("has-error");

        if ($('#restaurant').val().length < 3)
        {
            $('#div-restaurant').attr("class", "input-group has-error");
            res = false;
        }
        else $('#div-restaurant').removeClass("has-error");

        if ($('#city').val().length < 3)
        {
            $('#div-city').attr("class", "input-group has-error");
            res = false;
        }
        else $('#div-city').removeClass("has-error");

        if ($('#tmp_phone').val().length < 5)
        {
            $('#div-phone').attr("class", "input-group has-error");
            res = false;
        }
        else $('#div-phone').removeClass("has-error");

        if (!validateEmail($('#email').val()))
        {
             $('#div-email').attr("class", "input-group has-error");
             res = false;
        }
        else $('#div-email').removeClass("has-error");

        if ($('#message').val().length < 3)
        {
            $('#div-message').attr("class", "form-group has-error");
            res = false;
        }
        else $('#div-message').removeClass("has-error");
        
        if(res === false){
            $("html, body").animate({scrollTop: "0px"});
            return false;
        }
        
		contactform.submit();
        return true;
    }

    function validateEmail(email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }
</script>  
