<section style=" margin-bottom: -10px;">
    <div class="main_picture">
        <div class="main_picture_content">
            <ul id='bxslider' class='bxslider' style='margin:0;padding:0;'>
                <li>
            <img style="width:100%" src="images/headers/header_partner_restaurant.jpg"> 
            <div style="font-family: Unkempt;transform: scale(1, 1) rotate(0deg); font-size: 50px; padding: 1px 4px 3px; margin: 0px; border-width: 0px; line-height: 36px; white-space: nowrap; min-width: 0px; width:100%; min-height: 0px; visibility: visible; opacity: 1; left:0; text-align:center; top:230px;"
                 class="tp-caption sfr very_big_white start" data-x="84" data-y="180" data-speed="600" data-start="400" data-easing="easeOutExpo">Excite your Restaurant<br/><br/>Business with Weeloy</div>
                </li></ul> </div>
    </div>

</section>


<section class="message-sec"> 
        <div class="text-center header_title">
        <h2 class ="title_info_page">Let WEELOY be YOUR preferred technology partner.</h2>
        <br/>
        <p class ="decription">We make dining out affordable and fun by providing consumers <br/>a fun, new way to search restaurants, compare best offers and book a table.
        <br/><br/>Our team of experts can help YOU increase YOUR online visibility, market reach and revenues.
        <br/><br/>Our dynamic distribution platform includes a content management system <br/>enabling YOU to easily update YOUR branded restaurant page on the Weeloy website in real time!
        <br/><br/>Imagine promoting YOUR offers, events, menus and chef in real time!
        </p>
    </div>
</section>




<section class="content-sec">         
    <div class="container">
                
        <div class="row">
            <div class="col-md-12" style="margin-top: 0px;">
                
                    <?php
        
        if(isset($_SESSION['info_message'])){
                echo '<div class="alert alert-'.$_SESSION['info_message']['type'].'"><a href="#" class="close" data-dismiss="alert">&times;</a><strong></strong> '.$_SESSION['info_message']['message'].'</div>';
                unset($_SESSION['info_message']);
        }          ?>
            <form class="form-horizontal"  action='registercontact.php' id="contactform" name="contactform" method="post">
                <input type='hidden' name='formcontact' value='partnerjoinus'>            
                <div class="row">

                    <div id="login-overlay" class="modal-dialog" >
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title text-primary" style="font-family:Helvetica" id="myModalLabel">Become a partner</h4>
                                <div class="header_title" ><p class ="decription">Contact our sales team to learn more about our services and solutions.</p></div>
                            </div>
                            <div class="modal-body">
                                <fieldset>
                                    <div class="col-md-2"></div>
                                    <div class="col-md-8">

                                        <div class='row' style='margin-bottom:15px;'>
                                            <div class="input-group" id="div-fname">
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-user icon-2x  text-primary"></i></span>
                                                <input type="text" class="form-control" value="" id='fname' name='fname' placeholder="Firstname">                                        
                                            </div>
                                        </div>

                                        <div class='row' style='margin-bottom:15px;'>
                                            <div class="input-group" id="div-lname">
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-user  text-primary"></i></span>
                                                <input type="text" class="form-control" value="" id='lname' name='lname' placeholder="Lastname">                                        
                                            </div>
                                        </div>

                                        <div class='row' style='margin-bottom:15px;'>
                                            <div class="input-group" id="div-email">
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-envelope  text-primary"></i></span>
                                                <input type="text" class="form-control" value="" id='email' name='email' placeholder="Email Address">                                        
                                            </div>
                                        </div>

                                        <div class='row' style='margin-bottom:15px;'>
                                            <div class="input-group" id="div-restaurant">
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-cutlery  text-primary"></i></span>
                                                <input type="text" class="form-control" value="" id='restaurant' name='restaurant' placeholder="Your Restaurant">                                        
                                            </div>
                                        </div>

                                        <div class='row' style='margin-bottom:15px;'>
                                            <div class="input-group" id="div-city">
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-tower  text-primary"></i></span>
                                                <input type="text" class="form-control" value="" id='city' name='city' placeholder="City">                                        
                                            </div>
                                        </div>

                                        <div class='row' style='margin-bottom:15px;'>

                                            <select id="countries_phone1" class="form-control bfh-countries" data-name="country"  data-country="SG" data-available="SG,TH,MY,HK,CN,JP,IN,ID,TW,AU,PH,NZ,KR,KH,VN,LA" ></select>
                                        </div>

                                        <div class='row' style='margin-bottom:15px;'>
                                            <div class="input-group" id="div-phone">
                                                <span class="input-group-addon"><i class="glyphicon glyphicon-earphone  text-primary"></i></span>
                                                <input type="text" class="form-control bfh-phone" id='tmp_phone' name='tmp_phone' data-name="phone"  data-country="countries_phone1" placeholder="Mobile Phone">                            
                                            </div>
                                        </div>

                                        <div class="form-group" id="div-message">
                                            <textarea class="form-control" id="message" name="message" placeholder="Enter your message here. We will get back to you within 2 business days." rows="7"></textarea>
                                        </div>

                                        <div class="form-group required">
                                            <div class="col-md-12 text-center">
                                                <button type="button" onclick="verifForm(contactform);" class="btn btn-primary btn-lg">Submit</button>
                                            </div>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
                
            </div>
        </div>
    </div>
</section>



<section class="logo_carousel">
    
    <div class="text-center header_title" style="color:#67676C;
        font-family: Helvetica; font-variant:small-caps">Our Restaurant Partners</div>
    
    <div class="container">
        <div class="row">
            <div class="logo-container">
                <img src="images/partner_mozaic.jpg">
            </div>
        </div>
    </div>
</section>

<script type="text/javascript">
    function verifForm(formulaire)
    {
        var res = true;
        $("#div-lname").attr("class", "input-group");
        $("#div-fname").attr("class", "input-group");
        $("#div-email").attr("class", "input-group");
        $("#div-message").attr("class", "input-group");


        if ($('#fname').val().length < 3)
        {
            $("#div-fname").attr("class", "input-group has-error");
            res = false;
        }
        else
            jQuery("#div-fname").removeClass("has-error");

        if ($('#lname').val().length < 3)
        {
            $('#div-lname').attr("class", "input-group has-error");
            res = false;
        }
        else
            $('#div-lname').removeClass("has-error");

        if ($('#restaurant').val().length < 3)
        {
            $('#div-restaurant').attr("class", "input-group has-error");
            res = false;
        }
        else
            $('#div-restaurant').removeClass("has-error");

        if ($('#city').val().length < 3)
        {
            $('#div-city').attr("class", "input-group has-error");
            res = false;
        }
        else
            $('#div-city').removeClass("has-error");

        if ($('#tmp_phone').val().length < 5)
        {
            $('#div-phone').attr("class", "input-group has-error");
            res = false;
        }
        else
            $('#div-phone').removeClass("has-error");

        if (!validateEmail($('#email').val()))
        {
            $('#div-email').attr("class", "input-group has-error");
            res = false;
        }
        else
            $('#div-email').removeClass("has-error");

        if ($('#message').val().length < 3)
        {
            $('#div-message').attr("class", "form-group has-error");
            res = false;
        }
        else
            $('#div-message').removeClass("has-error");



        if (res === false) {
            var bottom = $('#tttt').height(); 
            $("html, body").animate({scrollTop:   bottom + $('#tttt').offset().top});
            //$("html, body").animate({scrollTop: "0px"});
            return false;
        }

        contactform.submit();
        return true;
    }

    function validateEmail(email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }
</script>  

    <script type="text/javascript" src="js/bootstrap.min.js"></script> 
    <script type="text/javascript" src="js/headroom.js"></script>
    <script src="js/jquery.bxSlider.js"></script>
    <script type="text/javascript">
            $(document).ready(function () {
                $('#slider').bxSlider({
                    ticker: true,
                    tickerSpeed: 2000,
                    tickerHover: true
                });
            });
    </script>
