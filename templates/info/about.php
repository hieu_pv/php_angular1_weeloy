<script type="text/javascript">
    window.location.replace("how-it-works");
</script>
    

<link href="css/banner_image.css" rel="stylesheet" type="text/css" />


<section class="slider">
    <div class="main_picture">
        <div class="main_picture_content">
            <img style="width:100%" src="images/headers/header_aboutus.jpg"> 
            <div style="font-family: Unkempt;transform: scale(1, 1) rotate(0deg); font-size: 50px; padding: 1px 4px 3px; margin: 0px; border-width: 0px; line-height: 36px; white-space: nowrap; min-width: 0px; width:100%; min-height: 0px; visibility: visible; opacity: 1; left:0; text-align:center; top:230px;"
                 class="tp-caption sfr very_big_white start" data-x="84" data-y="180" data-speed="600" data-start="400" data-easing="easeOutExpo">Weeloy makes dining out <br/><br/>FUN & REWARDING</div>
        </div>
    </div>

</section>
<div style="clear: both;"></div>
<section class="content-sec" style="margin-top: -10px;">
    <div class="text-center header_title">
        <h2 class ="title_info_page">About us</h2>
        <p class ="decription">It's fun. It's rewarding. It's a game!</p>
        <p class ="decription">Weeloy is a new way to enjoy your favorite meals at participating restaurants<br/>and receive awesome prices and discounts.</p>
        
        <p class ="decription">We are delighted to provide you with an exciting experience in Singapore, Malaysia and Thailand.</p>
        <p class ="decription"></p>
        
    </div>	
    <p class='links'> 
            <a href="/search/restaurant/"> >&nbsp;&nbsp;Weeloy now!</a>
    </p>
</section>

<section class="content-sec" >         
    <div class="well well-sm" style="margin:0px; padding:0px; margin-bottom:10px; background-image: linear-gradient(to bottom, #F6F6F6 0px, #f5f5f5 100%)"  >

        <div class="text-center header_title"> <h2 class ="title">How it works?</h2>
        </div>


        <table style="width:100%" align="center">
            <tr>
                <td  width="15%"></td>
                <td width="15%" style="text-align:center;"><img src="images/home_localisation_250.png" style="max-width:180px; max-height:180px;"></img></td>
               
                <td width="70%" align="left"> 
                    <h3 class='h3_title'>1. Weeloy is FREE</h3>
                    <p class ="decription">Simply <b>visit our website</b> or download the Weeloy app - coming soon - to </p>
                    <p class ="decription">become a <b>Weeloyer</b> and plan your next FUN and REWARDING dining out experience,</p> 
                    <p class ="decription"> for <b>FREE.</b></p><p>&nbsp;</p>
                </td>		
            </tr>
            <tr><td  width="15%"></td>
                <td width="15%" style="text-align:center;"><img src="images/home_phone_sms_grey.png" style="text-align:center;max-width:150px; max-height:150px;"></img></td>
                
                <td width="70%" align="left">
                    <h3 class='h3_title'>2. Weeloy is EASY</h3>
                    <p class ="decription">You just have to search and select your restaurant according to the  </p>
                    <p class ="decription"><b>Weeloy Wheel score, </b>&nbsp;cuisine, location, budget and chef.</p>
                    <p class ="decription">Once you have selected your restaurant, you can book your table immediately </p>
                    <p class ="decription">and <b> get your weeloy code.</b> Automatically, you will receive a booking</p>
                    <p class ="decription">confirmation by email and text message.</p>
                    <p>&nbsp;</p>
                </td>						
            </tr>
            <tr><td  width="15%"></td>
                <td width="15%" style="text-align:center;"><img src="images/home_wheel.png" style="text-align:center;max-width:120px; max-height:120px;"></img></td>
                <td width="70%"align="left">
                    <h3 class='h3_title'>3. Weeloy is FUN</h3>
                    <p class ="decription">Upon your arrival at the restaurant, identify yourself as a Weeloyer and present your Weeloy Code.</p>
                    <p class ="decription">Restaurant staff will check your status and offer you to <b>spin the Weeloy Wheel.</b> </p>
                    <p class ="decription">The Wheel has 24 parts. In this example 88 is the wheel score based on </p>
                    <p class ="decription">the score of each offer of the wheel.</p><p class ="decription"><b>The higher the score, the better the offer.</b></p>
                    <p>&nbsp;</p>
                </td>							
            </tr>
        </table>
    </div>          

</section>


<section> 
   
    <div id="map" style="width:100%;height:380px; margin:0; padding:0;" align="center"></div>

</section>
<script type="text/javascript">

    var address = '32A duxton road';

    var map = new google.maps.Map(document.getElementById('map'), {
        mapTypeId: google.maps.MapTypeId.TERRAIN,
        zoom: 14,
        scrollwheel: false,
        navigationControl: false,
        mapTypeControl: false,
        scaleControl: false,
        draggable: true
    });

    var geocoder = new google.maps.Geocoder();

    geocoder.geocode({
        'address': address
    },
    function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            new google.maps.Marker({
                position: results[0].geometry.location,
                map: map
            });
            map.setCenter(results[0].geometry.location);
        }

    });

</script> 
<style>
    .decription{
        color:#67676C;
        font-family: Helvetica; 
        font-size:20px;

    }

    .title{
        color:#3399CC; 
        font-family: Helvetica; 
        font-size:25px;
        font-weight: bold;
        font-variant: small-caps;
        text-transform: none;
    }
    .h3_title{
        color:#3399CC; 
        font-family: Helvetica; 
        font-size:20px;
        font-weight: bold;
        font-variant: small-caps;
        text-transform: none;
    }
    .links{
        text-align :center;
        color:#3399CC;
        font-family: Helvetica;
        font-weight: Bold;
        font-size:18px;
    }
    </style>