

<section class="content-sec" >
        <?php
        
        if(isset($_SESSION['info_message'])){
                echo '<div class="alert alert-'.$_SESSION['info_message']['type'].'"><a href="#" class="close" data-dismiss="alert">&times;</a><strong></strong> '.$_SESSION['info_message']['message'].'</div>';
                unset($_SESSION['info_message']);
        }          
        ?>

    <div class="text-center header_title">
        <h2 class ="title_info_page">Your feedback is important to us.</h2>
        <p class ="decription">Please use the form below for any question. We would be happy to assist you.</b></p>
        <p class ="decription">To easily find your answers, please consult our <a href='info-faq'>FAQ's</a></p>
    </div>	
</section>
<section>
<form action='registercontact.php' class="form-horizontal" id="contactform" name="contactform" method="post">
<input type='hidden' name='formcontact' value='contact'>            
<div class="container">
    <div class="row">

    </div>
    <div class="row">
            
    <div id="login-overlay" class="modal-dialog" >
      <div class="modal-content">
          <div class="modal-header">
              <h4 class="modal-title text-primary" style="font-family:Helvetica"  id="myModalLabel">Contact us</h4>
          </div>
          <div class="modal-body">
			<fieldset>
				<div class="col-md-2"></div>
				<div class="col-md-8">
				
				<div class='row' style='margin-bottom:15px;'>
				<div class="input-group" id="div-fname">
						<span class="input-group-addon"><i class="glyphicon glyphicon-user icon-2x  text-primary"></i></span>
						<input type="text" class="form-control" value="" id='fname' name='fname' placeholder="Firstname">                                        
					</div>
					</div>

				<div class='row' style='margin-bottom:15px;'>
				<div class="input-group" id="div-lname">
						<span class="input-group-addon"><i class="glyphicon glyphicon-user  text-primary"></i></span>
						<input type="text" class="form-control" value="" id='lname' name='lname' placeholder="Lastname">                                        
					</div>
					</div>

				<div class='row' style='margin-bottom:15px;'>
				<div class="input-group" id="div-email">
						<span class="input-group-addon"><i class="glyphicon glyphicon-envelope  text-primary"></i></span>
						<input type="text" class="form-control" value="" id='email' name='email' placeholder="Email Address">                                        
					</div>
					</div>
				
				<div class='row' style='margin-bottom:15px;'>

				<select id="countries_phone1" class="form-control bfh-countries" data-name="country"  data-country="SG" data-available="SG,TH,MY,HK" ></select>
				</div>

				<div class='row' style='margin-bottom:15px;'>
				<div class="input-group" id="div-phone">
						<span class="input-group-addon"><i class="glyphicon glyphicon-earphone  text-primary"></i></span>
						<input type="text" class="form-control bfh-phone" id='tmp_phone' name='tmp_phone' data-name="phone"  data-country="countries_phone1" placeholder="Mobile Phone">                            
				</div>
				</div>

			   <div class="form-group" id="div-message">
				<textarea class="form-control" id="message" name="message" placeholder="Enter your message here. We will get back to you within 2 business days." rows="7"></textarea>
				</div>
				
				<div class="form-group required">
				<div class="col-md-12 text-center">
					<button type="button" onclick="verifForm(contactform);" class="btn btn-primary btn-lg">Submit</button>
				</div>
			</div>
			</div>
			</fieldset>
           	</div>
           	</div>
           	</div>
           	</div>
            </div>
    </div>
</div>
</form>
</section>

<script type="text/javascript">
    function verifForm(formulaire)
    {
        var res = true;
        $("#div-lname").attr("class", "input-group");
        $("#div-fname").attr("class", "input-group");
        $("#div-email").attr("class", "input-group");
        $("#div-message").attr("class", "input-group");


        if ($('#fname').val().length < 3)
        {
            $("#div-fname").attr("class", "input-group has-error");
            res = false;
        }
        else jQuery("#div-fname").removeClass("has-error");

        if ($('#lname').val().length < 3)
        {
            $('#div-lname').attr("class", "input-group has-error");
            res = false;
        }
        else $('#div-lname').removeClass("has-error");

        if ($('#tmp_phone').val().length < 5)
        {
            $('#div-phone').attr("class", "input-group has-error");
            res = false;
        }
        else $('#div-phone').removeClass("has-error");

        if (!validateEmail($('#email').val()))
        {
             $('#div-email').attr("class", "input-group has-error");
             res = false;
        }
        else $('#div-email').removeClass("has-error");

        if ($('#message').val().length < 3)
        {
            $('#div-message').attr("class", "form-group has-error");
            res = false;
        }
        else $('#div-message').removeClass("has-error");
        
        if(res === false){
            $("html, body").animate({scrollTop: "0px"});
            return false;
        }
        
		contactform.submit();
        return true;
    }

    function validateEmail(email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }
</script>  
</div>