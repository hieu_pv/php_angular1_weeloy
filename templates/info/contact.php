<div id="contact-page" ng-controller="InfoContactCtrl">
    <div class="container">
        <div class="contact-info col-lg-7 col-lg-offset-1 col-md-7 col-md-offset-1 col-sm-8 col-xs-12">
            <div class="col-lg-8 col-md-8 col-sm-10 col-xs-12">
                <h3>Your feedback is important to us.</h3>
                <div class="info">
                    <p>Please use the form for any question. We would be happy to assist you.</p>
                    <p>To easily find your answers, please check the FAQ's page.</p>
                </div>
                <div class="address">
                    <p>Contact information:</p>
                    <p><strong>Weeloy Pte Ltd</strong></p>
                    <p>163 Tras Street, Level 5</p>
                    <p>Lian Huat Building</p>
                    <p>079024, SIngapore</p>
                    <p>E-mail: contactus@weeloy.com</p>
                </div>
            </div>
        </div>
        <div class="contact-form col-lg-4 col-md-4 col-sm-4 col-xs-12">
            <p class="contact-form-title">Contact form</p>
            <form method="post" name="ContactForm" ng-submit="contact.$valid && ContactSubmit(contact)" novalidate>
                <div class="form-group">
                    <input type="text" name="firstname" ng-model="contact.firstname" placeholder="First name" ng-pattern="/^[a-z ,.'-]+$/i" required>
                    <div class="error">
                        <span ng-show="ContactForm.$submitted && ContactForm.firstname.$error.required">Please enter your first name.</span>
                        <span ng-show="ContactForm.$submitted && ContactForm.firstname.$error.pattern">First name must be alpha only.</span>
                    </div>
                </div>
                <div class="form-group">
                    <input type="text" name="lastname" ng-model="contact.lastname" placeholder="Last name" ng-pattern="/^[a-z ,.'-]+$/i" maxlength="40" required>
                    <div class="error">
                        <span ng-show="ContactForm.$submitted && ContactForm.lastname.$error.required">Please enter your last name.</span>
                        <span ng-show="ContactForm.$submitted && ContactForm.lastname.$error.pattern">Last name must be alpha only.</span>
                    </div>
                </div>
                <div class="form-group">
                    <input type="email" name="email" ng-model="contact.email" placeholder="Email address" maxlength="40" ng-pattern="/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i" required>
                    <div class="error">
                        <span ng-show="ContactForm.$submitted && ContactForm.email.$error.required">Please enter your email.</span>
                        <span ng-show="ContactForm.$submitted && ContactForm.email.$error.pattern">Please enter a valid email.</span>
                    </div>
                </div>
                <div class="form-group">
                    <select name="city" ng-model="contact.city" ng-options="city as city.name for city in cities"></select> 
                </div>
                <div class="form-group">
                    <input type="text" name="phone" ng-model="contact.phone" placeholder="Phone" ng-pattern="/^[0-9\+| ]{6,20}/" maxlength="600" required>
                    <div class="error">
                        <span ng-show="ContactForm.$submitted && ContactForm.phone.$error.required">Please enter your phone number.</span>
                        <span ng-show="ContactForm.$submitted && ContactForm.phone.$error.pattern">Please enter a valid phone number.</span>
                    </div>
                </div>
                <div class="form-group">
                    <textarea ng-model="contact.message" name="message" placeholder="Write your message here. We will get back to you within 2 business days." required></textarea>
                    <div class="error">
                        <span ng-show="ContactForm.$sumitted && ContactForm.message.$error.required">Please enter a message.</span>
                    </div>
                </div>
                <div class="form-group">
                    <input type="submit" value="Submit">
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    function verifForm(formulaire)
    {
        var res = true;
        $("#div-lname").attr("class", "input-group");
        $("#div-fname").attr("class", "input-group");
        $("#div-email").attr("class", "input-group");
        $("#div-message").attr("class", "input-group");


        if ($('#fname').val().length < 3)
        {
            $("#div-fname").attr("class", "input-group has-error");
            res = false;
        }
        else jQuery("#div-fname").removeClass("has-error");

        if ($('#lname').val().length < 3)
        {
            $('#div-lname').attr("class", "input-group has-error");
            res = false;
        }
        else $('#div-lname').removeClass("has-error");

        if ($('#tmp_phone').val().length < 5)
        {
            $('#div-phone').attr("class", "input-group has-error");
            res = false;
        }
        else $('#div-phone').removeClass("has-error");

        if (!validateEmail($('#email').val()))
        {
             $('#div-email').attr("class", "input-group has-error");
             res = false;
        }
        else $('#div-email').removeClass("has-error");

        if ($('#message').val().length < 3)
        {
            $('#div-message').attr("class", "form-group has-error");
            res = false;
        }
        else $('#div-message').removeClass("has-error");
        
        if(res === false){
            $("html, body").animate({scrollTop: "0px"});
            return false;
        }
        
		contactform.submit();
        return true;
    }

    function validateEmail(email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }
</script>  