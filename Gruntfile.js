module.exports = function(grunt) {

    // Project configuration.
    grunt.initConfig({
        uglify: {
            my_target: {
                files: {
                    'inc/search_restaurant/weeloy.min.js': [
                        'app/bower_components/bootstrap/dist/js/bootstrap.min.js',
                        'app/bower_components/typeahead.js/dist/typeahead.jquery.js',
                        'app/bower_components/angular/angular.js',
                        'inc/search_restaurant/directives.js',
                        'inc/search_restaurant/filters.js',
                        'inc/search_restaurant/controller.js'
                    ]
                }
            }
        },
        concat: {
            options: {
                separator: ';',
            },
            dist: {
                src: [
                    'inc/directives/*.js',
                    'inc/filters/*.js',
                    'inc/home/*.js',
                    'inc/search_restaurant/*.js',
                    'inc/info-contact/*.js',
                    'inc/app.js'
                ],
                dest: 'inc/weeloy.js',
            },
        },
        watch: {
            scripts: {
                files: [
                    'inc/directives/*.js',
                    'inc/filters/*.js',
                    'inc/home/*.js',
                    'inc/search_restaurant/*.js',
                    'inc/info-contact/*.js',
                    'inc/app.js'
                ],
                tasks: ['concat'],
                options: {
                    spawn: false,
                },
            },
        },
    });

    // Load the plugin that provides the "uglify" task.
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-watch');

    // Default task(s).
    grunt.registerTask('default', ['watch']);

};