<?php
/**
 * @file
 * User has successfully authenticated with Twitter. Access tokens saved to session and DB.
 */

/* Load required lib files. */
session_start();
require_once('twitteroauth/twitteroauth.php');
require_once('config.php');

/* If access tokens are not available redirect to connect page. */
if (empty($_SESSION['access_token']) || empty($_SESSION['access_token']['oauth_token']) || empty($_SESSION['access_token']['oauth_token_secret'])) {
    header('Location: ./clearsessions.php');
}
/* Get user access tokens out of the session. */
$access_token = $_SESSION['access_token'];

/* Create a TwitterOauth object with consumer/user tokens. */
$connection = new TwitterOAuth(CONSUMER_KEY, CONSUMER_SECRET, $access_token['oauth_token'], $access_token['oauth_token_secret']);

/* If method is set change API call made. Test is called by default. */
$content = $connection->get('account/verify_credentials');

/* Some example calls */
//$connection->get('users/show', array('screen_name' => 'abraham'));
//$connection->post('statuses/update', array('status' => date(DATE_RFC822)));
//$connection->post('statuses/destroy', array('id' => 5437877770));
//$connection->post('friendships/create', array('id' => 9436992));
//$connection->post('friendships/destroy', array('id' => 9436992));

/* Include HTML to display on the page */
// include("htmk.inc");

$myar = get_object_vars($content);
$usrAr = explode(' ', $myar['name']);

$_POST['userid'] = $myar['id'];
$_POST['userfirstname'] = $usrAr[0];
$_POST['userlastname'] = $usrAr[1];
$_POST['email'] = 'not_specified';
$_POST['application'] = 'twitter';
$_POST['timezone'] = $myar['time_zone'];
$_POST['langue'] = $myar['lang'];
$_POST['socialname'] = $myar['screen_name'];
$_POST['type_action'] = "Login";

require_once("../redirectURI.php");

//$myar['followers_count'],  $myar['friends_count'],  $myar['created_at'],  $myar['utc_offset'], 
//print_r($myar); 

?>
