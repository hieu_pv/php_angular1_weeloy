<?php

/*
$fb_data = array ( "token" => "CAAUn9QNxfu8BAMpDqJydtxHRMNRoXNrZB5ZBdOZBTr7o3ZAtLS5kD7tribbV1LncWi2bXOxzuWqd0naRwchbrDpx677levZBSDoXcGRoV1y2GH1xTuQ74yvsHrUJ0dsDdZA4SxxgO27tlGMUZBBj3BzcAWkC33LqXJuui6J0HUZADWKtrYFrtsizqb1e6tInYTZBhSxX7KZABaxMCpswfqHRyJvErlZA50l5TMZD",
 		"username" => "Richard Kefs",
		"userlastname" => "Kefs",
		"userfirstname" => "Richard",
		"userid" => "10204408720718496",
		"timezone" => "8",
		"application" => "facebook",
		"type_action" => "Login",
		"email" => "richard@kefs.me",
		"password" => ""
		);
*/		

require_once "conf/conf.init.inc.php";
require_once "lib/wpdo.inc.php";
require_once "conf/conf.session.inc.php";
require_once "lib/class.login.inc.php";
require_once "lib/class.coding.inc.php";
require_once "lib/class.mail.inc.php";
require_once "lib/class.sms.inc.php";
require_once "lib/class.spool.inc.php";
require_once "lib/class.async.inc.php";

//var_dump($_REQUEST['type_action']);

$login_type = get_valid_login_type($_REQUEST['patform']);
error_log("REDIRECT " . $login_type);

$login = new WY_Login($login_type);
$login->init();

$data = new WY_Arg;

if (isset($_REQUEST['type_action']) && $_REQUEST['type_action'] == 'Logout') {
	$_REQUEST['type_action'] = 'Logout';
	$_SESSION['website_logged_out'] = true;
}

if (empty($_REQUEST['type_action'])) {
	header("Location: login.php?message=Invalid Request");
}

$data->timezone = (isset($_REQUEST['timezone'])) ? $_REQUEST['timezone'] : "";
$data->userid = (isset($_REQUEST['userid'])) ? $_REQUEST['userid'] : "";
$data->token = (isset($_REQUEST['token'])) ? $_REQUEST['token'] : "";
$data->socialname = (isset($_REQUEST['socialname'])) ? $_REQUEST['socialname'] : "";
$data->application = (isset($_REQUEST['application'])) ? $_REQUEST['application'] : "";
$data->type_action = (isset($_REQUEST['type_action'])) ? $_REQUEST['type_action'] : "";

if ($data->application == "facebook" || $data->application == "linkedin") {
	$data->extrafield = $data->userid . " : " . $data->token;
}

//else if ($data->application == "twitter")
//    $data->extrafield = $data->userid . " : " . $data->socialname;



$reloadmess = "";
switch ($data->type_action) {
	case 'Logout':
		$login->logout($data);

		$reloadmess = "&state=reload";

		break;

	case 'Login':
		$reloadmess = "&state=login0";
		$data->email = $_REQUEST['email'];
		$data->password = $_REQUEST['password'];

		$_SESSION['website_logged_out'] = false;
		if ($login->process_login("Login", $data) > 0) {
			$reloadmess = "&state=loggedin";
		}

		break;

	case 'Register':
		$data->email = $_REQUEST['email'];
		$data->lastname = ucfirst($_REQUEST['lastname']);
		$data->firstname = ucfirst($_REQUEST['firstname']);
		$data->mobile = $_REQUEST['mobile'];
		$data->country = $_REQUEST['country'];
		$data->password = $_REQUEST['r_password'];
		$data->rpassword = $_REQUEST['r_rpassword'];
		if ($login->process_login("Register", $data) > 0) {
			$reloadmess = "&state=registered";
		}

		break;

	case 'LostPassword':
		$data->email = $_REQUEST['email'];
		$login->process_login("LostPassword", $data);
		break;

	case 'UpdatePassword':
		$data->email = $_REQUEST['email'];
		$data->password = $_REQUEST['u_password'];
		$data->rpassword = $_REQUEST['u_rpassword'];
		$data->npassword = $_REQUEST['u_npassword'];
		$login->process_login("UpdatePassword", $data);
		break;
	default:
}

/*
if ($data->type_action == 'Login' && empty($login->msg) && false) {
	if (empty($login->platform) || $data->application != "twitter") {
		header("Location: login.php?message=" . $login->msg . "&platform=" . $login_type . $reloadmess);
	} else {
		header("Location: ../../login.php?message=" . $login->msg . "&platform=" . $login_type . $reloadmess);
	}

} else {
*/
	$_SESSION['info_message']['type'] = ($data->type_action == 'Login' && $login->result < 0) ? 'danger' : 'success';

	if (isset($login->msg) && $login->msg != '') {
		$_SESSION['info_message']['message'] = $login->msg;
	}

	$url = getLoginURL($login_type);
	header("Location: $url");

//if(empty($data->application) || $data->application != "twitter")
//	header("Location: login.php?message=" . $login->msg . "&platform=" . $login_type . $reloadmess);
//else header("Location: ../../login.php?message=" . $login->msg . "&platform=" . $login_type . $reloadmess);
?>
