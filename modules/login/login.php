<?php

$message = "";
if (isset($_REQUEST['message'])) {
	$message = $_REQUEST['message'];
}

if (!$_REQUEST['state']) {
	$_REQUEST['state'] = '';
}
if ($_REQUEST['state'] == "reload") {
	header("location: reload.html?mess=$message");
	exit;
}
require_once("conf/conf.init.inc.php");
require_once("lib/wpdo.inc.php");
require_once("lib/class.login.inc.php");
require_once("lib/class.coding.inc.php");
require_once("lib/class.mail.inc.php");
require_once("lib/class.sms.inc.php");
require_once("lib/class.spool.inc.php");
require_once("conf/conf.session.inc.php");
require_once("lib/class.facebook.inc.php");

if ($_REQUEST['state'] == 'Logout') {
	require_once "logout.php";
}

$login_type = get_valid_login_type($_REQUEST['platform']);
$_SESSION['login_type'] = $login_type;
error_log("LOGIN " . $_SESSION['login_type']);


$login = new WY_Login($login_type);
$login->init();

$login->CheckLoginStatus();

$firstname = $login->signIn['firstname'];
$message .= "<br>" . $login->msg;

if($login->result > 0) {
	if ($_REQUEST['state'] == "loggedin") {
		require_once "loggedin.php";
	} else {
		require_once "logout.php";
	}
} else {
	require_once "prelogin.php";
}
?>
