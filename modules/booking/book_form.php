<?php
require_once("lib/Browser.inc.php");
$browser = new Browser();

require_once("conf/conf.init.inc.php");
require_once("lib/wpdo.inc.php");
//require_once("lib/class.session.inc.php");
require_once("conf/conf.session.inc.php");
require_once("lib/wglobals.inc.php");
require_once("lib/class.media.inc.php");
require_once("lib/class.restaurant.inc.php");
require_once("lib/class.promotion.inc.php");
require_once("lib/class.analytics.inc.php");
require_once("lib/class.coding.inc.php");
require_once("lib/class.member.inc.php");

$res = new WY_restaurant;
$restaurant_code = '';
if(isset($_REQUEST['signed_request'])) $signed_request = $_REQUEST['signed_request'];
$facebookappid = "1590587811210971";

if (!empty($signed_request)) {

	$facebookappid = "755782447874157";
	$_REQUEST['bktracking'] = "facebook";
	
	$data_signed_request = explode('.', $signed_request); // Get the part of the signed_request we need.
	$jsonData = base64_decode($data_signed_request['1']); // Base64 Decode signed_request making it JSON.
	$objData = json_decode($jsonData, true); // Split the JSON into arrays.
	$pageData = $objData['page'];
	$liked = $pageData['liked']; //you have the liked boolean
	$sourceData = $objData['app_data']; // yay you got the damn data in an string, slow clap for you
	$appDataArray = explode(';', $sourceData); // explode the app data because you hate it as a string, only do this if you are sending an array of variables through the app_data

	// go to fangate bc they dont like you, use a script so you don't get header errors
	if ($liked === false) {
		//page was not liked
		// go to other page bc they liked and send the app_data in a string
	} else {
		//all good
		//print_r($objData);

		//identify restaurant by page_id
		if (isset($objData['page']['id'])) {
			$facebook_pid = $objData['page']['id'];

			if (method_exists($res, 'getRestaurantByFacebookPageId')) {
				$res->getRestaurantByFacebookPageId($facebook_pid);
			} else {
				$where = 'facebook_pid = ' . $facebook_pid;
				$data = pdo_single_select("SELECT ID, restaurant, facebook_pid from restaurant WHERE $where LIMIT 1");

				$res = false;
				if ($data['facebook_pid'] != '' && $data['restaurant'] != '') {
					$res = $data['facebook_pid'];

					$res->getRestaurant($data['restaurant']);
				}
			}

			//echo "restaurant founded:";
			//print_r($res);

			if ($res) {
				$_REQUEST['bkrestaurant'] = $res->restaurant;
                $_REQUEST['bktitle'] = $res->title;
				$restaurant_code = $res->restaurant;
			}
		}

		$_SESSION['user'] = array();
		$_SESSION['user']['fbid'] = $objData['user_id'];

		//init user country
		if (!empty($objData['user']) && isset($objData['user']['country']) && empty($_REQUEST['bkcountry'])) {
			$facebook_user_country = strtoupper($objData['user']['country']);
			if (in_array($facebook_user_country, array('SG', 'TH', 'MY', 'HK', 'CN', 'ID', 'JP'))) {
				$_REQUEST['bkcountry'] = $facebook_user_country;
				//echo 'found country ' . $facebook_user_country;
			}
		}

	}
}




if (empty($_REQUEST['bkrestaurant'])) {
	$restaurant_code = 'SG_SG_R_TheFunKitchen';
	$_REQUEST['bkrestaurant'] = $restaurant_code;
	$_REQUEST['bktitle'] = 'The Fun Kitchen';
}

// CHECK IF TRACKING ACTIVE
$_REQUEST['bktracking'] = filter_input(INPUT_COOKIE, 'weelredir', FILTER_SANITIZE_STRING);


$arglist = array('bklangue', 'bksalutation', 'bklast', 'bkfirst', 'bkemail', 'bkcover', 'bkdate', 'bktime', 'bkmobile', 'bkcountry', 'bkrestaurant', 'bktitle', 'bkparam', 'bkspecialrequest', 'brwsr_type', 'bkpage', 'bktracking', 'data', 'booker', 'company', 'hotelguest', 'babychair');
foreach ($arglist as $label) {
	if(empty($_REQUEST[$label])) $_REQUEST[$label] = $$label = "";
    else $$label = preg_replace("/\'\"/", "’", $_REQUEST[$label]);
    }

if(empty($bkrestaurant)) {
	header("location: https://www.weeloy.com");
	exit;
	}

$cstate = "again"; 
if(empty($bklangue)) { $bklangue = "en"; $cstate = "new"; }

$bkparam = date('M') . strval(microtime(true));
$bkparam = tokenize($bkparam);
$timeout = (!empty($_REQUEST['timeout'])) ? $_REQUEST['timeout'] : "";

$res = new WY_restaurant;
$res->getRestaurant($bkrestaurant);

if(empty($res->restaurant)) {
	header("location: https://www.weeloy.com");
	exit;
	}

$minpers = intval($res->dfminpers);
if($minpers < 1 || $minpers > 3)
	$minpers = 1;
	
$AvailperPax = ($res->perPaxBooking()) ? "1":"0";

//tracking 
//test tracking cookie
$logger = new WY_log("website");
$logger->LogEvent($_SESSION['user']['id'], 801, '', $res->restaurant, '', date("Y-m-d H:i:s"));


// book / request process
$action = 'book';
if(filter_input(INPUT_GET, $action, FILTER_SANITIZE_STRING) == 'request'){
    $action = 'request';
}


$fullfeature = preg_match("/TheFunKitchen/", $bkrestaurant) || preg_match("/TheOneKitchen/", $bkrestaurant) || in_array($_SESSION['user']['member_type'], $fullfeature_member_type_allowed) || ($_SESSION['user']['member_type'] == 'weeloy_sales' && $res->status == 'demo_reference' || $res->status == 'active');

$typeBooking = ($res->is_bookable && ($res->status == 'active' || $res->status == 'demo_reference')) ? "true" : "false";
$is_listing = false;
if(empty($res->is_wheelable) || !$res->is_wheelable){
    $is_listing = true;
}

$nginit = "typeBooking=" . $typeBooking . ";";

$mediadata = new WY_Media($bkrestaurant);
$logo = $mediadata->getLogo($bkrestaurant);

$restaurant_restaurant_tnc = $res->restaurant_tnc;

$date = date_create(null, timezone_open("Asia/Singapore"));
$data_min = date_format($date, "d/m/Y");
date_add($date, date_interval_create_from_date_string("60 days"));
$data_max = date_format($date, "d/m/Y");

if (empty($bkdate))
    $bkdate = $data_min;

if (empty($bktime))
    $bktime = "19:00";

$pickDate = substr($bkdate, 6, 4) . "-" . substr($bkdate, 3, 2) . "-" . substr($bkdate, 0, 2);
$objToday = new DateTime(date("Y-m-d"));
$objDateTime = new DateTime($pickDate);
$ndays = $objToday->diff($objDateTime)->format('%a');

$actionfile = $_SERVER['PHP_SELF'];

$brwsr_type = ($browser->isMobile() || (isset($_REQUEST['brwsr_type']) && $_REQUEST['brwsr_type'] == "mobile")) ? "mobile" : "";
if($browser->isTablet()) $brwsr_type = "tablette";

$iphoneflg = (strtolower($browser->getPlatform()) == "iphone");
$termsconditions = "../../templates/tnc/termsservices.php?closebut=" . time(); // force to reload. Refresh does not force it on modal.

$countriesAr = array( array( 'a' => 'Australia', 'b' => 'au', 'c' => '+61' ), array( 'a' => 'China', 'b' => 'cn', 'c' => '+86' ), array( 'a' => 'Hong Kong', 'b' => 'hk', 'c' => '+852' ), array( 'a' => 'India', 'b' => 'in', 'c' => '+91' ), array( 'a' => 'Indonesia', 'b' => 'id', 'c' => '+62' ), array( 'a' => 'Japan', 'b' => 'jp', 'c' => '+81' ), array( 'a' => 'Malaysia', 'b' => 'my', 'c' => '+60' ), array( 'a' => 'Myanmar', 'b' => 'mm', 'c' => '+95' ), array( 'a' => 'New Zealand', 'b' => 'nz', 'c' => '+64' ), array( 'a' => 'Philippines', 'b' => 'ph', 'c' => '+63' ), array( 'a' => 'Singapore', 'b' => 'sg', 'c' => '+65' ), array( 'a' => 'South Korea', 'b' => 'kr', 'c' => '+82' ), array( 'a' => 'Thailand', 'b' => 'th', 'c' => '+66' ), array( 'a' => 'Vietnam', 'b' => 'vn', 'c' => '+84' ), array( 'a' => '', 'b' => '', 'c' => '' ), array( 'a' => 'Canada', 'b' => 'ca', 'c' => '+1' ), array( 'a' => 'France', 'b' => 'fr', 'c' => '+33' ), array( 'a' => 'Germany', 'b' => 'de', 'c' => '+49' ), array( 'a' => 'Italy', 'b' => 'it', 'c' => '+39' ), array( 'a' => 'Russia', 'b' => 'ru', 'c' => '+7' ), array( 'a' => 'Spain', 'b' => 'es', 'c' => '+34' ), array( 'a' => 'Sweden', 'b' => 'se', 'c' => '+46' ), array( 'a' => 'Switzerland', 'b' => 'ch', 'c' => '+41' ), array( 'a' => 'UnitedKingdom', 'b' => 'gb', 'c' => '+44' ), array( 'a' => 'UnitedStates', 'b' => 'us', 'c' => '+1' ), array( 'a' => '', 'b' => '', 'c' => '' ), array( 'a' => 'Afghanistan', 'b' => 'af', 'c' => '+93' ), array( 'a' => 'Albania', 'b' => 'al', 'c' => '+355' ), array( 'a' => 'Algeria', 'b' => 'dz', 'c' => '+213' ), array( 'a' => 'Andorra', 'b' => 'ad', 'c' => '+376' ), array( 'a' => 'Angola', 'b' => 'ao', 'c' => '+244' ), array( 'a' => 'Antarctica', 'b' => 'aq', 'c' => '+672' ), array( 'a' => 'Argentina', 'b' => 'ar', 'c' => '+54' ), array( 'a' => 'Armenia', 'b' => 'am', 'c' => '+374' ), array( 'a' => 'Aruba', 'b' => 'aw', 'c' => '+297' ), array( 'a' => 'Austria', 'b' => 'at', 'c' => '+43' ), array( 'a' => 'Azerbaijan', 'b' => 'az', 'c' => '+994' ), array( 'a' => 'Bahrain', 'b' => 'bh', 'c' => '+973' ), array( 'a' => 'Bangladesh', 'b' => 'bd', 'c' => '+880' ), array( 'a' => 'Belarus', 'b' => 'by', 'c' => '+375' ), array( 'a' => 'Belgium', 'b' => 'be', 'c' => '+32' ), array( 'a' => 'Belize', 'b' => 'bz', 'c' => '+501' ), array( 'a' => 'Benin', 'b' => 'bj', 'c' => '+229' ), array( 'a' => 'Bhutan', 'b' => 'bt', 'c' => '+975' ), array( 'a' => 'Bolivia', 'b' => 'bo', 'c' => '+591' ), array( 'a' => 'BosniaandHerzegovina', 'b' => 'ba', 'c' => '+387' ), array( 'a' => 'Botswana', 'b' => 'bw', 'c' => '+267' ), array( 'a' => 'Brazil', 'b' => 'br', 'c' => '+55' ), array( 'a' => 'Brunei', 'b' => 'bn', 'c' => '+673' ), array( 'a' => 'Bulgaria', 'b' => 'bg', 'c' => '+359' ), array( 'a' => 'BurkinaFaso', 'b' => 'bf', 'c' => '+226' ), array( 'a' => 'Burundi', 'b' => 'bi', 'c' => '+257' ), array( 'a' => 'Cambodia', 'b' => 'kh', 'c' => '+855' ), array( 'a' => 'Cameroon', 'b' => 'cm', 'c' => '+237' ), array( 'a' => 'CapeVerde', 'b' => 'cv', 'c' => '+238' ), array( 'a' => 'CentralAfricanRepublic', 'b' => 'cf', 'c' => '+236' ), array( 'a' => 'Chad', 'b' => 'td', 'c' => '+235' ), array( 'a' => 'Chile', 'b' => 'cl', 'c' => '+56' ), array( 'a' => 'ChristmasIsland', 'b' => 'cx', 'c' => '+61' ), array( 'a' => 'CocosIslands', 'b' => 'cc', 'c' => '+61' ), array( 'a' => 'Colombia', 'b' => 'co', 'c' => '+57' ), array( 'a' => 'Comoros', 'b' => 'km', 'c' => '+269' ), array( 'a' => 'CookIslands', 'b' => 'ck', 'c' => '+682' ), array( 'a' => 'CostaRica', 'b' => 'cr', 'c' => '+506' ), array( 'a' => 'Croatia', 'b' => 'hr', 'c' => '+385' ), array( 'a' => 'Cuba', 'b' => 'cu', 'c' => '+53' ), array( 'a' => 'Curacao', 'b' => 'cw', 'c' => '+599' ), array( 'a' => 'Cyprus', 'b' => 'cy', 'c' => '+357' ), array( 'a' => 'CzechRepublic', 'b' => 'cz', 'c' => '+420' ), array( 'a' => 'DemocraticRepCongo', 'b' => 'cd', 'c' => '+243' ), array( 'a' => 'Denmark', 'b' => 'dk', 'c' => '+45' ), array( 'a' => 'Djibouti', 'b' => 'dj', 'c' => '+253' ), array( 'a' => 'EastTimor', 'b' => 'tl', 'c' => '+670' ), array( 'a' => 'Ecuador', 'b' => 'ec', 'c' => '+593' ), array( 'a' => 'Egypt', 'b' => 'eg', 'c' => '+20' ), array( 'a' => 'ElSalvador', 'b' => 'sv', 'c' => '+503' ), array( 'a' => 'EquatorialGuinea', 'b' => 'gq', 'c' => '+240' ), array( 'a' => 'Eritrea', 'b' => 'er', 'c' => '+291' ), array( 'a' => 'Estonia', 'b' => 'ee', 'c' => '+372' ), array( 'a' => 'Ethiopia', 'b' => 'et', 'c' => '+251' ), array( 'a' => 'FalklandIslands', 'b' => 'fk', 'c' => '+500' ), array( 'a' => 'FaroeIslands', 'b' => 'fo', 'c' => '+298' ), array( 'a' => 'Fiji', 'b' => 'fj', 'c' => '+679' ), array( 'a' => 'Finland', 'b' => 'fi', 'c' => '+358' ), array( 'a' => 'FrenchPolynesia', 'b' => 'pf', 'c' => '+689' ), array( 'a' => 'Gabon', 'b' => 'ga', 'c' => '+241' ), array( 'a' => 'Gambia', 'b' => 'gm', 'c' => '+220' ), array( 'a' => 'Georgia', 'b' => 'ge', 'c' => '+995' ), array( 'a' => 'Ghana', 'b' => 'gh', 'c' => '+233' ), array( 'a' => 'Gibraltar', 'b' => 'gi', 'c' => '+350' ), array( 'a' => 'Greece', 'b' => 'gr', 'c' => '+30' ), array( 'a' => 'Greenland', 'b' => 'gl', 'c' => '+299' ), array( 'a' => 'Guatemala', 'b' => 'gt', 'c' => '+502' ), array( 'a' => 'Guernsey', 'b' => 'gg', 'c' => '+44-1481' ), array( 'a' => 'Guinea', 'b' => 'gn', 'c' => '+224' ), array( 'a' => 'Guinea-Bissau', 'b' => 'gw', 'c' => '+245' ), array( 'a' => 'Guyana', 'b' => 'gy', 'c' => '+592' ), array( 'a' => 'Haiti', 'b' => 'ht', 'c' => '+509' ), array( 'a' => 'Honduras', 'b' => 'hn', 'c' => '+504' ), array( 'a' => 'Hungary', 'b' => 'hu', 'c' => '+36' ), array( 'a' => 'Iceland', 'b' => 'is', 'c' => '+354' ), array( 'a' => 'Iran', 'b' => 'ir', 'c' => '+98' ), array( 'a' => 'Iraq', 'b' => 'iq', 'c' => '+964' ), array( 'a' => 'Ireland', 'b' => 'ie', 'c' => '+353' ), array( 'a' => 'IsleofMan', 'b' => 'im', 'c' => '+44-1624' ), array( 'a' => 'Israel', 'b' => 'il', 'c' => '+972' ), array( 'a' => 'IvoryCoast', 'b' => 'ci', 'c' => '+225' ), array( 'a' => 'Jersey', 'b' => 'je', 'c' => '+44-1534' ), array( 'a' => 'Jordan', 'b' => 'jo', 'c' => '+962' ), array( 'a' => 'Kazakhstan', 'b' => 'kz', 'c' => '+7' ), array( 'a' => 'Kenya', 'b' => 'ke', 'c' => '+254' ), array( 'a' => 'Kiribati', 'b' => 'ki', 'c' => '+686' ), array( 'a' => 'Kosovo', 'b' => 'xk', 'c' => '+383' ), array( 'a' => 'Kuwait', 'b' => 'kw', 'c' => '+965' ), array( 'a' => 'Kyrgyzstan', 'b' => 'kg', 'c' => '+996' ), array( 'a' => 'Laos', 'b' => 'la', 'c' => '+856' ), array( 'a' => 'Latvia', 'b' => 'lv', 'c' => '+371' ), array( 'a' => 'Lebanon', 'b' => 'lb', 'c' => '+961' ), array( 'a' => 'Lesotho', 'b' => 'ls', 'c' => '+266' ), array( 'a' => 'Liberia', 'b' => 'lr', 'c' => '+231' ), array( 'a' => 'Libya', 'b' => 'ly', 'c' => '+218' ), array( 'a' => 'Liechtenstein', 'b' => 'li', 'c' => '+423' ), array( 'a' => 'Lithuania', 'b' => 'lt', 'c' => '+370' ), array( 'a' => 'Luxembourg', 'b' => 'lu', 'c' => '+352' ), array( 'a' => 'Macao', 'b' => 'mo', 'c' => '+853' ), array( 'a' => 'Macedonia', 'b' => 'mk', 'c' => '+389' ), array( 'a' => 'Madagascar', 'b' => 'mg', 'c' => '+261' ), array( 'a' => 'Malawi', 'b' => 'mw', 'c' => '+265' ), array( 'a' => 'Maldives', 'b' => 'mv', 'c' => '+960' ), array( 'a' => 'Mali', 'b' => 'ml', 'c' => '+223' ), array( 'a' => 'Malta', 'b' => 'mt', 'c' => '+356' ), array( 'a' => 'MarshallIslands', 'b' => 'mh', 'c' => '+692' ), array( 'a' => 'Mauritania', 'b' => 'mr', 'c' => '+222' ), array( 'a' => 'Mauritius', 'b' => 'mu', 'c' => '+230' ), array( 'a' => 'Mayotte', 'b' => 'yt', 'c' => '+262' ), array( 'a' => 'Mexico', 'b' => 'mx', 'c' => '+52' ), array( 'a' => 'Micronesia', 'b' => 'fm', 'c' => '+691' ), array( 'a' => 'Moldova', 'b' => 'md', 'c' => '+373' ), array( 'a' => 'Monaco', 'b' => 'mc', 'c' => '+377' ), array( 'a' => 'Mongolia', 'b' => 'mn', 'c' => '+976' ), array( 'a' => 'Montenegro', 'b' => 'me', 'c' => '+382' ), array( 'a' => 'Morocco', 'b' => 'ma', 'c' => '+212' ), array( 'a' => 'Mozambique', 'b' => 'mz', 'c' => '+258' ), array( 'a' => 'Namibia', 'b' => 'na', 'c' => '+264' ), array( 'a' => 'Nauru', 'b' => 'nr', 'c' => '+674' ), array( 'a' => 'Nepal', 'b' => 'np', 'c' => '+977' ), array( 'a' => 'Netherlands', 'b' => 'nl', 'c' => '+31' ), array( 'a' => 'NetherlandsAntilles', 'b' => 'an', 'c' => '+599' ), array( 'a' => 'NewCaledonia', 'b' => 'nc', 'c' => '+687' ), array( 'a' => 'Nicaragua', 'b' => 'ni', 'c' => '+505' ), array( 'a' => 'Niger', 'b' => 'ne', 'c' => '+227' ), array( 'a' => 'Nigeria', 'b' => 'ng', 'c' => '+234' ), array( 'a' => 'Niue', 'b' => 'nu', 'c' => '+683' ), array( 'a' => 'NorthKorea', 'b' => 'kp', 'c' => '+850' ), array( 'a' => 'Norway', 'b' => 'no', 'c' => '+47' ), array( 'a' => 'Oman', 'b' => 'om', 'c' => '+968' ), array( 'a' => 'Pakistan', 'b' => 'pk', 'c' => '+92' ), array( 'a' => 'Palau', 'b' => 'pw', 'c' => '+680' ), array( 'a' => 'Palestine', 'b' => 'ps', 'c' => '+970' ), array( 'a' => 'Panama', 'b' => 'pa', 'c' => '+507' ), array( 'a' => 'PapuaNewGuinea', 'b' => 'pg', 'c' => '+675' ), array( 'a' => 'Paraguay', 'b' => 'py', 'c' => '+595' ), array( 'a' => 'Peru', 'b' => 'pe', 'c' => '+51' ), array( 'a' => 'Pitcairn', 'b' => 'pn', 'c' => '+64' ), array( 'a' => 'Poland', 'b' => 'pl', 'c' => '+48' ), array( 'a' => 'Portugal', 'b' => 'pt', 'c' => '+351' ), array( 'a' => 'Qatar', 'b' => 'qa', 'c' => '+974' ), array( 'a' => 'RepublicCongo', 'b' => 'cg', 'c' => '+242' ), array( 'a' => 'Reunion', 'b' => 're', 'c' => '+262' ), array( 'a' => 'Romania', 'b' => 'ro', 'c' => '+40' ), array( 'a' => 'Rwanda', 'b' => 'rw', 'c' => '+250' ), array( 'a' => 'SaintBarthelemy', 'b' => 'bl', 'c' => '+590' ), array( 'a' => 'SaintHelena', 'b' => 'sh', 'c' => '+290' ), array( 'a' => 'SaintMartin', 'b' => 'mf', 'c' => '+590' ), array( 'a' => 'Samoa', 'b' => 'ws', 'c' => '+685' ), array( 'a' => 'SanMarino', 'b' => 'sm', 'c' => '+378' ), array( 'a' => 'SaudiArabia', 'b' => 'sa', 'c' => '+966' ), array( 'a' => 'Senegal', 'b' => 'sn', 'c' => '+221' ), array( 'a' => 'Serbia', 'b' => 'rs', 'c' => '+381' ), array( 'a' => 'Seychelles', 'b' => 'sc', 'c' => '+248' ), array( 'a' => 'SierraLeone', 'b' => 'sl', 'c' => '+232' ), array( 'a' => 'Slovakia', 'b' => 'sk', 'c' => '+421' ), array( 'a' => 'Slovenia', 'b' => 'si', 'c' => '+386' ), array( 'a' => 'SolomonIslands', 'b' => 'sb', 'c' => '+677' ), array( 'a' => 'Somalia', 'b' => 'so', 'c' => '+252' ), array( 'a' => 'SouthAfrica', 'b' => 'za', 'c' => '+27' ), array( 'a' => 'SouthSudan', 'b' => 'ss', 'c' => '+211' ), array( 'a' => 'SriLanka', 'b' => 'lk', 'c' => '+94' ), array( 'a' => 'Sudan', 'b' => 'sd', 'c' => '+249' ), array( 'a' => 'Suriname', 'b' => 'sr', 'c' => '+597' ), array( 'a' => 'Swaziland', 'b' => 'sz', 'c' => '+268' ), array( 'a' => 'Syria', 'b' => 'sy', 'c' => '+963' ), array( 'a' => 'Taiwan', 'b' => 'tw', 'c' => '+886' ), array( 'a' => 'Tajikistan', 'b' => 'tj', 'c' => '+992' ), array( 'a' => 'Tanzania', 'b' => 'tz', 'c' => '+255' ), array( 'a' => 'Togo', 'b' => 'tg', 'c' => '+228' ), array( 'a' => 'Tokelau', 'b' => 'tk', 'c' => '+690' ), array( 'a' => 'Tonga', 'b' => 'to', 'c' => '+676' ), array( 'a' => 'Tunisia', 'b' => 'tn', 'c' => '+216' ), array( 'a' => 'Turkey', 'b' => 'tr', 'c' => '+90' ), array( 'a' => 'Turkmenistan', 'b' => 'tm', 'c' => '+993' ), array( 'a' => 'Tuvalu', 'b' => 'tv', 'c' => '+688' ), array( 'a' => 'Uganda', 'b' => 'ug', 'c' => '+256' ), array( 'a' => 'Ukraine', 'b' => 'ua', 'c' => '+380' ), array( 'a' => 'UnitedArabEmirates', 'b' => 'ae', 'c' => '+971' ), array( 'a' => 'Uruguay', 'b' => 'uy', 'c' => '+598' ), array( 'a' => 'Uzbekistan', 'b' => 'uz', 'c' => '+998' ), array( 'a' => 'Vanuatu', 'b' => 'vu', 'c' => '+678' ), array( 'a' => 'Vatican', 'b' => 'va', 'c' => '+379' ), array( 'a' => 'Venezuela', 'b' => 've', 'c' => '+58' ), array( 'a' => 'WallisandFutuna', 'b' => 'wf', 'c' => '+681' ), array( 'a' => 'WesternSahara', 'b' => 'eh', 'c' => '+212' ), array( 'a' => 'Yemen', 'b' => 'ye', 'c' => '+967' ), array( 'a' => 'Zambia', 'b' => 'zm', 'c' => '+260' ), array( 'a' => 'Zimbabwe', 'b' => 'zw', 'c' => '+263' ) );

if(empty($bkcountry)) {
	$bkcountry = "Singapore";
	}

$phoneIndex = "";
$startingPage = (empty($bkpage)) ? 1 : 0;
	
if(empty($bkpage)) {
	$userlastname = (isset($_SESSION['user']['lastname'])) ? $_SESSION['user']['lastname'] : "";
	$userfirstname = (isset($_SESSION['user']['lastname'])) ? $_SESSION['user']['firstname'] : "";
	$useremail = (isset($_SESSION['user']['email'])) ? $_SESSION['user']['email'] : "";
	$usermobile = (isset($_SESSION['user']['mobile'])) ? $_SESSION['user']['mobile'] : "";
	$usergender = (isset($_SESSION['user']['gender']) && $_SESSION['user']['gender'] == 'f') ? "Mrs." : "Mr.";
	if(isset($_SESSION['user']['country'])) {
		$pat = strtolower($_SESSION['user']['country']);
		$limit = count($countriesAr);
		for($i = 0; $i < $limit; $i++)
			if($pat == $countriesAr[$i]['b']) {
				$bkcountry = $countriesAr[$i]['a'];
				$phoneIndex = $countriesAr[$i]['c'];
				}
		}
	
	if (empty($bkfirst) && !empty($userfirstname))
		$bkfirst = $userfirstname;
	if (empty($bklast) && !empty($userlastname))
		$bklast = $userlastname;
	if (empty($bkemail) && !empty($useremail))
		$bkemail = $useremail;
	if (empty($bkmobile) && !empty($usermobile))
		$bkmobile = $usermobile;

	if(!empty($phoneIndex)) {
		if(empty($bkmobile)) $bkmobile = $phoneIndex;
		if(substr($bkmobile, 0, strlen($phoneIndex)) == $phoneIndex)
			$bkmobile = $phoneIndex . " " . substr($bkmobile, strlen($phoneIndex)); 			
		else if(preg_match("/\+\d{2,3} /", $bkmobile))
			$bkmobile = $phoneIndex . " " . preg_replace("/\+\d{2,3} /", "", $bkmobile); 
		else if(preg_match("/^\+/", $bkmobile))
			$bkmobile = $phoneIndex . " " . substr($bkmobile, 1); 
		}
		
	if (empty($bksalutation) && !empty($usergender))
		$bksalutation = $usergender;
	$bkpage = 1;
}

if($is_listing) {
    $promo = new WY_Promotion($bkrestaurant);
    $promo_details = $promo->getActivePromotionDetails();
    
    $phpdate = strtotime( $promo_details['end'] );
    $promo_end_date = date( 'd-m-Y', $phpdate );
	}

$hiddenlist = array( 'fbtoken' => '', 'fbemail' => '', 'fbname' => '', 'fblastname' => '', 'fbfirstname' => '', 'fbuserid' => '', 'fbtimezone' => '', 'userid' => '', 'token' => '', 'timezone' => '', 'application' => 'html', 'socialname' => '', 'bkrestaurant' => $bkrestaurant, 'bkpage' => $bkpage, 'bktitle' => $bktitle, 'bklangue' => $bklangue, 'bkparam' => $bkparam, 'bktracking' => $bktracking, 'brwsr_type' => $brwsr_type );
?>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv='expires' content='mon, 1 jan 1998 05:00:00 gmt'>
<meta http-equiv='pragma' content='no-cache'>
<meta http-equiv='pragma' content='cache-control: max-age=0'>
<meta http-equiv='last-modified' content='mon, 1 jan 1998 05:00:00 gmt'>
<meta http-equiv='cache-control' content='no-cache, must-revalidate'>
<meta name='robots' content='noindex, nofollow'>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<meta name="HandheldFriendly" content="true" />
<title>Book your table now - Weeloy Code - Weeloy.com</title>
<link href="../../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="../../bower_components/bootstrap/dist/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
<link href="../../bower_components/fontawesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="../../css/bootstrap-select.css" rel="stylesheet" >
<link href="../../css/bootstrap-social.css" rel="stylesheet" >
<link href="../../css/bookstyle.css" rel="stylesheet" >
<link href="../../css/famfamfam-flags.css" rel="stylesheet" >
<link href="../../css/dropdown.css" rel="stylesheet" >
<link href="../booking/bookstyle.css" rel="stylesheet" >
<script type="text/javascript" src="../../bower_components/jquery/dist/jquery.min.js"></script>
<script type='text/javascript' src="../../bower_components/angular/angular.min.js"></script>
<script type="text/javascript" src="../../js/jquery-ui.js"></script>
<script type="text/javascript" src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script type='text/javascript' src="../../js/ui-bootstrap-tpls-0.12.1.min.js"></script>
<script type="text/javascript" src="../../js/dayroutine.js"></script>
<script type="text/javascript" src="../../js/ngStorage.min.js"></script>
<script type="text/javascript" src="../../js/mylocal.js"></script>
<style>

<?php if ($brwsr_type != "mobile") echo ".mainbox { margin: 10px 30px 20px 55px; width:550px; } "; ?>

</style>
<?php include_once("ressources/analyticstracking.php") ?>
</head>

    <body ng-app="myApp" ng-cloak>
        <form class='form-horizontal' action='<?php echo $actionfile ?>' role='form' id='book_form' name='book_form' method='POST' enctype='multipart/form-data'>

            <div id='bookingCntler' ng-controller='MainController' ng-init="typeBooking = true;" >

                <div class="container mainbox">    
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <div class="panel-title" > 
                            <table width='80%'><tr><td>{{ zang['bookingtitle'].vl | uppercase}} <span ng-if='timeout'>({{zang['timeouttag'].vl }})</span></td><td align='right'>

  							<div class="dropdown">
                            <button type='button' id='itemdfcountry' class='btn btn-default btn-xs dropdown-toggle' data-toggle='dropdown'><i class="{{curlangicon}}"></i>&nbsp; <strong>{{curlang}}</strong> <span class='caret'></span></button>
							<ul class='dropdown-menu dropdown_itemdfcountry scrollable-menu-all' style='font-size:12px;'>
								<li ng-repeat="s in languages track by $index"  class='showall'><a ng-if="s.b != '';" href="javascript:;" ng-click="setmealtime($index, 5);" ><i class='famfamfam-flag-{{s.b}}'></i> {{s.a}}</a><hr ng-if="s.b == '';" /></li>
							</ul>                               
                            </div>
							</td></tr></table>
							
                            </div>
                        </div>     
                        <p <p class="slogan"><strong> {{zang['slogan'].vl}}&nbsp;!</strong></p>

                        <div class="panel-body" >
						   <?php
							reset($hiddenlist);
							while(list($label, $value) = each($hiddenlist))
								printf("<input type='hidden' id='%s' name='%s' value='%s'>", $label, $label, $value);
							?>

<?php if ($brwsr_type == "mobile") : ?>
							<div>				
                            <p><img ng-src='{{imglogo}}' max-width='120' max-height='120' id='theLogo' name='theLogo'></p>				
<?php else : ?>
                            <div class='col-xs-5'>
							<center><img ng-src='{{imglogo}}' max-width='120' max-height='120' id='theLogo' name='theLogo'></center><br>
							<table width='90%'>
							<tr ng-repeat="x in checkmark"><td><span class='glyphicon glyphicon-ok checkmark'></span> <br />&nbsp;</td><td ng-class='pcheckmark'><span >{{x.label1}} {{ x.label2}} <br />&nbsp;</td>
							</table><br/>
					   
							<a class="btn btn-sm btn-social btn-facebook" id='loginBtn' ng-click="facebkclik();" title="fill in using your Facebook account"><i class="fa fa-facebook"></i>{{ zang['facebookfillin'].vl }}</a>
							</div>
							<div class='col-xs-7 separation-left'>
<?php endif; ?>
							<div class="modal fade" id="remoteModal" tabindex="-1" role="dialog" aria-labelledby="remoteModalLabel" aria-hidden="true">  
								<div class="modal-dialog" style="background-color: #fff">  
									<div class="modal-content" style='font-size:11px;'></div>  
								</div>  
							</div>  

							<div class="row">
							<div class="alert alert-info" style='text-align:center;padding: 5px 0 5px 0;color:white;'>{{zang['bookingdetail'].vl | uppercase}}</div>
							</div>
							
							<div class="input-group">
								<span class="input-group-addon" ng-click="opendropdown($event, 'date')"><i class="glyphicon glyphicon-calendar"></i> &nbsp; <span class='caret'></span></span>

								<input ng-if="!$scope.is_test_server" type="text" class="form-control input" id='bkdate' name='bkdate' datepicker-popup="{{format}}" datepicker-options="dateOptions" ng-model="bkdate" ng-focus="opendropdown($event, 'date')" 
								is-open="date_opened" min-date="minDate" max-date="maxDate" date-disabled="disabled(date, mode)" ng-required="true" ng-change='updatendays();' close-text="{{ zang['close'].vl}}" current-text="{{ zang['today'].vl}}" clear-text="{{ zang['clear'].vl}}" readonly/>
                                                         	<input ng-if="$scope.is_test_server" type="text" class="form-control input" id='bkdate' name='bkdate' ng-model="bkdate" ng-required="true" />
							</div>

							<div class='input-group' ng-if="brwsr_type != 'mobile' && brwsr_type != 'tablette'">
								<div class='input-group-btn' dropdown is-open="time_opened">
									<button type='button' class='btn btn-default btn dropdown-toggle' data-toggle='dropdown'>
										&nbsp;<i class="glyphicon glyphicon-time"></i> &nbsp; <span class='caret'></span>
									</button>
									<ul class="dropdown-menu multi-level dropdown_itemdftime">
										<li class="dropdown-submenu">
											<a href="javascript:;">{{zang.lunchtag.vl}}</a>
											<ul class="dropdown-menu">
												<li ng-repeat="x in lunch"><a href="javascript:;" ng-click="setmealtime(x, 1);">{{ x }}</a></li>
											</ul>
										</li>
										<li class="dropdown-submenu">
											<a href="javascript:;">{{zang.dinnertag.vl}}</a>
											<ul class="dropdown-menu">
												<li ng-repeat="y in dinner"><a href="javascript:;" ng-click="setmealtime(y, 1);">{{ y }}</a></li>
											</ul>
										</li>
									</ul>
								</div>
								<input type='text' ng-model='bktime' value='' class='form-control input' id='bktime' name='bktime' ng-click="opendropdown($event, 'time')" readonly>
							</div>

						<div class='input-group' ng-if="brwsr_type == 'mobile' || brwsr_type == 'tablette'">
							<div class='input-group-btn' dropdown is-open="time_opened">
								<button type='button' class='btn btn-default btn dropdown-toggle' data-toggle='dropdown'>
									 &nbsp;<i class="glyphicon glyphicon-time"></i>&nbsp; {{zang.lunchtag.vl}}/{{zang.dinnertag.vl}} <span class='caret'></span>
								</button>
								<ul class='dropdown-menu dropdown_itemdftime scrollable-menu'>
									<li ng-repeat="x in lunch"><a href="javascript:;" ng-click="setmealtime(x, 1);">{{ x }}</a></li>
									<li>   -----------------------------  </li>
									<li ng-repeat="y in dinner"><a href="javascript:;" ng-click="setmealtime(y, 1);">{{ y }}</a></li>
								</ul>
							</div>
							<input type='text' ng-model="bktime" class='form-control input' id='bktime' name='bktime' ng-click="opendropdown($event, 'time')" readonly>
						</div>

						<div class='input-group'>
							<div class='input-group-btn' dropdown  is-open="cover_opened">
								<button type='button' class='btn btn-default btn dropdown-toggle' data-toggle='dropdown'>
									&nbsp;<i class="glyphicon glyphicon-cutlery"></i>&nbsp; {{zang.guesttag.vl}} <span class='caret'></span>
								</button>
								<ul class='dropdown-menu dropdown_itemdfminpers scrollable-menu'>
									<li ng-repeat="p in npers"><a href="javascript:;" ng-click="setmealtime(p, 2);">{{ p }}</a></li>
								</ul></div>
							<input type='text' ng-model='bkcover' class='form-control input' id='bkcover' name='bkcover' ng-click="opendropdown($event, 'cover')"  readonly >
						</div>

						<div class="row">
						<div class="alert alert-info" style='text-align:center;padding: 5px 0 5px 0;color:white;'>{{ zang['personaldetail'].vl | uppercase}}</div>
						</div>

						<div class='input-group' ng-show="langue !='jp';">
							<div class='input-group-btn' dropdown is-open="title_opened">
								<button type='button' id='itemdfsalut' class='btn btn-default btn dropdown-toggle' data-toggle='dropdown'>
									&nbsp;<i class="glyphicon glyphicon-tag"></i>&nbsp; {{zang.titletag.vl}} <span class='caret'></span>
								</button>
								<ul class='dropdown-menu dropdown_itemdfsalut'>
									<li ng-repeat="s in salutations"><a href="javascript:;" ng-click="setmealtime(s, 3);">{{ s}}</a></li>
								</ul>
							</div>
							<input type='text' ng-model='bksalutation' class='form-control input' id='bksalutation' name='bksalutation' ng-click="opendropdown($event, 'title')" readonly>
						</div>

						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
							<input type="text" ng-model='bkfirst' class="form-control input" id='bkfirst' name='bkfirst' placeholder="{{zang.firsttag.vl}}" ng-change='bkfirst=cleantext(bkfirst);'>                                        
						</div>

						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
							<input type="text" ng-model='bklast' class="form-control input" id='bklast' name='bklast' placeholder="{{zang.lasttag.vl}}" ng-change='bklast=cleantext(bklast);'>                                        
						</div>

						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
							<input type="text" ng-model='bkemail' class="form-control input" id='bkemail' name='bkemail' placeholder="{{zang.emailtag.vl}}" ng-change='bkemail=cleanemail(bkemail);'>                                        
						</div>

						<div class="input-group">
							<div class='input-group-btn '>
								 <button type='button' id='itemdfcountry' class='btn btn-default btn dropdown-toggle' data-toggle='dropdown'>
									&nbsp;<i class="glyphicon glyphicon-earphone"></i>&nbsp;<span class='caret'></span>
								</button>
								<ul class='dropdown-menu dropdown_itemdfcountry scrollable-menu' style='font-size:12px;'>
									<li ng-repeat="s in countries"><a ng-if="s.b != '';" href="javascript:;" ng-click="setmealtime(s.a, 4);" ><i class='famfamfam-flag-{{s.b}}'></i> {{s.a}}</a><hr ng-if="s.b == '';" /></li>
								</ul></div>
							  <input type="text" ng-model='bkmobile' class="form-control input" id='bkmobile' name='bkmobile' placeholder="{{zang.mobiletag.vl}}" ng-change='bkmobile=cleantel(bkmobile);' ng-blur='checkvalidtel();'>                            
							  <input type="hidden" ng-model='bkcountry' id='bkcountry' name='bkcountry'>                            
						</div>
						
						<div class="input-group">
							<span class="input-group-addon"><i class="glyphicon glyphicon-info-sign"></i></span>
							<textarea type="text" ng-model='bkspecialrequest' class="form-control input" id='bkspecialrequest' name='bkspecialrequest' rows="2"  placeholder="{{zang.requesttag.vl}}" ng-change='bkspecialrequest=cleantext(bkspecialrequest);'></textarea>
						</div>
						
						<div class="input-group nomarginbottom" >
							<a href="javascript:;" ng-click="checksubmit();" class="btn {{buttonClass}}"> {{ zang['buttonbook'].vl }} </a>
						</div>
                    </div>

					<div class='row' ng-if="brwsr_type == 'mobile'">
						<hr>
						<a class="btn btn-sm btn-social btn-facebook" id='loginBtn' ng-click="facebkclik();" title="fill in using your Facebook account" style='margin-left:5px;'><i class="fa fa-facebook"></i>{{ zang['facebookfillin'].vl}}</a>
						<hr>
						<ul>
						<li ng-repeat="x in checkmark"><p class='glyphicon glyphicon-ok checkmark' ng-if='x.glyph'></p><span ng-class='pcheckmark'> {{x.label1}} <br /> {{ x.label2}}<br/></span></li>
						</ul>
					</div>
					
                        </div>  
                    </div>
                </div>

            </div>

        </form>

<script>

<?php
echo "var tracking = '$bktracking';";
printf("var fullfeature = %s;", ($fullfeature) ? "true" : "false"); 
echo "var restaurant = '$bkrestaurant';";
echo "var minpers = '$minpers';";
echo "var maxpax = '30';";
echo "var maxpaxcc = '99';";
echo "var cstate = '$cstate';";

echo "var curndays = '$ndays';";
echo "var curdate = '$bkdate';";
echo "var curtime = '$bktime';";
echo "var curcountry = '$bkcountry';";
echo "var typeBooking = $typeBooking;";
echo "var timeout='$timeout';";
echo "var brwsr_type = '$brwsr_type';";
echo "var imglogo = '$logo';";
echo "var langue = '$bklangue';";
echo "var AvailperPax = '$AvailperPax';";	// true or false
echo "var startingPage = '$startingPage';";	// true or false

printf("var is_listing = %s;", $is_listing ? 'true' : 'false');
printf("var remainingday = %d; ", 364 - intval(date("z")));

echo "var arglist = [";
$sep = "";
for (reset($arglist); list($index, $label) = each($arglist); $sep = ", ") {
    echo $sep . "'" . $label . "', '" . $$label . "'";
}
echo "];";

?>

var topic = "BOOKING";
var token = "cc0987kj86hmns";

$(document).ready(function() {  });

var app = angular.module("myApp", ['ui.bootstrap', 'ngLocale', 'ngStorage']);

app.service('HttpServiceAvail',['$http',function($http){
	this.checkAvail = function(date, time, pers) {

		if(typeof date !== "string") 
			date = this.getStringDate(date);	
		date = date.replace(/\//g, "-");
		return $http.post("../../api/restaurant/availslot/",
			{
				'restaurant': restaurant,
				'date' 	: date,
				'time' 	: time,
				'pers' 	: pers,
				'token'	: token
			}).then(function(response) { return response.data;})
		};
		
	this.getStringDate = function(dd) { 
		aa = new Date(dd);
		yy = aa.getFullYear();
		mm = parseInt(aa.getMonth())+1;
		dd = aa.getDate()
		if(mm < 10) mm = "0" + mm;
		if(dd < 10) dd = "0" + dd;
		return dd + "/" + mm + "/" + yy;
		};
}]);

app.controller('MainController', function($scope, $http, $locale, $timeout, $location, $localStorage, HttpServiceAvail) {


	angular.copy(locales['en'], $locale);
	
	$scope.bkemail = $scope.bklast = $scope.bkfirst = $scope.bkmobile = $scope.userid = $scope.token = "";
	$scope.available = 1;
	$scope.imglogo = imglogo;
	$scope.bkdate = new Date();
	$scope.bkdate.setHours(0,0,0,0);
	$scope.timeout = (timeout == "y");
	$scope.is_test_server = (window.location.hostname.indexOf("test8910.weeloy.com") > -1);
        
	$scope.brwsr_type = brwsr_type;
	$scope.buttonClass = (typeBooking) ? 'btn-danger' : 'btn-green';
	dropdown_label = [ 'date', 'time', 'cover', 'title'];
	for(i = 0; i < dropdown_label.length; i++) $scope[dropdown_label[i] + '_opened'] = false;
	
	$scope.buttonLabel = (typeBooking) ? 'book now' : 'request your weeloy code';
	$scope.buttonConfirm = 'Confirm';
	$scope.bookingTitle = (typeBooking) ? "book your table" : "get your weeloy code";

	$scope.listTags2 = (typeBooking) ? "Instant Reservation" : "No reservartion";
	$scope.listTags3 = (typeBooking) ? "" : "First come, first serve";
	$scope.listTags4 = (!is_listing) ? "Win for every booking" : "Enjoy incredible";
	$scope.listTags5 = (!is_listing) ? "" : " promotion";
	$scope.slogan = "You are not far from your next FUN dining out experience";
	$scope.facebookfillin = "Book with Facebook";

	$scope.bookingtitlecf = (typeBooking) ? "BOOKING CONFIRMED" : "REQUESTED CONFIRMED";
	$scope.confirmsg = (typeBooking) ? "Your booking is confirmed at " : "Your request has been sent to ";
	$scope.confirmsg2 = (typeBooking) ? "We are happy to confirm your reservation" : "Your reservation number is ";
	$scope.confirmsg3 = "You will receive the details of your reservation by email and SMS";    
	$scope.confirmsg4 = "You can sign in using your email to view all your bookings and leave your reviews";

	$scope.error0 = 'invalid date. Try again';
	$scope.error1 = 'invalid time. Try again';
	$scope.error2 = 'invalid number of persons. Try again';
	$scope.error3 = 'invalid email. Try again';
	$scope.error4 = 'invalid lastname. Try again';
	$scope.error5 = 'invalid firstname. Try again';
	$scope.error6 = 'invalid mobile number. Try again';
	$scope.error7 = 'not available at that time. Try again'  ;    
	$scope.error8 = 'the restaurant is close that day. Try another time/day';
	$scope.error9 = 'there is not enough seats for this request. Modify your search';
	$scope.error10 = 'too many guests';
	$scope.error11 = 'seats left';
	$scope.error12 = 'not available for lunch';
	$scope.error13 = 'not available for dinner';
	$scope.msg1 = 'how many persons ?';

	$scope.tmc1 = "By confirming, I agree to";
	$scope.tmc2 = "the terms and conditions";
	
	$scope.langdata = $localStorage.langdata;
	if(cstate == "again")
		if(typeof $scope.langdata !== "undefined" && $scope.langdata.version !== "undefined" && $scope.langdata.version.lb !== "undefined") {
			 if($scope.langdata.version.lb == restaurant)
				$scope.zang = $scope.langdata;
			}
	
	if(typeof $scope.zang === "undefined") {
		$scope.zang = {
		  'version' : { "lb": restaurant, "vl": $scope.bkdate, "f":"c" }, 
		  'lunchtag': { "lb": "lunch", "vl": "Lunch", "f":"c" }, "dinnertag": { "lb" : "dinner", "vl": "Dinner", "f":"c" }, "guesttag": { "lb" : "guests", "vl": "Guests", "f":"c" }, "titletag": { "lb" : "title", "vl": "Title", "f":"c" }, "firsttag": { "lb" : "firstname", "vl": "First name", "f":"c" }, 
		  'datetag': {"lb": "Date", "vl": "Date", "f":"c" }, 'timetag': {"lb": "time", "vl": "time", "f":"c" }, "nametag": { "lb" : "name", "vl": "name", "f":"c" }, 
		  'lasttag': { "lb" : "lastname", "vl": "Last name", "f":"c" }, "emailtag": { "lb" : "email", "vl": "Email", "f":"c" }, "mobiletag": { "lb" : "mobile", "vl": "Mobile", "f":"c" }, "requesttag": { "lb" : "special request", "vl": "Special request", "f":"c" },
		  'today': { "lb" : "today", "vl": "Today", "f":"c" }, 'clear': { "lb" : "clear", "vl": "clear", "f":"c" }, 'close': { "lb" : "close", "vl": "close", "f":"c" }, 
		  'bookingdetail': { "lb": "booking details", "vl": "booking details", "f":"u" },
		  'personaldetail': { "lb": "personal details", "vl": "personal details", "f":"u" },
		  'slogan': { "lb": "You are not far from your next FUN dining out experience", "vl": "You are not far from your next FUN dining out experience", "f":"n" },
		  'confinfo': {"lb": "Please confirm your information", "vl": "Please confirm your information", "f":"c" } ,
		  'bookingtitlecf': { "lb" : $scope.bookingtitlecf, "vl": $scope.bookingtitlecf, "f":"u" },
		  'facebookfillin': { "lb": "Book with Facebook", "vl": "Book with Facebook", "f":"c" } ,
		  'buttonbook': { "lb" : $scope.buttonLabel, "vl": $scope.buttonLabel, "f":"u" },
		  'buttonconfirm': { "lb" : $scope.buttonConfirm, "vl": $scope.buttonConfirm, "f":"u" },
		  'bookingtitle': { "lb" : $scope.bookingTitle, "vl": $scope.bookingTitle, "f":"u" },
		  'buttonmodify': { "lb" : "modify booking", "vl": "modify booking", "f":"n" },
		  'timeouttag': { "lb" : "time out", "vl": "time out", "f":"u" },
		  'listtag1': { "lb" : "free booking", "vl": "free booking", "f":"c" },
		  'listtag2': { "lb" : $scope.listTags2, "vl": $scope.listTags2, "f":"c" },
		  'listtag3': { "lb" : $scope.listTags3, "vl": $scope.listTags3, "f":"c" },
		  'listtag4': { "lb" : $scope.listTags4, "vl": $scope.listTags4, "f":"c" },
		  'listtag5': { "lb" : $scope.listTags5, "vl": $scope.listTags5, "f":"n" },
		  'confirmsg': { "lb" : $scope.confirmsg, "vl": $scope.confirmsg, "f":"n" },
		  'confirmsg2': { "lb" : $scope.confirmsg2, "vl": $scope.confirmsg2, "f":"n" },
		  'confirmsg3': { "lb" : $scope.confirmsg3, "vl": $scope.confirmsg3, "f":"n" },
		  'confirmsg4': { "lb" : $scope.confirmsg4, "vl": $scope.confirmsg4, "f":"n" },
		  'mr': { "lb" : "Mr.", "vl": "Mr.", "f":"c" },
		  'mrs': { "lb" : "Mrs.", "vl": "Mrs.", "f":"c" },
		  'ms': { "lb" : "Ms.", "vl": "Ms.", "f":"c" },
		  'msg1': { "lb" : $scope.msg1, "vl": $scope.msg1, "f":"n" },
		  'error0': { "lb" : $scope.error0, "vl": $scope.error0, "f":"c" },
		  'error1': { "lb" : $scope.error1, "vl": $scope.error1, "f":"c" },
		  'error2': { "lb" : $scope.error2, "vl": $scope.error2, "f":"c" },
		  'error3': { "lb" : $scope.error3, "vl": $scope.error3, "f":"c" },
		  'error4': { "lb" : $scope.error4, "vl": $scope.error4, "f":"c" },
		  'error5': { "lb" : $scope.error5, "vl": $scope.error5, "f":"c" },
		  'error6': { "lb" : $scope.error6, "vl": $scope.error6, "f":"c" },
		  'error7': { "lb" : $scope.error7, "vl": $scope.error7, "f":"c" },
		  'error8': { "lb" : $scope.error8, "vl": $scope.error8, "f":"c" },
		  'error9': { "lb" : $scope.error9, "vl": $scope.error9, "f":"c" },
		  'error10': { "lb" : $scope.error10, "vl": $scope.error10, "f":"c" },
		  'error11': { "lb" : $scope.error11, "vl": $scope.error11, "f":"c" },
		  'error12': { "lb" : $scope.error12, "vl": $scope.error12, "f":"n" },
		  'error13': { "lb" : $scope.error13, "vl": $scope.error13, "f":"n" },
		  'tmc1': { "lb" : $scope.tmc1, "vl": $scope.tmc1, "f":"c" },
		  'tmc2': { "lb" : $scope.tmc2, "vl": $scope.tmc2, "f":"c" }		  
		  };
		  for(ele in $scope.zang) {
		  	$scope.zang[ele].vl = $scope.zang[ele].lb;
		  	if($scope.zang[ele].f == 'c') $scope.zang[ele].vl = $scope.zang[ele].vl.capitalize();
		  	else if($scope.zang[ele].f == 'u') $scope.zang[ele].vl = $scope.zang[ele].vl.toUpperCase();
		  	}
		  }

	$localStorage.langdata = $scope.zang;
	$scope.langue = langue;
	$scope.pcheckmark = ($scope.langue != 'cn' && $scope.langue != 'th') ? 'pcheckmark': 'pcheckmarkcn';
		  
    $scope.checkmark = [ { "label1": $scope.zang['listtag1'].vl, "label2": " ", "glyph": true }, { "label1": $scope.zang['listtag2'].vl, "label2": $scope.zang['listtag3'].vl, "glyph": true }, { "label1": $scope.zang['listtag4'].vl, "label2": $scope.zang['listtag5'].vl, "glyph": true } ];
	
	$scope.bkcover = (minpers < 2) ? 2 : minpers;
	if(AvailperPax == '1' && startingPage == "1") {
		$scope.bkcover = "";
		$scope.available = 0;
		}

	for (i = 0; i < arglist.length; i += 2) if (arglist[i + 1] != '') {
		if($('#' + arglist[i]).is(':checkbox'))
			$('#' + arglist[i]).click();
		else $scope[arglist[i]] = arglist[i + 1];
		}

	$scope.curday = 0;
	$scope.lunchdata = ""; 
	$scope.dinnerdata = ""; 
	$scope.npers = [];
	for(i = minpers; i <= maxpax; i++) $scope.npers.push(String(i));

	$scope.salutations = [ $scope.zang['mr'].vl, $scope.zang['mrs'].vl, $scope.zang['ms'].vl ];

	$scope.countries = [ { 'a':'Australia', 'b':'au', 'c':'+61' }, { 'a':'China', 'b':'cn', 'c':'+86' }, { 'a':'Hong Kong', 'b':'hk', 'c':'+852' }, { 'a':'India', 'b':'in', 'c':'+91' }, { 'a':'Indonesia', 'b':'id', 'c':'+62' }, { 'a':'Japan', 'b':'jp', 'c':'+81' }, { 'a':'Malaysia', 'b':'my', 'c':'+60' }, { 'a':'Myanmar', 'b':'mm', 'c':'+95' }, { 'a':'New Zealand', 'b':'nz', 'c':'+64' }, { 'a':'Philippines', 'b':'ph', 'c':'+63' }, { 'a':'Singapore', 'b':'sg', 'c':'+65' }, { 'a':'South Korea', 'b':'kr', 'c':'+82' }, { 'a':'Thailand', 'b':'th', 'c':'+66' }, { 'a':'Vietnam', 'b':'vn', 'c':'+84' }, { 'a':'', 'b':'', 'c':'' }, { 'a':'Canada', 'b':'ca', 'c':'+1' }, { 'a':'France', 'b':'fr', 'c':'+33' }, { 'a':'Germany', 'b':'de', 'c':'+49' }, { 'a':'Italy', 'b':'it', 'c':'+39' }, { 'a':'Russia', 'b':'ru', 'c':'+7' }, { 'a':'Spain', 'b':'es', 'c':'+34' }, { 'a':'Sweden', 'b':'se', 'c':'+46' }, { 'a':'Switzerland', 'b':'ch', 'c':'+41' }, { 'a':'UnitedKingdom', 'b':'gb', 'c':'+44' }, { 'a':'UnitedStates', 'b':'us', 'c':'+1' }, { 'a':'', 'b':'', 'c':'' }, { 'a':'Afghanistan', 'b':'af', 'c':'+93' }, { 'a':'Albania', 'b':'al', 'c':'+355' }, { 'a':'Algeria', 'b':'dz', 'c':'+213' }, { 'a':'Andorra', 'b':'ad', 'c':'+376' }, { 'a':'Angola', 'b':'ao', 'c':'+244' }, { 'a':'Antarctica', 'b':'aq', 'c':'+672' }, { 'a':'Argentina', 'b':'ar', 'c':'+54' }, { 'a':'Armenia', 'b':'am', 'c':'+374' }, { 'a':'Aruba', 'b':'aw', 'c':'+297' }, { 'a':'Austria', 'b':'at', 'c':'+43' }, { 'a':'Azerbaijan', 'b':'az', 'c':'+994' }, { 'a':'Bahrain', 'b':'bh', 'c':'+973' }, { 'a':'Bangladesh', 'b':'bd', 'c':'+880' }, { 'a':'Belarus', 'b':'by', 'c':'+375' }, { 'a':'Belgium', 'b':'be', 'c':'+32' }, { 'a':'Belize', 'b':'bz', 'c':'+501' }, { 'a':'Benin', 'b':'bj', 'c':'+229' }, { 'a':'Bhutan', 'b':'bt', 'c':'+975' }, { 'a':'Bolivia', 'b':'bo', 'c':'+591' }, { 'a':'BosniaandHerzegovina', 'b':'ba', 'c':'+387' }, { 'a':'Botswana', 'b':'bw', 'c':'+267' }, { 'a':'Brazil', 'b':'br', 'c':'+55' }, { 'a':'Brunei', 'b':'bn', 'c':'+673' }, { 'a':'Bulgaria', 'b':'bg', 'c':'+359' }, { 'a':'BurkinaFaso', 'b':'bf', 'c':'+226' }, { 'a':'Burundi', 'b':'bi', 'c':'+257' }, { 'a':'Cambodia', 'b':'kh', 'c':'+855' }, { 'a':'Cameroon', 'b':'cm', 'c':'+237' }, { 'a':'CapeVerde', 'b':'cv', 'c':'+238' }, { 'a':'CentralAfricanRepublic', 'b':'cf', 'c':'+236' }, { 'a':'Chad', 'b':'td', 'c':'+235' }, { 'a':'Chile', 'b':'cl', 'c':'+56' }, { 'a':'ChristmasIsland', 'b':'cx', 'c':'+61' }, { 'a':'CocosIslands', 'b':'cc', 'c':'+61' }, { 'a':'Colombia', 'b':'co', 'c':'+57' }, { 'a':'Comoros', 'b':'km', 'c':'+269' }, { 'a':'CookIslands', 'b':'ck', 'c':'+682' }, { 'a':'CostaRica', 'b':'cr', 'c':'+506' }, { 'a':'Croatia', 'b':'hr', 'c':'+385' }, { 'a':'Cuba', 'b':'cu', 'c':'+53' }, { 'a':'Curacao', 'b':'cw', 'c':'+599' }, { 'a':'Cyprus', 'b':'cy', 'c':'+357' }, { 'a':'CzechRepublic', 'b':'cz', 'c':'+420' }, { 'a':'DemocraticRepCongo', 'b':'cd', 'c':'+243' }, { 'a':'Denmark', 'b':'dk', 'c':'+45' }, { 'a':'Djibouti', 'b':'dj', 'c':'+253' }, { 'a':'EastTimor', 'b':'tl', 'c':'+670' }, { 'a':'Ecuador', 'b':'ec', 'c':'+593' }, { 'a':'Egypt', 'b':'eg', 'c':'+20' }, { 'a':'ElSalvador', 'b':'sv', 'c':'+503' }, { 'a':'EquatorialGuinea', 'b':'gq', 'c':'+240' }, { 'a':'Eritrea', 'b':'er', 'c':'+291' }, { 'a':'Estonia', 'b':'ee', 'c':'+372' }, { 'a':'Ethiopia', 'b':'et', 'c':'+251' }, { 'a':'FalklandIslands', 'b':'fk', 'c':'+500' }, { 'a':'FaroeIslands', 'b':'fo', 'c':'+298' }, { 'a':'Fiji', 'b':'fj', 'c':'+679' }, { 'a':'Finland', 'b':'fi', 'c':'+358' }, { 'a':'FrenchPolynesia', 'b':'pf', 'c':'+689' }, { 'a':'Gabon', 'b':'ga', 'c':'+241' }, { 'a':'Gambia', 'b':'gm', 'c':'+220' }, { 'a':'Georgia', 'b':'ge', 'c':'+995' }, { 'a':'Ghana', 'b':'gh', 'c':'+233' }, { 'a':'Gibraltar', 'b':'gi', 'c':'+350' }, { 'a':'Greece', 'b':'gr', 'c':'+30' }, { 'a':'Greenland', 'b':'gl', 'c':'+299' }, { 'a':'Guatemala', 'b':'gt', 'c':'+502' }, { 'a':'Guernsey', 'b':'gg', 'c':'+44-1481' }, { 'a':'Guinea', 'b':'gn', 'c':'+224' }, { 'a':'Guinea-Bissau', 'b':'gw', 'c':'+245' }, { 'a':'Guyana', 'b':'gy', 'c':'+592' }, { 'a':'Haiti', 'b':'ht', 'c':'+509' }, { 'a':'Honduras', 'b':'hn', 'c':'+504' }, { 'a':'Hungary', 'b':'hu', 'c':'+36' }, { 'a':'Iceland', 'b':'is', 'c':'+354' }, { 'a':'Iran', 'b':'ir', 'c':'+98' }, { 'a':'Iraq', 'b':'iq', 'c':'+964' }, { 'a':'Ireland', 'b':'ie', 'c':'+353' }, { 'a':'IsleofMan', 'b':'im', 'c':'+44-1624' }, { 'a':'Israel', 'b':'il', 'c':'+972' }, { 'a':'IvoryCoast', 'b':'ci', 'c':'+225' }, { 'a':'Jersey', 'b':'je', 'c':'+44-1534' }, { 'a':'Jordan', 'b':'jo', 'c':'+962' }, { 'a':'Kazakhstan', 'b':'kz', 'c':'+7' }, { 'a':'Kenya', 'b':'ke', 'c':'+254' }, { 'a':'Kiribati', 'b':'ki', 'c':'+686' }, { 'a':'Kosovo', 'b':'xk', 'c':'+383' }, { 'a':'Kuwait', 'b':'kw', 'c':'+965' }, { 'a':'Kyrgyzstan', 'b':'kg', 'c':'+996' }, { 'a':'Laos', 'b':'la', 'c':'+856' }, { 'a':'Latvia', 'b':'lv', 'c':'+371' }, { 'a':'Lebanon', 'b':'lb', 'c':'+961' }, { 'a':'Lesotho', 'b':'ls', 'c':'+266' }, { 'a':'Liberia', 'b':'lr', 'c':'+231' }, { 'a':'Libya', 'b':'ly', 'c':'+218' }, { 'a':'Liechtenstein', 'b':'li', 'c':'+423' }, { 'a':'Lithuania', 'b':'lt', 'c':'+370' }, { 'a':'Luxembourg', 'b':'lu', 'c':'+352' }, { 'a':'Macao', 'b':'mo', 'c':'+853' }, { 'a':'Macedonia', 'b':'mk', 'c':'+389' }, { 'a':'Madagascar', 'b':'mg', 'c':'+261' }, { 'a':'Malawi', 'b':'mw', 'c':'+265' }, { 'a':'Maldives', 'b':'mv', 'c':'+960' }, { 'a':'Mali', 'b':'ml', 'c':'+223' }, { 'a':'Malta', 'b':'mt', 'c':'+356' }, { 'a':'MarshallIslands', 'b':'mh', 'c':'+692' }, { 'a':'Mauritania', 'b':'mr', 'c':'+222' }, { 'a':'Mauritius', 'b':'mu', 'c':'+230' }, { 'a':'Mayotte', 'b':'yt', 'c':'+262' }, { 'a':'Mexico', 'b':'mx', 'c':'+52' }, { 'a':'Micronesia', 'b':'fm', 'c':'+691' }, { 'a':'Moldova', 'b':'md', 'c':'+373' }, { 'a':'Monaco', 'b':'mc', 'c':'+377' }, { 'a':'Mongolia', 'b':'mn', 'c':'+976' }, { 'a':'Montenegro', 'b':'me', 'c':'+382' }, { 'a':'Morocco', 'b':'ma', 'c':'+212' }, { 'a':'Mozambique', 'b':'mz', 'c':'+258' }, { 'a':'Namibia', 'b':'na', 'c':'+264' }, { 'a':'Nauru', 'b':'nr', 'c':'+674' }, { 'a':'Nepal', 'b':'np', 'c':'+977' }, { 'a':'Netherlands', 'b':'nl', 'c':'+31' }, { 'a':'NetherlandsAntilles', 'b':'an', 'c':'+599' }, { 'a':'NewCaledonia', 'b':'nc', 'c':'+687' }, { 'a':'Nicaragua', 'b':'ni', 'c':'+505' }, { 'a':'Niger', 'b':'ne', 'c':'+227' }, { 'a':'Nigeria', 'b':'ng', 'c':'+234' }, { 'a':'Niue', 'b':'nu', 'c':'+683' }, { 'a':'NorthKorea', 'b':'kp', 'c':'+850' }, { 'a':'Norway', 'b':'no', 'c':'+47' }, { 'a':'Oman', 'b':'om', 'c':'+968' }, { 'a':'Pakistan', 'b':'pk', 'c':'+92' }, { 'a':'Palau', 'b':'pw', 'c':'+680' }, { 'a':'Palestine', 'b':'ps', 'c':'+970' }, { 'a':'Panama', 'b':'pa', 'c':'+507' }, { 'a':'PapuaNewGuinea', 'b':'pg', 'c':'+675' }, { 'a':'Paraguay', 'b':'py', 'c':'+595' }, { 'a':'Peru', 'b':'pe', 'c':'+51' }, { 'a':'Pitcairn', 'b':'pn', 'c':'+64' }, { 'a':'Poland', 'b':'pl', 'c':'+48' }, { 'a':'Portugal', 'b':'pt', 'c':'+351' }, { 'a':'Qatar', 'b':'qa', 'c':'+974' }, { 'a':'RepublicCongo', 'b':'cg', 'c':'+242' }, { 'a':'Reunion', 'b':'re', 'c':'+262' }, { 'a':'Romania', 'b':'ro', 'c':'+40' }, { 'a':'Rwanda', 'b':'rw', 'c':'+250' }, { 'a':'SaintBarthelemy', 'b':'bl', 'c':'+590' }, { 'a':'SaintHelena', 'b':'sh', 'c':'+290' }, { 'a':'SaintMartin', 'b':'mf', 'c':'+590' }, { 'a':'Samoa', 'b':'ws', 'c':'+685' }, { 'a':'SanMarino', 'b':'sm', 'c':'+378' }, { 'a':'SaudiArabia', 'b':'sa', 'c':'+966' }, { 'a':'Senegal', 'b':'sn', 'c':'+221' }, { 'a':'Serbia', 'b':'rs', 'c':'+381' }, { 'a':'Seychelles', 'b':'sc', 'c':'+248' }, { 'a':'SierraLeone', 'b':'sl', 'c':'+232' }, { 'a':'Slovakia', 'b':'sk', 'c':'+421' }, { 'a':'Slovenia', 'b':'si', 'c':'+386' }, { 'a':'SolomonIslands', 'b':'sb', 'c':'+677' }, { 'a':'Somalia', 'b':'so', 'c':'+252' }, { 'a':'SouthAfrica', 'b':'za', 'c':'+27' }, { 'a':'SouthSudan', 'b':'ss', 'c':'+211' }, { 'a':'SriLanka', 'b':'lk', 'c':'+94' }, { 'a':'Sudan', 'b':'sd', 'c':'+249' }, { 'a':'Suriname', 'b':'sr', 'c':'+597' }, { 'a':'Swaziland', 'b':'sz', 'c':'+268' }, { 'a':'Syria', 'b':'sy', 'c':'+963' }, { 'a':'Taiwan', 'b':'tw', 'c':'+886' }, { 'a':'Tajikistan', 'b':'tj', 'c':'+992' }, { 'a':'Tanzania', 'b':'tz', 'c':'+255' }, { 'a':'Togo', 'b':'tg', 'c':'+228' }, { 'a':'Tokelau', 'b':'tk', 'c':'+690' }, { 'a':'Tonga', 'b':'to', 'c':'+676' }, { 'a':'Tunisia', 'b':'tn', 'c':'+216' }, { 'a':'Turkey', 'b':'tr', 'c':'+90' }, { 'a':'Turkmenistan', 'b':'tm', 'c':'+993' }, { 'a':'Tuvalu', 'b':'tv', 'c':'+688' }, { 'a':'Uganda', 'b':'ug', 'c':'+256' }, { 'a':'Ukraine', 'b':'ua', 'c':'+380' }, { 'a':'UnitedArabEmirates', 'b':'ae', 'c':'+971' }, { 'a':'Uruguay', 'b':'uy', 'c':'+598' }, { 'a':'Uzbekistan', 'b':'uz', 'c':'+998' }, { 'a':'Vanuatu', 'b':'vu', 'c':'+678' }, { 'a':'Vatican', 'b':'va', 'c':'+379' }, { 'a':'Venezuela', 'b':'ve', 'c':'+58' }, { 'a':'WallisandFutuna', 'b':'wf', 'c':'+681' }, { 'a':'WesternSahara', 'b':'eh', 'c':'+212' }, { 'a':'Yemen', 'b':'ye', 'c':'+967' }, { 'a':'Zambia', 'b':'zm', 'c':'+260' }, { 'a':'Zimbabwe', 'b':'zw', 'c':'+263' } ];	
	$scope.currentcc = 10;

	/* if($location.host() != "www.weeloy.com") */
	$scope.allowlang = [ 'en', 'fr', 'cn', 'th', 'my', 'es', 'it', 'kr', 'jp', 'hk', 'de', 'pt', 'vi', 'ru'];
	$scope.languages = [ { 'a':'English', 'b':'us', 'c':'en' }, { 'a':'華文', 'b':'cn', 'c':'cn' }, { 'a':'文言', 'b':'hk', 'c':'hk' }, { 'a':'ภาษาไทย', 'b':'th', 'c':'th' }, { 'a':'Bahasa Melayu', 'b':'my', 'c':'my' }, { 'a':'日本語', 'b':'jp', 'c':'jp' }, { 'a':'한국어', 'b':'kr', 'c':'kr' }, { 'a':'Tiếng Việt', 'b':'vn', 'c':'vi' }, { 'a':'Русский', 'b':'ru', 'c':'ru' }, { 'a':'Français', 'b':'fr', 'c':'fr' }, { 'a':'Español', 'b':'es', 'c':'es' }, { 'a':'Italiano', 'b':'it', 'c':'it' }, { 'a':'Deutsch', 'b':'de', 'c':'de' }, { 'a':'Portuguese', 'b':'pt', 'c':'pt' } ];
	if($location.host() != "www.weeloy.com" || restaurant == 'SG_SG_R_TheFunKitchen') { 
		// $scope.languages.push({ 'a':'Deutsch', 'b':'de', 'c':'de' });
		}
		
	$scope.curlangindex = 0;
	$scope.curlangicon = "famfamfam-flag-" + $scope.languages[$scope.curlangindex].b;
	$scope.curlang = $scope.languages[$scope.curlangindex].a;
					
	$http.get("../../api/restaurant/allote/" + restaurant).success(function(response) { 
		$scope.lunchdata = response.data.lunchdata;
		$scope.dinnerdata = response.data.dinnerdata;
		$scope.updateTimerScope(curndays);
		});
	
  	$scope.phoneindex = function(code) {
		val = $scope.bkmobile;
		val = val.trim();
		val = val.replace(/[^0-9 \+]/g, "");
		val = val.replace(/[ ]+/g, " ");

		if(val.indexOf(code + ' ') == 0) {			
			val = val.replace(/^\+(\d{2,3})[ ]*\1[ ]*/, '+$1 '); // replace repetition of contry code
			return $scope.bkmobile = val.trim(); 
			}
					
		if(val.indexOf(code) == 0) {
			val = code + ' ' + val.substring(code.length);			
			val = val.replace(/^\+(\d{2,3})[ ]*\1[ ]*/, '+$1 '); // replace repetition of contry code
			return $scope.bkmobile = val.trim(); 
			}
			
		if((res = val.match(/^\+\d{2,3} /)) != null) {
			val = val.replace(/^\+\d{2,3} /, "");
			val = code + ' ' + val;
			val = val.replace(/^\+(\d{2,3})[ ]*\1[ ]*/, '+$1 '); // replace repetition of contry code
			return $scope.bkmobile = val.trim(); 
			}
			
		if((res = val.match(/^\+\d{2,3}$/)) != null) {
			val = val.replace(/^\+\d{2,3}/, "");
			val = code + ' ' + val;
			val = val.replace(/^\+(\d{2,3})[ ]*\1[ ]*/, '+$1 '); // replace repetition of contry code
			return $scope.bkmobile = val.trim(); 
			}
			
		if(val.match(/^\+/) != null) {
			val = code + ' ' + val.substring(1);
			val = val.replace(/^\+(\d{2,3})[ ]*\1[ ]*/, '+$1 '); // replace repetition of contry code
			return $scope.bkmobile = val.trim(); 
			}

		val = val.replace(/\+ /, "");
		val = code + ' ' + val;
		val = val.replace(/^\+(\d{2,3})[ ]*\1[ ]*/, '+$1 '); // replace repetition of contry code
		return $scope.bkmobile = val.trim(); 
		};
		
	if(curcountry == '')
		curcountry = 'Singapore';
		
	for(i = 0; i < $scope.countries.length; i++) {
		if($scope.countries[i].a == curcountry) {
			$scope.currentcc = i;
			$scope.curflgicon = "famfamfam-flag-" + $scope.countries[$scope.currentcc].b;
			$scope.phoneindex($scope.countries[$scope.currentcc].c);
			break;	
			}
		}
								
	$scope.updatendays = function() {

		if(typeof $scope.bkdate !== "string")
			$scope.bkdate = HttpServiceAvail.getStringDate($scope.bkdate);
		n = aday.numberDay($scope.bkdate);
		$scope.updateTimerScope(n);
		
		if(AvailperPax == '1') 
			$scope.checkPerPaxData();
	};

	$scope.updateTimerScope = function(n) {
	
		$scope.curday = n;
		$scope.lunch = [];
		$scope.dinner = [];
		nchar_lunch = 4;
		nchar_dinner = 4;
		index = n * nchar_lunch;
		patlunch = parseInt('0x' + $scope.lunchdata.substring(index, index + nchar_lunch));
		index = n * nchar_dinner;
		patdinner = parseInt('0x' + $scope.dinnerdata.substring(index, index + nchar_dinner));

		curval = $scope.bktime;
		found = 0;
		localtime = "";
		limit = nchar_lunch * 4;
		for (log = i = 0, k = 1; i < limit; i++, k *= 2) 
			if ((patlunch & k) == k) {
				log++;
				ht = (9 + Math.floor(i / 2)) + ':' + ((i % 2) * 3) + '0';
				if(ht == curval) found = 1;
				$scope.lunch.push(ht);
				}
		if(log == 0) {
			$scope.lunch.push($scope.zang['error12'].vl);		// 'not available for lunch'
			localtime = $scope.zang['error12'].vl;
			}
		
		limit = nchar_dinner * 4;
		//if(limit > 17) limit = 16;	// minight
		for (log = i = 0, k = 1; i < limit; i++, k *= 2) 
			if ((patdinner & k) == k) {
				log++;
				ht = (16 + Math.floor(i / 2)) + ':' + ((i % 2) * 3) + '0';
				if(ht == curval) found = 1;
				$scope.dinner.push(ht);
				}
		if(log == 0) {
			$scope.dinner.push($scope.zang['error13'].vl);		//'not available for dinner'
			localtime = $scope.zang['error12'].vl;
			}

		curval = $('#bktime').val();
		if(localtime.substr(0,5) == 'close') {
			curtime = localtime;
			$scope.bktime = curtime;
			}
		else if(curval.substr(0,5) == 'close' || found == 0) {
			$scope.bktime = '';
			}
		};	
		
	$scope.setmealtime = function(tt, section) {
		switch(section) {
			case 1: 	// time
				$scope.bktime = tt;
				if(AvailperPax == '1') 
					$scope.checkPerPaxData();					
				break;
				
			case 2: 	// number of pers
				if(tt.substring(0, 4) == "more") {
					if((pers = prompt("How many persons ?", 10)) != null) {
						pers = parseInt(pers);
						if(pers > 9 && pers < maxpaxcc) tt = pers;
						else tt = $scope.bkcover;
						}
					}
				$scope.bkcover = String(tt);
				if(AvailperPax == '1') 
					$scope.checkPerPaxData();					
				break;
				
			case 3: 	// salutation
				$scope.bksalutation = tt;
				 break;
				 
			case 4: 	// phone
				for(i = 0; i < $scope.countries.length; i++) 
					if($scope.countries[i].a == tt) {
						$scope.currentcc = i;
						break;
						}
						
				if(i >= $scope.countries.length) {
					alert('Invalid Country Code');
					$scope.currentcc = 10; // Singapore
					}
				$scope.curflgicon = "famfamfam-flag-" + $scope.countries[$scope.currentcc].b;
				$('#bkcountry').val($scope.countries[$scope.currentcc].a);
				$scope.phoneindex($scope.countries[$scope.currentcc].c);
				break;
			
			case 5:		// langue
				if($scope.setFlg($scope.languages[tt].c))
					$scope.setLang($scope.languages[$scope.curlangindex].c);
				break;

			default:
			}
		};

	$scope.setFlg = function(lg) {	
		for(i = 0; i < $scope.languages.length; i++)
			if($scope.languages[i].c == lg)
				break;
		if(i >= $scope.languages.length)
			return false;
				
		$scope.curlangindex = i;
		$scope.curlangicon = "famfamfam-flag-" + $scope.languages[$scope.curlangindex].b;
		$scope.curlang = $scope.languages[$scope.curlangindex].a;
		$('#bklangue').val(lg);
		$scope.langue = lg;
		$scope.pcheckmark = ($scope.langue != 'cn') ? 'pcheckmark': 'pcheckmarkcn';

		return true;
		}
		
	$scope.checkvalidtel = function() {
		if($scope.bkmobile == "") {
			$scope.bkmobile == $scope.countries[$scope.currentcc].c.trim() + ' ';
			return;
			}
		
		$scope.curflgicon = "famfamfam-flag-" + $scope.countries[$scope.currentcc].b;
		$('#bkcountry').val($scope.countries[$scope.currentcc].a);
		$scope.phoneindex($scope.countries[$scope.currentcc].c);
		};
			
	$scope.opendropdown = function($event, selector) {
		$scope[selector + '_opened'] = true;
		$event.preventDefault();
		$event.stopPropagation();
		};
		
	$scope.setLang = function (lang) {
		if($scope.allowlang.indexOf(lang) < 0) {
			alert('language not supported: '+ lang);
			return;
			}
		// droddown closed
		$scope.date_opened = false;
		// changes $locale
		angular.copy(locales[lang], $locale);
		// changes dt to apply the $locale changes
		tt = new Date();
		$scope.bkdate= new Date(tt.getTime() + ($scope.curday * 24 * 3600 * 1000));	// $scope.bkdate.getTime()
		$scope.langue = lang;
		$scope.pcheckmark = ($scope.langue != 'cn') ? 'pcheckmark': 'pcheckmarkcn';
		
		$http.get("../../api/services.php/translation/gettrans/" + lang + "/" + topic)
    		.then(function(response) {
    			trans = response.data.data.row;
    			for(ele in $scope.zang) {
					if(typeof  $scope.zang[ele].lb =='undefined' ||  $scope.zang[ele].lb == "")
						continue;
    				str = $scope.zang[ele].lb.toLowerCase();
    				for(i = 0; i < trans.length; i++) {
    					if(typeof trans[i].content =='undefined' || trans[i].content == "")
    						continue;
    					if(trans[i].content.toLowerCase() == str) {
   							cc = trans[i].translated;
    						if($scope.zang[ele].f == 'c')
    							$scope.zang[ele].vl = cc.capitalize();
    						else if($scope.zang[ele].f == 'u') 
    							$scope.zang[ele].vl = cc.toUpperCase();
    						else $scope.zang[ele].vl = cc;
    						//console.log(cc + ' ' + ele);
    						break
    						}
    					}
    				} 
    			$scope.checkmark = [ { "label1": $scope.zang['listtag1'].vl, "label2": " ", "glyph": true }, { "label1": $scope.zang['listtag2'].vl, "label2": $scope.zang['listtag3'].vl, "glyph": true }, { "label1": $scope.zang['listtag4'].vl, "label2": $scope.zang['listtag5'].vl, "glyph": true } ];
				if($scope.langue != 'de')
					$scope.salutations = [ $scope.zang['mr'].vl, $scope.zang['mrs'].vl, $scope.zang['ms'].vl ];
				else $scope.salutations = [ $scope.zang['mr'].vl, $scope.zang['mrs'].vl ];
				$scope.bksalutation = $scope.zang['mr'].vl;
				$localStorage.langdata = $scope.zang;
				if($scope.lunch[0] == $scope.zang['error12'].lb) $scope.lunch[0] = $scope.zang['error12'].vl;
				if($scope.dinner[0] == $scope.zang['error13'].lb) $scope.dinner[0] = $scope.zang['error13'].vl;
		  		//console.log($scope.zang);
    			});

	  };
	  
	$scope.formats = ['dd-MM-yyyy', 'dd/MMMM/yyyy', 'yyyy/MM/dd', 'dd/MM/yyyy', 'dd.MM.yyyy', 'shortDate', 'fullDate'];
	$scope.format = $scope.formats[3];
	$scope.minDate = new Date();
	$scope.maxDate = new Date();
	$scope.maxDate.setTime($scope.maxDate.getTime() + (60 * 24 * 3600 * 1000));	// remainingday  120 days

	$scope.disabled = function(date, mode) {
		return false;
	//return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
	};

	$scope.dateOptions = {
		formatYear: 'yy',
		startingDay: 1
	};

	if($scope.langue != "en")
		$scope.setFlg($scope.langue);
		
	$scope.checksubmit = function() {

		$scope.phoneindex($scope.countries[$scope.currentcc].c);

		$('#book_form input[type=text], input[type=password]').each(function() {
			this.value = this.value.trim();
			});

		mandatoryAr = [ 'bkdate', 'bktime', 'bkcover', 'bkemail', 'bklast', 'bkfirst', 'bkmobile' ];

		for(i = 0; i < mandatoryAr.length; i++) {	// manage the autocomplete !!! 
				$scope[mandatoryAr[i]] = $('#'+mandatoryAr[i]).val();
				}
			
		for(i = 0; i < mandatoryAr.length; i++) {
			if(typeof $scope[mandatoryAr[i]] === 'undefined' || $scope[mandatoryAr[i]] == "")
				return $scope.invalid($scope[mandatoryAr[i]], $scope.zang['error'+i].vl);
			}	

		if(typeof $scope.bkdate !== "string")
			$scope.bkdate = HttpServiceAvail.getStringDate($scope.bkdate);
		nday = aday.numberDay($scope.bkdate);
		if(isNaN(nday) || nday > 60 || nday < 0) {
			return $scope.invalid('bkdate', $scope.zang['error0'].vl);
			}

		if($scope.bktime.match(/^\d{2}\:\d{2}$/) == null) return $scope.invalid('bktime', $scope.zang['error1'].vl);        
		if(!$scope.IsEmail($scope.bkemail)) return $scope.invalid('bkemail', $scope.zang['error3'].vl + ': ' + $scope.bkemail);
		if($scope.bkmobile.length < 5) return $scope.invalid('bkmobile', $scope.zang['error6'].vl);

		if($scope.available == 0)
			return $scope.invalid("", $scope.zang['error7'].vl);			
	
		if(tracking != '') {
			removelgcookie("weeloy_be_tracking");
			setlgcookie("weeloy_be_tracking", tracking, 1);
			}
		
		$('#book_form').attr('action', '../booking/prebook.php'); 
		book_form.submit();
		return false;	
		};

	$scope.IsEmail = function(email) { var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/; return regex.test(email); }
	$scope.cleanemail = function(obj) { obj = obj.replace(/[!#$%^&*()=}{\]\[\"\':;><\?/|\\]/g, '');  return obj; }
	$scope.cleantel = function(obj) { obj = obj.replace(/[^0-9 \+]/g, '');  return obj; }
	$scope.cleanpass = function(obj) { obj = obj.replace(/[\'\"]/g, '');  return obj; }
	$scope.cleantext = function(obj) { obj = obj.replace(/[!@#$%^&*()=}{\]\[\"\':;><\?/|\\]/g, ''); return obj; }	
	$scope.invalid = function(id, str) { alert(str); if(id != "") $('#' + id).focus(); }	

	$scope.setVariable = function(varname, varval) {
		for(i = 0; i < varname.length; i++)
			if(typeof $scope[varname[i]] !== "undefined" && typeof varval[i] !== "undefined") {
				$scope[varname[i]] = varval[i];
				}
		$scope.$apply();
		};	

	$scope.checkPerPaxData = function() {

		$scope.available = 0;			
		dateflg = (typeof $scope.bkdate !== "undefined" && (($scope.bkdate instanceof Date) || (typeof $scope.bkdate === "string" && $scope.bkdate.length > 7)));
		timeflg = (typeof $scope.bktime === "string" && $scope.bktime.length > 3);
		persflg = (typeof $scope.bkcover === "string" && parseInt($scope.bkcover) > 1);
		if((dateflg && timeflg && persflg) == false)
			return;
			
		HttpServiceAvail.checkAvail($scope.bkdate, $scope.bktime, $scope.bkcover).then(function(response) {  
			if(response.data == "1") $scope.available = 1; 
			else if(response.count > 0) { alert($scope.zang['error10'].vl + ". " + response.count + " " + $scope.zang['error11'].vl); }
			else { alert($scope.zang['error9'].vl); }
			console.log(response); 
			});
		}
		
	$scope.facebkclik = function() {
		FB.login(function(response) {
		console.log('in login');
		console.log(response);
		if (response.authResponse) {
			//user just authorized your app
			//document.getElementById('loginBtn').style.display = 'none';
			if(typeof response.authResponse !== 'undefined')
				$('#fbtoken').val(response.authResponse.accessToken);

			if($('#fbtoken').val().length > 10)
				submitflg = true;
			getUserData();
			}
		}, {scope: 'email,public_profile', return_scopes: true});
	  }
})

// this is the important bit:
.directive('datepickerPopup', function (){
  return {
    restrict: 'EAC',
    require: 'ngModel',
    link: function(scope, element, attr, controller) {
      //remove the default formatter from the input directive to prevent conflict
      controller.$formatters.shift();
    }
  }
});
</script>


<script>

<?php
echo "var apidfb = '" . $facebookappid . "';";
?>


submitflg = false;
function getUserData() {
	FB.api('/me', function(response) {
			console.log(response);
		if(response.name != "") {
			console.log(response);
			$('#fbuserid').val(response.id);
			$('#fbemail').val(response.email);
			$('#fbname').val(response.name);
			$('#fbfirstname').val(response.first_name);
			$('#fblastname').val(response.last_name);
			$('#fbtimezone').val(response.timezone);
			}
		if($('#fbuserid').val() != '' && submitflg) {
			$('#application').val('facebook');
			updateinput();
			getWeeloyUser(); 
			}
	});
}
 
window.fbAsyncInit = function() {
	//SDK loaded, initialize it
	FB.init({
		appId      : apidfb,
		xfbml      : true,
		version    : 'v2.2'
	});
 
	//check user session and refresh it
	FB.getLoginStatus(function(response) {
		if (response.status === 'connected') {
			//user is authorized
			//document.getElementById('loginBtn').style.display = 'none';
			if(typeof response.authResponse !== 'undefined')
				$('#fbtoken').val(response.authResponse.accessToken);

			submitflg = false;
			getUserData();
		} else {
			//user is not authorized
		}
	});
};
 
//load the JavaScript SDK
(function(d, s, id){
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) {return;}
	js = d.createElement(s); js.id = id;
	js.src = "//connect.facebook.net/en_US/sdk.js";
	fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
 

/*
document.getElementById('loginBtndd').addEventListener('click', function() { 	FB.login(function(response) { 		if (response.authResponse) { 			if(typeof response.authResponse !== 'undefined') 				$('#fbtoken').val(response.authResponse.accessToken);  			if($('#fbtoken').val().length > 10) 				submitflg = true; 			getUserData(); 		} 	}, {scope: 'email,public_profile', return_scopes: true}); }, false); 
*/

</script>

<script>

function updateinput() { 
	var email = $('#fbemail').val();
	var first = $('#fbfirstname').val();
	var last = $('#fblastname').val();
	var user = $('#fbuserid').val();
	var token = $('#fbtoken').val();
	angular.element($('#bookingCntler')).scope().setVariable(['bkemail', 'bkfirst', 'bklast', 'userid', 'token'], [email, first, last, user, token]);
}

function getWeeloyUser() {

	var userid, email, token;
	
	email = $('#fbemail').val();
	userid = $('#fbuserid').val();
	token = $('#fbtoken').val();
	
	if(tracking == "facebook" )
		userid = userid + "_facebook_weeloy";
	apiurl = "../../api/facebookuser/" + email + "/" + userid + "/" + token;
	console.log(apiurl);
	$.ajax({
		url : apiurl,
		type: "GET",
		success: function(response, textStatus, jqXHR) { 
				if(response.status == 0) {
					alert(response.errors);
					return;
					}
				if(typeof response.data.mobile !== "undefined" && response.data.mobile !== "" && response.data.mobile.length > 7) {
					mobile = response.data.mobile.substring(0, 3) + ' ' + response.data.mobile.substring(3);
	    			angular.element($('#bookingCntler')).scope().setVariable(['bkmobile'], [mobile]);
		    		}
				if(typeof response.data.country !== "undefined" && response.data.country !== "" && response.data.country.length > 5) {
		    		country = response.data.country;
					if(country == 'Singapore' || country == 'Thailand' || country == 'Malaysia' || country == 'Hong Kong')
						angular.element($('#bookingCntler')).scope().setVariable(['bkcountry'], [country]);
					}
		 		},
			error: function (jqXHR, textStatus, errorThrown) { /* alert("Unknown Error. " + textStatus); */ }
		});		
}

function termsconditions() {  $('#remember').toggleClass('show'); }

String.prototype.capitalize = function () {
  return this.replace(/^./, function (match) {
    return match.toUpperCase();
  });
};
        
function setlgcookie(name, value, duration){
	value = escape(value);
	if(duration){
		var date = new Date();
		date.setTime(date.getTime() + (duration * 1 * 3600*1000));
		value += "; expires=" + date.toGMTString();
	}
	document.cookie = name + "=" + value;
}

function getlgcookie(name){
	value = document.cookie.match('(?:^|;)\\s*'+name+'=([^;]*)');
	return value ? unescape(value[1]) : "";
}

//Removes the cookies
function removelgcookie(name) { setlgcookie(name, '', -1); }

</script>
</body>
</html>