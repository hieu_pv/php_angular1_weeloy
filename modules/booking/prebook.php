<?php
require_once("conf/conf.init.inc.php");
require_once("lib/wpdo.inc.php");
require_once("lib/wglobals.inc.php");
require_once('conf/conf.session.inc.php');
require_once("lib/class.media.inc.php");
require_once("lib/class.restaurant.inc.php");
require_once("lib/class.promotion.inc.php");
require_once("lib/class.analytics.inc.php");
require_once("lib/class.coding.inc.php");
require_once("lib/class.member.inc.php");


$IPaddr = ip2long($_SERVER['REMOTE_ADDR']);

$arglist = array('bklangue', 'bksalutation', 'bklast', 'bkfirst', 'bkemail', 'bkcover', 'bkdate', 'bktime', 'bkmobile', 'bkcountry', 'bkrestaurant', 'bktitle', 'bkparam', 'bkspecialrequest', 'brwsr_type', 'bkpage', 'bktracking', 'data', 'booker', 'company', 'hotelguest', 'babychair');
foreach ($arglist as $label) {
	$$label = $_REQUEST[$label];
    $$label = preg_replace("/\'\"/", "’", $$label);
    $$label = preg_replace("/\s/", " ", $$label);
    }
    
$bkpage = intval($bkpage) + 1;

$fullfeature = preg_match("/TheFunKitchen/", $bkrestaurant) || preg_match("/TheOneKitchen/", $bkrestaurant) || in_array($_SESSION['user']['member_type'], $fullfeature_member_type_allowed) || ($_SESSION['user']['member_type'] == 'weeloy_sales' && $res->status == 'demo_reference' || $res->status == 'active');

if(empty($bkrestaurant)) {
	header("location: https://www.weeloy.com");
	exit;
	}
	
$res = new WY_restaurant;
$res->getRestaurant($bkrestaurant);

if(empty($res->restaurant)) {
	header("location: https://www.weeloy.com");
	exit;
	}

// if the booking session is valid and not over 600s

$action = "book_form.php"; 

$bkparamdecoded = detokenize($bkparam);
if(substr($bkparamdecoded, 0, 3) != date('M') || microtime(true) - floatval(substr($bkparamdecoded, 3)) > 600)	
	header("Location: " . $action . "?bkrestaurant=" . $bkrestaurant . "&bktitle=" . $bktitle . "&bktracking=" . $bktracking . "&data=" . $data . "&timeout=y");

$mediadata = new WY_Media();
$logo = $mediadata->getLogo($bkrestaurant);

$restaurant_restaurant_tnc = $res->restaurant_tnc;
$typeBooking = ($res->is_bookable && ($res->status == 'active' || $res->status == 'demo_reference')) ? "true " : "false ";
$is_listing = false;
if(empty($res->is_wheelable) || !$res->is_wheelable){
    $is_listing = true;
}

$logger = new WY_log("website");
$logger->LogEvent($_SESSION['user']['id'], 802, '', $res->restaurant, '', date("Y-m-d H:i:s"));

?>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv='expires' content='mon, 1 jan 1998 05:00:00 gmt'>
<meta http-equiv='pragma' content='no-cache'>
<meta http-equiv='pragma' content='cache-control: max-age=0'>
<meta http-equiv='last-modified' content='mon, 1 jan 1998 05:00:00 gmt'>
<meta http-equiv='cache-control' content='no-cache, must-revalidate'>
<meta name='robots' content='noindex, nofollow'>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
<meta name="HandheldFriendly" content="true" />
<title>Weeloy Code</title>
<link href="../../css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="../../css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
<link href="../../css/bootstrap-formhelpers.min.css" rel="stylesheet" media="screen">
<link href="../../css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="../../css/style.css" rel="stylesheet" type="text/css" />
<link href="bookstyle.css" rel="stylesheet" >
<link href='https://fonts.googleapis.com/css?family=Montserrat|Unkempt|Roboto:400,900,700italic,700,500italic,500,400italic' rel='stylesheet' type='text/css'>
<script type="text/javascript" src="../../js/jquery.min.js"></script>
<script type="text/javascript" src="../../js/bootstrap.min.js"></script>
<script type='text/javascript' src="../../js/angular.min.js"></script>
<script type="text/javascript" src="../../js/ngStorage.min.js"></script>
<style>

<?php if ($brwsr_type != "mobile") echo ".mainbox { margin: 10px 30px 20px 55px; width:550px; } "; ?>

</style>

<?php include_once("ressources/analyticstracking.php") ?>
</head>
    <body ng-app="myApp">
        <form class='form-horizontal' action='confirmation.php' role='form' id='book_form' name='book_form' method='POST' enctype='multipart/form-data'> 
            <?
            reset($arglist);
            while(list($index, $label) = each($arglist))
            	printf("<input type='hidden' id='%s' name='%s' value='%s'>", $label, $label, $$label);
            ?>

				<div class="modal fade" id="remoteModal" tabindex="-1" role="dialog" aria-labelledby="remoteModalLabel" aria-hidden="true" >  
					<div class="modal-dialog" style="background-color: #fff">  
						<div class="modal-content" style='font-size:11px;'></div>  
					</div>  
				</div>  

            <div id='booking' ng-controller='MainController' ng-init=" ;" >

                <div class="container mainbox">    
                    <div class="panel panel-info" >
                        <div class="panel-heading">
                            <div class="panel-title" style='width:300px;'>{{ zang['bookingtitle'].vl | uppercase }} </div>
                        </div>     

                        <p class="slogan"><b>{{zang['confinfo'].vl}}</b><br /></p>
                        <div class="panel-body" >

<?php if ($brwsr_type == "mobile") : ?>	
					<div>			
					<center><img ng-src='{{imglogo}}' max-width='120' max-height='120' id='theLogo' name='theLogo'></center><br>
<?php else : ?>
								
						<div class='col-xs-5 separation'>
						<center><img ng-src='{{imglogo}}' max-width='120' max-height='120' id='theLogo' name='theLogo'></center><br>
						<table width='90%'>
						<tr ng-repeat="x in checkmark"><td><span class='glyphicon glyphicon-ok checkmark'></span> <br />&nbsp;</td><td ng-class='pcheckmark'><span >{{x.label1}} {{ x.label2}} <br />&nbsp;</td>
						</table><br/>
						</div>
						<div class='col-xs-7'  style="font-size:12px;">
<?php endif; ?>
						<div ng-repeat="x in bookinginfo" class='row'><span ng-if="x.value !='' && x.label !='divider'"> {{x.label}} : {{x.value}} </span><div ng-if="x.label=='divider'" class="alert alert-info alert-label">{{x.value | uppercase}}</div> </div>


						<table width='100%'><tr><td><a style='padding:5px' href="javascript:book_form.submit();" class="btn {{buttonClass}}"> {{zang['buttonconfirm'].vl}} </a></td><td align='right'><a href="javascript:modify();" >{{zang['buttonmodify'].vl}}</a></td></tr></table><br />
						<span style='font-familly:helvetica;font-size:10px;margin:0'>{{zang['tmc1'].vl}} <a href="../../templates/tnc/fullterms.php?1" id='buttonterms' data-toggle="modal" data-target="#remoteModal" style='color:#5285a0;margin:0'>{{zang['tmc2'].vl}}</a></span>

                     </div> 
				</div>                     
			</div>  
		</div>
	</div>
</form>
<script>

<?php

echo "var timeout='$timeout';";
echo "var brwsr_type = '$brwsr_type';";
echo "var imglogo = '$logo';";
echo "var typeBooking = $typeBooking;";
echo "var restaurant = '$bkrestaurant';";
echo "var action = '$action';";
echo "var pdate = '$bkdate';";
echo "var ptime = '$bktime';";
echo "var pcover = '$bkcover';";
echo "var psalutation = '$bksalutation';";
echo "var pfirst = '$bkfirst';";
echo "var plast = '$bklast';";
echo "var pemail = '$bkemail';";
echo "var pmobile = '$bkmobile';";
echo "var langue = '$bklangue';";
echo "var topic = 'BOOKING';";
echo "var prequest = '$bkspecialrequest';";

printf("var is_listing = %s;", $is_listing ? 'true' : 'false');

?>

var app = angular.module("myApp", ['ngStorage']);

app.controller('MainController', function ($scope, $http, $locale, $timeout, $localStorage) {

	$scope.buttonClass = (typeBooking) ? 'btn-danger' : 'btn-green';
	$scope.imglogo = imglogo;
	$scope.brwsr_type = brwsr_type;
	$scope.langue = langue;
	$scope.pcheckmark = ($scope.langue != 'cn') ? 'pcheckmark': 'pcheckmarkcn';

	$scope.pdate = pdate;
	$scope.ptime = ptime;	
	$scope.pcover = pcover;	
	$scope.psalutation = psalutation;	
	$scope.pfirst = pfirst;	
	$scope.plast = plast;	
	$scope.pname = psalutation + ' ' + pfirst + ' ' + plast;	
	$scope.pemail = pemail;	
	$scope.pmobile = pmobile;	
	$scope.prequest = prequest;

	$scope.buttonLabel = (typeBooking) ? 'book now' : 'request your weeloy code';
	$scope.bookingTitle = (typeBooking) ? "book your table" : "get your weeloy code";
	$scope.buttonConfirm = 'Confirm';
	$scope.timeouttag = "time out";
	$scope.listTags0 = "Free Booking";
	$scope.listTags1 = "";
	$scope.listTags2 = (typeBooking) ? "Instant Reservation" : "No reservartion";
	$scope.listTags3 = (typeBooking) ? "" : "First come, first serve";
	$scope.listTags4 = (!is_listing) ? "Win for every booking" : "Enjoy incredible promotion";

	$scope.tmc1 = "By confirming, I agree to";
	$scope.tmc2 = "the terms and conditions";
	
	$scope.langdata = $localStorage.langdata;
	if(typeof $scope.langdata !== "undefined" && $scope.langdata.version !== "undefined" && $scope.langdata.version.lb !== "undefined") {
			if($scope.langdata.version.lb == restaurant)
				$scope.zang = $scope.langdata;
			}

		
	if(typeof $scope.zang === "undefined")
		$scope.zang = {
		  'datetag': { lb: "Date", vl: "Date", f:"c" }, 'timetag': { lb: "time", vl: "time", f:"c" }, 'lunchtag': { lb: "lunch", vl: "Lunch", f:"c" }, "dinnertag": { "lb" : "dinner", "vl": "Dinner", f:"c" }, "guesttag": { "lb" : "number of guests", "vl": "number of guests", f:"c" }, "titletag": { "lb" : "title", "vl": "Title", f:"c" }, "firsttag": { "lb" : "firstname", "vl": "First name", f:"c" }, "lasttag": { "lb" : "lastname", "vl": "Last name", f:"c" }, "nametag": { "lb" : "name", "vl": "name", f:"c" }, "emailtag": { "lb" : "email", "vl": "Email", f:"c" }, "mobiletag": { "lb" : "mobile", "vl": "Mobile", f:"c" }, "requesttag": { "lb" : "special request", "vl": "Special request", f:"c" },
		  'bookingdetail': { lb: "booking details", vl: "booking details", f:"u" },
		  'personaldetail': { lb: "personal details", vl: "personal details", f:"u" },
		  'buttonmodify': { lb: "modify booking", vl: "modify booking", f:"n" },
		  'confinfo': { lb: "Please confirm your information", vl: "Please confirm your information", f:"c" } ,
		  'buttonbook': { lb : $scope.buttonLabel, vl: $scope.buttonLabel, f:"u" },
		  'buttonconfirm': { "lb" : $scope.buttonConfirm, "vl": $scope.buttonConfirm, "f":"u" },
		  'bookingtitle': { lb : $scope.bookingTitle, vl: $scope.bookingTitle, f:"u" },
		  'timeouttag': { lb : "timeout", vl: "timeout", f:"u" },
		  'listtag1': { "lb" : "free booking", "vl": "free booking", "f":"c" },
		  'listtag2': { "lb" : $scope.listTags2, "vl": $scope.listTags2, "f":"c" },
		  'listtag3': { "lb" : $scope.listTags3, "vl": $scope.listTags3, "f":"c" },
		  'listtag4': { "lb" : $scope.listTags4, "vl": $scope.listTags4, "f":"c" },
		  'listtag5': { "lb" : $scope.listTags5, "vl": $scope.listTags5, "f":"n" },
		  'confirmsg': { "lb" : $scope.confirmsg, "vl": $scope.confirmsg, "f":"n" },
		  'confirmsg2': { "lb" : $scope.confirmsg2, "vl": $scope.confirmsg2, "f":"n" },
		  'confirmsg3': { "lb" : $scope.confirmsg3, "vl": $scope.confirmsg3, "f":"n" },
		  'confirmsg4': { "lb" : $scope.confirmsg4, "vl": $scope.confirmsg4, "f":"n" },
		  'mr': { "lb" : "Mr.", "vl": "Mr.", "f":"c" },
		  'mrs': { "lb" : "Mrs.", "vl": "Mrs.", "f":"c" },
		  'ms': { "lb" : "Ms.", "vl": "Ms.", "f":"c" },
		  'error0': { "lb" : $scope.error0, "vl": $scope.error0, "f":"c" },
		  'error1': { "lb" : $scope.error1, "vl": $scope.error1, "f":"c" },
		  'error2': { "lb" : $scope.error2, "vl": $scope.error2, "f":"c" },
		  'error3': { "lb" : $scope.error3, "vl": $scope.error3, "f":"c" },
		  'error4': { "lb" : $scope.error4, "vl": $scope.error4, "f":"c" },
		  'error5': { "lb" : $scope.error5, "vl": $scope.error5, "f":"c" },
		  'error6': { "lb" : $scope.error6, "vl": $scope.error6, "f":"c" },
		  'error7': { "lb" : $scope.error7, "vl": $scope.error7, "f":"c" },
		  'error8': { "lb" : $scope.error8, "vl": $scope.error8, "f":"c" },
		  'error9': { "lb" : $scope.error9, "vl": $scope.error9, "f":"c" },
		  'error10': { "lb" : $scope.error10, "vl": $scope.error10, "f":"c" },
		  'error11': { "lb" : $scope.error11, "vl": $scope.error11, "f":"c" },
		  'tmc1': { "lb" : $scope.tmc1, "vl": $scope.tmc1, "f":"c" },
		  'tmc2': { "lb" : $scope.tmc2, "vl": $scope.tmc2, "f":"c" }
		  };
		  
    $scope.checkmark = [ { "label1": $scope.zang['listtag1'].vl, "label2": " ", "glyph": true }, { "label1": $scope.zang['listtag2'].vl, "label2": $scope.zang['listtag3'].vl, "glyph": true }, { "label1": $scope.zang['listtag4'].vl, "label2": $scope.zang['listtag5'].vl, "glyph": true } ];
	$scope.bookinginfo = [ { "label" : "divider", "value" : $scope.zang['bookingdetail'].vl }, { "label" : $scope.zang['datetag'].vl, "value" : $scope.pdate }, { "label" :  $scope.zang['timetag'].vl, "value" : $scope.ptime }, { "label" :  $scope.zang['guesttag'].vl, "value" : $scope.pcover }, { "label" : "divider", "value" : $scope.zang['personaldetail'].vl }, { "label" : $scope.zang['nametag'].vl, "value" : $scope.pname }, { "label" : $scope.zang['emailtag'].vl, "value" : $scope.pemail }, { "label" : $scope.zang['mobiletag'].vl, "value" : $scope.pmobile }, { "label" : $scope.zang['requesttag'].vl, "value" : $scope.prequest }  ];

});

function modify() {

	$('#book_form').attr('action', action);
	$('#book_form').submit();
}

String.prototype.capitalize = function () {
  return this.replace(/^./, function (match) {
    return match.toUpperCase();
  });
};

$(document).ready(function () {

});

	</script>
    </body>
</html>

