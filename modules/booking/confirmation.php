<?php
require_once("conf/conf.init.inc.php");
require_once("lib/wpdo.inc.php");
require_once('conf/conf.session.inc.php');
require_once("lib/wglobals.inc.php");
require_once("lib/class.images.inc.php");
require_once("lib/class.restaurant.inc.php");
require_once("lib/class.booking.inc.php");
require_once("lib/class.mail.inc.php");
require_once("lib/class.sms.inc.php");
require_once("lib/class.pushnotif.inc.php");
require_once("lib/class.spool.inc.php");
require_once("lib/class.async.inc.php");
require_once("lib/Browser.inc.php");
require_once("lib/class.qrcode.inc.php");
require_once("lib/class.analytics.inc.php");
require_once("lib/class.media.inc.php");
require_once("lib/class.coding.inc.php");
require_once("lib/class.login.inc.php");
require_once("lib/class.transitory.inc.php");
require_once("lib/class.member.inc.php");
require_once("lib/class.allote.inc.php");

$arglist = array('bklangue', 'bksalutation', 'bklast', 'bkfirst', 'bkemail', 'bkcover', 'bkdate', 'bktime', 'bkmobile', 'bkcountry', 'bkrestaurant', 'bktitle', 'bkparam', 'bkspecialrequest', 'brwsr_type', 'bkpage', 'bktracking', 'data', 'booker', 'company', 'hotelguest', 'babychair');
foreach ($arglist as $label)
    $$label = preg_replace("/\'\"/", "’", $_REQUEST[$label]);

$fullfeature = preg_match("/TheFunKitchen/", $bkrestaurant) || in_array($_SESSION['user']['member_type'], $fullfeature_member_type_allowed) || ($_SESSION['user']['member_type'] == 'weeloy_sales' && $res->status == 'demo_reference' || $res->status == 'active');

if ($fullfeature)
    $fullfeatflag = "'fullfeature=true;'";
else
    $fullfeatflag = "'fullfeature=false;'";

if (empty($bkrestaurant)) {
    header("location: https://www.weeloy.com");
    exit;
}
$fbId = WEBSITE_FB_APP_ID;
$res = new WY_restaurant;
$getRest = $res->getRestaurant($bkrestaurant);

$mediadata = new WY_Media($bkrestaurant);
$logo = $mediadata->getLogo('SG_SG_R_TheFunKitchen');

if (empty($res->restaurant)) {
    header("location: https://www.weeloy.com");
    exit;
}

$action = "book_form.php";
$booker = "";
$logger = new WY_log("website");
$logger->LogEvent($_SESSION['user']['id'], 820, '', $res->restaurant, '', date("Y-m-d H:i:s"));

$bkparamdecoded = detokenize($bkparam);
if (substr($bkparamdecoded, 0, 3) != date('M') || microtime(true) - floatval(substr($bkparamdecoded, 3)) > 600)
    header("Location: " . $action . "?bkrestaurant=" . $bkrestaurant . "&bktitle=" . $bktitle . "&bktracking=" . $bktracking . "&data=" . $data . "&timeout=y");

$restaurant_restaurant_tnc = $res->restaurant_tnc;

$booking = new WY_Booking();

$transitory = new WY_Transitory;
$transitory->read('BOOKING-' . $bkparam);

if ($transitory->result > 0) {
    $prev_confirmation = preg_replace("/BOOKING-/", "", $transitory->content);
    $booking->getBooking($prev_confirmation);
}

if (empty($booking->restaurant)) {

    $booking->restaurant = $bkrestaurant;
    $dateAr = explode("/", $bkdate);
    $bkDBdate = $dateAr[2] . "-" . $dateAr[1] . "-" . $dateAr[0];  // be carefull msqyl Y-m-d, html d/m/Y  qrc Ymd
    $language = $bklangue;


    if (empty($bktracking) && !empty($_COOKIE['weeloy_be_tracking']))
        $bktracking = $_COOKIE['weeloy_be_tracking'];

    $status = $booking->createBooking($bkrestaurant, $bkDBdate, $bktime, $bkemail, $bkmobile, $bkcover, $bksalutation, $bkfirst, $bklast, $bkcountry, $language, $bkspecialrequest, "booking", $bktracking, $booker, "", 0);

    if ($status < 0) {
        header("Location: " . $action . "?bkrestaurant=" . $bkrestaurant . "&bktitle=" . $bktitle . "&bktracking=" . $bktracking . "&error=" . $booking->msg);
        exit;
    }

    $bkconfirmation = $booking->confirmation;
    if ($booking->getBooking($bkconfirmation) == false) {
        header("Location: ./" . $action . "?" . $_SERVER['QUERY_STRING']);
        exit;
    }

    $transitory->create('BOOKING-' . $bkparam, $booking->confirmation);

    //qrcode generartion
    $path_tmp = explode(':', get_include_path());
    $qrcode_filename = $path_tmp[count($path_tmp) - 1] . 'tmp/' . $bkconfirmation . '.png';
    $qrcode = new QRcode();
    $qrcode->png('{"membCode":"' . $booking->membCode . '"}', $qrcode_filename);
    //file_put_contents($qrcode_filename,$qrcode->image(8));

    $booking->notifyBooking($qrcode_filename);
} else {
    // this is a reload
    $bkconfirmation = $booking->confirmation;
    $bktime = $booking->rtime;
    $bkemail = $booking->email;
    $bkmobile = $booking->mobile;
    $bkcover = $booking->cover;
    $bksalutation = $booking->salutation;
    $bkfirst = $booking->firstname;
    $bklast = $booking->lastname;
    $bkcountry = $booking->country;
    $language = $booking->language;
    $bkspecialrequest = $booking->specialrequest;
}

//kala added this line for fbshare date formate
if (@$bkdate) {
    $dateAr = explode("/", $bkdate);
    $bkDBdate = $dateAr[2] . "-" . $dateAr[1] . "-" . $dateAr[0];
    $fbShareDate = date('Y,M,d', strtotime($bkDBdate));
}


//qrcode delete
//unlink($qrcode_filename);
//error_log("<br><br>BOOKING = " . $status . " " . $booking->msg);

$QRCodeArg = "&bkrestaurant=$bkrestaurant&bkconfirmation=$bkconfirmation";

$typeBooking = ($res->is_bookable && ($res->status == 'active' || $res->status == 'demo_reference')) ? "true " : "false ";
$is_listing = false;
if (empty($res->is_wheelable) || !$res->is_wheelable) {
    $is_listing = true;
}

if (empty($tracking)) {
    $tracking = '';
}
?>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html><head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv='expires' content='mon, 1 jan 1998 05:00:00 gmt'>
        <meta http-equiv='pragma' content='no-cache'>
        <meta http-equiv='pragma' content='cache-control: max-age=0'>
        <meta http-equiv='last-modified' content='mon, 1 jan 1998 05:00:00 gmt'>
        <meta http-equiv='cache-control' content='no-cache, must-revalidate'>
        <meta name='robots' content='noindex, nofollow'>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
        <meta name="HandheldFriendly" content="true" />
        <title>Weeloy Code</title>
        <link href="../../css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../../css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
        <link href="../../css/bootstrap-select.css" rel="stylesheet" >
        <link href="../../css/bootstrap-social.css" rel="stylesheet" >
        <link href="../../css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="bookstyle.css" rel="stylesheet" type="text/css" />
        <link href='https://fonts.googleapis.com/css?family=Montserrat|Unkempt|Roboto:400,900,700italic,700,500italic,500,400italic' rel='stylesheet' type='text/css'>
        <script type="text/javascript" src="../../js/jquery.min.js"></script>
        <script type="text/javascript" src="../../js/bootstrap.min.js"></script>
        <script type='text/javascript' src="../../js/angular.min.js"></script>
        <script type="text/javascript" src="../../js/ngStorage.min.js"></script>
        <style>

            <?php if ($brwsr_type != "mobile") echo ".mainbox { margin: 10px 30px 20px 55px; width:550px; } "; ?>

        </style>

    </head>
    <body ng-app="myApp">


        <!-- tracking code for arun -->
        <?php
        $isTest = false;
        if (strpos($bkspecialrequest, 'test') !== false) {
            $isTest = true;
        }
        if (strpos($bkfirst, 'test') !== false) {
            $isTest = true;
        }
        if (strpos($bklast, 'test') !== false) {
            $isTest = true;
        }
        if (strpos($bkrestaurant, 'SG_SG_R_TheFunKitchen') !== false) {
            $isTest = true;
        }
        if (strpos($bkrestaurant, 'SG_SG_R_TheOneKitchen') !== false) {
            $isTest = true;
        }
        if (!$isTest) {
            ?>
            <script>

                dataLayer = [];
                dataLayer.push({
                    'pax': <?php echo $bkcover; ?>,
                    'reservation_id': '<?php echo $bkconfirmation; ?>',
                    'reserved_by': '<?php if (isset($_SESSION['user']['email']))
            echo 'user';
        else
            echo 'guest';
        ?>',
                    'reserver_name': '<?php echo $bkfirst . ' ' . $bklast; ?>',
                    'reserver_loginid': '<?php echo $bkemail; ?>',
                    'reservation_date': '<?php echo $bkdate; ?>',
                    'reservation_time': '<?php echo $bktime; ?>',
                    'restaurant_name': '<?php echo $bktitle; ?>',
                    'restaurant_id': '<?php echo $bkrestaurant; ?>'
                });
            </script>
<?php } ?>

<?php include_once("ressources/analyticstracking.php") ?>


        <div id='booking' ng-controller='MainController'  >

            <div class="container mainbox">    
                <div class="panel panel-info" >
                    <div class="panel-heading">
                        <div class="panel-title" style='width:300px;'> {{ zang['bookingtitlecf'].vl | uppercase}} </div>
                    </div>     

                    <p class="slogan">{{ zang['confirmsg'].vl}} {{hoteltitle}}<br /><br />
                        <strong>{{ zang['confirmsg2'].vl}} {{confirmation}}</strong><br/>{{ zang['confirmsg3'].vl}}<br /><span style="font-size:11px;" ng-if='flgsg'><a href='https://www.weeloy.com' target=_blank>{{ zang['confirmsg4'].vl}}</a> </span> </p>

                    <div class="panel-body" >

                            <?php if ($brwsr_type == "mobile") : ?>	
                            <div>			
                                <center><img ng-src='{{imglogo}}' max-width='120' max-height='120' id='theLogo' name='theLogo'></center><br>
<?php else : ?>								
                                <div class='col-xs-5 separation'>
                                    <center><img ng-src='{{imglogo}}' max-width='120' max-height='120' id='theLogo' name='theLogo'></center><br>
                                    <ul><li ng-repeat="x in checkmark"><p class='glyphicon glyphicon-ok checkmark' ng-if='x.glyph'></p><span ng-class='pcheckmark'> {{x.label1}} <br /> {{ x.label2}}<br/></span></li>
                                    </ul>
                                    <button class ="btn btn-sm btn-social btn-facebook" ng-click="fbshareclik();" ><i class="fa fa-facebook"></i>Share On Facebook</button> 
                                    <br/></br/>
<!--                                    <a href="../../mybookings&f=today"> <button class ="btn btn-sm-8 btn-success"  ><i class="fa fa-share"></i>&nbsp;Invite Friends</button> </a>-->
                                    <br/><br/>
                                </div>

                                <div class='col-xs-7' separation-left style="font-size:12px">
<?php endif; ?>
                                <div ng-repeat="x in bookinginfo" class='row' style="margin-left:5px"><span ng-if="x.value != '' && x.label != 'divider'"> {{x.label}} : {{x.value}} </span><div ng-if="x.label == 'divider'" class="alert alert-info alert-label">{{x.value| uppercase}}</div> </div>

                                <table><tr>
                                        <td><a href='javascript:openWin(1);'><br/>QRCode Agenda</a></td>
                                        <td width='50'>&nbsp;</td>
                                        <td><a href='javascript:openWin(0);'><br/>QRCode Contact</a></td>
                                    </tr></table>
                            </div>

                        </div>
                    </div>
                </div>                     
            </div>  
        </div>



        <script>

<?php
echo "var confirmation = '" . $booking->confirmation . "';";
echo "var tracking = '" . $booking->tracking . "';";
echo "var hoteltitle = '" . $booking->restaurantinfo->title . "';";
echo "var restaurant = '$bkrestaurant';";
echo "var typeBooking = $typeBooking;";
echo "var brwsr_type = '$brwsr_type';";
echo "var imglogo = '$logo';";
echo "var pdate = '$bkdate';";
echo "var ptime = '$bktime';";
echo "var pcover = '$bkcover';";
echo "var pfirst = '$bkfirst';";
echo "var plast = '$bklast';";
echo "var pemail = '$bkemail';";
echo "var pmobile = '$bkmobile';";
echo "var prequest = '$bkspecialrequest';";
echo "var langue = '$bklangue';";
echo "var topic = 'BOOKING';";

printf("var flgsg = %s;", (!preg_match('/CALLCENTER|WEBSITE/', $tracking)) ? 'true' : 'false');
printf("var is_listing = %s;", $is_listing ? 'true' : 'false');
?>

            var app = angular.module("myApp", ['ngStorage']);

            app.controller('MainController', function ($scope, $http, $locale, $timeout, $localStorage) {

<?php include("../../js/mylocal.js"); ?>

                angular.copy(locales['en'], $locale);

                $scope.langue = langue;
                $scope.pcheckmark = ($scope.langue != 'cn') ? 'pcheckmark' : 'pcheckmarkcn';
                $scope.imglogo = imglogo;
                $scope.brwsr_type = brwsr_type;
                $scope.hoteltitle = hoteltitle;
                $scope.confirmation = confirmation;
                $scope.flgsg = flgsg;

                $scope.bookingtitlecf = (typeBooking) ? "BOOKING CONFIRMED" : "REQUESTED CONFIRMED";
                $scope.confirmsg = (typeBooking) ? "Your booking is confirmed at " : "Your request has been sent to ";

                $scope.confirmsg2 = (typeBooking) ? "We are happy to confirm your reservation" : "Your reservation number is ";
                $scope.confirmsg3 = "You will receive the details of your reservation by email and SMS";
                $scope.confirmsg4 = "You can sign in using your email to view all your bookings and leave your reviews";
                $scope.listTags0 = "Free Booking";
                $scope.listTags1 = "";
                $scope.listTags2 = (typeBooking) ? "Instant Reservation" : "No reservartion";
                $scope.listTags3 = (typeBooking) ? "" : "First come, first serve";
                $scope.listTags4 = (!is_listing) ? "Win" : "Enjoy incredible";
                $scope.listTags5 = (!is_listing) ? "for every bookings" : " promotion";

                $scope.pdate = pdate;
                $scope.ptime = ptime;
                $scope.pcover = pcover;
                $scope.pfirst = pfirst;
                $scope.plast = plast;
                $scope.pemail = pemail;
                $scope.pmobile = pmobile;
                $scope.prequest = prequest;
                $scope.pname = pfirst + ' ' + plast;

                $scope.langdata = $localStorage.langdata;
                if (typeof $scope.langdata !== "undefined" && $scope.langdata.version !== "undefined" && $scope.langdata.version.lb !== "undefined") {
                    if ($scope.langdata.version.lb == restaurant)
                        $scope.zang = $scope.langdata;
                }

                if (typeof $scope.zang === "undefined")
                    $scope.zang = {
                        'datetag': {lb: "Date", vl: "Date", f: "c"}, 'timetag': {lb: "time", vl: "time", f: "c"}, 'lunchtag': {lb: "lunch", vl: "Lunch", f: "c"}, "dinnertag": {"lb": "dinner", "vl": "Dinner", f: "c"}, "guesttag": {"lb": "number of guests", "vl": "number of guests", f: "c"}, "titletag": {"lb": "title", "vl": "Title", f: "c"}, "firsttag": {"lb": "firstname", "vl": "First name", f: "c"}, "lasttag": {"lb": "lastname", "vl": "Last name", f: "c"}, "nametag": {"lb": "name", "vl": "Name", f: "c"}, "emailtag": {"lb": "email", "vl": "Email", f: "c"}, "mobiletag": {"lb": "mobile", "vl": "Mobile", f: "c"}, "requesttag": {"lb": "special request", "vl": "Special request", f: "c"},
                        'bookingdetail': {lb: "booking details", vl: "booking details", f: "u"},
                        'personaldetail': {lb: "personal details", vl: "personal details", f: "u"},
                        'buttonmodify': {lb: "modify", vl: "modify", f: "n"},
                        'confinfo': {lb: "Please confirm your information", vl: "Please confirm your information", f: "c"},
                        'buttonbook': {lb: $scope.buttonLabel, vl: $scope.buttonLabel, f: "u"},
                        'bookingtitlecf': {lb: $scope.bookingtitlecf, vl: $scope.bookingtitlecf, f: "u"},
                        'confirmsg': {lb: $scope.confirmsg, vl: $scope.confirmsg, f: "n"},
                        'confirmsg2': {lb: $scope.confirmsg2, vl: $scope.confirmsg2, f: "n"},
                        'confirmsg3': {lb: $scope.confirmsg3, vl: $scope.confirmsg3, f: "n"},
                        'confirmsg4': {lb: $scope.confirmsg4, vl: $scope.confirmsg4, f: "n"},
                        'timeouttag': {lb: "timeout", vl: "timeout", f: "u"},
                        'listtag1': {lb: "free booking", vl: "free booking", f: "c"},
                        'listtag2': {lb: $scope.listTags2, vl: $scope.listTags2, f: "c"},
                        'listtag3': {lb: $scope.listTags3, vl: $scope.listTags3, f: "c"},
                        'listtag4': {lb: $scope.listTags4, vl: $scope.listTags4, f: "c"},
                        'listtag5': {lb: $scope.listTags5, vl: $scope.listTags5, f: "n"}
                    };
                $scope.checkmark = [{"label1": $scope.zang['listtag1'].vl, "label2": " ", "glyph": true}, {"label1": $scope.zang['listtag2'].vl, "label2": $scope.zang['listtag3'].vl, "glyph": true}, {"label1": $scope.zang['listtag4'].vl, "label2": $scope.zang['listtag5'].vl, "glyph": true}];
                $scope.bookinginfo = [{"label": "divider", "value": $scope.zang['bookingdetail'].vl}, {"label": $scope.zang['datetag'].vl, "value": $scope.pdate}, {"label": $scope.zang['timetag'].vl, "value": $scope.ptime}, {"label": $scope.zang['guesttag'].vl, "value": $scope.pcover}, {"label": "divider", "value": $scope.zang['personaldetail'].vl}, {"label": $scope.zang['nametag'].vl, "value": $scope.pname}, {"label": $scope.zang['emailtag'].vl, "value": $scope.pemail}, {"label": $scope.zang['mobiletag'].vl, "value": $scope.pmobile}, {"label": $scope.zang['requesttag'].vl, "value": $scope.prequest}];
                $scope.fbshareclik = function () {

                    var fbObj = fbshare();
                    FB.ui(fbObj, function (response) {
                        console.log(JSON.stringify(response));
                    });

                }
                function fbshare() {
                    var obj = {
                        method: 'feed',
                        link: "https://www.weeloy.com/<?php echo $getRest['internal_path']; ?>",
                        picture: $scope.imglogo,
                        name: $scope.hoteltitle,
                        caption: 'booking restaurant at ' + $scope.hoteltitle + '  Promotion with Weeloy from ' + '<?php echo $fbShareDate ?>of ' + $scope.ptime,
                        description: "<?php echo mb_strimwidth($getRest['description'], 0, 200, "..."); ?>",
                        display: 'popup'
                    };
                    return obj;

                }
                function sortMethod(a, b) {
                    var x = a.name.toLowerCase();
                    var y = b.name.toLowerCase();
                    return ((x < y) ? -1 : ((x > y) ? 1 : 0));
                }


            });

            String.prototype.capitalize = function () {
                return this.replace(/^./, function (match) {
                    return match.toUpperCase();
                });
            };

            function openWin(type) {
                var arg = <?php echo "'$QRCodeArg'"; ?>;
                window.open('../qrcode/QRCode.htm?type=' + type + arg, 'QRCode', 'toolbar=no, scrollbars=no, resizable=no, top=30, left=30, width=330, height=330');
            }

            $(document).ready(function () {

                window.fbAsyncInit = function () {
                    FB.init({
                        appId: "<?php echo @$fbId ?>", //weeloy localhost fb appId
                        status: true,
                        cookie: true,
                        xfbml: true
                    });

                }; //end fbAsyncInit

                // Load the SDK Asynchronously
                (function (d) {
                    var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
                    if (d.getElementById(id)) {
                        return;
                    }
                    js = d.createElement('script');
                    js.id = id;
                    js.async = true;
                    js.src = "//connect.facebook.net/en_US/all.js";
                    ref.parentNode.insertBefore(js, ref);
                }(document)); //end loadSDF


            });

        </script>
    </body>
</html>

