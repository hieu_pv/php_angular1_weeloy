<?php

require_once("conf/conf.init.inc.php");
require_once("lib/wpdo.inc.php");
require_once("lib/wglobals.inc.php");
require_once("conf/conf.session.inc.php");


require_once("lib/Browser.inc.php");
//business class
require_once("lib/class.restaurant.inc.php");
require_once("lib/class.member.inc.php");
require_once("lib/class.media.inc.php");
require_once("lib/class.menu.inc.php");
require_once("lib/class.service.inc.php");
require_once("lib/class.allote.inc.php");
require_once("lib/class.review.inc.php");
require_once("lib/class.promotion.inc.php");

// management class
require_once("lib/class.page.inc.php");
require_once("conf/conf.page.inc.php");
require_once("lib/class.translate.inc.php");
require_once("conf/conf.translate.inc.php");
require_once("conf/conf.twig.inc.php");
//require_once("ressources/analyticstracking.php");


$restaurant_id = $_REQUEST['restaurantname'];


// NOTIFICATION MESSAGE
if (isset($_SESSION['info_message'])) {
    $twig_param['session'] = $_SESSION['info_message'];
    unset($_SESSION['info_message']);
}


// FOR HOME TITLE
if ($twig_param['page']['title'] == 'Book Best Restaurants in Singapore and Get Rewarded with Weeloy' && $_SESSION['user']['search_city'] != 'Singapore') {
    $twig_param['page']['title'] = str_replace('Singapore', $_SESSION['user']['search_city'], $twig_param['page']['title']);
    $twig_param['page']['desc'] = str_replace('Singapore', $_SESSION['user']['search_city'], $twig_param['page']['desc']);
}

// FOR RESTAURANT TITLE
if ($twig_param['page']['title'] == 'Book restaurant Now | Weeloy') {

    $res = new WY_restaurant();

    $res->getRestaurant($restaurant_id);

    $twig_param['page']['title'] = $res->title . ' - Book with Weeloy and Get Rewarded';
    $twig_param['page']['desc'] = 'Book a Restaurant in Singapore at ' . $res->title . ' with Weeloy and get yourself rewarded with exclusive deals and promotions. Simply book your table and spin the wheel!';
    $twig_param['page']['keywords'] = 'restaurants in Singapore, where to eat in Singapore, fine dining Singapore, best food in Singapore';
}

if (empty($path_adaptor)) {
    $path_adaptor = '';
}

$_SESSION['user']['lang'] = 'cn';

if (!isset($included_data)) {
    $included_data = '';
}
echo $twig->render('index_user.html.twig', array('path_adaptor' => $path_adaptor, 'twig_param' => $twig_param, 'translate' => $trsl[$_SESSION['user']['lang']], 'data' => $included_data));
?>
