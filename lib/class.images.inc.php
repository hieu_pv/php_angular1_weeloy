<?php

require_once("imagelib.inc.php");


define("IMG_RESTAURANT", 'restaurant');
define("IMG_EVENT", 'event');
define("IMG_LOGO", 'logo');
define("IMG_CHEF", 'chef');

$GLOBALS['picture_typelist'] = array('restaurant', 'event', 'logo', 'chef', 'menu', 'promotion', 'sponsor', 'profile');


class WY_Images {

    var $dirname;
    var $rootdir;
    var $tmpdir;
    var $globalshow;
    var $globalmedia;
    var $restaurant;
    var $newimg;
    var $oldimg;
    var $logo;
    var $chef;
    var $rPictures;  // restaurant pictures
    var $ePictures;  // event pictures
    var $storedPictures;  // event pictures
    var $msg;  // event pictures
    var $s3;
    var $bucket;
    var $object_type;

    function __construct($theRestaurant = "") {

        if (AWS) {
            $this->globalshow = __S3HOST__ . __S3DIR__;
        } else {
            $this->globalshow = __SHOWDIR__;
        }
        if (AWS) {
            $this->globalmedia = __S3HOST__;
        } else {
            $this->globalmedia = __MEDIADIR__;
        }
        $this->rootdir = __SERVERROOT__ . __ROOTDIR__;
        $this->tmpdir = __TMPDIR__;

        if ($theRestaurant == "")
            return;

        $this->restaurant = $theRestaurant;
        if (AWS) {
            $this->dirname = __S3DIR__ . "$theRestaurant/";
        } else {
            $this->dirname = __UPLOADDIR__ . "$theRestaurant/";
        }
        $datarestaurant = pdo_single_select("SELECT logo, chef_logo, images, event_images FROM restaurant WHERE restaurant = '$theRestaurant' limit 1");
        $this->logo = $datarestaurant['logo'];
        $this->chef = $datarestaurant['chef_logo'];
/*
        $this->rPictures = $datarestaurant['images'];			// obselete
        $this->ePictures = $datarestaurant['event_images'];			// obselete
        $this->storedPictures = $this->getStoredRestaurant();
*/
    }

    function l_file_exists($fname) {
        if (AWS) {
            require_once("conf/conf.s3.inc.php");
            return $this->getS3()->getObjectInfo($this->bucket, $this->dirname . $fname);
        } else {
            return file_exists($this->dirname . $fname);
        }
    }

    function l_unlink($fname, $scope = NULL) {
        if (AWS) {
            require_once("conf/conf.s3.inc.php");
            if($scope == NULL){
                $this->getS3()->deleteObject($this->bucket, $this->dirname . '1440/' . $fname);
                $this->getS3()->deleteObject($this->bucket, $this->dirname . '1024/' . $fname);
                $this->getS3()->deleteObject($this->bucket, $this->dirname . '500/' . $fname);
            }
            return $this->getS3()->deleteObject($this->bucket, $this->dirname . $fname);
        } else {
            if($scope == NULL){
                unlink($this->dirname . '1440/' . $fname);
                unlink($this->dirname . '1024/' . $fname);
                unlink($this->dirname . '500/' . $fname);
            }
            return unlink($this->dirname . $fname);
        }
    }

    function l_rename($oname, $fname, $object_type = '') {
        if (AWS) {
            require_once("conf/conf.s3.inc.php");
            if($object_type == 'wheel'){$cache_value = "max-age=1";}else{$cache_value = "max-age=19";}
            $this->getS3()->copyObject($this->bucket, $this->dirname . '1440/' . $oname, $this->bucket, $this->dirname . '1440/' . $fname, S3::ACL_PUBLIC_READ, array("Cache-Control" => "$cache_value", "Content-Type" => "image/jpeg"), array("Cache-Control" => "$cache_value", "Content-Type" => "image/jpeg"));
            //$this->l_unlink($oname);
            $this->getS3()->copyObject($this->bucket, $this->dirname . '1024/' . $oname, $this->bucket, $this->dirname . '1024/' . $fname, S3::ACL_PUBLIC_READ, array("Cache-Control" => "$cache_value", "Content-Type" => "image/jpeg"), array("Cache-Control" => "$cache_value", "Content-Type" => "image/jpeg"));
            //$this->l_unlink($oname);
            $this->getS3()->copyObject($this->bucket, $this->dirname . '500/' . $oname, $this->bucket, $this->dirname . '500/' . $fname, S3::ACL_PUBLIC_READ, array("Cache-Control" => "$cache_value", "Content-Type" => "image/jpeg"), array("Cache-Control" => "$cache_value", "Content-Type" => "image/jpeg"));
            $this->getS3()->copyObject($this->bucket, $this->dirname . $oname, $this->bucket, $this->dirname . $fname, S3::ACL_PUBLIC_READ, array("Cache-Control" => "$cache_value", "Content-Type" => "image/jpeg"), array("Cache-Control" => "$cache_value", "Content-Type" => "image/jpeg"));
            return $this->l_unlink($oname);
        } else {

            rename($this->dirname . '1440/' . $oname, $this->dirname . '1440/' . $fname);
            rename($this->dirname . '1024/' . $oname, $this->dirname . '1024/' . $fname);
            rename($this->dirname . '500/' . $oname, $this->dirname . '500/' . $fname);
            return rename($this->dirname . $oname, $this->dirname . $fname);
        }
    }

    function l_copy_restaurant_directory($src_name, $dst_name) {
        if (AWS) {
            require_once("conf/conf.s3.inc.php");
            $objects = $this->getS3()->getBucket($this->bucket, __S3DIR__ . $src_name . "/");
            foreach ($objects as $object) {
                $dest = str_replace($src_name, $dst_name, $object['name']);
                $this->getS3()->copyObject($this->bucket, $object['name'], $this->bucket, $dest, S3::ACL_PUBLIC_READ, array("Cache-Control" => "max-age=3", "Content-Type" => "image/jpeg"),array("Cache-Control" => "max-age=3", "Content-Type" => "image/jpeg"));
            }
            return true;
        } else {
            $src = __UPLOADDIR__ . $src_name;
            $dst = str_replace($src_name, $dst_name, $src);
            recurse_copy($src, $dst);
        }
    }

    
    function l_delete_restaurant_directory($src_name) {
         if (AWS) {
            require_once("conf/conf.s3.inc.php");
            $objects = $this->getS3()->getBucket($this->bucket, __S3DIR__ . $src_name . "/");
            foreach ($objects as $object) {
                $this->getS3()->deleteObject($this->bucket, $object['name']);
            }
            return true;
        } else {
            $src = __UPLOADDIR__ . $src_name;
            recurse_delete($src);
        }
    }
    
    function l_resizecompress($oname, $fname) {
        if (AWS) {
            require_once("conf/conf.s3.inc.php");
            $onameTmp = $this->tmpdir . $this->restaurant . $oname;
            $fnameTmp = $this->tmpdir . $this->restaurant . $fname;
            if ($this->getS3()->getObject($this->bucket, $this->dirname . $oname, $onameTmp)) {
                if (resizecompress($onameTmp, $fnameTmp, 1440)) {
                    $this->l_move_uploaded_file($fnameTmp, '1440/' . $fname);
                }
                if (resizecompress($onameTmp, $fnameTmp, 1024)) {
                    $this->l_move_uploaded_file($fnameTmp, '1024/' . $fname);
                }
                if (resizecompress($onameTmp, $fnameTmp, 500)) {
                    $this->l_move_uploaded_file($fnameTmp, '500/' . $fname);
                }
                if (resizecompress($onameTmp, $fnameTmp)) {
                    $this->l_move_uploaded_file($fnameTmp, $fname);
                }
                $this->l_unlink($oname);
                unlink($onameTmp);
                unlink($fnameTmp);
                return true;
            }
            return false;
        } else {
            resizecompress($this->dirname . $oname, $this->dirname . '1440/' . $fname, 1440);
            resizecompress($this->dirname . $oname, $this->dirname . '1024/' . $fname, 1024);
            resizecompress($this->dirname . $oname, $this->dirname . '500/' . $fname, 500);
            return resizecompress($this->dirname . $oname, $this->dirname . $fname);
        }
    }

    function l_move_uploaded_file($name, $fname, $content_type = 'image/jpeg', $object_type = '') {
        if (AWS) {
            require_once 'conf/conf.s3.inc.php';
            if($object_type == 'wheel'){$cache_value = "max-age=1";}else{$cache_value = "max-age=11";}
            $this->getS3()->putObjectFile($name, $this->bucket, $this->dirname . $fname, S3::ACL_PUBLIC_READ, array("Cache-Control" => "$cache_value", "Content-Type" => "$content_type"), array("Cache-Control" => "$cache_value", "Content-Type" => "$content_type"));
            return unlink($name);
        } else {
            return move_uploaded_file($name, $this->dirname . $fname);
        }
    }

    function l_move_file($oname, $fname, $content_type = 'image/jpeg', $object_type = '') {
        if (AWS) {
            require_once 'conf/conf.s3.inc.php';
            if($object_type == 'wheel'){$cache_value = "max-age=1";}else{$cache_value = "max-age=11";}
            $this->getS3()->putObjectFile($oname, $this->bucket, $this->dirname . $fname, S3::ACL_PUBLIC_READ, array("Cache-Control" => "$cache_value", "Content-Type" => "$content_type"), array("Cache-Control" => "$cache_value", "Content-Type" => "$content_type"));
        } else {
            return rename($oname, $this->dirname . $fname);
        }
    }

    function test() {

        require_once("conf/conf.s3.inc.php");
        $res = $this->getS3()->getBucket($this->bucket, 'upload/restaurant/' . $theRestaurant . '/');
        var_dump($res);
        die;
    }

    function getStoredRestaurant() {

        $fileAr = array();
        $pattern = "/\.gif|\.jpeg|\.jpg|\.png/";
        $handle = opendir($this->dirname);
        $logoName = "";
        while ($file = readdir($handle))
            if (preg_match($pattern, $file)) {
                if (substr($file, 0, 5) == "wheel")
                    continue;
                else if (substr($file, 0, 5) != "Logo.")
                    $fileAr[] = $file;
                else
                    $logoName = $file;
            }
        if ($logoName != "")
            $fileAr[] = $logoName;

        closedir($handle);
        return $fileAr;
    }

    function showPicture($subdir, $name, $size = '') {

        if (!empty($size)) {
            $size = $size . '/';
        }

        if ($subdir == "images")
            return "$subdir/" . $size . $name;

        return $this->globalshow . "$subdir/" . $size . $name;
    }

    function clean($str) {

        if ($str[0] == "|")
            $str = substr($str, 1);

        if ($str[strlen($str) - 1] == "|")
            $str = substr($str, 0, strlen($str) - 1);

        $str = preg_replace("/\|\|+/", "|", $str);

        return $str;
    }

    function getWheelSource($wheelvalue) {
        return $this->globalmedia . "upload/wheelvalues/wheelvalue_" . $wheelvalue . ".png";
    }

    function updateImageList($type) {

        if ($type == IMG_RESTAURANT)
            $qyimg = "images='" . $this->rPictures . "'";
        else if ($type == IMG_EVENT)
            $qyimg = "event_images='" . $this->ePictures . "'";

        return pdo_exec("Update restaurant set $qyimg where restaurant = '$this->restaurant' limit 1");
    }

    function updateLogo($logo) {		// obselete

        if ($this->l_file_exists($logo) == false)
            return -1;

        $this->logo = $logo;
        return pdo_exec("Update restaurant set logo = '$logo' where restaurant = '$this->restaurant' limit 1");
    }

    function updateChef($chef) {	// obselete

        if ($this->l_file_exists($chef) == false)
            return -1;

        $this->chef = $chef;
        return pdo_exec("Update restaurant set chef_logo = '$chef' where restaurant = '$this->restaurant' limit 1");
    }

    function deleteImg($image, $type) {
        if ($image == "" || ($type != IMG_RESTAURANT && $type != IMG_EVENT))
            return $this->retstatus(-1, "Empty image or wrong Type " . $image);

        if ($this->l_file_exists($image) == true)
        //return $this->retstatus(-1, "The image does not exist " . $image);
            $this->l_unlink($image);

        if ($type == IMG_RESTAURANT)
            $this->rPictures = $this->clean(str_replace("|" . $image . "|", "|", "|" . $this->rPictures . "|"));
        else if ($type == IMG_EVENT)
            $this->ePictures = $this->clean(str_replace("|" . $image . "|", "|", "|" . $this->ePictures . "|"));

        $this->updateImageList($type);
        return $this->retstatus(1, "The image has been deleted " . $image);
    }

    function addImg($image, $type) {
        if ($image == "" || ($type != IMG_RESTAURANT && $type != IMG_EVENT))
            return $this->retstatus(-1, "Invalid Type or empty image");

        if ($this->l_file_exists($image) == false)
            return $this->retstatus(-1, "The file has not been uploaded " . $image);

        if ($type == IMG_RESTAURANT) {
            if (preg_match("/\|$image\|/", "|" . $this->rPictures . "|"))
                return $this->retstatus(-1, "The image is currently set");
            $this->rPictures = $this->clean($image . "|" . $this->rPictures);
        }
        else if ($type == IMG_EVENT) {
            if (preg_match("/\|$image\|/", "|" . $this->ePictures . "|"))
                return $this->retstatus(-1, "The image is currently set");
            $this->ePictures = $this->clean($image . "|" . $this->ePictures);
        }

        $this->updateImageList($type);
        return $this->retstatus(1, "The image has been upload");
    }

    function retstatus($log, $mess) {
        $this->msg = $mess;
        return $log;
    }

    function renameImg($image, $newimage, $type) {


        if ($image == "" || $newimage == "" || ($type != IMG_RESTAURANT && $type != IMG_EVENT))
            return $this->retstatus(-1, 'Wrong type of invalid images ' . $newimage . ' for ' . $this->restaurant);

        if (strtolower(pathinfo($image, PATHINFO_EXTENSION)) != strtolower(pathinfo($newimage, PATHINFO_EXTENSION)))
            return $this->retstatus(-1, 'Cannot rename file with different extension. Old ' . $image . ' new ' . $newimage . ' for ' . $this->restaurant);

        if ($type == IMG_RESTAURANT)
            $imglist = $this->rPictures;
        else if ($type == IMG_EVENT)
            $imglist = $this->ePictures;
        else
            return $this->retstatus(-1, 'Invalid Image Type ' . $type . ' for ' . $this->restaurant);

        if (!preg_match("/\|$image\|/", "|$imglist|"))
            return $this->retstatus(-1, 'Image does not exist ' . $image . ' for ' . $this->restaurant);
        if (preg_match("/\|$newimage\|/", "|$imglist|"))
            return $this->retstatus(-1, 'Image already exist ' . $image . ' for ' . $this->restaurant);

        if ($this->l_file_exists($image) == false || $this->l_file_exists($newimage) == true)
            return $this->retstatus(-1, 'File doest not exists or duplicate. Old ' . $image . ' new ' . $newimage . ' for ' . $this->restaurant);

        $this->l_rename($image, $newimage);
        $imglist = $this->clean(preg_replace("/\|$image\|/", "|$newimage|", "|$imglist|"));

        if ($type == IMG_RESTAURANT)
            $this->rPictures = $imglist;
        else if ($type == IMG_EVENT)
            $this->ePictures = $imglist;

        $this->updateImageList($type);
    }

    function uploadImage($type) {

        $log = 0;
        $msg = "Unable to Upload image. Try again";
        if (isset($_POST["Stage"]) && $this->restaurant != "")
            if ($_POST["Stage"] == "Load") {
                list($log, $msg) = $this->modifyRecord();
                if (intval($log) <= 0)
                    return array("-1", $msg);

                $filename = $msg;
                $Ar[0] = $this->addImg($filename, $type);
                $Ar[1] = $this->msg;
                return $Ar;
            }

        return array(-1, "Unknow Error");
    }

    function modifyRecord() {

        $allowedExts = array("gif", "jpeg", "jpg", "png");
        $allowedTypes = array("image/gif", "image/jpeg", "image/jpg", "image/png");
        $extension = strtolower(pathinfo($_FILES["file"]["name"], PATHINFO_EXTENSION));
        if ((!in_array($_FILES['file']['type'], $allowedTypes)) || ($_FILES["file"]["size"] >= MAX_FILE_SIZE) || !in_array($extension, $allowedExts))
            return array("-1", "Invalid file " . $_FILES["file"]["name"] . " type = " . $_FILES['file']['type'] . " size = " . $_FILES['file']['size'] . " MAXSIZE " . MAX_FILE_SIZE);

        $filename = $_FILES["file"]["name"];

        if ($_FILES["file"]["error"] > 0)
            return array("-1", "Return Code: " . $_FILES["file"]["error"] . "<br>");

        if ($this->l_file_exists($filename))
            array("-1", $filename . " already exists. ");

        $tmp_image = "l_" . $filename;
        $this->l_move_uploaded_file($_FILES["file"]["tmp_name"], $tmp_image);
        $this->l_resizecompress($tmp_image, $filename);
        $cflg = "";
        if ($this->l_file_exists($filename)) {
            $this->l_unlink($tmp_image);
            $cflg = "(cr) ";
        } else
            $this->l_rename($tmp_image, $filename);

        return array("1", $filename);
    }

/*    function getDefaultImage() {
        $img_list = explode('|', $this->rPictures);
        sort($img_list, SORT_NATURAL);

        foreach ($img_list as $pic) {
            if (substr($pic, 0, 5) != "wheel" && substr($pic, 0, 5) != "Logo." && substr($pic, 0, 5) != "Chef.") {
                $defaultImage = $pic;
                break;
            }
        }
        return $defaultImage;
    }
*/
    function getImageLogo() {
        return $this->logo;
    }

    function getImageChef() {
        return $this->chef;
    }

    function getS3() {

        if (!isset($this->s3)) {
            $this->bucket = awsBucket;
            $this->s3 = new S3(awsAccessKey, awsSecretKey);
            $this->s3->putBucket($this->bucket, S3::ACL_PUBLIC_READ);
        }
        return $this->s3;
    }

}

?>