<?php

class WY_Review {

    var $dirname;
    var $showdirname;
    var $confirmation;
    var $user_id;
    var $restaurant;
    var $reviewgrade;
    var $foodgrade;
    var $ambiancegrade;
    var $servicegrade;
    var $pricegrade;
    var $comment;
    var $reviewdtvisite;
    var $reviewdtcreate;
    var $response_to;
    var $data;
    var $restaurant_title;
    var $msg;
    var $reviews;

    function __construct($theRestaurant = NULL) {
        $this->restaurant = $theRestaurant;
        $this->dirname = __UPLOADDIR__ . "$theRestaurant/";
        $this->showdirname = __SHOWDIR__ . "$theRestaurant/";
    }

    /*
      check the name to avoid duplicate
      insert the menu name, with empty sub items
      which will be updated during modification -> save
     */

    /* function insert() {

      $theRestaurant = $this->restaurant;
      $dbLabel = array('itemreviewdesc', 'itemreviewguest', 'itemreviewgrade', 'itemreviewdtvisite', 'itemreviewdtcreate');	// 'DATE_FORMAT(datevisit,\'%M %d, %Y\')'
      for($i = 0; $i < count($dbLabel); $i++)
      ${$dbLabel[$i]} = $this->clean_input($_REQUEST[$dbLabel[$i]]);

      //error_log("REVIEW ($theRestaurant) = $itemreviewdtvisite $itemreviewguest $itemreviewgrade $itemreviewdesc");

      if($itemreviewdtvisite != "")
      $tmpAr = explode('/', $itemreviewdtvisite);
      if(count($tmpAr) > 2)
      {
      $itemreviewdtvisite = $tmpAr[2] . "-" . $tmpAr[1] . "-" . $tmpAr[0];
      pdo_exec("INSERT INTO  review (reviewdesc, reviewguest, reviewgrade, reviewdtvisite, reviewdtcreate, restaurant ) VALUES ('$itemreviewdesc', '$itemreviewguest', '$itemreviewgrade', '$itemreviewdtvisite', 'NOW()', '$theRestaurant')");
      return array(1, "");
      }
      return array(-1, "Some error " . $reviewdtvisite);
      } */

    function postReview($confirmation_id, $user_id, $restaurant_id, $food_rate, $ambiance_rate, $service_rate, $price_rate, $comment, $response_to = NULL) {
        $review_grade = ($food_rate + $ambiance_rate + $service_rate + $price_rate) / 4;
        $sql = "INSERT INTO  review (confirmation, user_id, restaurant, reviewgrade, foodgrade, ambiancegrade, servicegrade, pricegrade, comment, reviewdtvisite, reviewdtcreate, response_to) 
                                VALUES ('$confirmation_id', '$user_id', '$restaurant_id', '$review_grade', '$food_rate', '$ambiance_rate', '$service_rate', '$price_rate', '$comment', NOW(), NOW(), '$response_to')";
        
        if (pdo_exec($sql)) {
            //notifi post review 
            $this->notifyPostReview($confirmation_id, $response_to);
            return true;
        }
        return false;
    }

    function saveResponse($confirmation, $response) {

        $theRestaurant = $this->restaurant;
        $response = clean_input($response);
        $Sql = "Update review set response_to='$response' where restaurant = '$theRestaurant' and confirmation = '$confirmation' limit 1";
        pdo_exec($Sql);
    }

    function getReview($confirmation_id, $response_to = NULL) {

		$this->confirmation = "";
		
        $where = '';
        if (isset($response_to) && $response_to != '') {
            $where = " AND response_to='" . $response_to . "'";
        	}
        	
        $sql = "SELECT * FROM  review WHERE confirmation LIKE '" . $confirmation_id . "'" . $where;
        $row = pdo_single_select($sql);

		if(count($row) <= 0) {
			error_log("REVIEW " . $confirmation_id . " " . count($row));
			return;
			}
		
        $this->confirmation = $row['confirmation'];
        $this->user_id = $row['user_id'];
        $this->restaurant = $row['restaurant'];
        $this->reviewgrade = $row['reviewgrade'];
        $this->foodgrade = $row['foodgrade'];
        $this->ambiancegrade = $row['ambiancegrade'];
        $this->servicegrade = $row['servicegrade'];
        $this->pricegrade = $row['pricegrade'];
        $this->comment = $row['comment'];
        $this->reviewdtvisite = $row['reviewdtvisite'];
        $this->reviewdtcreate = $row['reviewdtcreate'];
        $this->response_to = $row['response_to'];
    }

    function getReviews() {
        $theRestaurant = $this->restaurant;
        $this->data = pdo_multiple_select("SELECT confirmation, user_id, restaurant, reviewgrade, foodgrade, ambiancegrade, servicegrade, pricegrade, comment, reviewdtvisite, reviewdtcreate, reviewdtcreate, response_to FROM review WHERE restaurant ='$theRestaurant' ORDER by reviewdtvisite DESC");
    }

    function getReviewsList($limit = '') {
        $theRestaurant = $this->restaurant;
        $sql = "SELECT reviewgrade as score, foodgrade as food_score, ambiancegrade as ambiance_scrore, servicegrade as service_score, pricegrade as price_score, comment, reviewdtvisite as booking_date, reviewdtcreate as post_date, firstname as user_name, email FROM review, member WHERE review.user_id = member.email AND restaurant ='$theRestaurant' ORDER by reviewdtvisite DESC $limit";
        $this->data = pdo_multiple_select_array($sql);
    }    
    
    
    function getReviewsCount() {
        $theRestaurant = $this->restaurant;
        $sql = "SELECT COUNT(ID) as count, AVG(reviewgrade) as score_avg FROM review WHERE restaurant = '$theRestaurant'";
        $this->data = pdo_single_select($sql);
        return array('count'=>  $this->data['count'], 'score'=>  round($this->data['score_avg'],1), 'review_desc'=> $this->getReviewDesc($this->data['score_avg']));
    }
    
    
    private function getReviewDesc($score){
        if($score > 4.49){
            return 'excellent';
        }elseif ($score > 3.74) {
            return 'very good';
        }elseif ($score > 2.99) {
            return 'good';
        }else{
            return 'average';
        }
        return 'good';
    }
            
    function getUserReviews($email, $options) {
        $where = '';
        $order = '';
        $limit = '';


        ////  FILTER CREATION  ////
        if (isset($options['last'])) {
            $order .= ' reviewdtcreate DESC';
            $limit .= $options['last'];
        }

        if (!empty($order)) {
            $order = ' ORDER BY ' . $order;
        }
        if (!empty($limit)) {
            $limit = ' LIMIT ' . $limit;
        }
        //// END FILTER CREATION  ////


        $sql = "SELECT confirmation, user_id, r.restaurant, restau.title as title, reviewgrade, foodgrade, ambiancegrade, servicegrade, pricegrade, comment, reviewdtvisite, reviewdtcreate, reviewdtcreate, response_to FROM review r, restaurant restau WHERE 1 AND restau.restaurant = r.restaurant AND r.user_id = '$email' $where $order $limit";

        $data = pdo_multiple_select($sql);

        if (count($data) <= 0)
            return false;
        foreach ($data as $row) {
            $current = new WY_Review();
            $current->confirmation = $row['confirmation'];
            $current->user_id = $row['user_id'];
            $current->restaurant = $row['restaurant'];
            $current->reviewgrade = $row['reviewgrade'];
            $current->foodgrade = $row['foodgrade'];
            $current->ambiancegrade = $row['ambiancegrade'];
            $current->servicegrade = $row['servicegrade'];
            $current->pricegrade = $row['pricegrade'];
            $current->comment = $row['comment'];
            $current->reviewdtvisite = $row['reviewdtvisite'];
            $current->reviewdtcreate = $row['reviewdtcreate'];
            $current->response_to = $row['response_to'];

            $current->restaurant_title = $row['title'];

            $this->reviews[] = $current;
        }

        return true;
    }

    function clean_input($str) {

        if ($str == "")
            return;

        $str = preg_replace("/\'\"/", "’", $str);

        return $str;
    }

    function notifyPostReview($confirmation_id, $response_to = NULL) {

        $mailer = new JM_Mail;

        $booking = new WY_Booking();
        $booking->getBooking($confirmation_id);

        $this->getReview($confirmation_id, $response_to);

        // Notification Member
        $rtime = preg_replace("/:00/", "h", $booking->rtime);
        $rdate = date("F j, Y, g:i a", mktime(substr($booking->rtime, 0, 2), substr($booking->rtime, 3, 4), 0, intval(substr($booking->rdate, 5, 2)), intval(substr($booking->rdate, 8, 2)), intval(substr($booking->rdate, 0, 4))));
        $booking->rtime = $rtime;
        $booking->rdate = $rdate;

        $data_object = array('booking'=>$booking, 'review'=>$this);

        $header['from'] = array('sales@weeloy.com' => 'Weeloy.com');
        $header['replyto'] = array('sales@weeloy.com');

        $recipient = $booking->email;
        
        $subject = "New review on the " . $booking->restaurantinfo->title;
        $body = $mailer->getTemplate('notifyreview_member', $data_object);
        $mailer->sendmail($recipient, $subject, $body, $header);
       
        //email restau
        $recipient = $booking->restaurantinfo->email;
        //$recipient = "support@weeloy.com";
        $subject = "You have a new review";

        $body = $mailer->getTemplate('notifyreview_restaurant', $data_object);
        $mailer->sendmail($recipient, $subject, $body, $header);
    }

}

?>
