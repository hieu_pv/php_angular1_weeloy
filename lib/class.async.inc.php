<?php

/**
 *	Richard Kefs (c) 2014
 *
 * 	Contains all classes definition for login session.
 *
*/


class WY_Async {

	function __construct() {
   	}
   
	   // analyse du header d'une requete HTTP
	function extractHeader($doc) {
		$doc = $txt=explode("\n", $doc);
		$header = array();
			  // traiter la premiere ligne
			  // par example : HTTP/1.1 200 OK
		strtok($doc[0],"/");
		$header['http'] = strtok(" ");//version HTTP
		$header['code'] = strtok(" ");// code retour
		$header['lib'] = strtok("");// libelle
		for($i = 1; $i < count($doc); $i++) {
			$line = $doc[$i];
			if($line == "") 
				return $header; // premiere ligne vide ==> fin du header
		  	$last = strtok($line,": ");  // la cle
		  	$data = strtok("");          // la valeur
		  	$header[$last] = $data;
		  	}
		return $header;
	}

   // test d'une URL
	function getURLData($url) {
	  
		$ret = 0;
		$doc = "";

		$url = parse_url($url);
		$scheme =  $url[scheme];
		if($scheme != "" && $scheme != "http")
			return array(-10, "scheme not supported : $scheme\n");

		$site = $url[host];
		$port = ($url[port]=="" ? 80 : $url[port]);
		$path = $url[path];
		$path .= ( ($url[query] != "") ? $url[query] : "" );
		if ($path == "") 
			$path = "/";

		if($fp = @fsockopen ($site, $port, $errno, $errstr, 30)) {
			// envoi de la demande
			fputs ($fp, "GET $path HTTP/1.0\r\n");
			fputs ($fp, "Host: $site:$port\r\n");
			fputs ($fp, "\r\n");

			while (!feof($fp)) {
				$doc .= fgets($fp, 8000);
				}
			fclose ($fp);

			$hdr = $this->extractHeader($doc);

			$code = $hdr['code'];
			$ret = -1;
				// echo "Successful<br>";		
			if($code >= 200 && $code <300) $ret = 2;
				// echo "Redirected<br>";
			else if($code >= 300 && $code <400) $ret = -3;
				// echo "Client error<br>";
			else if($code >= 400 && $code <500) $ret = -4;
				// echo "Server error<br>";
			else if($code >= 500 && $code <600) $ret = -5;
			else $ret = -6;
			}
		return array($ret, $doc);  // renvoyer le code retour du serveur ( ($ret==2) ==> OK)

	}
	
	// good for batch files
	function async_curl($url) {
		$cmd = "curl $url > /dev/null 2>&1 &";
		exec($cmd);
        }
}

?>