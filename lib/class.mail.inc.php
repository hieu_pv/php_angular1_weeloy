<?php

/**
 * 	Richard Kefs (c) 2014
 *
 * 	Contains all classes definition for mail session.
 *
 */


require_once("lib/class.spool.inc.php");
require_once("lib/composer/vendor/autoload.php");
class JM_Mail {

    var $db;
    var $recipient;
    var $subject;
    var $body;
    var $header;
    var $result;
    
    var $twig;

    function sendmail($recipient, $subject, $body, $opts = NULL) {

	$spool = new WY_Spool;
        $spool->register_mail($recipient, $subject, $body, $opts);
    }

    function send_fromswiftmailer($recipient, $subject, $body, $opts = NULL) {
       
        if(isset($opts['from'])){
            $fromAr = $opts['from'];
        }else{
            $fromAr = array('support@weeloy.com' => 'Weeloy - Support');
        }
        
        $transport = Swift_SmtpTransport::newInstance('email-smtp.us-west-2.amazonaws.com', 587, 'tls')
                ->setUsername(AWSAccessKeyId)
                ->setPassword(AWSSecretKey);


        $mailer = Swift_Mailer::newInstance($transport);
        //Create the message
        $message = Swift_Message::newInstance()
                ->setSubject($subject)
                ->setFrom($fromAr);
       
        $message->setTo($recipient);
         

        if (!isset($body) || $body == "") {
            $body = 'default body';
        }
        
          $body = str_replace('’','"',$body);
          $root_path = explode(':',get_include_path());
          
          
          /*   if (isset($opts['photo'])){
          $cid2 = $message->embed(Swift_Image::fromPath(get_include_path().'/templates_html/default_2012/img/email_template/header_eatem.png'));
          //$cid3 = $message->embed(Swift_Image::fromPath($_SERVER['DOCUMENT_ROOT'].'/templates_html/default_2012/img/email_template/visuel.png'));
          $cid4 = $message->embed(Swift_Image::fromPath(get_include_path().'/templates_html/default_2012/img/email_template/bot_bg.png'));
          }

          if (isset($opts['photo'])){
          $body = str_replace('[top_bg.png]',$cid1,$body);
          $body = str_replace('[header_eatem.png]',$cid2,$body);
          //$body = str_replace('[visuel.png]',$cid3,$body);
          $body = str_replace('[bot_bg.png]',$cid4,$body);
          }

          if (isset($opts['photo_tracking'])){
          //$cid1 = $message->embed(Swift_Image::fromPath($_SERVER['DOCUMENT_ROOT'].'/newsletter/tracking.php'));
          }

          if (isset($opts['email_content_pic'])){
          $path_content = "/templates_html/default_2012/img/email_content/".$opts['page'];
          if($opts['user_gender'])
          {$path_content .= "_".$opts['user_gender'];}

          $path_content .='.png';

          $cid5 = $message->embed(Swift_Image::fromPath(get_include_path().$path_content));
          }

          
////////// IMAGE mangement  ////////
           *  */
          
        if(!isset($opts['white_label'])){
          $cid1 = $message->embed(Swift_Image::fromPath($root_path[count($root_path)-1].'images/logo_w_t_small.png'));
          $body = str_replace('[top_logo.png]',$cid1,$body);
        } 
          
          
        if (isset($opts['guest_qrcode']) && file_exists($opts['guest_qrcode'])){
            $cid5 = $message->embed(Swift_Image::fromPath($opts['guest_qrcode']));
            $body = str_replace('[guest_qrcode.png]',$cid5,$body);
        }
          

        
        if (isset($opts['replyto']) && $opts['replyto'] != "") {
            $message->setReplyTo(
                    $opts['replyto']
            );
        }
        
        if (isset($opts['bcc'])) {
            $message->setBcc($opts['bcc']);
        }
        
        $message->setBcc(array('weeloyhistory@gmail.com'));
        $message->setBody($body, 'text/html');

        if ($mailer->send($message, $failures)) {
            return true;
        } else {
            var_dump($failures);
            return false;
            //print_r($failures);
        }
    }

    function getTemplate($template, $data){
        $root_path = explode(':',get_include_path());
        $loader = new Twig_Loader_Filesystem($root_path[count($root_path)-1].'templates/email');
        $this->twig = new Twig_Environment($loader,array('debug' => true));
        
        $lexer = new Twig_Lexer($this->twig, array(
            'tag_comment'   => array('{#', '#}'),
            'tag_block'     => array('{%', '%}'),
            'tag_variable'  => array('{[{', '}]}'), // was array('{{', '}}')
            'interpolation' => array('#{', '}'),
        ));
        $this->twig->setLexer($lexer);
        
        return  $this->twig->render($template.'.html.twig', array('data' => $data));
    }
    
    function ltest() {
        $recipient = "richard@kefs.me";
        $subject = "testing mail";
        $body = "this is a test";

        $this->sendmail($recipient, $subject, $body);
    }


}

?>