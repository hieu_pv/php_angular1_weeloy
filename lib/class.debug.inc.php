<?php

$debug_type = array("LOG" => 0, "DEBUG" => 1, "ERROR" => 2, "ERROR-API" => 4, "ERROR-PDO" => 8);

class WY_debug {

   var $type; 
   var $iplong;
   var $category;
   var $cdate;
   var $content;
   
	function __construct() {
        $real_client_ip = '';
		$headers = apache_request_headers();
		if(isset($headers["X-Forwarded-For"])) {
			$real_client_ip  = $headers["X-Forwarded-For"];
        	}

		if(isset($real_client_ip) && $real_client_ip !='')
			$adresse_ip = $real_client_ip;
		else $adresse_ip = $_SERVER['REMOTE_ADDR'];

		if($adresse_ip == '::1'){
			$adresse_ip = '127.0.0.1';
			}
		$adresse_ip_tmp = explode(',',$adresse_ip);
		$adresse_ip = $adresse_ip_tmp[0];
                
		$this->iplong = ip2long($adresse_ip);
		}
			
	function writeDebug($type, $category, $content) {
	
        global $debug_type;
        
        $typevalue = 0;
		if(array_key_exists($type, $debug_type))
			$typevalue = $debug_type[$type];
		
		$category= clean_input($category);
		$content= clean_input($content);
		$ip = $this->iplong;
		
		error_log($type . " " . $category . " : " . $content);
		pdo_exec("INSERT INTO log_debug(type, category, content, ip) VALUES ('$typevalue', '$category', '$content', '$ip')",'dwh');
	}
	
	function readDebug($type, $category, $firstdate, $lastdate) {
	
        global $debug_type;
        
        $typevalue = 0;
		if(array_key_exists($type, $debug_type))
			$typevalue = $debug_type[$type];

		return pdo_multiple_select("SELECT * from log_debug WHERE type = '$typevalue' and category = '$category' and cdate > '$firstdate' and cdate < '$lastdate'", "dwh");	
	}
}

?>
