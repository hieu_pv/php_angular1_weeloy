<?php

define("__CALL_CENTER__", 1);
define("__RESTRICTED_RESERVATION__", 2);
define("__RESTRICTED_SUPERADMIN_COUNTRY__", 4);
define("__RESTRICTED_AVAILABILITY__", 16);

define("__ALLOWCREATEDELETE__", 0x8000);

$permlabelAr = array(
						"perm_callcenter" => "call center", 
						"perm_limited_reservation" => "restricted booking", 
						"perm_limited_country" => "restricted country",
						"perm_limited_availability" => "restricted availability",
						"perm_RestoCRD" => "Allow create/Restaurant"
						);

$permvalueAr = array(
						"perm_callcenter" => __CALL_CENTER__, 
						"perm_limited_reservation" => __RESTRICTED_RESERVATION__, 
						"perm_limited_country" => __RESTRICTED_SUPERADMIN_COUNTRY__,
						"perm_limited_availability" => __RESTRICTED_AVAILABILITY__,
						"perm_RestoCRD" => __ALLOWCREATEDELETE__
						);

$admin_member_type_allowed = array('visitor', 'super_weeloy', 'admin');
$backoffice_member_type_allowed = array('visitor','restaurant','weeloy_sales','super_weeloy', 'admin');
$super_member_type_allowed = array('super_weeloy', 'admin');
$backoffice_listing_member_type_allowed = array('restaurant', 'restaurant_booking');
$fullfeature_member_type_allowed = array('weeloy_agent', 'weeloy_admin', 'admin');
$rotation_member_type_allowed = array('visitor','restaurant','weeloy_sales','super_weeloy', 'admin');

class WY_Member {

    var $id;
    var $email;
    var $mobile;
    var $name;
    var $firstname;
    var $country;
    var $member;
    var $gender;
    var $address;
    var $address1;
    var $city;
    var $zip;
    var $region;
    var $state;
    var $fax;
    var $cuisine;
    var $mealtype;
    var $creditcard;
    var $pricing;
    var $stars;
    var $rating;
    var $evoucher;
    var $member_permission;
    var $status;

    function __construct($user = NULL) {

        if (AWS) {
            $this->globalshow = __S3HOST__ . __S3DIR__;
        } else {
            $this->globalshow = __SHOWDIR__;
        }
        if (!empty($user)) {
            $this->email = $user;
        }
    }
    public function sendContactEmail($email, $message, $firstname, $lastname) {
        $body = "Message form " . $firstname . " " . $lastname . "<br/>"
                . $email . "<br/>"
                . $message;

        $mailer = new JM_Mail();
        $email_from = 'info@weeloy.com';
        $email_to = 'info@weeloy.com';

        $opts['from'] = array($email_from => 'Info Weeloy.com');
        $opts['replyto'] = $email;

        return $mailer->sendmail($email_to, "Weeloy - Contact support", nl2br($body), $opts);
    }

    function getMember($theMember) {
        $data = pdo_single_select("SELECT 
                        id, mobile, email, name, firstname, email, country, member, gender, address, address1, city, zip, region, state, fax, cuisine, mealtype, creditcard, pricing, stars, rating, evoucher, member_permission, member_type, status
                        from member WHERE email = '$theMember' LIMIT 1");

		if(count($data) < 1)
			return $data;
			
        $this->id = $data['id'];
        $this->email = $data['email'];
        $this->mobile = $data['mobile'];
        $this->name = $data['name'];
        $this->firstname = $data['firstname'];
        $this->email = $data['email'];
        $this->country = $data['country'];
        $this->member = $data['member'];
        $this->gender = $data['gender'];
        $this->address = $data['address'];
        $this->address1 = $data['address1'];
        $this->city = $data['city'];
        $this->zip = $data['zip'];
        $this->region = $data['region'];
        $this->state = $data['state'];
        $this->fax = $data['fax'];
        $this->cuisine = $data['cuisine'];
        $this->mealtype = $data['mealtype'];
        $this->creditcard = $data['creditcard'];
        $this->pricing = $data['pricing'];
        $this->stars = $data['stars'];
        $this->rating = $data['rating'];
        $this->evoucher = $data['evoucher'];
        $this->member_permission = $data['member_permission'];
        $this->status = $data['status'];
        
        return $data;
    }

	function deleteMember($email) {
    	return pdo_exec("UPDATE member SET status = 'deleted' where email = '$email' limit 1");	
		}
	
    function getListMember() {

        $this->email = array();
        $this->name = array();
        $this->firstname = array();

        $data = pdo_multiple_select("SELECT email, name, firstname, country FROM member order by name");
  		if(count($data) < 1)
			return;

		foreach ($data as $row) {
            $this->email[] = $row['email'];
            $this->name[] = $row['name'];
            $this->firstname[] = $row['firstname'];
            $this->country[] = $row['country'];
        }
    }

	function read_permission($email) {

        $data = pdo_single_select("SELECT member_permission FROM member where email = '$email' limit 1");
        if(count($data) > 0)
        	return intval($data['member_permission']);
        return 0;
		}
	
	function is_restrictedCountry($email) {
		return ($this->read_permission($email) & __RESTRICTED_SUPERADMIN_COUNTRY__);
		}
	
	function is_allowCreateResto($email) {
		return ($this->read_permission($email) & __ALLOWCREATEDELETE__);
		}
	
			
	function write_permission($email, $permission) {

        pdo_exec("update member set member_permission = '$permission' where email = '$email' limit 1");
		}
		
    function getListMemberRestaurant() {

        $this->email = array();
        $this->name = array();
        $this->firstname = array();
        $this->country = array();

        $data = pdo_multiple_select("SELECT email, name, firstname, country FROM member where member_type != 'member' order by name");
  		if(count($data) < 1)
			return;

        foreach ($data as $row) {
            $this->email[] = $row['email'];
            $this->name[] = $row['name'];
            $this->firstname[] = $row['firstname'];
            $this->country[] = $row['country'];
        }
    }

    function addMembersRestaurants($member_id, $restaurantAr) {
        foreach ($restaurantAr as $restaurant) {
            $Sql = "INSERT INTO restaurants_managers (member_id, restaurant_id, status)
                    VALUES ('$member_id', '$restaurant', 'active') ON DUPLICATE KEY UPDATE status = 'active' ";
            pdo_exec($Sql);
        }
    }

    function deleteMembersRestaurants($member_id, $restaurantAr) {
        foreach ($restaurantAr as $restaurant) {
            $Sql = "INSERT INTO restaurants_managers (member_id, restaurant_id, status)
                    VALUES ('$member_id', '$restaurant', 'deleted') ON DUPLICATE KEY UPDATE status = 'deleted' ";
            pdo_exec($Sql);
        }
    }

    function addAppManagerRestaurants($user_id, $restaurantAr) {
        foreach ($restaurantAr as $restaurant) {
            $Sql = "INSERT INTO restaurant_app_managers (user_id, restaurant_id, status)
                    VALUES ('$user_id', '$restaurant', 'active') ON DUPLICATE KEY UPDATE status = 'active' ";
            pdo_exec($Sql);
        }
    }

    function deleteAppManagerRestaurants($user_id, $restaurantAr) {
        foreach ($restaurantAr as $restaurant) {
            $Sql = "INSERT INTO restaurant_app_managers (user_id, restaurant_id, status)
                    VALUES ('$user_id', '$restaurant', 'deleted') ON DUPLICATE KEY UPDATE status = 'deleted' ";
            pdo_exec($Sql);
        }
    }

    function saveNewsletterAddress($email) {
        $Sql = "INSERT INTO members_newsletter (email, status)
                    VALUES ('$email', 'active') ON DUPLICATE KEY UPDATE status = 'active' ";
        return pdo_exec($Sql);
//return true;
    }

    function getIdByEmail($email) {
        $sql = "SELECT ID from member WHERE email ='$email'";
        $data = pdo_single_select($sql);
        return $data['ID'];
    }

    function partialCreateMember($type, $gender, $first, $name, $mobile, $country, $email) {
        pdo_insert("INSERT INTO `login` (`ID`, `Email`, `Password`, `tmpPassword`, `tmpTimeStamp`, `Token`, `TheTS`, `applitoken`, `appliname`, `userid`, `facebookid`, `UI`, `oldID`, `stateIMP`) VALUES (NULL, '$email', 'no_pass_please_reset_pass', NULL, CURRENT_TIMESTAMP, NULL, '0000-00-00 00:00:00.000000', '', '', '', '', NULL, NULL, NULL);");
        return pdo_insert("INSERT into member (member_type, gender, firstname, name, mobile, country, email) values ('$type', '$gender', '$first', '$name', '$mobile', '$country', '$email')");
		}
		
    function partialUpdateMember($type, $gender, $first, $name, $mobile, $country, $email) {
    	
        $Sql = "UPDATE member SET 
                member_type = '$type',
                gender = '$gender',
                firstname = '$first', 
                name = '$name', 
                mobile = '$mobile', 
                country = '$country' WHERE email = '$email' limit 1";

       	return pdo_exec($Sql);
		}
		
    function updateMember() {
        $Sql = "UPDATE member SET 
                gender = '$this->gender', 
                email = '$this->email',
                firstname = '$this->firstname', 
                name = '$this->name', 
                mobile = '$this->mobile', 
                country = '$this->country',
                member = '$this->member',
                address = '$this->address',
                address1 = '$this->address1',
                city = '$this->city',
                zip = '$this->zip',
                region = '$this->region',
                state = '$this->state',
                fax = '$this->fax',
                cuisine = '$this->cuisine',
                mealtype = '$this->mealtype',
                creditcard = '$this->creditcard',
                pricing = '$this->pricing',
                stars = '$this->stars',
                rating = '$this->rating',
                evoucher = '$this->evoucher',   
                status = '$this->status'  


                WHERE email = '$this->email'";
        return pdo_exec($Sql);
//return true;
    }

    public function setFavoriteRestaurant($restaurant, $is_favorite) {
        $sql = "INSERT into members_restaurants_favorite (member, restaurant, is_favorite) VALUES ('$this->email','$restaurant', '$is_favorite') ON DUPLICATE KEY UPDATE is_favorite = '$is_favorite' ";
        return pdo_exec($sql);
    }

    public function getBooking($date1 = NULL, $date2 = NULL, $sort='ASC') {
        $where = '';

        if(empty($date1) && empty($date2)){
            $date1 = date('Y-m-d', time() - 3600 * 24 * 30);
            $date2 = date('Y-m-d', time() + 3600 * 24 * 84);
        }
        
        
        $sql = "SELECT b.ID, b.confirmation, b.type, b.restaurant, rest.title as title,rest.cuisine,rest.is_wheelable, m.name as image, m.morder, rest.address, rest.address1, rest.zip, rest.city, rest.country, b.restable, b.status, b.restCode, b.membCode, b.email, b.mobile, b.salutation, b.lastname, b.firstname, b.cdate, b.rdate, b.rtime, "
                . "b.cover, b.revenue, b.specialrequest,rest.mealtype, b.wheelsegment,wheelwin, b.wheeldesc, b.spinsource, b.browser, b.language, b.country, b.ip,"
                . "r.reviewgrade, r.foodgrade, r.ambiancegrade, r.servicegrade, r.pricegrade, r.comment, r.response_to, r.reviewdtcreate "
                . " FROM booking b "
                . "LEFT JOIN restaurant rest ON b.restaurant = rest.restaurant "
                . "LEFT JOIN media m ON m.restaurant = rest.restaurant "
                . "LEFT JOIN review r ON b.confirmation = r.confirmation "
                . "WHERE b.email=:query AND rdate > :date1 AND rdate < :date2 AND m.object_type = 'restaurant' AND m.media_type = 'picture' GROUP BY confirmation ORDER BY rdate $sort, m.morder ASC";

//$sql = "SELECT * FROM booking b, restaurant r WHERE email=:query ORDER BY rdate ASC";

        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("query", $this->email);
        
        $stmt->bindParam("date1", $date1);
        $stmt->bindParam("date2", $date2);
        
        $stmt->execute();
        $bookings_data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $db = null;

        $bookings = array();
        $bookingsAr = array();
        $bkglabel = array('ID', 'confirmation', 'restaurant', 'title', 'cuisine', 'status', 'restCode', 'membCode', 'address', 'address1', 'zip', 'city', 'country', 'restable', 'email', 'mobile', 'salutation', 'lastname', 'firstname', 'cdate', 'rdate', 'rtime', 'cover', 'revenue', 'specialrequest', 'mealtype', 'wheelsegment', 'wheelwin', 'wheeldesc', 'spinsource', 'browser', 'language', 'country', 'ip', 'image', 'morder', 'is_wheelable');
        $reviewlabel = array('reviewgrade', 'foodgrade', 'ambiancegrade', 'servicegrade', 'pricegrade', 'comment', 'reviewdtcreate');
        foreach ($bookings_data as $data) {
            $reviewgrade = $data['reviewgrade'];
            $response_to = $data['response_to'];

            foreach ($data as $key => $value) {
                if (in_array($key, $bkglabel))
                    $bookings[$key] = $value;
                else if (!in_array($key, $reviewlabel) || $reviewgrade == NULL)
                    continue;
                else if (!$response_to)
                    $bookings['review'][$key] = $value;
                else if ($response_to)
                    $bookings['review_response'][$key] = $value;
            }

            if ($reviewgrade == NULL || $response_to)
                $bookings['review'] = NULL;
            if ($reviewgrade == NULL || !$response_to)
                $bookings['review_response'] = NULL;

            $bookingsAr[] = $bookings;
            $bookings = array();
        }
        return $bookingsAr;
    }

    public function getBookingsUserFilter($user, $filter, $date1, $date2, $booking_status = NULL) {
        $where = '';
        if ($date1 == '2013-12-01') {
            $date1 = date('Y-m-d', time() - 3600 * 24);
        }
        if ($date2 == '2015-12-01') {
            $date2 = date('Y-m-d', time() + 3600 * 24 * 1000);
        }

        if ($date1 == 'START_DATE') {
            $date1 = date('Y-m-d', time());
            $date2 = date('Y-m-d', time() + 3600 * 24 * 7);
        }

        if ($filter == 'all') {
            $where = '';
        } else {
            if ($filter == 'not_spun') {
                $where = ' AND (wheelwin IS NULL OR wheelwin LIKE "") ';
            } else {
                $where = ' AND (wheelwin IS NOT NULL && wheelwin NOT LIKE "") ';
            }
        }
        if (!empty($booking_status)) {
            $where .= " AND b.status NOT LIKE 'cancel' ";
        } 
        $selectresto = "b.restaurant='" . $user . "'";
       

        $sql = "SELECT b.ID, b.confirmation, b.type, b.restaurant, rest.title as title, rest.address, rest.address1, rest.zip, rest.city, rest.country, b.restable, b.status, b.restCode, b.membCode, b.email, b.mobile, b.salutation, b.lastname, b.firstname, b.cdate, b.rdate, b.rtime, "
                . "b.cover, b.revenue, b.specialrequest,rest.mealtype, b.wheelsegment,wheelwin, b.wheeldesc, b.spinsource, b.browser, b.language, b.country, b.ip,"
                . "r.reviewgrade, r.foodgrade, r.ambiancegrade, r.servicegrade, r.pricegrade, r.comment, r.response_to, r.reviewdtcreate, m.name as image, m.morder"
                . " FROM booking b "
                . "LEFT JOIN restaurant rest ON b.restaurant = rest.restaurant "
                . "LEFT JOIN media m ON m.restaurant = rest.restaurant "
                . "LEFT JOIN review r ON b.confirmation = r.confirmation "
                . "WHERE type = 'booking' AND " . $selectresto . " "
                . "AND rdate > :date1 AND rdate < :date2 $where "
                . "AND m.object_type = 'restaurant' "
                . "AND m.media_type = 'picture' GROUP BY confirmation ORDER BY rdate ASC, m.morder ASC ";

   


        $db = getConnection();
        $stmt = $db->prepare($sql);
//$stmt->bindParam("query", $user);
        $stmt->bindParam("date1", $date1);
        $stmt->bindParam("date2", $date2);
        $stmt->execute();
        $data_sql = $stmt->fetchAll(PDO::FETCH_BOTH);
        $db = null;

        $bookingsAr = array();
        foreach ($data_sql as $data) {
            $bookings['ID'] = $data['ID'];
            $bookings['confirmation'] = $data['confirmation'];
            $bookings['restaurant'] = $data['restaurant'];
            $bookings['title'] = $data['title'];
            $bookings['status'] = $data['status'];
            $bookings['restCode'] = $data['restCode'];
            $bookings['membCode'] = $data['membCode'];
            $bookings['address'] = $data['address'];
            $bookings['address1'] = $data['address1'];
            $bookings['zip'] = $data['zip'];
            $bookings['city'] = $data['city'];
            $bookings['country'] = $data['country'];
            $bookings['image'] = $data['image'];
            $bookings['table'] = $data['restable'];
            $bookings['email'] = $data['email'];
            $bookings['mobile'] = $data['mobile'];
            $bookings['salutation'] = $data['salutation'];
            $bookings['lastname'] = $data['lastname'];
            $bookings['firstname'] = $data['firstname'];
            $bookings['cdate'] = $data['cdate'];
            $bookings['rdate'] = $data['rdate'];
            $bookings['rtime'] = $data['rtime'];
            $bookings['cover'] = $data['cover'];
            $bookings['revenue'] = $data['revenue'];
            $bookings['specialrequest'] = $data['specialrequest'];
            $bookings['mealtype'] = $data['mealtype'];
            $bookings['wheelsegment'] = $data['wheelsegment'];
            $bookings['wheelwin'] = $data['wheelwin'];
            $bookings['wheeldesc'] = $data['wheeldesc'];
            $bookings['spinsource'] = $data['spinsource'];
            $bookings['browser'] = $data['browser'];
            $bookings['language'] = $data['language'];
            $bookings['country'] = $data['country'];
            $bookings['ip'] = $data['ip'];
            if ($data['reviewgrade'] != NULL && !$data['response_to']) {
                $bookings['review']['reviewgrade'] = $data['reviewgrade'];
                $bookings['review']['foodgrade'] = $data['foodgrade'];
                $bookings['review']['ambiancegrade'] = $data['ambiancegrade'];
                $bookings['review']['servicegrade'] = $data['servicegrade'];
                $bookings['review']['pricegrade'] = $data['pricegrade'];
                $bookings['review']['comment'] = $data['comment'];
                $bookings['review']['reviewdtcreate'] = $data['reviewdtcreate'];
            } else {
                $bookings['review'] = NULL;
            }

            if ($data['reviewgrade'] != NULL && $data['response_to']) {
                $bookings['review_response']['reviewgrade'] = $data['reviewgrade'];
                $bookings['review_response']['foodgrade'] = $data['foodgrade'];
                $bookings['review_response']['ambiancegrade'] = $data['ambiancegrade'];
                $bookings['review_response']['servicegrade'] = $data['servicegrade'];
                $bookings['review_response']['pricegrade'] = $data['pricegrade'];
                $bookings['review_response']['comment'] = $data['comment'];
                $bookings['review_response']['reviewdtcreate'] = $data['reviewdtcreate'];
            } else {
                $bookings['review_response'] = NULL;
            }

            $bookingsAr[] = $bookings;
            $bookings = array();
        }
        return $bookingsAr;
    }

    public function addEmailNewsletter($email){
        $sql = "INSERT INTO members_newsletter (email, status) VALUES ('$email', 'active') ON DUPLICATE KEY UPDATE status = 'active';";
        return pdo_exec($sql);
    }
}

?>
