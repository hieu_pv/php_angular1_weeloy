<?php

// $langtemplate = array( 'af', /* afrikaans */ 'ar', /* arabic */ 'bg', /* bulgarian */ 'ca', /* catalan */ 'cs', /* czech */ 'da', /* danish */ 'de', /* german */ 'el', /* greek */ 'en', /* english */ 'es', /* spanish */ 'et', /* estonian */ 'fi', /* finnish */ 'fr', /* french */ 'gl', /* galician */ 'he', /* hebrew */ 'hi', /* hindi */ 'hr', /* croatian */ 'hu', /* hungarian */ 'id', /* indonesian */ 'it', /* italian */ 'ja', /* japanese */ 'ko', /* korean */ 'ka', /* georgian */ 'lt', /* lithuanian */ 'lv', /* latvian */ 'ms', /* malay */ 'nl', /* dutch */ 'no', /* norwegian */ 'pl', /* polish */ 'pt', /* portuguese */ 'ro', /* romanian */ 'ru', /* russian */ 'sk', /* slovak */ 'sl', /* slovenian */ 'sq', /* albanian */ 'sr', /* serbian */ 'sv', /* swedish */ 'th', /* thai */ 'tr', /* turkish */ 'uk', /* ukrainian */ 'zh' /* chinese */ ); 
require_once("lib/class.debug.inc.php");
require_once("lib/class.restaurant.inc.php");

class WY_restaurantConf {

    var $restaurant;
    var $title;
    var $tel;
    var $email;
    var $smsid;
    var $URLrestaurant;
    var $logo;
    var $address;
    var $zip;
    var $city;
    var $country;
    var $GPS;
    var $images;
    var $map;
    var $url;
    var $restaurant_tnc;
    var $status;
    var $is_bookable;
    var $is_wheelable;
    var $internal_path;

}

class WY_Booking {

    var $restaurant;
    var $confirmation;
    var $type;
    var $status;
    var $restCode;
    var $membCode;
    var $salutation;
    var $firstname;
    var $lastname;
    var $email;
    var $mobile;
    var $cdate;
    var $rdate;
    var $rtime;
    var $cover;
    var $company;
    var $hotelguest;
    var $state;
    var $canceldate;
    var $revenue;
    var $specialrequest;
    var $mealtype;
    var $wheelsegment;
    var $wheelwin;
    var $wheeldesc;
    var $wheelwinangle;
    var $country;
    var $language;
    var $tracking;
    var $booker;
    var $ip;
    var $browser;
    var $msg;
    var $mailer;
    var $restaurantinfo;
    var $reviewgrade;
    var $bookings;
    var $result;

    function __construct() {
        $this->restaurant = "";
    }

    function error_msg($val, $msg) {

        error_log("SET BOOKING MSG " . $msg);

        $this->msg = $msg;
        return $this->result = $val;
    }

    function createBooking($theRestaurant, $rdate, $rtime, $email, $mobile, $cover, $salutation, $firstname, $lastname, $country, $language, $specialrequest, $type, $tracking, $booker, $company = "", $hotelguest = 0) {

        if ($this->email_validation($email) < 0) {
            $this->error_msg(-1, $this->msg = "Invalid email " . $email);
        }

        $res = new WY_restaurant;
        $res->CheckAvailability($theRestaurant, $rdate, $rtime, $cover, '');
        if ($res->result <= 0) {
            $debug = new WY_debug;
            $debug->writeDebug("ERROR", "BOOKING", "No more availability. Restaurant =  " . $theRestaurant . ", date = " . $rdate . ", time = " . $rtime . ", covers = " . $cover);
            $this->msg = $res->msg;
            $this->result = -1;
            return -1;
        }

        date_default_timezone_set("Asia/Singapore");
        $cdateday = date("Y-m-d");

        // check that rdate >= cdate
        if (w_checkdate($cdateday, $rdate) < 0) {
            $debug = new WY_debug;
            $debug->writeDebug("ERROR", "BOOKING", "wrong date. Createdate =  " . $cdateday . " . Booking date = " . $rdate);
            return -1;
        }

        $browser = new Browser();
        $this->browser = "name=" . $browser->getBrowser() . "|version=" . $browser->getVersion() . "|platform=" . $browser->getPlatform() . "|language=" . substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);
        $this->ip = ip2long($_SERVER['REMOTE_ADDR']);

        $this->restaurant = $theRestaurant;
        $this->cdate = $cdateday . ' ' . date("H:i");
        $this->type = $type;
        $this->rdate = $rdate;
        $this->rtime = $rtime;
        $this->email = $email;
        $this->mobile = $mobile;
        $this->cover = $cover;
        $this->company = $company;
        $this->salutation = $salutation;
        $this->firstname = $firstname;
        $this->lastname = $lastname;
        $this->country = $country;
        $this->language = $language;
        $this->tracking = $tracking;
        $this->booker = $booker;
        $this->specialrequest = $specialrequest;
        $this->hotelguest = $hotelguest;


        $avgpAr = explode(",", $res->pricing);
        if (count($avgpAr) < 0)
            $avgpAr[0] = 100;
        if (count($avgpAr) == 1)
            $avgpAr[1] = $avgpAr[0];
        $this->revenue = ((intval($avgpAr[0]) + intval($avgpAr[1])) / 2) * $this->cover;

        list($this->confirmation, $this->restCode, $this->membCode) = $this->createUniqConfNumer();
        if (preg_match("/TheFunKitchen/", $this->restaurant))
            $this->membCode = "1122";
        $sql = "INSERT INTO booking (confirmation, type,restCode, membCode, restaurant, cdate, rdate, rtime, email, mobile, country, cover, company, hotelguest, salutation, lastname, firstname, mealtype, specialrequest, revenue, browser, language, tracking, booker, ip) VALUES ('$this->confirmation','$this->type', '$this->restCode', '$this->membCode', '$this->restaurant', '$this->cdate', '$this->rdate', '$this->rtime', '$this->email', '$this->mobile', '$this->country', '$this->cover', '$this->company', '$this->hotelguest', '$this->salutation', '$this->lastname', '$this->firstname', '$this->mealtype', '$this->specialrequest', '$this->revenue', '$this->browser', '$this->language', '$this->tracking', '$this->booker', '$this->ip')";
        pdo_exec($sql);

        // every booker become a member
        if ($email != "no@email.com" && $this->tracking != "CALLCENTER") {
            $login = new WY_Login(set_valid_login_type("member"));
            $firsttimeuser = $login->register_booking($email, $firstname, $lastname, $mobile, $country);
        }

        return 1;
    }

    function cancelBooking($confirmation) {

        $restaurant = $this->restaurant;

        if (!isset($restaurant)) {
            $this->msg = "Invalid Restaurant";
            return -1;
        }

        date_default_timezone_set("Asia/Singapore");
        $cdate = date("Y-m-d");
        $data = pdo_single_select("SELECT confirmation from booking WHERE rdate >= '$cdate' and restaurant = '$restaurant' and confirmation = '$confirmation' and wheelwin = '' limit 1");

        if (count($data) <= 0) {
            $this->msg = "Reservation date already passed or won some price.";
            return -1;
        }

        pdo_exec("update booking set status = 'cancel', canceldate=NOW() where restaurant = '$restaurant' and confirmation = '$confirmation' limit 1");
        $this->status = 'cancel';
        $this->notifyCancel();

        return 1;
    }

    function modifBooking($confirmation, $email, $cover, $rtime, $rdate) {

        if (!isset($confirmation)) {
            $this->msg = "Invalid confirmation";
            return -1;
        }

        if ($this->getBooking($confirmation) == false) {
            $this->msg = "Unable to find confirmation $confirmation";
            return -1;
        }

        if (!isset($this->restaurant)) {
            $this->msg = "Invalid Restaurant";
            return -1;
        }

        $restaurant = $this->restaurant;
        pdo_exec("update booking set cover='$cover', rtime='$rtime', rdate='$rdate' where restaurant = '$restaurant' and confirmation = '$confirmation' limit 1");
        pdo_insert("insert into booking_modif (restaurant, confirmation, rdate, rtime, cover) values ('$restaurant', '$confirmation', '$this->rdate', '$this->rtime', '$this->cover')");
        return 1;
    }

    function submitBookingWin($confirmation, $restCode, $membCode, $spinwin) {

        if ($this->getBooking($confirmation) == false)
            return $verify = "wrong_confirmation " . $confirmation;

        if ($this->wheelwin != "")
            return $verify = "wrong_spin";

        if ($restCode != $this->restCode || $membCode != $this->membCode)
            return $verify = "wrong_code";

        if ($spinwin == "")
            return $verify = "wrong_value";

        $resto = new WY_restaurant();
        $resto->getRestaurant($this->restaurant);

        //if (!preg_match("/\|$spinwin\|/", $resto->wheel))
        //    return $verify = "wrong_choice";
        // if (strpos($resto->wheel,$spinwin) === false) {
        //      return $verify = "wrong_choice";
        //  }


        error_log("DATA RESULT " . $spinwin);

        $this->storeBookingWin($spinwin, $wheeldesc);
        return "ok";
    }

    function submitBookingCompleteWin($confirmation, $restCode, $membCode, $spinsegment, $spinwin, $spindesc, $spinsource, $wheelversion = NULL, $angle = NULL) {

        if ($this->getBooking($confirmation) == false)
            return $verify = "wrong_confirmation " . $confirmation;

        if ($this->wheelwin != "")
            return $verify = "wrong_spin";

        if ($restCode != $this->restCode || $membCode != $this->membCode)
            return $verify = "wrong_code";

        if ($spinwin == "")
            return $verify = "wrong_value";

        $resto = new WY_restaurant;
        $resto->getRestaurant($this->restaurant);

        //if (!preg_match("/\|$spinwin\|/", $resto->wheel))
        //    return $verify = "wrong_choice";
        //if (strpos($resto->wheel, $spinwin) === false) {
        //  return $verify = "wrong_choice";
        //}

        error_log("DATA RESULT " . $spinwin);

        $this->storeBookingCompleteWin($spinsegment, $spinwin, $spindesc, $spinsource, $wheelversion, $angle);
        return "ok";
    }

    function verifyBookingWin($confirmation, $restCode, $membCode) {

        if ($this->getBooking($confirmation) == false)
            return $verify = "wrong_confirmation" . $confirmation;

        if ($this->wheelwin != "")
            return $verify = "wrong_spin";

        if ($restCode != $this->restCode || $membCode != $this->membCode)
            return $verify = "wrong_code";

        //CHECK COSTUMER/RESTAURANT-CODE HERE
        return $verify = "ok";
    }

    function storeBookingWin($wheelwin, $wheeldesc) {

        $restaurant = $this->restaurant;
        $confirmation = $this->confirmation;
        $this->wheelwin = $wheelwin;
        $this->wheeldesc = $wheeldesc;
        pdo_exec("update booking set wheelwin = '$wheelwin' where restaurant = '$restaurant' and confirmation = '$confirmation' limit 1");

        $this->notifyWin();
    }

    function storeBookingCompleteWin($wheelsegment, $wheelwin, $wheeldesc, $spinsource, $wheelversion = NULL, $angle = NULL) {

        $restaurant = $this->restaurant;
        $confirmation = $this->confirmation;
        $this->wheelwin = $wheelwin;
        $this->wheeldesc = $wheeldesc;

        pdo_exec("update booking set wheelversion = '$wheelversion', wheelsegment = '$wheelsegment', wheelwinangle = '$angle', wheelwin = '$wheelwin', wheeldesc = '$wheeldesc', spinsource = '$spinsource' where restaurant = '$restaurant' and confirmation = '$confirmation' limit 1");

        $this->notifyWin();
    }

    // assume that a getBooking was already done	
    function notifyBooking($qrcode_guest = NULL) {

        $mailer = new JM_Mail;
        $sms = new JM_Sms();
        $apn = new JM_Pushnotif();

        // Notification Member
        $rtime = preg_replace("/:00/", "h", $this->rtime);
        $date_sms = date("j/m/Y H:i", mktime(substr($this->rtime, 0, 2), substr($this->rtime, 3, 4), 0, intval(substr($this->rdate, 5, 2)), intval(substr($this->rdate, 8, 2)), intval(substr($this->rdate, 0, 4))));
        $rdate = date("F j, Y, g:i a", mktime(substr($this->rtime, 0, 2), substr($this->rtime, 3, 4), 0, intval(substr($this->rdate, 5, 2)), intval(substr($this->rdate, 8, 2)), intval(substr($this->rdate, 0, 4))));

        $rdate_date_only = date("F j, Y", mktime(substr($this->rtime, 0, 2), substr($this->rtime, 3, 4), 0, intval(substr($this->rdate, 5, 2)), intval(substr($this->rdate, 8, 2)), intval(substr($this->rdate, 0, 4))));
        $rdate_time_only = date("H:i", mktime(substr($this->rtime, 0, 2), substr($this->rtime, 3, 4), 0, intval(substr($this->rdate, 5, 2)), intval(substr($this->rdate, 8, 2)), intval(substr($this->rdate, 0, 4))));

        $data_object = $this;
        $data_object->rtime = $rtime;
        $data_object->rdate = $rdate;

        $data_object->rdate_time_only = $rdate_time_only;
        $data_object->rdate_date_only = $rdate_date_only;

        $header['from'] = array('reservation@weeloy.com' => $this->restaurantinfo->title);
        $header['replyto'] = array($this->restaurantinfo->email);
        if (isset($qrcode_guest)) {
            $header['guest_qrcode'] = $qrcode_guest;
        }

        $recipient = $this->email;
        $recipient_mobile = $this->mobile;
        $tracking = $this->tracking;

        if ($tracking == "CALLCENTER") {
            $header['white_label'] = true;
        }

        //$recipient = "philippe.benedetti@weeloy.com";
        $subject = "Reservation at " . $this->restaurantinfo->title . " on " . $rdate;

        if ($tracking == "WEBSITE") {
            $body = $mailer->getTemplate('notifylisting_member', $data_object);
            $sms_message = $this->restaurantinfo->title . "\n" . $date_sms . "\n" . "Confirmation: " . $this->confirmation . ", \n" . str_replace(" ", "", $this->restaurantinfo->tel) . ' ' . $this->restaurantinfo->address . " " . $this->restaurantinfo->zip . ", \n" . $this->restaurantinfo->city;
        } else if ($tracking == "CALLCENTER") {
            $body = $mailer->getTemplate('notify_callcenter', $data_object);
            $sms_message = $this->restaurantinfo->title . "\n" . $date_sms . "\n" . "Confirmation: " . $this->confirmation .  ", \n" . str_replace(" ", "", $this->restaurantinfo->tel) . ' ' . $this->restaurantinfo->address . " " . $this->restaurantinfo->zip . ", \n" . $this->restaurantinfo->city;
        } else if (!$this->restaurantinfo->is_bookable && $this->restaurantinfo->is_wheelable) {
            $body = $mailer->getTemplate('notifyrequest_member', $data_object);
            $sms_message = $this->restaurantinfo->title . "\n" . $date_sms . "\n" . "Reference: " . $this->confirmation . "\nWeeloy Code: " . $this->membCode . " (for rewards)" . ", \n" . str_replace(" ", "", $this->restaurantinfo->tel) . "\n" . $this->restaurantinfo->address . " " . $this->restaurantinfo->zip . ", \n" . $this->restaurantinfo->city;
        } else {
            if ($this->restaurantinfo->is_wheelable) {
                $body = $mailer->getTemplate('notifybooking_member', $data_object);
                $sms_message = $this->restaurantinfo->title . "\n" . $date_sms . "\n" . "Confirmation: " . $this->confirmation . "\nWeeloy Code: " . $this->membCode . " (for rewards)" . ", \n" . str_replace(" ", "", $this->restaurantinfo->tel) . "\n" . $this->restaurantinfo->address . " " . $this->restaurantinfo->zip . ", \n" . $this->restaurantinfo->city;
            } else {
                $body = $mailer->getTemplate('notifylisting_member', $data_object);
                $sms_message = $this->restaurantinfo->title . "\n" . $date_sms . "\n" . "Confirmation: " . $this->confirmation . ", \n" . str_replace(" ", "", $this->restaurantinfo->tel) . "\n" . $this->restaurantinfo->address . " " . $this->restaurantinfo->zip . ", \n" . $this->restaurantinfo->city;
            }
        }

        if (!empty($this->email) && $this->email != "no@email.com")
            $mailer->sendmail($recipient, $subject, $body, $header);



        $smsid = "Weeloy";
        if (preg_match("/CALLCENTER|WEBSITE/", $data_object->tracking) == true && !empty($data_object->restaurantinfo->smsid))
            $smsid = $data_object->restaurantinfo->smsid;
        $sms->sendSmsMessage($recipient_mobile, $sms_message, $smsid);
        //unset($header['guest_qrcode']);
        //unlink('tmp',$a->image(6));

        $recipient = $this->restaurantinfo->email;
        // $recipient = "support@weeloy.com";
        $subject = "Reservation at " . $this->restaurantinfo->title . " on " . $rdate;

        if ($tracking == "CALLCENTER") {
            $body = $mailer->getTemplate('notify_callcenter', $data_object);
        } else if (!$this->restaurantinfo->is_bookable && $this->restaurantinfo->is_wheelable) {
            $body = $mailer->getTemplate('notifyrequest_restaurant', $data_object);
        } else {
            if ($this->restaurantinfo->is_wheelable) {
                $body = $mailer->getTemplate('notifybooking_restaurant', $data_object);
            } else {
                $body = $mailer->getTemplate('notifylisting_restaurant', $data_object);
            }
        }
        $mailer->sendmail($recipient, $subject, $body, $header);


        $notif_message = "New booking: " . $this->restaurantinfo->title . "\n" . $this->cover . 'pp - ' . $date_sms;
        //get all device related to this account
        $sql = "SELECT md.mobile_id, md.user_id, md.mobile_type FROM restaurant_app_managers ram, mobile_devices md WHERE md.status = 'active' AND ram.user_id = md.user_id AND `restaurant_id` LIKE '" . $this->restaurant . "'";
        $data = pdo_multiple_select($sql);
        foreach ($data as $row) {
            $recipient = $row['mobile_id'];
            $pn_type = $row['mobile_type'];
            $apn->sendPushnotifMessage($recipient, $notif_message, $pn_type, null); // null for options ?
        }
    }

    function notifyBookingWalkable() {

        $mailer = new JM_Mail;
        $sms = new JM_Sms();
        // Notification Member

        $rtime = $this->rtime;
        $rdate = $this->rdate;
        //$date_sms = date("j/m/Y H:i", mktime(substr($this->time, 0, 2), substr($this->time, 3, 4), 0, intval(substr($this->date, 5, 2)), intval(substr($this->rdate, 8, 2)), intval(substr($this->rdate, 0, 4))));
        //$rdate = date("F j, Y, g:i a", mktime(substr($this->time, 0, 2), substr($this->time, 3, 4), 0, intval(substr($this->date, 5, 2)), intval(substr($this->rdate, 8, 2)), intval(substr($this->rdate, 0, 4))));

        $data_object = $this;
        $data_object->rtime = $rtime;
        $data_object->rdate = $rdate;

        $recipient = $this->email;
        $recipient_mobile = $this->mobile;
        $res = new WY_restaurant;
        $res->getRestaurant($this->restaurant);
        $header['from'] = array('reservation@weeloy.com' => $res->title);
        $header['replyto'] = array($res->email);

        $subject = "Wheel Prize at " . $res->title . " on " . $rdate;
        $body = $mailer->getTemplate('notifywin_walkin_member', $this);

        $mailer->sendmail($recipient, $subject, $body, $header);

        $sms_message = 'Congratulations, you won ' . $this->wheelwin . ' by turning the Weeloy Wheel! Thank you for choosing ' . $res->title;


        $smsid = "Weeloy";
        if (preg_match("/CALLCENTER|WEBSITE/", $data_object->tracking) == true && !empty($data_object->restaurantinfo->smsid))
            $smsid = $data_object->restaurantinfo->smsid;
        $sms->sendSmsMessage($recipient_mobile, $sms_message, $smsid);

        // Notification Restaurant

        $recipient = $res->email;
        $subject = "Wheel Prize at " . $res->title . " on " . $rdate;
        $body = $mailer->getTemplate('notifywin_walkin_restaurant', $data_object);

        $mailer->sendmail($recipient, $subject, $body, $header);
    }

    function notifyCancel() {

        $mailer = new JM_Mail;
        $sms = new JM_Sms();

        $rtime = preg_replace("/:00/", "h", $this->rtime);
        $date_sms = date("j/m/Y H:i", mktime(substr($this->rtime, 0, 2), substr($this->rtime, 3, 4), 0, intval(substr($this->rdate, 5, 2)), intval(substr($this->rdate, 8, 2)), intval(substr($this->rdate, 0, 4))));
        $rdate = date("F j, Y, g:i a", mktime(substr($this->rtime, 0, 2), substr($this->rtime, 3, 4), 0, intval(substr($this->rdate, 5, 2)), intval(substr($this->rdate, 8, 2)), intval(substr($this->rdate, 0, 4))));

        $data_object = $this;
        $data_object->rtime = $rtime;
        $data_object->rdate = $rdate;

        $recipient = $this->email;
        $recipient_mobile = $this->mobile;
        $header['from'] = array('reservation@weeloy.com' => $this->restaurantinfo->title);
        $header['replyto'] = array($this->restaurantinfo->email);

        $subject = "Your Reservation at " . $this->restaurantinfo->title . " on " . $rdate . " has been cancelled";

        $body = $mailer->getTemplate('cancelbooking_member', $this);
        $mailer->sendmail($recipient, $subject, $body, $header);

        $sms_message = 'Your booking at ' . $this->restaurantinfo->title . "\n on the " . $date_sms . "\n has been canceled";
        $smsid = "Weeloy";
        if (preg_match("/CALLCENTER|WEBSITE/", $data_object->tracking) == true && !empty($data_object->restaurantinfo->smsid))
            $smsid = $data_object->restaurantinfo->smsid;
        $sms->sendSmsMessage($recipient_mobile, $sms_message, $smsid);
        // Notification Restaurant

        $recipient = $this->restaurantinfo->email;
        $subject = "Cancellation Reservation at " . $this->restaurantinfo->title . " on " . $rdate;

        $body = $mailer->getTemplate('cancelbooking_restaurant', $data_object);
        $mailer->sendmail($recipient, $subject, $body, $header);
    }

    function notifyWin() {

        $mailer = new JM_Mail;
        $sms = new JM_Sms();
        // Notification Member
        $rtime = preg_replace("/:00/", "h", $this->rtime);
        $date_sms = date("j/m/Y H:i", mktime(substr($this->rtime, 0, 2), substr($this->rtime, 3, 4), 0, intval(substr($this->rdate, 5, 2)), intval(substr($this->rdate, 8, 2)), intval(substr($this->rdate, 0, 4))));
        $rdate = date("F j, Y, g:i a", mktime(substr($this->rtime, 0, 2), substr($this->rtime, 3, 4), 0, intval(substr($this->rdate, 5, 2)), intval(substr($this->rdate, 8, 2)), intval(substr($this->rdate, 0, 4))));

        $data_object = $this;
        $data_object->rtime = $rtime;
        $data_object->rdate = $rdate;

        $recipient = $this->email;
        $recipient_mobile = $this->mobile;
        $header['from'] = array('reservation@weeloy.com' => $this->restaurantinfo->title);
        $header['replyto'] = array($this->restaurantinfo->email);

        $subject = "Wheel Prize at " . $this->restaurantinfo->title . " on " . $rdate;
        $body = $mailer->getTemplate('notifywin_member', $this);

        $mailer->sendmail($recipient, $subject, $body, $header);

        $sms_message = 'Congratulations, you won ' . $this->wheelwin . ' by turning the Weeloy Wheel! Thank you for choosing ' . $this->restaurantinfo->title;

        $smsid = "Weeloy";
        if (preg_match("/CALLCENTER|WEBSITE/", $data_object->tracking) == true && !empty($data_object->restaurantinfo->smsid))
            $smsid = $data_object->restaurantinfo->smsid;
        $sms->sendSmsMessage($recipient_mobile, $sms_message, $smsid);

        // Notification Restaurant

        $recipient = $this->restaurantinfo->email;
        $subject = "Wheel Prize at " . $this->restaurantinfo->title . " on " . $rdate;
        $body = $mailer->getTemplate('notifywin_restaurant', $data_object);

        $mailer->sendmail($recipient, $subject, $body, $header);
    }

    function getBooking($conf) {

        $restaurant = new WY_restaurant();

        $data = pdo_single_select("SELECT booking.restaurant as brestaurant, confirmation, booking.status as bstatus, restCode, membCode, salutation, lastname, firstname, booking.mobile, booking.email as member_email, smsid, rdate, rtime, cdate, cover, company, hotelguest, booking.state as bkstate, canceldate, title, tel, wheelwin, tracking, booker, ip, restaurant.email as res_email, address, city, zip, logo, booking.country as res_country, map, GPS, restaurant_tnc, restaurant.status as res_status, is_wheelable, is_bookable from booking, restaurant WHERE restaurant.restaurant = booking.restaurant and confirmation = '$conf' LIMIT 1");

        if (count($data) <= 0)
            return false;

        $this->restaurant = $data['brestaurant'];
        $this->confirmation = $data['confirmation'];
        $this->status = $data['bstatus'];
        $this->restCode = $data['restCode'];
        $this->membCode = $data['membCode'];
        $this->salutation = $data['salutation'];
        $this->firstname = $data['firstname'];
        $this->lastname = $data['lastname'];
        $this->mobile = $data['mobile'];
        $this->email = $data['member_email'];
        $this->rdate = $data['rdate'];
        $this->cdate = $data['cdate'];
        $this->rtime = $data['rtime'];
        if ($this->rtime != "")
            $this->rtime = substr($this->rtime, 0, strlen($this->rtime) - 3);
        $this->cover = $data['cover'];
        $this->company = $data['company'];
        $this->state = $data['bkstate'];
        $this->canceldate = $data['canceldate'];
        $this->tracking = $data['tracking'];
        $this->booker = $data['booker'];
        $this->ip = $data['ip'];
        $this->wheelwin = $data['wheelwin'];
        $this->hotelguest = $data['hotelguest'];

        $this->restaurantinfo = new WY_restaurantConf;
        $this->restaurantinfo->title = $data['title'];
        $this->restaurantinfo->tel = $data['tel'];
        $this->restaurantinfo->email = $data['res_email'];
        $this->restaurantinfo->smsid = $data['smsid'];
        //$this->restaurantinfo->URLrestaurant = $data['url'];
        $this->restaurantinfo->address = $data['address'];
        $this->restaurantinfo->city = $data['city'];
        $this->restaurantinfo->zip = $data['zip'];
        $this->restaurantinfo->country = $data['res_country'];
        $this->restaurantinfo->logo = $data['logo'];
        $this->restaurantinfo->GPS = $data['GPS'];
        $this->restaurantinfo->map = $data['map'];
        $this->restaurantinfo->restaurant_tnc = $data['restaurant_tnc'];

        $this->restaurantinfo->status = $data['res_status'];
        $this->restaurantinfo->is_bookable = $data['is_bookable'];
        $this->restaurantinfo->is_wheelable = $data['is_wheelable'];

        $this->restaurantinfo->internal_path = $restaurant->getRestaurantInternalPath($data['brestaurant']);

        return true;
    }

    function duplicate($restaurant, $rdate, $email, $mobile) {

        $this->result = -1;
        $rdate = substr($rdate, 6, 4) . "-" . substr($rdate, 3, 2) . "-" . substr($rdate, 0, 2);
        error_log("DUP : " . "SELECT salutation, lastname, firstname, email, mobile, company, country from booking where restaurant = '$restaurant' and rdate = '$rdate' and (email = '$email' or mobile = '$mobile') limit 1");
        $data = pdo_single_select("SELECT salutation, lastname, firstname, email, mobile, company, country from booking where restaurant = '$restaurant' and rdate = '$rdate' and status = '' and (email = '$email' or mobile = '$mobile') limit 1");
        if (count($data) > 0)
            $this->result = 1;
    }

    function getBookingEmail($restaurant, $email) {

        return pdo_single_select("SELECT salutation, lastname, firstname, email, mobile, company, country, hotelguest from booking where restaurant = '$restaurant' and email = '$email' order by ID desc limit 1");
    }

    function getBookingPhone($restaurant, $phone) {

        return pdo_single_select("SELECT salutation, lastname, firstname, email, mobile, company, country, hotelguest from booking where restaurant = '$restaurant' and mobile = '$phone' order by ID desc limit 1");
    }

    function setBooking($restaurant, $confirmation, $value, $label) {

        if ($this->getBooking($confirmation) == false)
            return $this->error_msg(-1, "Confirmation " . $confirmation . " does not exits. Unable so set $label to $value.");

        if ($this->confirmation != $confirmation || $this->restaurant != $restaurant)
            return $this->error_msg(-1, "Invalid confirmation " . $confirmation . " for " . $this->restaurant . ". Unable so set $label to $value.");

        switch ($label) {
            case 'state':
                pdo_exec("update booking set $label='$value' where confirmation = '$confirmation' and restaurant = '$restaurant' limit 1");
                break;
        }

        $this->msg = "The $label '$value' for confirmation $confirmation for restaurant $restaurant has been updated";
        //error_log("SET BOOKING " . "update booking set $label='$value' where confirmation = '$confirmation' and restaurant = '$restaurant' limit 1");
        return $this->result = 1;
    }

    function getUserBookings($email, $options) {
        $restaurant = new WY_restaurant();
        $select = '';
        $where = '';
        $order = '';
        $limit = '';
        $join = '';


        ////  FILTER CREATION  ////
        if (isset($options['last'])) {
            $order .= ' rdate DESC, rtime DESC';
            $limit .= $options['last'];
        }
        if (isset($options['win_only'])) {
            $where .= " AND wheelwin NOT LIKE ''";
        }
        if (!empty($order)) {
            $order = ' ORDER BY ' . $order;
        }
        if (!empty($limit)) {
            $limit = ' LIMIT ' . $limit;
        }
        
        $now_date = date('Y-m-d');
        
        if (!empty($options['period'])) {
            switch ($options['period']) {
                case 'today':
                    $where .= " AND rdate = '$now_date' ";
                    break;
                case 'past':
                    $where .= " AND rdate < '$now_date' ";
                    break;
                default:
                    $where .= " AND rdate >= '$now_date' ";
                    break;
            }
        }

        //// END FILTER CREATION  ////
        //force review 
        $options['include_reviews'] = true;
        if (isset($options['include_reviews'])) {
            $select .= ', review.reviewgrade ';
            $join .= ' LEFT JOIN review ON  review.confirmation = booking.confirmation';
        }
        if (isset($options['expected_reviews'])) {
            $where .= " AND review.reviewgrade IS NULL ";
        }


        $sql = "SELECT booking.id,booking.confirmation,booking.status as bstatus, membCode, booking.restaurant, booking.email as member_email, lastname, firstname, rdate, rtime, cover, company, booking.state as bkstate, title, tel, wheelwin, tracking, booker, ip, hotelguest, restaurant.email, address, city, zip, booking.country, map, GPS, restaurant.images $select from  restaurant, booking $join WHERE 1 and restaurant.restaurant = booking.restaurant and booking.email = '$email' $where $order $limit";

        $data = pdo_multiple_select($sql);

        if (count($data) <= 0)
            return false;
        foreach ($data as $row) {
            $current = new WY_Booking();
            //kala added booking id
            $current->bookid = $row['id'];
            $current->confirmation = $row['confirmation'];
            $current->membcode = $row['membCode'];
            $current->status = $row['bstatus'];
            $current->firstname = $row['firstname'];
            $current->lastname = $row['lastname'];
            $current->email = $row['member_email'];
            $current->rdate = $row['rdate'];
            $current->rtime = $row['rtime'];
            $current->cover = $row['cover'];
            $current->company = $row['company'];
            $current->state = $row['bkstate'];
            $current->tracking = $row['tracking'];
            $current->booker = $row['booker'];
            $current->ip = $row['ip'];
            $current->wheelwin = $row['wheelwin'];
            $this->hotelguest = $row['hotelguest'];

            $current->reviewgrade = $row['reviewgrade'];

            $current->restaurantinfo = new WY_restaurantConf;
            
            $current->restaurantinfo->restaurant = $row['restaurant'];
            $current->restaurantinfo->title = $row['title'];
            $current->restaurantinfo->tel = $row['tel'];
            $current->restaurantinfo->email = $row['email'];
            $current->restaurantinfo->URLrestaurant = ""; //$data['url'];
            $current->restaurantinfo->address = $row['address'];
            $current->restaurantinfo->city = $row['city'];
            $current->restaurantinfo->zip = $row['zip'];
            $current->restaurantinfo->country = $row['country'];
            $current->restaurantinfo->GPS = $row['GPS'];

            $current->restaurantinfo->internal_path = $restaurant->getRestaurantInternalPath($row['restaurant']);

            //kala added this line for date interval from currentdate to booking date

            //$days = (strtotime($endDate) - strtotime($startDate)) / (60 * 60 * 24);


            $this->bookings[] = $current;
        }

        return true;
    }

    //kala created this function for load single booking based on booking id

    function getBookingId($bookId, $options) {
        require_once("lib/wpdo.inc.php");

        $restaurant = new WY_restaurant();
        $select = '';
        $where = '';
        $order = '';
        $limit = '';
        $join = '';
        $options['include_reviews'] = true;
        if (isset($options['include_reviews'])) {
            $select .= ', review.reviewgrade ';
            $join .= ' LEFT JOIN review ON  review.confirmation = booking.confirmation';
        }


        $sql = "SELECT booking.id,booking.confirmation,booking.status as bstatus, membCode, booking.restaurant, booking.email as member_email,mobile ,lastname, firstname, rdate, rtime, cover, company, booking.state as bkstate, title, tel, wheelwin, tracking, booker, ip, hotelguest, restaurant.email, address, city, zip, booking.country, map, GPS, restaurant.images $select from  restaurant, booking $join WHERE 1 and restaurant.restaurant = booking.restaurant and booking.id = '$bookId'";
        //$sql = "SELECT * from booking WHERE id = '$bookId'";

        $row = pdo_single_select($sql);
        $current = new WY_Booking();
        //kala added booking id
        $current->bookid = $row['id'];
        $current->confirmation = $row['confirmation'];
        $current->membcode = $row['membCode'];
        $current->status = $row['bstatus'];
        $current->firstname = $row['firstname'];
        $current->lastname = $row['lastname'];
        $current->email = $row['member_email'];
         $current->mobile =$row['mobile'];
        $current->rdate = $row['rdate'];
        $current->rtime = $row['rtime'];
        $current->cover = $row['cover'];
        $current->company = $row['company'];
        $current->state = $row['bkstate'];
        $current->tracking = $row['tracking'];
        $current->booker = $row['booker'];
        $current->ip = $row['ip'];
        $current->wheelwin = $row['wheelwin'];
        $this->hotelguest = $row['hotelguest'];

        $current->reviewgrade = $row['reviewgrade'];

        $current->restaurantinfo = new WY_restaurantConf;
        $current->restaurantinfo->restaurant = $row['restaurant'];
        $current->restaurantinfo->title = $row['title'];
        $current->restaurantinfo->tel = $row['tel'];
        $current->restaurantinfo->email = $row['email'];
        $current->restaurantinfo->URLrestaurant = ""; //$data['url'];
        $current->restaurantinfo->address = $row['address'];
        $current->restaurantinfo->city = $row['city'];
        $current->restaurantinfo->zip = $row['zip'];
        $current->restaurantinfo->country = $row['country'];
        $current->restaurantinfo->GPS = $row['GPS'];

        $current->restaurantinfo->internal_path = $restaurant->getRestaurantInternalPath($row['restaurant']);

        //echo $current->interval ;
        $this->bookings[] = $current;

        return $current;
    }

    //kala created this function for booking id convert to base64url encode
    function base64url_encode($data) {
        return rtrim(strtr(base64_encode($data), '+/', '-_'), '=');
    }

    function base64url_decode($data) {
        return base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT));
    }

    function getDateInterval($dateFrom, $dateNow = '') {
        $dateFrom = new DateTime($dateFrom);
        $dateTo = new DateTime($dateNow == '' ? 'now' : $dateNow);
        $interval = $dateFrom->diff($dateTo); //new DateTime('5 days ago') - 5 days ago date
        return $interval->d;
    }

    function getAReservation() {

        $theRestaurant = $this->restaurant;
        $theConfirmation = $this->confirmation;

        if (empty($theRestaurant) || empty($theConfirmation)) {
            $this->mesg = "Invalid Request, the name of the restaurant or confirmation is empty";
            return -1;
        }

        $this->email = "";
        $this->mobile = "";
        $this->cover = "";
        $this->company = "";
        $this->state = "";
        $this->lastname = "";
        $this->firstname = "";
        $this->rdate = "";
        $this->rtime = "";
        $this->wheelwin = "";
        $this->ip = "";
        $this->hotelguest = "";

        $data = pdo_single_select("SELECT confirmation, restaurant, rdate, cdate, rtime, email, mobile, cover, company, state, lastname, firstname, mealtype, wheelwin, tracking, booker, ip, hotelguest from booking WHERE restaurant = '$theRestaurant' and confirmation = '$theConfirmation' LIMIT 1");
        if (count($data) <= 0)
            return -1;

        $this->restaurant = $data['restaurant'];
        $this->confirmation = $data['confirmation'];
        $this->email = $data['email'];
        $this->mobile = $data['mobile'];
        $this->cover = $data['cover'];
        $this->company = $data['company'];
        $this->state = $data['state'];
        $this->lastname = $data['lastname'];
        $this->firstname = $data['firstname'];
        $this->rdate = $data['rdate'];
        $this->cdate = $data['cdate'];
        $this->rtime = $data['rtime'];
        $this->wheelwin = $data['wheelwin'];
        $this->tracking = $data['tracking'];
        $this->booker = $data['booker'];
        $this->ip = $data['ip'];
        $this->hotelguest = $data['hotelguest'];

        return 1;
    }

    function getRestoConfirmation($theRestaurant) {

        $this->bookings = array();

        $data = pdo_multiple_select("SELECT confirmation, restCode, restaurant, rdate, cdate, rtime, email, mobile, cover, company, hotelguest, state, lastname, firstname, mealtype, wheelwin, tracking, booker, ip from booking WHERE restaurant = '$theRestaurant' order by rdate, rtime ");
        if (count($data) <= 0)
            return -1;

        foreach ($data as $row) {
            $current = new WY_Booking();
            $current->restaurant = $row['restaurant'];
            $current->confirmation = $row['confirmation'];
            $current->restCode = $row['restCode'];
            $current->firstname = $row['firstname'];
            $current->lastname = $row['lastname'];
            $current->mobile = $row['mobile'];
            $current->email = $row['email'];
            $current->rdate = $row['rdate'];
            $current->cdate = $row['cdate'];
            $current->rtime = $row['rtime'];
            $current->cover = $row['cover'];
            $current->company = $row['company'];
            $current->hotelguest = $row['hotelguest'];
            $current->state = $row['state'];
            $current->tracking = $row['tracking'];
            $current->booker = $row['booker'];
            $current->ip = $row['ip'];
            $current->wheelwin = $row['wheelwin'];
            $this->bookings[] = $current;
        }
        return 1;
    }

    function getAllConfirmation() {

        $this->bookings = array();

        $data = pdo_multiple_select("SELECT confirmation, restaurant, rdate, cdate, rtime, email, mobile, cover, company, hotelguest, state, lastname, firstname, mealtype, ip from booking order by restaurant, rdate, rtime");
        if (count($data) <= 0)
            return -1;

        foreach ($data as $row) {
            $current = new WY_Booking();
            $current->restaurant = $row['restaurant'];
            $current->confirmation = $row['confirmation'];
            $current->restCode = $row['restCode'];
            $current->firstname = $row['firstname'];
            $current->lastname = $row['lastname'];
            $current->mobile = $row['mobile'];
            $current->email = $row['email'];
            $current->rdate = $row['rdate'];
            $current->cdate = $row['cdate'];
            $current->rtime = $row['rtime'];
            $current->cover = $row['cover'];
            $current->company = $row['company'];
            $current->hotelguest = $row['hotelguest'];
            $current->state = $row['state'];
            $current->ip = $row['ip'];
            $current->wheelwin = $row['wheelwin'];
            $this->bookings[] = $current;
        }
        return 1;
    }

    // only get 'active' booking, to calculate remaining allotement
    function getDateBooking($theRestaurant) {

        $objToday = new DateTime(date("Y-m-d"));
        $today = $objToday->format("Y-m-d");

        $data = pdo_multiple_select("SELECT confirmation, rdate, rtime, cover from booking WHERE restaurant = '$theRestaurant' and rdate >= '$today' and status = '' order by rdate, rtime");
        if (count($data) <= 0)
            return array();

        $kk = 0;
        foreach ($data as $row) {
            $objDateTime = new DateTime($row['rdate']);
            $ndays = $objToday->diff($objDateTime)->format('%a');
            $horaire = ((intval(substr($row['rtime'], 0, 2)) * 2) + ceil((intval(substr($row['rtime'], 3, 5)) / 30)));
            $mydata[$kk]['ndays'] = $ndays;
            $mydata[$kk]['confirmation'] = $row['confirmation'];
            $mydata[$kk]['date'] = $row['rdate'];
            $mydata[$kk]['rtime'] = $row['rtime'];
            $mydata[$kk]['cover'] = $row['cover'];
            $mydata[$kk]['time'] = $horaire;
            $kk++;
        }
        return $mydata;
    }

    // only get 'active' booking, to calculate remaining allotement
    function getThatDayBooking($theRestaurant, $rdate) {

        // current day of week. Mysql format => conversion d-m-Y to Y-m-d
        $ctime = strtotime($rdate);
        $thedate = date('Y-m-d', $ctime);

        $data = pdo_multiple_select("SELECT confirmation, rdate, rtime, cover from booking WHERE restaurant = '$theRestaurant' and rdate = '$thedate' and status = '' order by rtime");
        if (count($data) <= 0)
            return array();

        $kk = 0;
        foreach ($data as $row) {
            $horaire = ((intval(substr($row['rtime'], 0, 2)) * 2) + ceil((intval(substr($row['rtime'], 3, 5)) / 30)));
            $mydata[$kk]['ndays'] = 0;
            $mydata[$kk]['confirmation'] = $row['confirmation'];
            $mydata[$kk]['date'] = $row['rdate'];
            $mydata[$kk]['rtime'] = $row['rtime'];
            $mydata[$kk]['cover'] = $row['cover'];
            $mydata[$kk]['time'] = $horaire;
            $kk++;
        }
        return $mydata;
    }
    
    function getBookingByTracking($tracking){
        $sql = "SELECT ID, type, restaurant, status, lastname, cdate, rdate, rtime, wheelwin FROM booking WHERE tracking ='".$tracking."' ";
        return pdo_multiple_select_array($sql);
    }
    function getBookingBySource($tracking){
        switch ($tracking) {
            case 'gourmand':
                    $campaign = 'w-gourmand_restaurant_20150702_0018';
                break;
            case 'marie-france':
                    $campaign = '';
                break;
            default:
                    $campaign = $tracking;
                break;
        }
        $sql = "SELECT ID, type, restaurant, status, lastname, cdate, rdate, rtime, wheelwin FROM booking WHERE tracking ='".$campaign."' ";
        return pdo_multiple_select_array($sql);
    }
    public function getSpinResult($confirmation) {
        $data = pdo_single_select("SELECT wheelsegment, wheelwin, wheeldesc, wheelwinangle, spinsource from booking WHERE confirmation = '$confirmation' LIMIT 1");
        if (count($data) <= 0)
            return -1;

        $this->wheelsegment = $data['wheelsegment'];
        $this->wheelwin = $data['wheelwin'];
        $this->wheeldesc = $data['wheeldesc'];
        $this->wheelwinangle = $data['wheelwinangle'];
        $this->spinsource = $data['spinsource'];

        return $data;
    }

    function createUniqConfNumer() {

        // no l, O, I
        $keyMiniscule = array("a", "b", "c", "d", "e", "f", "g", "h", "j", "k", "m", "n", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z");
        $keyMajuscule = array("A", "B", "C", "D", "E", "F", "G", "H", "J", "K", "M", "N", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z");
        $keyNumber = array("1", "2", "3", "4", "5", "6", "7", "8", "9");
        $keyAr = array_merge($keyMiniscule, $keyMajuscule);
        $keyAr = array_merge($keyAr, $keyNumber);

        $limitMi = count($keyMiniscule) - 1;
        $limitMa = count($keyMajuscule) - 1;
        $limit = count($keyAr) - 1;

        for ($k = 10; $k > 0; $k--) {
            $Conf = substr($this->restaurant, 6, 5);
            $Conf .= substr($this->lastname, 0, 2);
            $Conf .= substr($this->firstname, 0, 2);
            for ($i = 0; $i < 5; $i++) {
                $Conf .= $keyAr[rand(0, $limit)];
            }
            if ($this->getAReservation() <= 0) {  // is this reservation unique ?
                $restCode = $keyMajuscule[rand(0, $limitMa)] . rand(1, 9) . $keyMiniscule[rand(0, $limitMi)] . rand(1, 9); // majuscule, chiffre, minuscule, chiffre
                $membCode = $keyMiniscule[rand(0, $limitMi)] . rand(1, 9) . $keyMajuscule[rand(0, $limitMa)] . rand(1, 9); // minuscule, chiffre, majuscule, chiffre
                if (strpos(strtolower($this->restaurant), 'fairmont') !== false) {
                    $membCode = '1122';
                }
                return array($Conf, $restCode, $membCode);
            }
        }
        return "0000";
    }

    function setConfirmation($confirmation) {
        $this->confirmation = $confirmation;
    }

    function saveTable($table) {
        $res = pdo_exec("UPDATE booking SET restable = '$table' WHERE confirmation = '$this->confirmation';");
        if ($res === 0) {
            $res = 1;
        }
        return $res;
    }

    private function email_validation($email) {
        if (empty($email)) {
            return -1;
        }

        if (!preg_match("/([\w\-]+\@[\w\-]+\.[\w\-]+)/", $email)) {
            return -1;
        }

        return 1;
    }

    function clean($str) {

        if ($str[0] == "|")
            $str = substr($str, 1);

        if ($str[strlen($str) - 1] == "|")
            $str = substr($str, 0, strlen($str) - 1);

        $str = preg_replace("/\|\|+/", "|", $str);

        return $str;
    }

}

?>
