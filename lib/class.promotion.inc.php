<?php

class WY_Promotion {

   var $dirname;
   var $globalshow;
   var $restaurant;
   var $name;
   var $description;
   var $value;
   var $tnc;
   var $start;
   var $end;
   var $is_default;
  
   
   var $timeline;
   var $promotionInfo;
   
   var $msg;
   

   function __construct($theRestaurant) {
   		$this->restaurant = $theRestaurant;
		$this->dirname = __UPLOADDIR__ . "$theRestaurant/";
		if(AWS){$this->globalshow = __S3HOST__.__S3DIR__;}else{$this->globalshow = __SHOWDIR__;}
   }

	function insert($promotionname, $promotionvalue = NULL, $promotiontnc = NULL) {

                if($promotionname == "")
			return array(-1, "Promotion name is invalid -$promotionname-");
		 
                $theRestaurant = $this->restaurant;
		$sql_check = "SELECT name FROM promotion where name = '$promotionname' and restaurant = '$theRestaurant'  limit 1";
		$sql_ins = "INSERT INTO  promotion (name, restaurant) VALUES ('$promotionname', '$theRestaurant')";

                
                $result = pdo_insert_unique($sql_check, $sql_ins);
                
                $this->save($promotionname);
                
		return ($result > 0) ? array(1, "Promotion $promotionname has been created") : array(-1, "Promotion $promotionname already exists. No new promotion created");
	}

	function save($promotionname) {

		$adderror = "";
	
		$is_default_promotion = pdo_single_select("SELECT COUNT(ID) as count FROM promotion WHERE restaurant = '$this->restaurant'");
		if($is_default_promotion['count'] == '1'){
			$is_default_promotion = 1;
		}else{
			$is_default_promotion = 0;
		}
		
		$theRestaurant = $this->restaurant;
                
		$value = (!isset($this->value)) ? 0 : $this->value;
		$tnc = ($this->tnc != NULL) ? 0 : $this->tnc;
		
		$offer = clean_input($_REQUEST['offer']);
		$startAr = explode("-", $_REQUEST['start']);
                
		$start = $_REQUEST['start'] ;
                
		$endAr = explode("-", $_REQUEST['end']);
                
		$end = $_REQUEST['end'];
		if($endAr[0] < $startAr[0] || ($endAr[0] == $startAr[0] && $endAr[1] < $startAr[1]) || ($endAr[0] == $startAr[0] && $endAr[1] == $startAr[1] && $endAr[2] < $startAr[2])) {
			$end = $start;
			$adderror = "Ending date starts before starting date. Resetting Ending Date";
			}
		$description = clean_input($_REQUEST['description']);
	
		$affected_rows = pdo_exec("Update promotion set offer='$offer', "
				. "start='$start', "
				. "end='$end', "
				. "description='$description', "
				. "value =$value, "
				. "tnc = '$tnc', "
				. "is_default = $is_default_promotion "
				. "where name='$promotionname' and restaurant = '$theRestaurant' limit 1");
		
                
		return array(1, "Promotion $promotionname has been updated. $adderror");
	}

	function delete($promotioname) {

		if($promotioname == "")
			return array(-1, "Promotion name is invalid -$promotioname-");
	
		$theRestaurant = $this->restaurant;
		$affected_rows = pdo_exec("DELETE FROM promotion WHERE name = '$promotioname' and restaurant = '$theRestaurant' LIMIT 1");
		return array(1, "Promotion $promotioname has been deleted");
	}

	function setDefault($promotioname) {
		if($promotioname == "")
			return array(-1, "Promotion name is invalid -$promotioname-");
	
	        $sql = "UPDATE promotion SET is_default = 1 WHERE restaurant = '$this->restaurant' AND name LIKE '$promotioname'  ";
                if(pdo_exec($sql)){
                    $sql = "UPDATE promotion SET is_default = 0 WHERE restaurant = '$this->restaurant' AND name NOT LIKE '$promotioname'  ";
                    if(pdo_exec($sql)){
                        return array(1, "$promotioname has been set as default");
                    }
                }
                return array(1, "Promotion $promotioname not found");
	}

        
	function readPromotionName() {

		$theRestaurant = $this->restaurant;
		return pdo_column("SELECT name FROM promotion where restaurant = '$theRestaurant' ORDER BY morder");
	}
	function getPromotionsList() {
		$theRestaurant = $this->restaurant;
		return pdo_multiple_select("SELECT ID, name, restaurant, offer, value, tnc, morder, description, start, end, is_default FROM promotion WHERE restaurant ='$theRestaurant' ORDER by morder");
	}

    public function getDefaultPromotionDetails(){
        
        $data = pdo_single_select("SELECT  offer,value, tnc, end FROM promotion WHERE restaurant = '$this->restaurant' AND is_default = 1");
        $res = 'NO PROMOTION AVAILABLE';
        
        if($data['offer'] != ''){
            return $data;
            
        }else{
            
            return $res;
        }
        
        return false;
    }        
        
    public function getActivePromotionDetails(){
        $data = pdo_single_select("SELECT  offer,value, tnc, end FROM promotion WHERE restaurant = '$this->restaurant' AND start <= '".date("Y-m-d")."' AND end >= '".date("Y-m-d")."' ORDER BY value desc LIMIT 1");

        if(isset($data['offer']) && $data['offer'] != ''){
            return $data;
        }else{
            return $this->getDefaultPromotionDetails();
        }
        return false;
    }


    function clean_input($str) {
    
    if($str == "")
    	return;

    $str = preg_replace("/\'\"/", "’", $str);
    
    return $str;
    }

}

?>
