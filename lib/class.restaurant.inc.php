<?php

define("TOO_SHORT", -1);
define("DOESNOT_EXISTS", -2);
define("ALREADY_EXISTS", -4);

define("NO_WALKING", 1);
define("SPONSOR_WHEEEL", 2);
define("OPEN24HOUR7BY7", 4);
define("PERPAXRESA", 8);			// booking per person. Standard is per table
define("CALLCENTEREXTRACTION", 16);			// booking per person. Standard is per table
define("FIVESLOTMEAL", 32);			// 5 slots meal, 150mn. if not set, normal slot time is set to 3 slots of 30mn -> 90mn
define("WITHPROMOTION", 64);			// 5 slots meal, 150mn. if not set, normal slot time is set to 3 slots of 30mn -> 90mn
define("USEMINFORMAX", 128);			// use the min field for max people

class WY_restaurant {

	var $ID;
	var $dirname;
	var $showdirname;
	var $restaurant;
	var $hotelname;
	var $title;
	var $logo;
	var $cuisine;
	var $description;
	var $address;
	var $address1;
	var $city;
	var $country;
	var $zip;
	var $tel;
	var $email;
	var $smsid;
	var $restaurant_tnc;
	var $url;
	var $map;
	var $region;
	var $pricing;
	var $currency;
	var $creditcard;
	var $pricerating;
	var $pricelunch;
	var $pricediner;
	var $rating;
	var $dinesfree;
	var $dfmealtype;
	var $dfminpers;
	var $award;
	var $GPS;
	var $status;
	var $is_displayed;
	var $is_wheelable;
	var $is_bookable;
	var $extraflag;
	var $wheel;
	var $wheelversion;
	var $wheelsponsor;
	var $chef;
	var $chef_award;
	var $chef_logo;
	var $chef_origin;
	var $chef_believe;
	var $chef_description;
	var $chef_type;
	var $likes;
    var $bo_name;
    var $bo_tel;
    var $bo_email;
    var $mgr_name;
    var $mgr_tel;
    var $mgr_email;
    var $POS;
	var $data;
	var $images;
	var $ReviewData;
	var $MenuObj;
	var $Timeline;
	var $where;
	var $glb_pricedesc;
	var $cn_row;
	var $msg;
	var $result;

    
	var $bestpromotion;

	var $active_promotion;

	var $distance;

        var $internal_path;
        
	function __construct() {
		$this->qry = array();
		$this->where = "";
		$this->glb_pricedesc = array("$" => 20, "$$" => 40, "$$$" => 70, "$$$$" => 100, "$$$$$" => 5000);

		if (AWS) {
			$this->globalshow = __S3HOST__ . __S3DIR__;
		} else {
			$this->globalshow = __SHOWDIR__;
		}
	}

	public function setPriceSegment($currency) {
		switch($currency) {
			default:
			case "USD":
			case "EUR":
			case "SGD":
			case "MYR":
				$this->glb_pricedesc = array("$" => 20, "$$" => 40, "$$$" => 70, "$$$$" => 100, "$$$$$" => 5000);
				return;
			case "HKD":
				$this->glb_pricedesc = array("$" => 200, "$$" => 400, "$$$" => 800, "$$$$" => 1500, "$$$$$" => 10000);
				return;
			case "THB":
				$this->glb_pricedesc = array("$" => 1000, "$$" => 2500, "$$$" => 5000, "$$$$" => 7000, "$$$$$" => 20000);
				return;
			}
		}
	function partialSave($data, $theRestaurant) {
		$Qry = "";
		reset($data);
		for ($sep = "";list($label, $val) = each($data); $sep = ", ") {
			$val = preg_replace("/\'|\"/", "’", $val);
			$label = substr($label, 4);
			$Qry .= $sep . $label . "='" . $val . "'";
		}
		$Sql = "Update restaurant set $Qry where restaurant = '$theRestaurant' limit 1";
		pdo_exec($Sql);
	}

	function saveOpeninghours($openhours, $theRestaurant) {
		$Sql = "Update restaurant set openhours='$openhours' where restaurant = '$theRestaurant' limit 1";
		pdo_exec($Sql);

	}
/*
 *  function getBookWheelRestaurant($country, $nb) {
        $resultat = array();

        $sql = "SELECT m.name as image, r.restaurant, title, cuisine, wheel, region, r.status, is_displayed, is_wheelable, is_bookable, extraflag, GPS, wheelvalue 
                    FROM restaurant r
                    LEFT JOIN media m ON m.restaurant = r.restaurant

                    WHERE is_displayed = '1' and is_wheelable = '1' and is_bookable = '1' and r.status != 'demo_reference' and city = '$country' and m.object_type='restaurant' AND m.media_type = 'picture' and m.status='active'
                    order by RAND() LIMIT $nb";

        $data = pdo_multiple_select($sql);
        if (count($data) <= 0)
            return array();

        foreach ($data as $row) {
            $res = array();
            $res['restaurant'] = $row['restaurant'];
            $res['title'] = $row['title'];
            $res['cuisine'] = $row['cuisine'];
            $res['region'] = $row['region'];
            $res['is_wheelable'] = $row['is_wheelable']; 
            $coord = explode(',', $row['GPS']);
            $res['lat'] = $coord[0];
            $res['lon'] = $coord[1];
            //$res['wheel'] = $row['wheel'];
            $res['best_offer'] = $this->getBestOffer($row['restaurant'], $row['wheel'], $row['is_wheelable'], 1);
            $media = new WY_Media($row['restaurant']);
            $res['wheelvalue'] = $media->getWheelSource($row['wheelvalue']);
            //$res['default_picture'] = $media->getFullPath(NULL, $row['image']);
            $res['internal_path'] = $this->getRestaurantInternalPath($row['restaurant']);
            $booktitle = "BOOK SOON";
            $custombutton = "custom_button_book_soon";
            if (($row['status'] == 'active' && $row['is_bookable']) || $row['status'] == 'demo_reference') {
                $booktitle = "BOOK NOW";
                $custombutton = "";
            }
            if ($row['is_bookable'] == false && $row['status'] == 'active') {
                $booktitle = "REQUEST NOW";
                $custombutton = "btn-green";
            }
            $res['book_button'] = $this->getBookButton($row['restaurant'], $row['title'], $booktitle, "", $custombutton);

            $resultat[] = $res;
        }
        return $resultat;
    }

 */
    function getBookWheelRestaurant($search_city = NULL, $nb_resto = 3) {
        $where = '';
        $limit = '';
        if(!empty($search_city)){
            $where .= " AND city = '$search_city' ";
        }
        
        $limit .= ' LIMIT 0,'.$nb_resto;
        
		$this->restaurant = array();
		$this->cn_row = 0;
        return pdo_multiple_select_array("SELECT * from (SELECT restaurant, title, status, is_displayed, is_wheelable, is_bookable, extraflag, wheelvalue, region from restaurant WHERE is_displayed = '1' and is_wheelable = '1' and is_bookable = '1' and status != 'demo_reference' $where ORDER BY RAND() DESC $limit ) as t order by wheelvalue DESC");
	}

function getShowcaseRestaurant($search_city = NULL, $nb_resto = 3) {
        $mediadata = new WY_Media();
        //$imgdata = new WY_Images();
        $res_showcase = $this->getBookWheelRestaurant($search_city, $nb_resto);
        foreach ($res_showcase as $resto){
            $this->getRestaurant($resto['restaurant']);
            $tmp = $resto ;
            $tmp['cuisine'] = $this->cuisine;
            $tmp['description'] = $this->restaurantDescriptionToArray($this->description);
            
            $tmp['best_offer'] = $this->getBestOffer($resto['restaurant'], $this->wheel, $this->is_wheelable, 1);
            
            $default_picture = $mediadata->getRandomPicture($resto['restaurant']);
            $tmp['image_path'] = $mediadata->getPath();
            $tmp['image'] = $mediadata->getName();

            $default_picture_tmp = explode('/', $default_picture);
            
            $review = new WY_Review($resto['restaurant']);
            $tmp['reviews'] = $review->getReviewsCount();
            
            $tmp['internal_path'] = $this->getRestaurantInternalPath();
            $tmp['pricing'] = $this->pricerating;
            
            $booktitle = "BOOK SOON";
            if (($this->status == 'active' && $this->is_bookable) || $this->status == 'demo_reference') {
                $booktitle = "BOOK NOW";
            }
            if ($this->is_bookable == false && $this->status == 'active') {
                $booktitle = "REQUEST NOW";
            }
            $tmp['book_button'] = array( 'label'=> $booktitle, 'style'=>'');

            $showcase[] = $tmp;
            
    }
    
    return $showcase;
	}
            
        
    function getGourmandRestaurants($country, $nb) {
		$resultat = array();
		
		$sql = "SELECT restaurant, title, cuisine, wheel, region, status, is_displayed, is_wheelable, is_bookable, extraflag, GPS from restaurant WHERE is_displayed = '1' "
				. "and is_wheelable = '1' and is_bookable = '1' "
				. "and status != 'demo_reference' "
				. "and country_iso_code = '$country' order by RAND() LIMIT $nb";

		$data = pdo_multiple_select($sql);
		if(count($data) <= 0) 
		return array();
	
		foreach ($data as $row) {
			$res = array();
			$res['restaurant'] = $row['restaurant'];
			$res['name'] = $row['title'];
			$res['cuisine'] = $row['cuisine'];
                        $res['region'] = $row['region'];
                        $coord = explode(',', $row['GPS']);
                        $res['lat'] = $coord[0];
                        $res['lon'] = $coord[1];
			//$res['wheel'] = $row['wheel'];
			$res['best_offer'] = $this->getBestOffer($row['restaurant'], $row['wheel'], $row['is_wheelable'], 1);
			$resultat[] = $res;
		}
		return $resultat;
	}

        
	function getRestaurant($theRestaurant) {
		$where = "WHERE restaurant = '" . $theRestaurant . "'";

		$this->ID = $this->restaurant = "";
		$this->result = -1;
		 
		$data = pdo_single_select("SELECT ID, restaurant, title, hotelname, cuisine, wheel, "
                        . "wheelversion, chef, chef_type, chef_award, chef_logo, chef_description, "
                        . "chef_origin, chef_believe, logo, url, description, "
                        . "address,address1, city, zip, country, tel, email, smsid, map, "
                        . "region, rating, creditcard, dinesfree, award, GPS, likes, bo_name, bo_tel, bo_email, mgr_name, mgr_tel,mgr_tel,mgr_email, POS, images, "
                        . "pricing, currency, rating, stars, openhours, restaurant_tnc, mealtype, "
                        . "dfmealtype, dfminpers, wheelvalue, wheelsponsor, status, is_displayed, "
                        . "is_wheelable, is_bookable, offer_type, extraflag from restaurant $where LIMIT 1");
		if (count($data) <= 0) {
			error_log("RESTAURANT " . $theRestaurant . " " . count($data));
			return array();
		}

		$this->result = 1;
		$this->ID = $data['ID'];
		$this->restaurant = $data['restaurant'];
		$this->title = $data['title'];
		$this->logo = $data['logo'];
		$this->cuisine = $data['cuisine'];
		$this->wheel = $data['wheel'];
		$this->wheelversion = $data['wheelversion'];
		$this->chef = $data['chef'];
		$this->chef_award = $data['chef_award'];
		$this->chef_logo = $data['chef_logo'];
		$this->chef_origin = $data['chef_origin'];
		$this->chef_believe = $data['chef_believe'];
		$this->chef_type = $data['chef_type'];
		$this->chef_description = $data['chef_description'];
		$this->description = $data['description'];
		$this->address = $data['address'];
		$this->address1 = $data['address1'];
		$this->city = $data['city'];
		$this->country = $data['country'];
		$this->zip = $data['zip'];
		$this->region = $data['region'];
		$this->tel = $data['tel'];
		$this->email = $data['email'];
		$this->smsid = $data['smsid'];
		$this->url = $data['url'];
		$this->map = $data['map'];
		$this->rating = $data['rating'];
		$this->creditcard = $data['creditcard'];
		$this->dinesfree = $data['dinesfree'];
		$this->dfmealtype = $data['dfmealtype'];
		$this->dfminpers = $data['dfminpers'];
		$this->GPS = $data['GPS'];
		$this->likes = $data['likes'];
		$this->award = $data['award'];
		$this->images = $data['images'];
		$this->pricing = $data['pricing'];
		$this->currency = $data['currency'];
		$this->setPriceSegment($this->currency);
		$this->pricerating = $this->PricingDollars($this->PriceMeal($data['pricing'], 3));
		$this->pricediner = (preg_match('/Dinner/', $data['mealtype'])) ? $this->PricingDollars($this->PriceMeal($data['pricing'], 1)) : "";
		$this->pricelunch = (preg_match('/Lunch/', $data['mealtype'])) ? $this->PricingDollars($this->PriceMeal($data['pricing'], 2)) : "";
		$this->rating = $data['rating'];
		$this->stars = $data['stars'];
		$this->status = $data['status'];
		$this->is_displayed = $data['is_displayed'];
		$this->is_wheelable = $data['is_wheelable'];
		$this->is_bookable = $data['is_bookable'];
		$this->extraflag = $data['extraflag'];
		$this->openhours = $data['openhours'];
		$this->restaurant_tnc = $data['restaurant_tnc'];
		$this->mealtype = $data['mealtype'];
		$this->wheelvalue = $data['wheelvalue'];
		$this->wheelsponsor = $data['wheelsponsor'];
		$this->bo_name = $data['bo_name'];
		$this->bo_tel = $data['bo_tel'];
		$this->bo_email = $data['bo_email'];
		$this->mgr_name = $data['mgr_name'];
		$this->mgr_tel = $data['mgr_tel'];
		$this->mgr_email = $data['mgr_email'];
		$this->POS = $data['POS'];
		$this->cn_row = 1;

		$this->dirname = __UPLOADDIR__ . "$theRestaurant/";
		$this->showdirname = __SHOWDIR__ . "$theRestaurant/";
		$this->data = $data;

                $this->internal_path = $this->getRestaurantInternalPath();
                
                //kala added this line  for international path
                
                 $data['internal_path'] =$this->internal_path;
         
		return $data;
	}

	function rename($oldname, $newname) {

		$newname = preg_replace("/[^a-zA-Z0-9_]/", "", $newname);
		$oldname = preg_replace("/[^a-zA-Z0-9_]/", "", $oldname);

		if(strlen($newname) < 8 || strlen($oldname) < 8) {
			$this->msg = "empty names or too short";
			return -1;
		}

		$data = pdo_single_select("SELECT ID from restaurant where restaurant = '$oldname' limit 1");
		if(count($data) <= 0) {
			$this->msg = "the restaurant '$oldname' does not exist";
			return DOESNOT_EXISTS;
		}

		$data = pdo_single_select("SELECT ID from restaurant where restaurant = '$newname' limit 1");
		if(count($data) > 0) {
			$this->msg = "the restaurant '$newname' already exists";
			return ALREADY_EXISTS;
		}

		$tables = list_table_names();
		foreach($tables as $tb) {

			$data = list_table_field($tb);
			if(in_array("restaurant", $data)) 
                pdo_exec("Update $tb set restaurant='$newname' where restaurant = '$oldname'");
			else if(in_array("restaurant_id", $data))
                pdo_exec("Update $tb set restaurant_id='$newname' where restaurant_id = '$oldname'");
			}

		// rename directory image ******
		$images = new WY_Images($oldname);
		$images->l_copy_restaurant_directory($oldname, $newname);
		//delete
		$images->l_delete_restaurant_directory($oldname);
		return 1;
	}

	function delete($name) {

		$name = preg_replace("/[^a-zA-Z0-9_]/", "", $name);

		if(strlen($name) < 8) {
			$this->msg = "empty names or too short";
			return -1;
		}

		$data = pdo_single_select("SELECT ID from restaurant where restaurant = '$name' limit 1");
		if(count($data) <= 0) {
			$this->msg = "the restaurant '$oldname' does not exist";
			return DOESNOT_EXISTS;
		}

		$tables = list_table_names();
		foreach($tables as $tb) {

			$data = list_table_field($tb);
			if(in_array("restaurant", $data))
                pdo_exec("delete from $tb where restaurant = '$name'");
			else if(in_array("restaurant_id", $data))
                pdo_exec("delete from $tb where restaurant_id = '$name'");

			}

		// delete directory images ******
		$images = new WY_Images($name);
		$images->l_delete_restaurant_directory($name);

		return 1;
	}

	function duplicate($name, $newname) {

		$newname = preg_replace("/[^a-zA-Z0-9_]/", "", $newname);
		$name = preg_replace("/[^a-zA-Z0-9_]/", "", $name);

		if(strlen($newname) < 8 || strlen($name) < 8) {
			$this->msg = "empty names or too short";
			return -1;
		}

		$data = pdo_single_select("SELECT ID from restaurant where restaurant = '$name' limit 1");
		if(count($data) <= 0) {
			$this->msg = "the restaurant '$oldname' does not exist";
			return DOESNOT_EXISTS;
		}

		
		$data = pdo_single_select("SELECT ID from restaurant where restaurant = '$newname' limit 1");
		if(count($data) > 0) {
			$this->msg = "the restaurant '$newname' already exists";
			return ALREADY_EXISTS;
		}

		$tables = list_table_names();
		foreach($tables as $tb) {

			$data = list_table_field($tb);
			if(in_array("restaurant", $data)) {
				$variable = preg_replace("/ID,|restaurant,/", "", implode(",", $data));
                pdo_exec("insert into $tb (restaurant, $variable) select '$newname', $variable from $tb where restaurant = '$name'");
			}
			else if(in_array("restaurant_id", $data)) {
				$variable = preg_replace("/ID,|restaurant_id,/", "", implode(",", $data));
                pdo_exec("insert into $tb (restaurant_id, $variable) select '$newname', $variable from $tb where restaurant_id = '$name'");
		}
			}

		// duplicate directory images ******
		$images = new WY_Images($oldname);
		$images->l_copy_restaurant_directory($name, $newname);

		return 1;
	}

	function reactivate($theRestaurant) {

		return pdo_exec("UPDATE restaurant SET status = '' where restaurant = '$theRestaurant' limit 1");
	}

	function deactivate($theRestaurant) {

		return pdo_exec("UPDATE restaurant SET status = 'deleted' where restaurant = '$theRestaurant' limit 1");
	}

	function getBestOffer($res = NULL, $wheel = NULL, $is_wheelable = NULL, $nb_offers = 1) {
            
                if(empty($wheel)){ $res = $this->restaurant; $wheel = $this->wheel; $is_wheelable = $this->is_wheelable;}
                
                if(!$is_wheelable){
                    return array('offer'=> $this->getActivePromotion($res), 'score'=> 0);
                }
            
		$best_offers = array();
                $best_score = 1;
                $wheel_ar = join("','",explode('|', $wheel));
                $sql = "SELECT offer, value as score FROM wheeloffer WHERE  offer IN ('$wheel_ar') AND (restaurant = '$res' OR restaurant = 'GLOBAL') ORDER BY value DESC LIMIT $nb_offers";
                return pdo_single_select_array($sql);
	}

        
	function getWheelButton($theRestaurant, $title, $imgdata, $width, $force_reload, $whtoken) {
		$dd = rand(100, 333);
		$dd = ($dd * 3) + 1;if ($dd % 2 != 0) {
			$dd += 2;
		}
		// 3 chars => not dividable by 2 nor 3
		$shwwhl = rand(10000, 99999) . $dd . rand(0, 9);
		$WheelImg = "<img src='" . $imgdata->getWheelSource($this->wheelvalue) . $force_reload . "' $width alt='$title'  />";
		return "<a rel='nofollow' href='wheel/rotation/rotation.php?restaurant=$theRestaurant&shwwhl=$shwwhl&whtoken=$whtoken' target='_blank'>$WheelImg</a>";
	}

    function getWheelButtonLink($theRestaurant) {   
    	$dd = rand(100, 333); $dd = ($dd * 3) + 1; if($dd % 2 != 0) $dd +=2;  // 3 chars => not dividable by 2 nor 3                                                                                                                                
    	$shwwhl = rand(10000,99999) . $dd . rand(0,9);

        return "index.php?page=wheel_details&restaurantname=$theRestaurant&shwwhl=$shwwhl";
    }
    function getBookButton($theRestaurant, $title, $label, $style='', $class = '', $path_adaptor = '') {

        switch ($class){
			case '':
				$res = "<button type='button' id='btn_book' class='btn custom_button' $style onclick=\"WindowOpen('" . $path_adaptor . "modules/booking/book_form.php?bkrestaurant=" . $theRestaurant . "&bktitle=" . $title . "');\">$label</button>";
				break;

			case 'btn-green':
				$res = "<button type='button' id='btn_book' class='btn btn-green' $style onclick=\"WindowOpen('" . $path_adaptor . "modules/booking/book_form.php?action=request&bkrestaurant=" . $theRestaurant . "&bktitle=" . $title . "');\">$label</button>";
				break;

			case 'not-visible':
				$res = "<button style=' visibility: hidden;' type='button' id='btn_book' class='btn btn-green' $style \">$label</button>";

				break;
			default:
				$res = "<button type='button' id='btn_book' class='btn $class' $style >$label</button>";
				break;
		}
		return $res;

		/*	$mobiletype = (isset($_SESSION['mobiletype']) && $_SESSION['mobiletype']);

	if($class != ""){
	$bk = "<button type='button' id='btn_book' class='btn $class' $style onclick='');\">$label</button>";
	}else{
	if(!$mobiletype) $bk = "<button type='button' id='btn_book' class='btn custom_button' $style onclick=\"WindowOpen('modules/booking/book_form.php?bkrestaurant=" . $theRestaurant . "&bktitle=" . $title . "');\">$label</button>";
	else $bk = "<a href='modules/booking/book_form.php?bkrestaurant=" . $theRestaurant . "&bktitle=" . $title . "' target='_blank' id='btn_book' class='btn custom_button' $style>$label.</a>";
	}
	return $bk;

	 */
	}

    function getBookButtonActionLink($theRestaurant, $title, $path_adaptor = '') {

        switch ($class){
            case '':
                $res = $path_adaptor . "modules/booking/book_form.php?bkrestaurant=" . $theRestaurant . "&bktitle=" . $title;    
                break;
            
           case 'btn-green':
                $res = $path_adaptor . "modules/booking/book_form.php?action=request&bkrestaurant=" . $theRestaurant . "&bktitle=" . $title;
                break;
            
            case 'not-visible':
                $res = "";    

                break;
            default:
                $res = "";
                break;
        }    
        return $res;
    }
    
    

	function noWalking() {
		return $this->extraflag & NO_WALKING;
	}

	function sponsor() {
		return $this->extraflag & SPONSOR_WHEEEL;
	}
	
	function open24hour7() {
		return $this->extraflag & OPEN24HOUR7BY7;
	}
	
	function localTimeBooking() {
		return $this->extraflag & LOCALTIMERESA;
	}
	
	function perPaxBooking() {
		return $this->extraflag & PERPAXRESA;
	}
	
	function fiveSlotMeal() {
		return $this->extraflag & FIVESLOTMEAL;
	}

	function withPromotion() {
		return $this->extraflag & WITHPROMOTION;
	}	
	
	function CCExtractionFormat() {
		return $this->extraflag & CALLCENTEREXTRACTION;
	}
	
	function minforMax() {
		return $this->extraflag & USEMINFORMAX;
	}
		
	function isDemo($theRestaurant) {
		if ($this->restaurant != $theRestaurant) {
			$this->getRestaurant($theRestaurant);
		}

		if ($this->restaurant != $theRestaurant) {
			$this->msg = "invalid restaurant " . $theRestaurant;
			$this->result = -1;
			return "";
		}

		return ($this->status == "demo");
	}

	function OpenToday($theRestaurant) {

		if ($this->restaurant != $theRestaurant) {
			$this->getRestaurant($theRestaurant);
		}

		if ($this->restaurant != $theRestaurant) {
			$this->msg = "invalid restaurant " . $theRestaurant;
			$this->result = -1;
			return "";
		}

		$ohvalue = $this->openhours;
		$allote = new WY_Allote($theRestaurant);
		$allote->mealduration = 3;
		$allote->perpax = 0;		
		if($this->perPaxBooking()) $allote->perpax = 1;
		if($this->fiveSlotMeal()) $allote->mealduration = 5;
		
		return $allote->ComputeOpenToday($this->openhours);
	}

	function reportAvailability($theRestaurant, $rdate) {
		if ($this->restaurant != $theRestaurant) {
			$this->getRestaurant($theRestaurant);
		}

		if ($this->restaurant != $theRestaurant) {
			$this->msg = "invalid restaurant " . $theRestaurant;
			$this->result = -1;
			return "";
		}

		$ohvalue = $this->openhours;
		$allote = new WY_Allote($theRestaurant);
		$allote->mealduration = 3;
		$allote->perpax = 0;		
		if($this->perPaxBooking()) $allote->perpax = 1;
		if($this->fiveSlotMeal()) $allote->mealduration = 5;

		$data = $allote->getAlloteReportSlot($ohvalue, $rdate);
		$this->result = $allote->result;
		return $data;
		}

	function CheckAvailability($theRestaurant, $rdate, $rtime, $rpers, $booking) {

		if ($this->restaurant != $theRestaurant) {
			$this->getRestaurant($theRestaurant);
		}

		if ($this->restaurant != $theRestaurant) {
			$this->msg = "invalid restaurant " . $theRestaurant;
			$this->result = -1;
			return "";
		}

		$ohvalue = $this->openhours;
		$allote = new WY_Allote($theRestaurant);
		$allote->mealduration = 3;
		$allote->perpax = 0;		
		if($this->perPaxBooking()) $allote->perpax = 1;
		if($this->fiveSlotMeal()) $allote->mealduration = 5;

		$ret = $allote->getAllote1Slot($ohvalue, $rdate, $rtime, $rpers, $booking);
		$this->result = $allote->result;
		return $ret;
	}


	// return templete to use as a popover
    function getOpeningHoursListing($theRestaurant, $no_html = false) {

     	if($this->restaurant != $theRestaurant)
			$this->getRestaurant($theRestaurant);

    	if($this->restaurant != $theRestaurant) {
			$this->msg = "invalid restaurant " . $theRestaurant;
			$this->result = -1;
			return "";
			}

        if($this->open24hour7()){
            return array(array('day'=>'', 'lunch'=>'Open Daily  ', 'dinner'=>'24h'));
        }
 
		$allote = new WY_Allote($theRestaurant);
		$allote->mealduration = 3;
		$allote->perpax = 0;		
		if($this->perPaxBooking()) $allote->perpax = 1;
		if($this->fiveSlotMeal()) $allote->mealduration = 5;

 		return $allote->templateAlloteOpenHour($this->openhours, $no_html);
	}

	// return all open available 1/2 hour slice data for lunch and for dinner
	function getAlloteOpenHours($theRestaurant) {

		if ($this->restaurant != $theRestaurant) {
			$this->getRestaurant($theRestaurant);
		}

		if ($this->restaurant != $theRestaurant) {
			$this->msg = "invalid restaurant " . $theRestaurant;
			$this->result = -1;
			return "";
		}

		$allote = new WY_Allote($theRestaurant);
		$allote->mealduration = 3;
		$allote->perpax = 0;		
		if($this->perPaxBooking()) $allote->perpax = 1;
		if($this->fiveSlotMeal()) $allote->mealduration = 5;

		$DayOfYear = intval(date('z'));	// current day of year
		$maxdaybooking = 365 - $DayOfYear;	//

		return $allote->AlloteComputeOpenHour($this->openhours, $maxdaybooking, $this->noWalking());
	}

	function getPriceDescription($theRestaurant) {

		if ($this->restaurant != $theRestaurant) {
			$this->getRestaurant($theRestaurant);
		}

		if ($this->restaurant != $theRestaurant) {
			$this->msg = "invalid restaurant " . $theRestaurant;
			$this->result = -1;
			return "";
		}

		if (!empty($this->pricelunch) && preg_match("/Lunch/", $this->mealtype)) {
			$meals["Lunch"] = $this->pricediner;
		}

		if (!empty($this->pricediner) && preg_match("/Dinner/", $this->mealtype)) {
			$meals["Dinner"] = $this->pricediner;
		}

		$this->setPriceSegment($this->currency);
		$template = $sep = "";
		while (list($label, $segment) = each($meals)) {
			if (key_exists($segment, $this->glb_pricedesc)) {
				$value = $this->glb_pricedesc[$segment];
				if ($segment == "$") {
					$template .= $sep . $label . " is under $" . $value;
				} else if ($segment == "$$$$$") {
					$template .= $sep . $label . " is over $" . $this->glb_pricedesc[substr($segment, 1)];
				} else {
					$template .= $sep . $label . " is between $" . $this->glb_pricedesc[substr($segment, 1)] . " and $" . $value;
				}

				$sep = "<br/>";
			}
		}
		return $template;
	}

	function getBestwheel() {

		$this->getQueryrestaurant(NULL, "logo !='' and title !='' and wheelvalue > 70 order by wheelvalue DESC");
		$this->where = "";
	}

	function bestMustTry() {

		$this->getQueryrestaurant(NULL, "rating !='' and title !='' order by wheelvalue DESC");
		$this->where = "";
	}

	function topChef() {

		$this->getQueryrestaurant(NULL, "chef !='' and chef_award !='' and title !=''");
		$this->where = "";
	}

	function checkRestaurant($theRestaurant) {

		$sql = "SELECT ID, restaurant, title FROM restaurant where restaurant = '$theRestaurant' LIMIT 1";
		$data = pdo_single_select($sql);

		return (count($data) > 0);
	}

	function getListHotelRestaurant() {
		return pdo_column("SELECT distinct hotelname FROM restaurant where hotelname != '' ORDER by hotelname");
	}

	function getDefaultRestaurant($email, $membertype) {
	
		global $backoffice_listing_member_type_allowed;
		
		$where = 'WHERE 1';
		$join = '';

		if (in_array($membertype, $backoffice_listing_member_type_allowed)) {
			$join .= ' JOIN restaurants_managers mr ON mr.restaurant_id=h.restaurant ';
			$where .= " AND mr.member_id='" . $email . "' AND mr.status = 'active'";
		}

		$sql = "SELECT ID, restaurant, title FROM restaurant h $join $where LIMIT 1";

		$data = pdo_single_select($sql);
		$this->restaurant = $data['restaurant'];
		$this->title = $data['title'];
		$this->ID = $data['ID'];
	}

	function getSimpleListRestaurant() {

		$data = pdo_multiple_select("SELECT restaurant FROM restaurant order by restaurant");
		foreach ($data as $row) {
			$restaurant[] = $row['restaurant'];
		}

		return $restaurant;
	}

    function getNbListRestaurant($email, $membertype) {

		global $super_member_type_allowed;

		$this->clear();

		$where = 'WHERE 1';
		$join = '';

		if (!in_array($membertype, $super_member_type_allowed)) {
			$join .= ' JOIN restaurants_managers mr ON mr.restaurant_id=h.restaurant ';
            $where .= " AND mr.member_id='" . $email . "' AND mr.status = 'active'";
			}

        $sql = "SELECT count(*) as number FROM restaurant h $join $where ";
        $data = pdo_single_select($sql);

        if(isset($data['number']))
        	return intval($data['number']);
        return 0;
    	}

	function getCallCenterAccount($email) {
	
		$cls = new WY_Cluster;
		$cls->read("SLAVE", $email, "CALLCENTER", "", "", "");		// should only return 1 answer
		if($cls->result > 0) {
			$data = $cls->read("MASTER",  $cls->clustparent[0], "CALLCENTER", "", "", "");
			if($cls->result > 0)
				return $cls->clustcontent[0];	// this is an email and booker -> email=one@account.com;booker=name1|name2|name3...;
			}
		return ""; // bad a this point
		}
		        
    function getListAccountRestaurant($member_id) {
        $sql = "SELECT restaurant, title FROM restaurant, restaurants_managers where member_id = '$member_id' && restaurant = restaurant_id and restaurants_managers.status = 'active' order by restaurant_id";
		return pdo_multiple_select($sql);    
    	}
    		
    function getListRestaurant($email, $membertype, $orderBy="title", $restrictedCountry = NULL) {

		global $super_member_type_allowed;

		$this->clear();
		
		$where = 'WHERE 1';
		$join = '';

		if (!in_array($membertype, $super_member_type_allowed)) {
			$join .= ' JOIN restaurants_managers mr ON mr.restaurant_id=h.restaurant ';
            $where .= " AND mr.member_id='" . $email . "' AND mr.status = 'active'";
		}

                if(!empty($restrictedCountry)){
                   $where .= " AND h.country = '$restrictedCountry' " ;
                }
                
                
        $sql = "SELECT restaurant, title, city, country FROM restaurant h $join $where order by $orderBy";

		$data = pdo_multiple_select($sql);
		foreach ($data as $row) {
			$this->restaurant[] = $row['restaurant'];
			$this->title[] = $row['title'];
            $this->city[] = $row['city'];
            $this->country[] = $row['country'];
		}
	}

	function getListNotAssignedRestaurant($theMember) {
		$data = pdo_multiple_select("SELECT restaurant, title FROM restaurant h WHERE id
                        NOT IN (SELECT id
                        FROM restaurant h, restaurants_managers mr
                        WHERE h.RESTAURANT = mr.restaurant_id
                        AND mr.status = 'active' AND mr.member_id = '$theMember') order by title");
		foreach ($data as $row) {
			$this->restaurant[] = $row['restaurant'];
			$this->title[] = $row['title'];
		}
	}

	function getListAssignedRestaurant($theMember) {
		$data = pdo_multiple_select("SELECT restaurant, title
                        FROM restaurant h, restaurants_managers mr
                        WHERE h.RESTAURANT = mr.restaurant_id
                        AND mr.status = 'active' AND mr.member_id = '$theMember' order by title");
		foreach ($data as $row) {
			$this->restaurant[] = $row['restaurant'];
			$this->title[] = $row['title'];
		}
	}

	function getListNotAssignedAppManagerRestaurant($theMember) {
		$data = pdo_multiple_select("SELECT restaurant, title FROM restaurant h WHERE id
                        NOT IN (SELECT id
                        FROM restaurant h, restaurant_app_managers ram
                        WHERE h.RESTAURANT = ram.restaurant_id
                        AND ram.status = 'active' AND ram.user_id = '$theMember') order by title");
		foreach ($data as $row) {
			$this->restaurant[] = $row['restaurant'];
			$this->title[] = $row['title'];
		}
	}

	function getListAssignedAppManagerRestaurant($theMember) {
		$data = pdo_multiple_select("SELECT restaurant, title
                        FROM restaurant h, restaurant_app_managers ram
                        WHERE h.RESTAURANT = ram.restaurant_id
                        AND ram.status = 'active' AND ram.user_id = '$theMember' order by title");
		foreach ($data as $row) {
			$this->restaurant[] = $row['restaurant'];
			$this->title[] = $row['title'];
		}
	}

	function is_city($city) {

		if (empty($city)) {
			return false;
		}

		$res = false;

		foreach ($GLOBALS['citylist'] as $city_val) {
			if (strpos(strtolower($city_val), strtolower($city)) !== false) {
				return $city;
			}
		}
		return $res;
	}

    
    function getRestaurantByManager($manager){
        $data = pdo_multiple_select("SELECT r.restaurant, title, m.name as logo, is_wheelable, is_bookable, extraflag "
                . "FROM restaurant r, restaurant_app_managers ram, media m "
                . "WHERE 1 AND m.restaurant = r.restaurant 
                    AND ram.status = 'active'
                    AND m.object_type = 'logo'
                    AND m.media_type = 'picture' 
                    AND ram.restaurant_id = r.restaurant 
                    AND ram.user_id = '".$manager."' ");
		$res = array();
		foreach ($data as $row) {

            $is_walkable = '1';
            if($row['extraflag'] & NO_WALKING){
                $is_walkable = '0';
		}
            
            $res[] = array('restaurant'=>$row['restaurant'], 'title'=>$row['title'],'logo'=>$row['logo'], 'is_wheelable'=>$row['is_wheelable'], 'is_bookable'=>$row['is_bookable'], 'is_walkable' => $is_walkable);
        }
		return $res;
	}

        /////////    TMP FOR PAGING COUNT   /////

    function countQueryrestaurant($filters, $where, $return_array = false, $nb_items = NULL, $page = NULL) {

        $this->clear();

		//free search - default bar
		$this->where = $where;
		$sep = ($where != "") ? ' and ' : ' ';
		$limit = '';
		if (isset($filters['free_search'])) {

			$free_search_tmp = $filters['free_search'];
			$this->where .=  $sep . "(r.title like '%$free_search_tmp%' OR "
			. "r.description like '%$free_search_tmp%' OR "
                    . "r.address like '%$free_search_tmp%' OR "
                    //. "r.city like '%$free_search_tmp%' OR "
			. "r.cuisine like '%$free_search_tmp%' OR "
                    //. "r.dinesfree like '%$free_search_tmp%' OR "
			. "m.item_description like '%$free_search_tmp%') AND ";
		}

		//is search = city
        if (isset($_SESSION['user']['forced_city'])) {
            $city_tmp = $_SESSION['user']['forced_city'];
			$this->where .= $sep . "r.city = '$city_tmp'";
			$sep = " and ";
        }   else{
            if (isset($_SESSION['user']['search_city'])) {
						$city_tmp = $_SESSION['user']['search_city'];
						$this->where .= $sep . "r.city = '$city_tmp'";
						$sep = " and ";
					} else {
						$this->where .= $sep . "r.city = 'Singapore'";
						$sep = " and ";
					}
				}
        
        if (isset($_SESSION['user']['forced_country'])) {
            $country_tmp = $_SESSION['user']['forced_country'];
            $this->where .= $sep . "r.country_iso_code = '$country_tmp'";
             $sep = " and ";
        }   else{
            if (isset($_SESSION['user']['search_country'])) {
             $country_tmp = $_SESSION['user']['search_country'];
             $this->where .= $sep . "r.country_iso_code = '$country_tmp'";
             $sep = " and ";
             } else {
                 $this->where .= $sep . "r.country_iso_code = 'SG'";
                 $sep = " and ";
			}
		}

        

		if (isset($filters['cuisine']) && is_array($filters['cuisine'])) {
			$cuis = "";
			$spcuis = "";
			while (list($label, $val) = each($filters['cuisine'])) {
                if (trim($val) == "")
					continue;
				$cuis .= $spcuis . "r.cuisine like '%$val%'";
				$spcuis = " or ";
			}
			if ($cuis != "") {
				$this->where .= $sep . "( $cuis )";
				$sep = " and ";
			}
		}

		if ($filters['wheelselect'] >= 0 && !empty($GLOBALS['wheeloffers']) && count($GLOBALS['wheeloffers']) < $filters['wheelselect'] * 3) {
			$this->where .= "wheel like '%" . preg_replace("/%/", "_", $GLOBALS['wheeloffers'][($filters['wheelselect'] * 3)]) . "%'";
			$sep = " and ";
		}

        
		/////if the first search is empty we return all restaurants we have.
		// if ($no_filter) { $this->where  = $sep = '';  }

        
		if (in_array($_SESSION['user']['member_type'], array('admin', 'super_weeloy', 'weeloy_sales'))) {
            $this->where  .= $sep . "  r.status IN ('active', 'comingsoon', 'demo', 'demo_reference') AND is_displayed = 1 ";
			$sep = " and ";
		} else {
            $this->where  .= $sep . "  r.status IN ('active', 'comingsoon') AND is_displayed = 1 ";
			$sep = " and ";
		}


		$this->restaurant = $this->map = $this->title = "";

		$qry_clause = ($this->where != "") ? " LEFT JOIN menu m ON m.restaurant = r.restaurant where 1 AND " . $this->where . ' ORDER BY r.status ASC, wheelvalue DESC' : "";

        
                //PAGINATION 
                if(!empty($nb_items) && !empty($page)){
                    $limit = " LIMIT ". ($page-1)*$nb_items . ", $nb_items";
                }
 
		$data = pdo_single_select("SELECT count(distinct(r.ID)) as nb from restaurant r" . $qry_clause.$limit);
                return $data['nb'];

		//error_log("<br><br>QUERY CLAUSE = " . $this->cn_row . "=> " . $qry_clause);
	}

          /////////    TMP FOR PAGING    /////
        
        
        
	function find($request) {
		foreach($request as $label => $value) {
			$request[$label] = preg_replace("/(l|L)(\'|\")/", "", $request[$label]);
			$request[$label] = preg_replace("/\'|\"/", "", $request[$label]);
			}
                $no_result = false;
		$selectjoin_both = '';	
		$leftjoin_both = '';
		$where_both = " AND (r.status like  '%" . $request['status'] . "%'";
		if($request['status'] == 'both' ){
			$where_both = " AND ((r.status like 'active' OR r.status like 'comingsoon') ";
		}
		// for tester only
                
                
                
                if(!empty($_SERVER['PHP_AUTH_USER']) || !empty($_SESSION['user']['email'])) {
                    $emailAr = array('chris.danguien@weeloy.com', 'qatester7822@gmail.com', 'craig.fong@weeloy.com', 'philippe.benedetti@weeloy.com', 'gaolinch@hotmail.com', 'shawn.chen@weeloy.com', 'joel.lai@weeloy.com', 'charlotte.donahue@weeloy.com', 'soraya.kefs@weeloy.com','diana.wong@weeloy.com',  'nicolas.finck@weeloy.com', 'jerome.arbault@weeloy.com', 'richard@kefs.me', 'vs.kala@weeloy.com', 'mail.singhsarabjit@gmail.com');
                            $email = $_SERVER['PHP_AUTH_USER'];
                            if(in_array($email, $emailAr)) {
                                    $where_both .= " OR r.status like  '%reference%'";
                            }else{
                                if(!empty($_SESSION['user']['email'])){
                                $email = $_SESSION['user']['email'];
                                    if(in_array($email, $emailAr)) {
                                            $where_both .= " OR r.status like  '%reference%'";
                                    }
                                }
                            }
                }
		$where_both .= ')';
		$limit_both = '';
                
                $item_page = 48;
                if(!empty($request['i_p'])){
                    $item_page = 12;
                }
		//limit calculation
		if (!empty($request['p'])) {
			$start = ($request['p'] - 1) * $item_page;
			$limit_both = " LIMIT $start,$item_page";
		}
                
                $where_union1 = '';
                $join_union2 = '';
                $where_union2 = '';
                
                
		if (!empty($request['free_search'])) {
			$where_union1 .= " AND (r.title like '%" . $request['free_search'] . "%'";
                        $where_union1 .= " OR r.cuisine like '%" . $request['free_search'] . "%')";
                        $where_union2 .= " AND me.item_description like '%" . $request['free_search'] . "%'  AND me.restaurant = r.restaurant  ";
                        $join_union2 = ', menu me';
                        
		}
	
		if (isset($request['city']) && !empty($request['city'])) {
			$where_both .= " AND city like '%" . $request['city'] . "%'";
		}
               
                // Request pricing
                $low_limit = $high_limit = 0;
                        
                if (isset($request['pricing']) && !empty($request['pricing'])) {
                    $currency = $this->getCityCurrency($request['city']);
                    $this->setPriceSegment($currency);
                    $res_tmp = $this->glb_pricedesc;
                
                    switch($request['pricing']){
                        case '1':
                                $where_both .= " AND pricing < ". $res_tmp['$'];
                                //$high_limit = $res_tmp['$'];
                            break;
                        case '2':
                                $where_both .= " AND pricing >= ".$res_tmp['$']."  AND pricing < ".$res_tmp['$$'];
                            break;
                        case '3':
                                $where_both .= " AND pricing >= ".$res_tmp['$$']."  AND pricing < ".$res_tmp['$$$'];
                            break;
                        case '4':
                                $where_both .= " AND pricing >= ".$res_tmp['$$$']."  AND pricing < ".$res_tmp['$$$$'];
                            break;
                    }
		}

		if (isset($request['cuisine']) && !empty($request['cuisine'])) {

			$queryAr = explode("|", $request['cuisine']);
			$limit_cuisine = count($queryAr);
			if ($limit_cuisine < 1) {
				$this->msg = "food is empty";
				$this->result = -1;
				return;
			}

			for ($i = 0, $sep = " AND ("; $i < $limit_cuisine; $i++, $sep = " or ") {
				$where_both .= $sep . " cuisine like '%" . $queryAr[$i] . "%'";
			}
			$where_both.=')';
		}
	
		$where_both = preg_replace("/_/", " ", $where_both);  // for Kuala Lumpur
	
		if(!empty($request['is_favorite'])){
			$where_both.= " AND is_favorite = '" . $request['is_favorite'] . "'"; 
			}
	
		$selectjoin = " '0' as is_favorite ";

		if(!empty($email)) {
			$selectjoin_both = ", IF(is_favorite = '1', 1, 0) as is_favorite";
			$leftjoin_both = "  LEFT JOIN members_restaurants_favorite mrf  ON mrf.restaurant = r.restaurant AND mrf.member = '$email' ";
			}		
                        
                $sql_order_both = " ORDER BY status ASC, wheelvalue DESC, morder ASC $limit_both";
                
		$sql_union1 = "SELECT distinct(r.restaurant), r.ID, r.title, r.hotelname, r.cuisine, r.city, r.status, m2.name as logo, r.address, r.address1, r.map, r.region, r.rating, r.zip, r.country, r.GPS, r.likes, r.currency,r.pricing, r.mealtype, r.rating, r.openhours, r.wheel, IF(is_wheelable = '0',10,r.wheelvalue) as wheelvalue, "
				. "is_displayed, is_wheelable,is_bookable, r.extraflag, m.path as image_path, m.name as image, m.morder $selectjoin_both
						FROM restaurant r $leftjoin_both, media m , media m2

						WHERE 1 

						AND m.restaurant = r.restaurant 
						AND m.status = 'active'
						AND m.object_type = 'restaurant'
						AND m.media_type = 'picture'
					
						AND m2.restaurant = r.restaurant 
						AND m2.status = 'active'
						AND m2.object_type = 'logo'
						AND m2.media_type = 'picture'
					
						AND r.is_displayed = '1'
						" .$where_both. " 
                                                " .$where_union1. "
						GROUP BY r.id";


                                            $sql_union2 = " UNION SELECT distinct(r.restaurant), r.ID, r.title, r.hotelname, r.cuisine, r.city, r.status, m2.name as logo, r.address, r.address1, r.map, r.region, r.rating, r.zip, r.country, r.GPS, r.likes, r.currency,r.pricing, r.mealtype, r.rating, r.openhours, r.wheel, IF(is_wheelable = '0',10,r.wheelvalue) as wheelvalue, "
                                            . "is_displayed, is_wheelable,is_bookable, r.extraflag, m.path as image_path, m.name as image, m.morder $selectjoin_both
                                                FROM restaurant r $leftjoin_both, media m , media m2 $join_union2

						WHERE 1 

						AND m.restaurant = r.restaurant 
						AND m.status = 'active'
						AND m.object_type = 'restaurant'
						AND m.media_type = 'picture'
					
						AND m2.restaurant = r.restaurant 
						AND m2.status = 'active'
						AND m2.object_type = 'logo'
						AND m2.media_type = 'picture'
					
						AND r.is_displayed = '1'

						" .$where_both. " 
                                                " .$where_union2. "
                                                     
						GROUP BY r.id";

                            if (!empty($request['free_search'])) {
                                $sql = $sql_union1 . $sql_union2 . $sql_order_both;
                            }else{
                                $sql = $sql_union1 . $sql_order_both;
                            }
                        $sql_tmp = str_replace('me.item description', 'me.item_description', $sql);
		$binlog = true;
    	$restaurant = pdo_multiple_select_array($sql_tmp);
        
        if(count($restaurant) < 1 && !isset($request['second_try'])){
            unset($request['pricing']);
            unset($request['cuisine']);
            unset($request['free_search']);
            $request['second_try'] = true;
            $no_result = true;
            return $this->find($request);
        }
        
        
	$data = array();
        foreach($restaurant as $row){
            if(!empty($row['restaurant']) ){
                if(!empty($row['wheel']) && $row['is_wheelable']){
                    $row['best_offer'] = $this->getBestOffer($row['restaurant'], $row['wheel'],  $row['is_wheelable'], 1);
                }
                $this->setPriceSegment($row['currency']);
                $row['pricing'] = $this->PricingDollars($this->PriceMeal($row['pricing'], 3));
                $row['pricelunch'] = (preg_match('/Lunch/', $row['mealtype'])) ? $this->PricingDollars($this->PriceMeal($row['pricing'], 1)) : "";
                $row['pricediner'] = (preg_match('/Dinner/', $row['mealtype'])) ? $this->PricingDollars($this->PriceMeal($row['pricing'], 1)) : "";
		
                //$row['internal_path']'] = $this->getRestaurantInternalPath();
                $booktitle = "BOOK SOON";
                $custombutton = "custom_button_book_soon";
                
                if (($row['status'] == 'active' && $row['is_bookable']) || $row['status'] == 'demo_reference') {
                    $booktitle = "BOOK NOW";
                    $custombutton = "";
                	}
                if ($row['is_bookable'] == false && ($row['status'] == 'active' || $row['status'] == 'demo_reference') ) {
                    $booktitle = "REQUEST NOW";
                    $custombutton = "btn-green";
                	}
                $book_btn_tmp = array( 'label'=> ''.$booktitle, 'style'=> $custombutton);
                if(!isset($row['is_favorite'])){
                    $row['is_favorite'] = '0';
                }
                $row['bookbutton'] = $book_btn_tmp;
                
                $reviews = new WY_Review($row['restaurant']);
                $reviews_count = $reviews->getReviewsCount();
                if($reviews_count['count'] > 0){
                    $row['reviews'] = $reviews_count;
                }
                        
                // $row['reviews'] = array('count'=>'234','score'=>'4.54', 'score_desc'=>'excellent');
                $data[] = $row;
                
            	}
            }
            $this->result = 1;
            return array('restaurant'=>$data, 'no_result'=>isset($request['second_try']));
        }
        
	function getQueryrestaurant($filters, $where, $return_array = false, $nb_items, $page) {

        $this->clear();

		//free search - default bar
		$this->where = $where;
		$sep = ($where != "") ? ' and ' : ' ';
		$limit = '';
		if (isset($filters['free_search'])) {

			$free_search_tmp = $filters['free_search'];
			$this->where .=  $sep . "(r.title like '%$free_search_tmp%' OR "
			. "r.description like '%$free_search_tmp%' OR "
                    . "r.address like '%$free_search_tmp%' OR "
                    //. "r.city like '%$free_search_tmp%' OR "
			. "r.cuisine like '%$free_search_tmp%' OR "
                    //. "r.dinesfree like '%$free_search_tmp%' OR "
			. "m.item_description like '%$free_search_tmp%') AND ";
		}

		//is search = city
        if (isset($_SESSION['user']['forced_city'])) {
            $city_tmp = $_SESSION['user']['forced_city'];
			$this->where .= $sep . "r.city = '$city_tmp'";
			$sep = " and ";
        }   else{
            if (isset($_SESSION['user']['search_city'])) {
						$city_tmp = $_SESSION['user']['search_city'];
						$this->where .= $sep . "r.city = '$city_tmp'";
						$sep = " and ";
					} else {
						$this->where .= $sep . "r.city = 'Singapore'";
						$sep = " and ";
					}
				}
        
        if (isset($_SESSION['user']['forced_country'])) {
            $country_tmp = $_SESSION['user']['forced_country'];
            $this->where .= $sep . "r.country_iso_code = '$country_tmp'";
             $sep = " and ";
        }   else{
            if (isset($_SESSION['user']['search_country'])) {
             $country_tmp = $_SESSION['user']['search_country'];
             $this->where .= $sep . "r.country_iso_code = '$country_tmp'";
             $sep = " and ";
             } else {
                 $this->where .= $sep . "r.country_iso_code = 'SG'";
                 $sep = " and ";
			}
		}

        

		if (isset($filters['cuisine']) && is_array($filters['cuisine'])) {
			$cuis = "";
			$spcuis = "";
			while (list($label, $val) = each($filters['cuisine'])) {
                if (trim($val) == "")
					continue;
				$cuis .= $spcuis . "r.cuisine like '%$val%'";
				$spcuis = " or ";
			}
			if ($cuis != "") {
				$this->where .= $sep . "( $cuis )";
				$sep = " and ";
			}
		}

		if ($filters['wheelselect'] >= 0 && !empty($GLOBALS['wheeloffers']) && count($GLOBALS['wheeloffers']) < $filters['wheelselect'] * 3) {
			$this->where .= "wheel like '%" . preg_replace("/%/", "_", $GLOBALS['wheeloffers'][($filters['wheelselect'] * 3)]) . "%'";
			$sep = " and ";
		}

        
		/////if the first search is empty we return all restaurants we have.
		// if ($no_filter) { $this->where  = $sep = '';  }

        
		if (in_array($_SESSION['user']['member_type'], array('admin', 'super_weeloy', 'weeloy_sales'))) {
            $this->where  .= $sep . "  r.status IN ('active', 'comingsoon', 'demo', 'demo_reference') AND is_displayed = 1 ";
			$sep = " and ";
		} else {
            $this->where  .= $sep . "  r.status IN ('active', 'comingsoon') AND is_displayed = 1 ";
			$sep = " and ";
		}


		$this->restaurant = $this->map = $this->title = "";

		$qry_clause = ($this->where != "") ? " LEFT JOIN menu m ON m.restaurant = r.restaurant where 1 AND " . $this->where . ' ORDER BY r.status ASC, wheelvalue DESC' : "";

        
                //PAGINATION 
                if(!empty($nb_items) && !empty($page)){
                    $limit = " LIMIT ". ($page-1)*$nb_items . ", $nb_items";
                }
 
		$data = pdo_multiple_select("SELECT distinct(r.ID), r.restaurant, r.title, r.hotelname, r.cuisine, r.wheel, r.city, r.chef, r.chef_type, r.chef_award, r.chef_logo, r.chef_description, r.chef_origin, r.chef_believe, r.logo, r.url, r.description, r.address, r.address1, r.tel, r.email, r.smsid, r.map, r.region, r.rating, r.zip, r.country, r.creditcard, r.dinesfree, r.award, r.GPS, r.likes, r.images, r.pricing, r.currency, r.rating, r.stars, r.openhours, r.mealtype, r.dfmealtype, r.dfminpers, IF(is_wheelable = '0',10,r.wheelvalue) as wheelvalue, "
			. "is_displayed, is_wheelable,is_bookable, r.extraflag, r.status from restaurant r" . $qry_clause.$limit);


		$this->cn_row = count($data);
                
		foreach ($data as $row) {
			$this->ID[] = $row['ID'];
			$this->restaurant[] = $row['restaurant'];
			$this->title[] = $row['title'];
			$this->logo[] = $row['logo'];
			$this->cuisine[] = $row['cuisine'];
			$this->wheel[] = $row['wheel'];
			$this->city[] = $row['city'];
			$this->chef[] = $row['chef'];
			$this->chef_award[] = $row['chef_award'];
			$this->chef_logo[] = $row['chef_logo'];
			$this->chef_origin[] = $row['chef_origin'];
			$this->chef_believe[] = $row['chef_believe'];
			$this->chef_type[] = $row['chef_type'];
			$this->chef_description[] = $row['chef_description'];
			$this->description[] = $row['description'];
			$this->address[] = $row['address'];
			$this->address1[] = $row['address1'];
			$this->tel[] = $row['tel'];
			$this->email[] = $row['email'];
			$this->smsid[] = $row['smsid'];
			$this->url[] = $row['url'];
			$this->map[] = $row['map'];
			$this->region[] = $row['region'];
			$this->rating[] = $row['rating'];
			$this->zip[] = $row['zip'];
			$this->country[] = $row['country'];
			$this->creditcard[] = $row['creditcard'];
			$this->dinesfree[] = $row['dinesfree'];
			$this->dfmealtype[] = $row['dfmealtype'];
			$this->dfminpers[] = $row['dfminpers'];
			$this->GPS[] = $row['GPS'];
			$this->likes[] = $row['likes'];
			$this->award[] = $row['award'];
			$this->images[] = $row['images'];
			$this->pricing[] = $row['pricing'];
			$this->currency[] = $currency = $row['currency'];
			$this->setPriceSegment($currency);
			$this->pricerating[] = $this->PricingDollars($this->PriceMeal($row['pricing'], 3));
			$this->pricediner[] = (preg_match('/Dinner/', $row['mealtype'])) ? $this->PricingDollars($this->PriceMeal($row['pricing'], 1)) : "";
			$this->pricelunch[] = (preg_match('/Lunch/', $row['mealtype'])) ? $this->PricingDollars($this->PriceMeal($row['pricing'], 2)) : "";
			$this->rating[] = $row['rating'];
			$this->stars[] = $row['stars'];
			$this->openhours[] = $row['openhours'];
			$this->mealtype[] = $row['mealtype'];
			$this->wheelvalue[] = $row['wheelvalue'];
			$this->status[] = $row['status'];
			$this->is_displayed[] = $row['is_displayed'];
			$this->is_wheelable[] = $row['is_wheelable'];
			$this->is_bookable[] = $row['is_bookable'];
			$this->extraflag[] = $row['extraflag'];
            
                        $this->internal_path[] = $this->getRestaurantInternalPath($row['restaurant']);
                
			$this->dirname[] = __UPLOADDIR__ . $row['restaurant'];
			$this->showdirname[] = __SHOWDIR__ . $row['restaurant'];
		}
                if($return_array){
                    return $data;
                }
		//error_log("<br><br>QUERY CLAUSE = " . $this->cn_row . "=> " . $qry_clause);
	}

	function clear() {
		unset($this->ID);
		unset($this->dirname);
		unset($this->showdirname);
		unset($this->restaurant);
		unset($this->hotelname);
		unset($this->title);
		unset($this->logo);
		unset($this->cuisine);
		unset($this->description);
		unset($this->address);
		unset($this->address1);
		unset($this->city);
		unset($this->country);
		unset($this->zip);
		unset($this->tel);
		unset($this->email);
		unset($this->smsid);
		unset($this->restaurant_tnc);
		unset($this->url);
		unset($this->map);
		unset($this->region);
		unset($this->pricing);
		unset($this->currency);
		unset($this->creditcard);
		unset($this->pricerating);
		unset($this->pricelunch);
		unset($this->pricediner);
		unset($this->rating);
		unset($this->dinesfree);
		unset($this->dfmealtype);
		unset($this->dfminpers);
		unset($this->award);
		unset($this->GPS);
		unset($this->likes);
		unset($this->status);
		unset($this->is_displayed);
		unset($this->is_wheelable);
		unset($this->is_bookable);
		unset($this->extraflag);
		unset($this->wheel);
		unset($this->wheelversion);
		unset($this->chef);
		unset($this->chef_award);
		unset($this->chef_logo);
		unset($this->chef_origin);
		unset($this->chef_believe);
		unset($this->chef_description);
		unset($this->chef_type);
		unset($this->bo_name);
		unset($this->bo_tel);
		unset($this->bo_email);
		unset($this->mgr_name);
		unset($this->mgr_tel);
		unset($this->mgr_email);
		unset($this->POS);
		unset($this->data);
		unset($this->images);
		unset($this->ReviewData);
		unset($this->MenuObj);
		unset($this->Timeline);
		unset($this->where);
		unset($this->cn_row);
		unset($this->msg);
	}

	function clean($str) {

		if ($str[0] == "|") {
			$str = substr($str, 1);
		}

		if ($str[strlen($str) - 1] == "|") {
			$str = substr($str, 0, strlen($str) - 1);
		}

		$str = preg_replace("/\|\|+/", "|", $str);

		$str = preg_replace("/\s+/", " ", $str);
		$str = trim($str);

		return $str;
	}

	function PricingDollars($pricing) {

		reset($this->glb_pricedesc);
		while (list($label, $value) = each($this->glb_pricedesc)) {
			if ($pricing < $value) {
				return $label;
			}
		}

		return "$$$$$$";
	}

	function PriceMeal($value, $choice) {

		if (empty($value)) {
			return 0;
		}

		$dd = explode(",", $value);
		if (count($dd) < 1) {
			return 0;
		}

		if (count($dd) < 2) {
			$dd[1] = 0;
		}

		$dd[0] = intval($dd[0]);
		$dd[1] = intval($dd[1]);

		if ($choice == 1) {
			return $dd[0];
		}

		if ($choice == 2) {
			return $dd[1];
		}

		return floor(($dd[0] + $dd[1]) / 2);
	}

	public function getActivePromotion($restaurant) {
            
		$data = pdo_single_select("SELECT  offer FROM promotion WHERE restaurant = '$restaurant' AND start <= '" . date("Y-m-d") . "' AND end >= '" . date("Y-m-d") . "' LIMIT 1");
		$res = 'NO PROMOTION AVAILABLE';
                
		if (count($data)>0 &&$data['offer'] != '') {
			$res = $data['offer'];
		}
		return $res;
	}
function getActiveCities(){
    $sql = "SELECT geo_city.id, city, city_iso_code, geo_city.country_iso_code, country FROM geo_city, geo_country WHERE geo_city.country_iso_code = geo_country.country_iso_code AND  geo_city.status = 'active' AND  geo_country.status = 'active'";
    $data = pdo_multiple_select($sql);
    return $data;
}

function getCityIsoCode($city){
    $sql = "SELECT city_iso_code FROM geo_city WHERE geo_city.city = '$city'";
    $data = pdo_single_select($sql);
    return $data['city_iso_code'];
}

function getCountryIsoCode($country){
    $sql = "SELECT country_iso_code FROM geo_country WHERE geo_country.country = '$country'";
    $data = pdo_single_select($sql);
    return $data['country_iso_code'];
}


function getCountyList(){
    $sql = "SELECT id, country_iso_code, country FROM geo_country WHERE status = 'active'";
    $data = pdo_multiple_select($sql);
    return $data;
}

function getCountryCurrency($country){
    $sql = "SELECT money FROM geo_country WHERE status = 'active' AND country LIKE '$country' ";
    $data = pdo_single_select($sql);
    return $data['money'];
}

function getCityCurrency($city){
    $sql = "SELECT geo_country.money FROM geo_country, geo_city WHERE  geo_country.country_iso_code = geo_city.country_iso_code AND geo_country.status = 'active' AND geo_city.city LIKE '$city' ";
    $data = pdo_single_select($sql);
    return $data['money'];
}

public function getRestaurantByFacebookPageId($facebook_pid) {

        if (empty($facebook_pid)) {
                return false;
        }

        $where = 'facebook_pid = ' . $facebook_pid;
        $data = pdo_single_select("SELECT ID, restaurant, facebook_pid from restaurant WHERE $where LIMIT 1");

        $res = false;
        if ($data['facebook_pid'] != '' && $data['restaurant'] != '') {
                $res = $data['facebook_pid'];

                return $this->getRestaurant($data['restaurant']);
        }
        return $res;
}

public function getRestaurantInternalPath($restaurant = NULL){
    
    if(empty($restaurant)){
        $restaurant = $this->restaurant;
    }
    $restaurant_details = explode('_', $restaurant);
    //foreach ($restaurant_details as $details){
        
    //}
    
    //$type = $restaurant_details[2];
    $type = 'restaurant';
    switch ($restaurant_details[0]){
        case 'SG':
                $country = 'singapore';
            break;
        case 'HK':
                $country = 'hong-kong';
            break;
        case 'TH':
                $country = 'thailand';
            break;
        case 'MY':
                $country = 'malaysia';
            break;
        default:
                $country = 'singapore';
            break;  
    }
    switch ($restaurant_details[1]){
        case 'SG':
                $city = 'singapore';
            break;
        case 'HK':
                $city = 'hong-kong';
            break;
        case 'BK':
                $city = 'bangkok';
            break;
        case 'PK':
                $city = 'phuket';
            break;
        case 'KL':
                $city = 'kuala-lumpur';
            break;
        default:
                $city = 'singapore';
            break;  
    }
    
    $restaurant_name = preg_replace('/([A-Z])/', '-$1', str_replace('_','',substr($restaurant,8))); 
    $restaurant_name = trim($restaurant_name, '-');
    $restaurant_name = strtolower($restaurant_name);
    
    if($country == $city){
        $restaurant_url = $type . '/' . $country . '/' . $restaurant_name;
    }else{
        $restaurant_url = $type . '/' . $country . '/' . $city . '/' . $restaurant_name;
    }
    
    return $restaurant_url;
}


 function restaurantDescriptionToArray($description){
    
    $descriptionAR = array();
    $tmp = explode('||||',$description);
    $cleaner = array('<br>','<br/>','</br>','<b>','</b>'); 
    foreach ($tmp as $t){
        if(!empty(trim($t))){
            
        
        $itemDesc = array();
        $title = $this->get_string_between($t, "<b>", "</b>");
        $title = str_replace($cleaner, '', $title);
        $itemDesc['title'] = trim($title);
        
            $body = str_replace($title, '', $t);
            $tmpbody = explode( '<br/>', trim($body));
            $tmpbody= str_replace($cleaner, '', $tmpbody);
             $tmpAr = array();
            foreach ($tmpbody as $tmp){
                if(!empty(trim($tmp))){
                    $tmpAr[] = trim($tmp);
                }
            }
            $itemDesc['body'] = $tmpAr;
        
        $descriptionAR[] = $itemDesc;
    }
}
    return $descriptionAR;
}
 function get_string_between($string, $start, $end){
    $string = " ".$string;
    $ini = strpos($string,$start);
    if ($ini == 0) return "";
    $ini += strlen($start);
    $len = strpos($string,$end,$ini) - $ini;
    return substr($string,$ini,$len);
}


}
function getPricing($theRestaurant) {

	$data = pdo_single_select("SELECT restaurant, pricing, currency FROM restaurant WHERE restaurant = '$theRestaurant' LIMIT 1");
	if (empty($data['pricing'])) {
		return array(80, 'SGD');
	}

	$value = $data['pricing'];
	$currency = $data['currency'];
	if (empty($value)) {
		return array(80, 'SGD');
	}

	$dd = explode(",", $value);
	if (count($dd) < 1) {
		return array(80, 'SGD');
	}

	if (count($dd) < 2) {
		$dd[1] = 0;
	}

	$dd[0] = intval($dd[0]);
	$dd[1] = intval($dd[1]);

	if ($choice == 1) {
		return array($dd[0], $currency);
	}

	if ($choice == 2) {
		return array($dd[1], $currency);
	}

	return array(floor(($dd[0] + $dd[1]) / 2), $currency);
}

?>
