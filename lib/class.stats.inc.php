<?php

class WY_stats {

   var $restaurant;
   var $sdate;
   var $edate;
   var $cover;
   var $ucover;
   var $avgcover;
   var $pricing;
   var $currency;
   var $revenue;
   var $booking;
   var $profit;
   var $avgorder;
   var $monthc;
   var $monthr;
   var $monthcbkg;
   var $monthrbkg;
   var $monthrcvr;
   var $yearcntr;
   var $yearlng;
   var $yearoffer;
   var $wheel_cn;
   
   var $msg;
   

   function __construct($theRestaurant, $sdate, $edate) {

		$this->restaurant = $theRestaurant;
		$this->sdate = $sdate;
		$this->edate = $edate;
	}
	
	function bullet() {

		$theRestaurant = $this->restaurant;
		$sdate = $this->sdate;
		$edate = $this->edate;

		$data = pdo_single_select("SELECT count(*), sum(cover), avg(cover) from booking WHERE restaurant = '$theRestaurant' and cdate > '$sdate' and cdate < '$edate'");

		$this->booking = intval($data['count(*)']);
		$this->cover = intval($data['sum(cover)']);
		$this->avgcover = floatval($data['avg(cover)']);

		$data = pdo_single_select("SELECT sum(cover) from (SELECT DISTINCT email, cover from booking WHERE restaurant = '$theRestaurant' and cdate > '$sdate' and cdate < '$edate') AS ACTION ");

		$this->ucover = intval($data['sum(cover)']);

		list($this->pricing, $this->currency) = getPricing($theRestaurant);
		$this->avgorder = floor($this->pricing * $this->avgcover);
		$this->revenue = $this->pricing * $this->cover;
		$this->profit = floor($this->revenue * 0.6);
		}

	function linechart() {

		$theRestaurant = $this->restaurant;
		$sdate = $this->sdate;
		$edate = $this->edate;

		$data = pdo_multiple_select("SELECT MONTH(cdate), COUNT(cdate) from booking WHERE restaurant = '$theRestaurant' and cdate > '$sdate' and cdate < '$edate' GROUP BY MONTH(cdate)");	

		foreach($data as $row) {
			$this->monthc[] = $row[0];
			$this->monthcbkg[] = intval($row[1]);
			}

		$data = pdo_multiple_select("SELECT MONTH(rdate), COUNT(rdate), sum(cover) from booking WHERE restaurant = '$theRestaurant' and cdate > '$sdate' and cdate < '$edate' GROUP BY MONTH(rdate)");
		
		foreach($data as $row) {
			$this->monthr[] = $row[0];
			$this->monthrbkg[] = intval($row[1]);
			$this->monthrcvr[] = intval($row[2]);
			}

		$data = pdo_multiple_select("SELECT country, count(country) from booking WHERE restaurant = '$theRestaurant' and cdate > '$sdate' and cdate < '$edate' GROUP BY country limit 6");
		
		foreach($data as $row) 
			$this->yearcntr[$row[0]] = intval($row[1]);
			
		$data = pdo_multiple_select("SELECT language, count(language) from booking WHERE restaurant = '$theRestaurant' and cdate > '$sdate' and cdate < '$edate' GROUP BY language limit 6");
		
		foreach($data as $row)
			$this->yearlng[$row[0]] = intval($row[1]);

		$data = pdo_single_select("SELECT count(wheelwin) from booking WHERE restaurant = '$theRestaurant' and cdate > '$sdate' and cdate < '$edate'");
		$this->wheel_cn = intval($data[0]);
		
		$data = pdo_multiple_select("SELECT wheelwin, count(wheelwin) from booking WHERE restaurant = '$theRestaurant' and cdate > '$sdate' and cdate < '$edate' GROUP BY wheelwin limit 9");
		
		foreach($data as $row) {
			$ext = sprintf("(%2.0f%%)", (floatval($row[1]) / $this->wheel_cn) * 100);
			$this->yearoffer[$row[0] . $ext] = intval($row[1]);
			}

		}
		
	function minmax($data) {
	
		$min = 999999999;
		$max = 0;
		for($i = 0; $i < count($data); $i++) {
			if($data[$i] > $max) $max = $data[$i];
			if($data[$i] < $min) $min = $data[$i];
			}
		return array($min, $max);	
		}
}

?>
