<?php

class WY_Service {

   private $ID;
   private $categorie;
   private $service;
   private $extravalue;
   

   function __construct() {

   }

   
   public function getCategories(){
       $sql = "SELECT sc.ID as categorie_id, sc.categorie as categorie FROM res_service_categorie sc WHERE 1";
       return pdo_multiple_select_array($sql);
   }
   
   public function getServices(){
       $sql = "SELECT sd.ID as service_id, sd.service as service, sd.categorie_id as categorie_id FROM res_service_details sd WHERE 1";
       return pdo_multiple_select_array($sql);
   }
   
   public function getRestaurantServices($restaurant, $option = NULL){
       $where = '';
       if($option == 'active_only'){
           $where = ' WHERE restaurant_id IS NOT NULL '; 
       }
       $sql = "SELECT sd.ID, sd.service, sd.categorie_id, sd.pico_service as pico_service, IF(restaurant_id IS NULL, FALSE, TRUE)as active, sc.categorie, sc.pico_url as pico_categorie "
               . "FROM  restaurant_service rs "
               . "RIGHT JOIN res_service_details sd ON rs.service_id = sd.ID AND restaurant_id = '$restaurant' "
               . "LEFT JOIN  res_service_categorie sc ON sc.ID = sd.categorie_id $where ORDER BY categorie_id";
       return pdo_multiple_select_array($sql);
   }
   
   public function updateServices($restaurant, $active_services){

        $services = '(';
        $insert_values = '';
        foreach ($active_services as $active_service){
            $services .= $active_service.',';
            $insert_values .= "('$restaurant',$active_service)," ;
        }
        $insert_values = rtrim($insert_values, ",");
        $services = rtrim($services, ","). ')';
        $sql = "INSERT INTO restaurant_service (restaurant_id, service_id) VALUES $insert_values ON DUPLICATE KEY UPDATE restaurant_id = '$restaurant'";
        $res = pdo_exec($sql);
        
        if($res >= 0){
           //remove old           
            $sql = "DELETE FROM  restaurant_service WHERE restaurant_id = '$restaurant' AND service_id NOT IN $services";
            $res = pdo_exec($sql);
            return $res;
        }
       return false;
   }
   
}
?>
