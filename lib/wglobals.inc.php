<?php

	/* Richard Kefs, Weeloy, Copyright (c) 2014 all rights reserved  */
	
$GLOBALS['db_restaurant'] = array('restaurant', 'title', 'hotelname', 'address', 'address1', 'city', 'zip', 'country', 'tel', 'email', 'map', 'description', 'state', 'fax', 'url', 'award', 'creditcard', 'wheel', 'wheelvalue', 'region', 'mealtype', 'pricing', 'openhours', 'images', 'chef', 'chef_award', 'chef_origin', 'chef_description', 'chef_believe', 'stars', 'cuisine', 'menus', 'rating', 'dinesfree', 'dfmealtype', 'dfminpers', 'GPS', 'bo_name', 'bo_tel', 'bo_email', 'mgr_name', 'mgr_tel', 'mgr_email', 'POS');
$GLOBALS['db_hotelmaster'] = array('name', 'title', 'address', 'city', 'state', 'zip', 'country', 'tel', 'fax', 'email', 'url', 'GPS', 'map', 'description', 'mgr_name', 'mgr_tel', 'mgr_email', 'images');
$GLOBALS['db_member'] = array('mobile', 'name', 'firstname', 'email', 'country', 'member', 'gender', 'address', 'address1', 'city', 'zip', 'region', 'state', 'fax', 'cuisine', 'mealtype', 'creditcard', 'pricing', 'stars', 'rating', 'dinesfree', 'password', 'temppassword');
$GLOBALS['db_menu'] = array('restaurant', 'name', 'menu', 'morder');
$GLOBALS['db_review'] = array('restaurant', 'reviewdesc', 'reviewguest', 'reviewgrade', 'reviewdtvisite', 'reviewdtcreate');
$GLOBALS['db_cuisine'] = array('cuisine', 'cuisineID');
$GLOBALS['db_wheeloffer'] = array('offer', 'description', 'type', 'morder');

$GLOBALS['AllMenuVar'] = "";

$GLOBALS['pstyle'] = "";
$GLOBALS['h2style'] = "";
$GLOBALS['ulstyle'] = "";
$GLOBALS['nextmenub'] = "";

$GLOBALS['loginMember'] = "";

$GLOBALS['cookie_restaurant'] = "";
$GLOBALS['cookie_member'] = "";
$GLOBALS['theListAr'] = array();
$GLOBALS['SingleSelect'] = array();
$GLOBALS['OnchangeSelect'] = array();

$GLOBALS['genderlist'] = array("Mr", "Mrs", "Ms");
//$GLOBALS['foodlist'] = array("African", "American", "Asian", "Australian", "Bar-Lounge", "Barbecue", "Brazilian", "British", "Cafe", "Californian", "Canadian", "Caribbean", "Chinese", "Cantonese", "Creole-Cajun", "Dim Sum", "Dessert", "European", "French", "Fusion", "German", "Greek", "Grill", "Halal", "Hawaiian", "Indian", "International", "Italian", "Japanese", "Korean", "Kosher", "Mediterranean", "Mexican", "Middle-Eastern", "New-Zealand", "Nordic", "Organic", "Pizza", "Pub", "Russian", "Scottish", "Seafood", "South-American", "Spanish", "Steak-House", "Sushi", "Tapas", "Thai", "Vegetarian", "Western", "Wine-Bar");
$GLOBALS['foodtype'] = array("Breakfast", "Brunch", "Dinner", "Lunch", "Tea");
$GLOBALS['ccardlist'] = array("Amex", "Diners", "JCB", "Master", "Visa");
$GLOBALS['citylist'] = array("Singapore", "Bangkok", "Phuket", "Bejing", "Hongkong", "Kuala Lumpur", "Jakarta", "Manila", "Tokyo","Bayan Lepas", "Johor Bahru", "George Town", "Petaling Jaya", "Putrajaya" , "Puchong", "Shah Alam", "Subang Jaya");
$GLOBALS['countlist'] = array("Singapore", "Thailand", "China", "Hongkong", "Malaysia", "Indonesia", "Philippines", "Japan");
$GLOBALS['tracktypelist'] = array("sms", "email", "geolocalisation", "facebook", "google plus");



$GLOBALS['wheelchoice'] = array("(3 parts) Food Discount 25%", "(6 parts) Food Discount 20%", "(7 parts) Food Discount 15%", "(8 parts) Food Discount 10%");

$GLOBALS['promotionchoice'] = array(array("15% off food", 1, "Discount applies to total food bill before applicable taxes and service charge. And, not applicable in conjunction with other discounts, promotional offers or for special festive holidays"), 
                                    array("20% off food",  2,  "Discount applies to total food bill before applicable taxes and service charge. And, not applicable in conjunction with other discounts, promotional offers or for special festive holidays"), 
                                    array("25% off food",  3,  "Discount applies to total food bill before applicable taxes and service charge. And, not applicable in conjunction with other discounts, promotional offers or for special festive holidays"), 
                                    array("30% off food",  4, "Discount applies to total food bill before applicable taxes and service charge. And, not applicable in conjunction with other discounts, promotional offers or for special festive holidays"), 
                                    array("40% off food",  5,  "Discount applies to total food bill before applicable taxes and service charge. And, not applicable in conjunction with other discounts, promotional offers or for special festive holidays"), 
                                    array("50% off food", 10, "Discount applies to total food bill before applicable taxes and service charge. And, not applicable in conjunction with other discounts, promotional offers or for special festive holidays"), 
                                    array("10% off total bill",  2,  "Discount applies to total bill before applicable taxes and service charge. And, not applicable in conjunction with other discounts, promotional offers or for special festive holidays"), 
                                    array("15% off total bill",  3,  "Discount applies to total bill before applicable taxes and service charge. And, not applicable in conjunction with other discounts, promotional offers or for special festive holidays"), 
                                    array("20% off total bill",  4, "Discount applies to total bill before applicable taxes and service charge. And, not applicable in conjunction with other discounts, promotional offers or for special festive holidays"), 
                                    array("25% off total bill",  5,  "Discount applies to total bill before applicable taxes and service charge. And, not applicable in conjunction with other discounts, promotional offers or for special festive holidays"), 
                                    array("30% off total bill", 7, "Discount applies to total bill before applicable taxes and service charge. And, not applicable in conjunction with other discounts, promotional offers or for special festive holidays"), 
                                    array("1 for 1 Main item",  7,  "Buy any 1 main menu item and enjoy 1 free main menu item. The lower priced main item to be deducted from the bill. Offer is not applicable in conjunction with other discounts, promotional offers or for special festive holidays."), 
                                    array("2 + 1 Main item",  5,  "Buy any 2 main menu item and enjoy 1 free main menu item. The lower priced main item to be deducted from the bill. Offer is not applicable in conjunction with other discounts, promotional offers or for special festive holidays."), 					
                                    array("3 + 1 Main item",  4, "Buy any 3 main menu item and enjoy 1 free main menu item. The lower priced main item to be deducted from the bill. Offer is not applicable in conjunction with other discounts, promotional offers or for special festive holidays."));

$GLOBALS['restaurantstatuslist'] = array("demo","comingsoon","active","pending","deleted");
$GLOBALS['restaurantoffertypelist'] = array("wheel","listing");

$GLOBALS['memberstatuslist'] = array("active","pending","suspended","deleted");
$GLOBALS['membertypelist'] = array("member","restaurant", "weeloy_sales");



// if any promotion is updated please also update the value in the database TABLE: wheeloffer
$GLOBALS['wheeloffers'] = array(
					"10% off food", 1, "Discount applies to total food bill before applicable taxes and service charge. And, not applicable in conjunction with other discounts, promotional offers or for special festive holidays", 
					"15% off food", 2, "Discount applies to total food bill before applicable taxes and service charge. And, not applicable in conjunction with other discounts, promotional offers or for special festive holidays", 
					"20% off food", 3, "Discount applies to total food bill before applicable taxes and service charge. And, not applicable in conjunction with other discounts, promotional offers or for special festive holidays", 
					"25% off food", 4, "Discount applies to total food bill before applicable taxes and service charge. And, not applicable in conjunction with other discounts, promotional offers or for special festive holidays", 
					"30% off food", 5, "Discount applies to total food bill before applicable taxes and service charge. And, not applicable in conjunction with other discounts, promotional offers or for special festive holidays", 
					"40% off food", 6, "Discount applies to total food bill before applicable taxes and service charge. And, not applicable in conjunction with other discounts, promotional offers or for special festive holidays", 
					"50% off food", 7, "Discount applies to total food bill before applicable taxes and service charge. And, not applicable in conjunction with other discounts, promotional offers or for special festive holidays", 
					"10% off total bill", 3, "Discount applies to total bill before applicable taxes and service charge. And, not applicable in conjunction with other discounts, promotional offers or for special festive holidays", 
					"15% off total bill", 4, "Discount applies to total bill before applicable taxes and service charge. And, not applicable in conjunction with other discounts, promotional offers or for special festive holidays", 
					"20% off total bill", 5, "Discount applies to total bill before applicable taxes and service charge. And, not applicable in conjunction with other discounts, promotional offers or for special festive holidays", 
					"25% off total bill", 6, "Discount applies to total bill before applicable taxes and service charge. And, not applicable in conjunction with other discounts, promotional offers or for special festive holidays", 
					"30% off total bill", 7, "Discount applies to total bill before applicable taxes and service charge. And, not applicable in conjunction with other discounts, promotional offers or for special festive holidays", 
					"1 for 1 Appetizer", 3, "Buy any 1 appetizer item and enjoy 1 free appetizer item. The lower priced appetizer to be deducted from the bill. Offer is not applicable in conjunction with other discounts, promotional offers or for special festive holidays.", 
					"1 for 1 Main item", 7, "Buy any 1 main menu item and enjoy 1 free main menu item. The lower priced main item to be deducted from the bill. Offer is not applicable in conjunction with other discounts, promotional offers or for special festive holidays.", 
					"2 + 1 Main item", 5, "Buy any 2 main menu item and enjoy 1 free main menu item. The lower priced main item to be deducted from the bill. Offer is not applicable in conjunction with other discounts, promotional offers or for special festive holidays.", 
					"3 + 1 Main item", 4, "Buy any 3 main menu item and enjoy 1 free main menu item. The lower priced main item to be deducted from the bill. Offer is not applicable in conjunction with other discounts, promotional offers or for special festive holidays.", 
					"Free Wine or Beer", 6, "Free glass of house wine or mug of draft beer per adult ordering a main meal item. Offer is not applicable in conjunction with other discounts, promotional offers or for special festive holidays.", 
					"Free Appetizer", 5, "Free daily special appetizer per person ordering a main meal item. Offer is not applicable in conjunction with other discounts, promotional offers or for special festive holidays.", 
					"Free Salad or Soup", 5, "Free house salad or daily soup per person ordering a main meal item. Offer is not applicable in conjunction with other discounts, promotional offers or for special festive holidays.", 
					"Free Ice Cream", 5, "Free scoop of ice cream per person ordering a main meal item. Offer is not applicable in conjunction with other discounts, promotional offers or for special festive holidays.", 
					"Kids Eat Free", 4, "Child under the age of 5 eats for free when dining with at least 1 adult ordering a main menu item. Child s free meal is ordered from Kid s menu. Offer is not applicable in conjunction with other discounts, promotional offers or for special festive holidays.", 
					"Free Bottle Wine", 5, "Choice - one (1) bottle of red or white wine as determined by restaurant per party. Valid only for adults of legal age in the respective country.", 
					"Free Bottle Liquor", 6, "Choice - one (1) bottle of vodka, gin, scotch as determined by restaurant per party. Valid only for adults of legal age in the respective country.", 
					"Free Room", 10, "Free Nights stay in a Standard Room for 1 night, room only for 2 persons. Offer is valid on Friday, Saturday or Sunday night and subject to advance reservation and availability." 
					);
					
// $GLOBALS['wheelvalues'] = array("10% off food" => 1,  "15% off food" => 2,  "20% off food" => 3,  "25% off food" => 4,  "30% off food" => 5,  "40% off food" => 6,  "50% off food" => 7,  "10% off total bill" => 3,  "15% off total bill" => 4,  "20% off total bill" => 5,  "25% off total bill" => 6,  "30% off total bill" => 7,  "1 for 1 Appetizer" => 3,  "1 for 1 Main item" => 7,  "2 + 1 Main item" => 5,  "3 + 1 Main item" => 4,  "Free Wine or Beer" => 6,  "Free Appetizer" => 6,  "Free Salad or Soup" => 5,  "Free Ice Cream" => 5,  "Kids Eat Free" => 4,  "Free Bottle Wine" => 5,  "Free Bottle Liquor" => 6,  "Free Room" => 10,  "Free NV Lunch for 2" => 15, "Free NV Dinner for 2" => 15 );
					
$GLOBALS['stdwheeloffers'] = array(0, 1, 2, 0, 3, 2, 0, 1, 1, 0, 2, 3, 0, 2, 1, 0, 1, 2, 0, 1, 3, 0, 2, 1);


$GLOBALS['glbCurrencyList'] = array('SGD', 'THB', 'HKD', 'MYR', 'EUR', 'USD');
$GLOBALS['glbChefTypeList'] = array('Chef - Male', 'Bartender - Male', 'Barista - Male', 'Male', 'Chef - Female', 'Bartender - Female', 'Barista - Female', 'Female');

if(isset($validArg) && count($validArg) > 0 && is_array($validArg))
	while(list($index, $label) = each($validArg)) {
		if(isset($_REQUEST[$label]))
			$$label = clean_input($_REQUEST[$label]);
		}

function getCuisine() {

		$cuisineAr = array();
        $data = pdo_multiple_select("SELECT cuisine FROM cuisine");
        foreach ($data as $row)
        	$cuisineAr[] = $row['cuisine'];
        return $cuisineAr;
}

?>