<?php
    
    /* Richard Kefs, Weeloy, Copyright (c) 2014 all rights reserved  */
require_once("lib/class.debug.inc.php");
    
function getConnection($db = NULL) {
        
        include('conf/conf.mysql.inc.php');
        
        switch ($db){
            case 'dwh':
                $dbh = new PDO("mysql:host=$dbhost_dwh;dbname=$dbname_dwh", $dbuser_dwh, $dbpass_dwh);
                break;
            case 'session':
                $dbh = new PDO("mysql:host=$dbhost_session;dbname=$dbname_session", $dbuser_session, $dbpass_session);
                break;
            default :
                $dbh = new PDO("mysql:host=$dbhost;dbname=$dbname", $dbuser, $dbpass);
                break;
        }
        $dbh->exec("set names utf8");
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $dbh;
    }

function pdo_error($content, $funcname) {

	$debug = new WY_debug;
	$debug->writeDebug("ERROR-PDO", strtoupper($funcname), $content);
	if($_SERVER['SERVER_NAME'] == "localhost")
		echo '{"error":{"text":' . $content . '}}';
}

function pdo_single_select($sql, $db = '') {

	try {
		$db = getConnection($db);
		$stmt = $db->query($sql);
		if($data = $stmt->fetch(PDO::FETCH_BOTH)) {
			return $data;
			}
	   } catch(PDOException $e) {
	   	pdo_error($e->getMessage() . ' -> ' . $sql, "pdo_single_select");
		}
	return array();
}

function pdo_single_select_array($sql, $db = '') {

	try {
		$db = getConnection($db);
		$stmt = $db->query($sql);
		if($data = $stmt->fetch(PDO::FETCH_ASSOC)) {
			return $data;
			}
	   } catch(PDOException $e) {
	   	pdo_error($e->getMessage() . ' -> ' . $sql, "pdo_single_select");
		}
	return array();
}

function pdo_multiple_select($sql, $db = '') {

	try {
		$db = getConnection($db);
		$stmt = $db->query($sql);
		if($data = $stmt->fetchAll(PDO::FETCH_BOTH)) {
			return $data;
			}
	   } catch(PDOException $e) {
		pdo_error($e->getMessage() . ' -> ' . $sql, "pdo_multiple_select");
		}
	return array();
}

function pdo_multiple_select_array($sql, $db = '') {

	try {
		$db = getConnection($db);
		$stmt = $db->query($sql);
		if($data = $stmt->fetchAll(PDO::FETCH_ASSOC)) {
			return $data;
			}
	   } catch(PDOException $e) {
		pdo_error($e->getMessage() . ' -> ' . $sql, "pdo_multiple_select_array");
		}
	return array();
}


function pdo_column($sql) {

    try {
        $db = getConnection();
		$stmt = $db->query($sql);
        return $stmt->fetchAll(PDO::FETCH_COLUMN, 0);
    } catch(PDOException $e) {
        pdo_error($e->getMessage() . ' -> ' . $sql, "pdo_column");
    }
	return array();
}

function pdo_exec($sql, $db = '') {

	try {
		$db = getConnection($db);
		$affected_rows = $db->exec($sql);
	   } catch(PDOException $e) {
               //PHIL gestion des erreurs.
        	return "error ". $e->getMessage() . ' -> ' . $sql;               
		}
	return $affected_rows;
}

function pdo_insert_unique($sql, $in_sql) {

   try {
		$db = getConnection();
		$stmt = $db->query($sql);
		$row_count = $stmt->rowCount();
		if($row_count > 0)
			return -1;			
		$result = $db->exec($in_sql);
		return 1;
	   } catch(PDOException $e) {
        pdo_error($e->getMessage() . ' -> ' . $sql, "pdo_insert_unique");
		}
}

function pdo_insert($sql) {

   try {
		$db = getConnection();
		$stmt = $db->prepare($sql);
		$stmt->execute();
		$id = $db->lastInsertId();
		return $id;
	   } catch(PDOException $e) {
        pdo_error($e->getMessage() . ' -> ' . $sql, "pdo_insert");
		}
	return -1;
}

function pdo_multiple_obj($sql) {

    try {
        $db = getConnection();
  		$stmt = $db->query($sql);
        return $stmt->fetchAll(PDO::FETCH_OBJ);
    } catch(PDOException $e) {
        pdo_error($e->getMessage() . ' -> ' . $sql, "pdo_multiple_obj");
    }
	return NULL;
}

function list_table_field($table) {

    try {
		$db = getConnection();
		$stmt = $db->query("DESCRIBE $table");
		return $stmt->fetchAll(PDO::FETCH_COLUMN);	
    } catch(PDOException $e) {
        pdo_error($e->getMessage() . ' -> ' . $sql, "list_table_field");
    }
	return NULL;
}

function list_table_names() {

    try {
		$db = getConnection();
		$stmt = $db->query("SHOW TABLES");
		return $stmt->fetchAll(PDO::FETCH_COLUMN);	
    } catch(PDOException $e) {
        pdo_error($e->getMessage() . ' -> ' . $sql, "list_table_names");
    }
	return NULL;
}


function clean_input($data) {
  if(empty($data)) return '';
  $data = trim($data);
  $data = stripslashes($data);
  if(!empty($data))
	  $data = preg_replace("/\'|\"/", "’", $data);	//\xc2\xb4
  //$data = htmlspecialchars($data);
  return $data;
}

function clean_number($data) {
  if(empty($data)) return '';
  $data = trim($data);
  $data = stripslashes($data);
  if(!empty($data))
  	$data = preg_replace( "/[^0-9\-\+\.\,]/", '', $data );  
  
  return $data;
}

function clean_tel($data) {
  if(empty($data)) return '';
  $data = trim($data);
  $data = stripslashes($data);
  if(!empty($data))
  	$data = preg_replace( "/[^0-9\+ ]/", '', $data );  
  
  return $data;
}

function clean_text($data) {
  if(empty($data)) return '';
  $data = trim($data);
  $data = stripslashes($data);
  if(!empty($data))
  	$data = preg_replace( "/[^0-9a-zA-Z-_\.\@ ]/", '', $data );  
  
  if(!empty($data))
	  $data = preg_replace("/\s+/", " ", $data);
  $data = trim($data);
  
  return $data;
}

function getnchoice($divider, $limit) {

	$choice[] = rand(0, $limit - 1);
	for($k = 0; count($choice) < $divider && $k < 100; $k++) {
	 	$m = rand(0, $limit - 1);
		if(!in_array($m, $choice))
			$choice[] = $m;
		}
		
	return $choice;
}

function w_checkdate($date1, $date2) {

	// $date2 >= $date1
	
	$cyear = intval(date("Y"));
	if(preg_match('/\d{4}-\d{1,2}-\d{1,2}/', $date1) == false)
		return -1;
	if(preg_match('/\d{4}-\d{1,2}-\d{1,2}/', $date2) == false)
		return -2;
	
	$d1 = explode("-", $date1);
	$d2 = explode("-", $date2);
	$year1 = intval($d1[0]);
	$month1 = intval($d1[1]);
	$day1 = intval($d1[2]);
	$year2 = intval($d2[0]);
	$month2 = intval($d2[1]);
	$day2 = intval($d2[2]);
	
	if($cyear != $year1 || ($cyear != $year2 && $cyear != $year2 - 1))
		return -3;
	if(checkdate($month1, $day1, $year1) == false)
		return -4;
	if(checkdate($month2, $day2, $year2) == false)
		return -5;

	if(mktime(0, 0, 0, $month1, $day1, $year1) > mktime(0, 0, 0, $month2, $day2, $year2))
		return -6;
	
	return 1;
}


?>