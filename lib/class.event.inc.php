<?php

class WY_Event {

   var $dirname;
   var $globalshow;
   var $restaurant;
   var $name;
   var $description;
   var $start;
   var $end;
   var $city;
   var $country;
   var $order;
   var $picture;
   var $timeline;
   var $eventInfo;
   
   var $msg;
   

   function __construct($theRestaurant) {
   		$this->restaurant = $theRestaurant;
		$this->dirname = __UPLOADDIR__ . "$theRestaurant/";
		if(AWS){$this->globalshow = __S3HOST__.__S3DIR__;}else{$this->globalshow = __SHOWDIR__;}
   }

	function insert($eventname) {

		if($eventname == "")
			return array(-1, "Event name is invalid -$eventname-");
		
		$theRestaurant = $this->restaurant;
		$sql_check = "SELECT name FROM event where name = '$eventname' and restaurant = '$theRestaurant'  limit 1";
		$sql_ins = "INSERT INTO  event (name, restaurant) VALUES ('$eventname', '$theRestaurant')";
		$result = pdo_insert_unique($sql_check, $sql_ins);
		return ($result > 0) ? array(1, "Event $eventname has been created") : array(-1, "Event $eventname already exists. No new event created");
	}

	function insertEvent($eventname, $title, $morder, $city, $country, $start, $end, $description, $picture) {
		if($eventname == "")
			return array(-1, "Event name is invalid -$eventname-");
		
		$theRestaurant = $this->restaurant;
		$sql_check = "SELECT name FROM event where name = '$eventname' and restaurant = '$theRestaurant'  limit 1";
		$sql_ins = "INSERT INTO  event (name, restaurant, morder, title, city, country, start, end, description, picture) VALUES ('$eventname', '$theRestaurant', '$morder', '$title', '$city', '$country', '$start', '$end', '$description', '$picture')";
		$result = pdo_insert_unique($sql_check, $sql_ins);
		return ($result > 0) ? array(1, "Event $eventname has been created") : array(-1, "Event $eventname already exists. No new event created");
	}

	function updateEvent($eventname, $title, $morder, $city, $country, $start, $end, $description, $picture) {

		$theRestaurant = $this->restaurant;
		
		$eventname = clean_input($eventname);
		$title = clean_input($title);
		$city = clean_input($city);
		$country = clean_input($country);
		$description = clean_input($description);
		$picture = clean_input($picture);
		$morder = intval(preg_replace("/[^0-9]/", "", $morder));

		if($eventname == "")
			return array(-1, "Undefined Event Name");
		
		$affected_rows = pdo_exec("Update event set morder='$morder', title='$title', city='$city', country='$country', start='$start', end='$end', description='$description', picture='$picture' where name='$eventname' and restaurant = '$theRestaurant' limit 1");
		return array(1, "Event $eventname has been updated");
	}

	function save($eventname) {

		$adderror = "";
	
		$theRestaurant = $this->restaurant;
		$title = clean_input($_REQUEST['itemeventtitle']);
		$startAr = explode("/", $_REQUEST['itemeventstart']);
		$start = $startAr[2] . "-" . $startAr[1] . "-" . $startAr[0];
		$endAr = explode("/", $_REQUEST['itemeventend']);
		$end = $endAr[2] . "-" . $endAr[1] . "-" . $endAr[0];
		if($endAr[2] < $startAr[2] || ($endAr[2] == $startAr[2] && $endAr[1] < $startAr[1]) || ($endAr[2] == $startAr[2] && $endAr[1] == $startAr[1] && $endAr[0] < $startAr[0])) {
			$end = $start;
			$adderror = "Ending date starts before starting date. Resetting Ending Date";
			}
		$picture = clean_input($_REQUEST['itemeventpicture']);
		$morder = clean_input($_REQUEST['itemeventorder']);
		$description = clean_input($_REQUEST['itemeventdesc']);
		$city = clean_input($_REQUEST['itemeventcity']);
		$country = clean_input($_REQUEST['itemeventcountry']);

		//error_log("CITY COUNTRY " . $city . " " . $country);	
		$affected_rows = pdo_exec("Update event set title='$title', city='$city', country='$country', start='$start', end='$end', description='$description', picture='$picture', morder='$morder' where name='$eventname' and restaurant = '$theRestaurant' limit 1");
		return array(1, "Event $eventname has been updated. $adderror");
	}

	function delete($eventame) {

		if($eventame == "")
			return array(-1, "Event name is invalid -$eventame-");
	
		$theRestaurant = $this->restaurant;
		$affected_rows = pdo_exec("DELETE FROM event WHERE name = '$eventame' and restaurant = '$theRestaurant' LIMIT 1");
		return array(1, "Event $eventame has been deleted");
	}

	function readEventName() {

		$theRestaurant = $this->restaurant;
		return pdo_column("SELECT name FROM event where restaurant = '$theRestaurant' ORDER BY morder");
	}

	function readAllEvents() {

		$theRestaurant = $this->restaurant;
		return pdo_multiple_obj("SELECT name, title, morder, city, country, start, end, description, picture FROM event where restaurant = '$theRestaurant' ORDER BY morder");
	}

	function getEvents() {

		$theRestaurant = $this->restaurant;
		return pdo_multiple_select_array("SELECT ID, name, title, morder, city, country, start, end, description, picture FROM event WHERE restaurant ='$theRestaurant' ORDER by morder");
	}
	
        function getActiveEvents() {

		$theRestaurant = $this->restaurant;
		return pdo_multiple_select_array("SELECT ID, name, title, morder, city, country, start, end, description, picture FROM event WHERE restaurant ='$theRestaurant' AND start < now() AND end > now() ORDER by morder");
	}
        
	function getComingEvents() {

		$theRestaurant = $this->restaurant;
		return pdo_multiple_select("SELECT ID, name, title, morder, city, country, start, end, description, picture FROM event WHERE end > NOW() AND restaurant ='$theRestaurant' ORDER by morder");
	}
        
	function getTimeline() {

		$theRestaurant = $this->restaurant;
		if($theRestaurant != "") 
			$this->Timeline = pdo_multiple_select("SELECT name, title, morder, city, country, start, end, description, picture, restaurant FROM event where restaurant = '$theRestaurant' and end > now() ORDER BY start");
		else $this->Timeline = pdo_multiple_select("SELECT name, title, morder, city, country, start, end, description, picture, restaurant FROM event where end > now() ORDER BY start");
	}


    function clean_input($str) {
    
    if($str == "")
    	return;

    $str = preg_replace("/\'\"/", "’", $str);
    
    return $str;
    }

}

?>
