<?php

require_once("lib/wglobals.inc.php");

class WY_wheel {

   var $dirname;
   var $showdirname;
   var $showglb;
   var $restaurant;
   var $wheel;
   var $wheelvalue;
   var $script;
   var $data;
   var $wheelconfoffer;
   var $wheelconfvalue;
   var $wheeltitle;

   var $image;
   
   var $msg;
   
   var $wheelversion;

   function __construct($theRestaurant, $source = NULL) {
        $this->wheelversion = time();
   		$this->restaurant = $theRestaurant;
        if($source != 'api'){
            $this->image = new WY_Images($this->restaurant);
            }
        $this->readConfig();
    }

	private function readConfig() {

		global $wheeloffers;
	
		$this->wheelconfoffer = $wheeloffers;
		
		$theRestaurant = $this->restaurant;
		$data = pdo_multiple_select("SELECT offer, description, value, morder FROM wheeloffer WHERE restaurant = '$theRestaurant' order by morder");

        foreach ($data as $row) {
            $this->wheelconfoffer[] = $row['offer'];
            $this->wheelconfoffer[] = intval($row['value']);
            $this->wheelconfoffer[] = $row['description'];
        	}
       
       $this->wheelconfvalue =  array();
       $limit = count($this->wheelconfoffer); 
       for($i = 0; $i < $limit; $i += 3) {
       		$this->wheelconfvalue[$this->wheelconfoffer[$i]] = $this->wheelconfoffer[$i + 1];
       		$this->wheeltitle[] = $this->wheelconfoffer[$i];
       	}

	}
	
	function readWheel() {

		$this->wheelversion = 0;
		$this->wheel = "";
		$theRestaurant = $this->restaurant;
		$data = pdo_single_select("SELECT wheel, wheelversion FROM restaurant WHERE restaurant = '$theRestaurant' limit 1");
		if(isset($data['wheelversion'])) {
			$this->wheelversion = $data['wheelversion'];
			$this->wheel = $data['wheel'];
			}
		return $this->wheel;
	}

	function save_wheel($arg) {
	
        $wheelversion = $this->wheelversion;            
		$wheelvalues = &$this->wheelconfvalue;
		$theRestaurant = $this->restaurant;
		
		$wheeldata = "";
		$totalValue = 0;
		
		if($arg == "sponsor") {		// the first parts 24, 1, 2 are equal based on 1
			$_REQUEST['itemwheelvalue24'] = $_REQUEST['itemwheelvalue2'] = $_REQUEST['itemwheelvalue1'];
			}
			
		for($i = 1; $i <= 24; $i++) {
			$item = "itemwheelvalue" . $i;
			if(isset($_REQUEST[$item])) {
				$label = $_REQUEST[$item];
				$totalValue += $wheelvalues[$label];
				$wheeldata .= "|" . $label;
				}
			else $wheeldata .= "|";
			}
		$wheeldata = "VALID" . "-" . $totalValue . "-" . $wheeldata;

        $wheelconfoffer = implode('|||',$this->wheelconfoffer);
                
		pdo_exec("UPDATE restaurant SET wheelversion='$wheelversion', wheel='$wheeldata', wheelvalue='$totalValue' WHERE restaurant = '$theRestaurant' limit 1");
        pdo_exec("UPDATE wheel SET status = 'deleted' WHERE restaurant = '$theRestaurant' ");
		pdo_exec("INSERT INTO wheel (restaurant, wheelversion, wheelvalue, wheeloffers, wheeldescriptions, status, creation_date) VALUES ('$theRestaurant', $wheelversion, $totalValue, '$wheeldata', '$wheelconfoffer', 'active', now() )");
		         
        return $this->saveImgWheel();	
	}

	function setsdtwheel($itemLabel) {

		global $stdwheeloffers;

		$wheeloffers = &$this->wheelconfoffer;
		
		for($i = 0; $i < count($itemLabel); $i++)
			{
			$label = $itemLabel[$i];
			if(substr($label, 0, 14) != "itemwheelvalue") continue;
			$index = intval(substr($label, 14)) - 1;
			$value = $wheeloffers[$stdwheeloffers[$index] * 3];
			$_REQUEST[$label] = $$label = $value;
			}
	}

	function save_stdwheel() {

		global $stdwheeloffers;
        
        $wheelversion = $this->wheelversion;
            
		$wheeloffers = &$this->wheelconfoffer;	
		$wheelvalues = &$this->wheelconfvalue;
		 
		$theRestaurant = $this->restaurant;
		$wheeldata = "";
		$totalValue = 0;
		for($i = 0; $i < 24; $i++) {
			$label = $wheeloffers[$stdwheeloffers[$i] * 3];
			$totalValue += $wheelvalues[$label];
			$wheeldata .= "|" . $label;
			}
		$wheeldata = "VALID" . "-" . $totalValue . "-" . $wheeldata;

        $wheelconfoffer = implode('|||',$this->wheelconfoffer);
                
		pdo_exec("UPDATE restaurant SET wheel='$wheeldata', wheelvalue='$totalValue' WHERE restaurant = '$theRestaurant' LIMIT 1");
		pdo_exec("UPDATE wheel SET status = 'deleted' WHERE restaurant = '$theRestaurant' ");
		pdo_exec("INSERT INTO wheel (restaurant, wheelversion, wheelvalue, wheeloffers, wheeldescriptions, status, creation_date) VALUES ('$theRestaurant', $wheelversion, $totalValue, '$wheeldata', '$wheelconfoffer', 'active', now() )");
		
                return $this->saveImgWheel();	
	}

	function saveImgWheel() {

		$theRestaurant = $this->restaurant;
		$datawheel = $this->readWheel();
        $wheelversion = $this->wheelversion;
	
		// save wheel value
		$stringAr = explode("|", strtoupper($datawheel));
		$wheelvalue = intval(substr($stringAr[0], 6));
		$_REQUEST["wheelvalue"] = $wheelvalue;

		/* a definir dans la config */
		$protocol = (strstr($_SERVER['HTTP_HOST'], 'localhost:8888') == false) ? 'https' : 'http';

		if(substr($datawheel, 0, 5) == "VALID")
			{
			$this->writescript($datawheel);
            $url = $protocol . '://' . $_SERVER['HTTP_HOST'] . __ROOTDIR__ . "/wheel/wheeldb.php?savewheel=w76849361&wheelversion=$wheelversion&restaurant=$theRestaurant";
                        //error_log("THE WHEEL IMAGE $url");		
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
			$output = curl_exec($ch); 
			curl_close($ch);
                        
            $url = $protocol . '://' . $_SERVER['HTTP_HOST'] . __ROOTDIR__ . "/wheel/wheelrating.php?savewheel=w76849361&wheelversion=$wheelversion&restaurant=$theRestaurant";
            $ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
			$output = curl_exec($ch); 
			curl_close($ch);
			}
		return array(1, "Done");
	}

	function savesponsor($sponsor) {
		error_log("SPONSOR " . $sponsor . " " . $theRestaurant);
		
		$theRestaurant = $this->restaurant;
		pdo_exec("UPDATE restaurant SET wheelsponsor = '$sponsor' WHERE restaurant = '$theRestaurant' limit 1");
		}
	
	function extractpart($offer) {
		
		$theRestaurant = $this->restaurant;
		return pdo_single_select("select offer, description, value, morder from wheeloffer WHERE restaurant = '$theRestaurant' and offer = '$offer' limit 1");
	}
		
	function savepart($offer, $desc, $value, $order) {
	
		$theRestaurant = $this->restaurant;
		pdo_exec("INSERT INTO wheeloffer (restaurant, offer, description, value, morder) VALUES ('$theRestaurant', '$offer', '$desc', '$value', '$order') ON DUPLICATE KEY UPDATE description='$desc', value='$value', morder='$order'");
		}
		
	function extraslices() {

		$theRestaurant = $this->restaurant;
		return pdo_multiple_select("select offer from wheeloffer WHERE restaurant = '$theRestaurant' order by morder");
	
	}
	
	function deletepart($offer) {
	
		//error_log("WHEEL DELETE : $offer");
		
		$theRestaurant = $this->restaurant;
		pdo_exec("delete from wheeloffer WHERE restaurant = '$theRestaurant' and offer = '$offer' limit 1");
		}
		
	function writescript($datawheel) {	
               
		$theRestaurant = $this->restaurant;
		$this->image = new WY_Images($theRestaurant);
		$tmp_filename = $this->image->tmpdir . $theRestaurant.'_wheelscript.js';
		$filename = 'wheelscript.js';

		if (!($fp = fopen($tmp_filename, 'w')))
			return;

		$wheeloffers = &$this->wheelconfoffer;	
		$dataAr = explode("|", $datawheel);
		fprintf($fp, "var wheelmsg = [");
		for($i = 1, $sep = ""; $i < 25; $i++, $sep = ",")
			fprintf($fp, "%s '%s' ", $sep, $dataAr[$i]);

		for($i = 1; $i < 25; $i++, $sep = ",") {
			$search = $dataAr[$i];
			if(($index = array_search($search, $this->wheeltitle)) !== false)	// do not forget !== as array search can return 0 as valid index, could be interpreted a false !
				fprintf($fp, "%s '%s' ", $sep, $wheeloffers[($index * 3) + 2]);
			else fprintf($fp, "%s '%s' ", $sep, "NOT FOUND: " . $search);
			}

		fprintf($fp, "];");
		fclose($fp);
                
		$this->image->l_move_file($tmp_filename, $filename, 'application/javascript', 'wheel');
		unlink($tmp_filename);
                
	}
	
	function wheeldescription() {
 	 
	   	$datawheel = $this->readWheel();
	
		$res_wheel = $res_desc = array();
    	if (substr($datawheel, 0, 5) == "VALID") {
			$wheeloffers = &$this->wheelconfoffer;
			$dataAr = explode("|", $datawheel);
	
			for ($i = 1;$i < 25; $i++)
				$res_wheel[] = $dataAr[$i];

			for($i = 1; $i < 25; $i++, $sep = ",") {
				$search = $dataAr[$i];
				if(($index = array_search($search, $this->wheeltitle)) !== false)	// do not forget !== as array search can return 0 as valid index, could be interpreted a false !
					$res_desc[] = $wheeloffers[($index * 3) + 2];
				else $res_desc[] = "NOT FOUND";
				}
			}
               		
		$data['wheel'] = $res_wheel;
		$data['desc'] = $res_desc;
        $data['wheelversion'] = $this->wheelversion;
		return $data;
	}
}

?>
