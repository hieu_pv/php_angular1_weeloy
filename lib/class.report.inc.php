<?php

require_once 'lib/class.tracking.inc.php';

class WY_Report {

    function __construct() {
        
    }

    public function getReworldReport() {
        $booking = new WY_Booking();
        $tracking = new WY_tracking();
        $gourmand = array(
            'direct_bookings' => $booking->getBookingByTracking('gourmand')
            , 'number_redirect' => $tracking->getNumberRedirect('gourmand')
            , 'indirect_bookings' => $booking->getBookingBySource('gourmand')
        );
        /* $marie_france = array(
          'direct_bookings' => $booking->getBookingByTracking('marie_france'),
          'number_of_redirect_marie-france' => $booking->getNumberRedirect('marie_france'),
          'indirect_bookings'=> $booking->getBookingBySource('marie_france'));
         * */

        $res = array('gourmand' => $gourmand
                //, 'marie-france'=>$marie_france
        );
        return $res;
    }

}

?>
