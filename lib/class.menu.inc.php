<?php

define("SPICY1", 1);
define("SPICY2", 2);
define("SPICY3", 4);
define("VEGI", 8);
define("CHEFRECO", 16);
define("HALAL", 32);

class WY_Menu {

    
    private $ID;
    private $restaurant;
    private $value;
    private $type;
    private $morder;
    private $price;
    private $currency;
    private $extraflag;
    
    public function __construct($theRestaurant) {
   		$this->restaurant = $theRestaurant;
    }
    
    public function getMenuCategories(){
        $sql = "SELECT * FROM menu_categorie WHERE restaurant ='$this->restaurant' AND status = 'active' ORDER BY type ASC, morder ASC";
        return pdo_multiple_select_array($sql);
    }
    
    public function getMenus(){
        $sql = "SELECT *, IF(`extraflag`&1>0,1,0) as spicy1, 
        IF(`extraflag`&2>0,1,0) as spicy2, 
        IF(`extraflag`&4>0,1,0) as spicy3,
        IF(`extraflag`&8>0,1,0) as vegi, 
        IF(`extraflag`&16>0,1,0) as chef_reco,
        IF(`extraflag`&32>0,1,0) as halal FROM menu WHERE restaurant ='$this->restaurant' AND status = 'active' ORDER BY type ASC, morder ASC";
        return pdo_multiple_select_array($sql);
    }
    
    
    function getCategorieList() {
        $sql = "SELECT * FROM  menu_categorie mc WHERE mc.restaurant ='$this->restaurant' AND mc.status = 'active' ORDER BY morder ASC";
        return pdo_multiple_select_array($sql);
    }
    
    function getCategorieItems($categorie_id) {
        $sql = "SELECT m.*, IF(`extraflag`&1>0,1,0) as spicy1,
        IF(`extraflag`&2>0,1,0) as spicy2, 
        IF(`extraflag`&4>0,1,0) as spicy3, 
        IF(`extraflag`&8>0,1,0) as vegi, 
        IF(`extraflag`&16>0,1,0) as chef_reco,
        IF(`extraflag`&32>0,1,0) as halal FROM menu m  WHERE m.menu_categorie_id = '$categorie_id' ORDER BY type ASC, morder ASC";
        return pdo_multiple_select_array($sql);
    }
    
    
    
    function getMenuList() {
        $sql = "SELECT m.*, mc.value as categorie FROM menu m, menu_categorie mc WHERE mc.id = m.menu_categorie_id AND m.restaurant ='$this->restaurant' AND m.status = 'active' AND mc.status = 'active' ORDER BY type ASC, morder ASC";
        return pdo_multiple_select($sql);
    }
    
    public function createMenu($title, $morder, $description, $items){
        
        $title = clean_input($title);
        $description = clean_input($description);
        $morder = clean_number($morder);
        
        $sql = "INSERT INTO menu_categorie (restaurant, value, description, type, morder, price, currency, extraflag, status) VALUES ('$this->restaurant', '$title', '$description', 'categorie', '$morder', '', '', '', 'active');";
        $categorie_id = pdo_insert($sql);

		$data = $categorie_id ;
		
        if(count($items) > 0) {
        	$labelAr = array('spicy1' => SPICY1, 'spicy2' => SPICY2, 'spicy3' => SPICY3, 'vegi' => VEGI, 'chef_reco' => CHEFRECO, 'halal' => HALAL); 
            foreach ($items as $item) {

				$id_item = clean_number($item['ID']);
				$item_title = clean_input($item['item_title']);
				$item_description = clean_input($item['item_description']);
				$morder = clean_number($item['morder']);
				$price = clean_number($item['price']);

            	if(empty($item['item_title']) || empty($item['item_description']))
            		continue;
            		
                $extraflag = 0;
                reset($labelAr);
                foreach($labelAr as $key => $value) {
                	if(!empty($item[$key]) && $item[$key] == '1' )
                		$extraflag |= $value; 
                	}            		

				$sql = "INSERT INTO menu (restaurant, item_title, item_description, type, morder, price, currency, extraflag, status, menu_categorie_id) VALUES ('$this->restaurant', '$item_title', '$item_description', 'item', '$morder', '$price', 'SGD', $extraflag, 'active', '$categorie_id');"; 
				$item_realid = pdo_insert($sql);
				$data .= "|" . $id_item . "=" . $item_realid;
            	}
        }
        return array(1, $data, "NEW");
    }
    
    public function updateMenu($id, $title, $description, $morder, $items) {        
        
        $data = $sep = "";
        
        $sql = "INSERT INTO menu_categorie (ID, restaurant, value, description, type, morder, status) VALUES ('$id', '$this->restaurant', '$title', '$description', 'categorie', '$morder', 'active') "
                . "ON DUPLICATE KEY UPDATE id=LAST_INSERT_ID(id), value = '$title', description = '$description', morder = '$morder', currency = 'SGD', extraflag = '1111', status = 'active' ";

        $categorie_id = pdo_insert($sql);
        	
        if(count($items) > 0) {
        	$labelAr = array('spicy1' => SPICY1, 'spicy2' => SPICY2, 'spicy3' => SPICY3, 'vegi' => VEGI, 'chef_reco' => CHEFRECO, 'halal' => HALAL); 
            foreach ($items as $item) {

				$id_item = clean_number($item['ID']);                               
				$item_title = clean_input($item['item_title']);
				$item_description = clean_input($item['item_description']);
				$morder = clean_number($item['morder']);
				$price = clean_number($item['price']);

				if(empty($item['item_title'])) {
					error_log("ID ITEM -" . "delete from menu where ID = '$id_item' and restaurant = '$this->restaurant' and menu_categorie_id = '$categorie_id' limit 1");
					if($id_item != '')
						pdo_exec("delete from menu where ID = '$id_item' and restaurant = '$this->restaurant' and menu_categorie_id = '$categorie_id' limit 1");
					continue;
					}
	
				$extraflag = 0;
				reset($labelAr);
				foreach($labelAr as $key => $value) {
					if(!empty($item[$key]) && $item[$key] == '1' )
						$extraflag |= $value; 
					}            		

				$id = (intval($id_item) > 0) ? $id_item : "";
				$sql = "INSERT INTO menu (ID, restaurant, item_title, item_description, type, morder, price, currency, extraflag, status, menu_categorie_id) VALUES ( '$id' , '$this->restaurant', '$item_title', '$item_description', 'item', '$morder', '$price', 'SGD', $extraflag, 'active', '$categorie_id')  
					   ON DUPLICATE KEY UPDATE id=LAST_INSERT_ID(id), item_title = '$item_title', item_description = '$item_description', morder = '$morder', price = '$price', currency ='SGD', extraflag = '$extraflag', status = 'active' "; 

				$item_realid = pdo_insert($sql);
				
				if(intval($id_item) < 0) {
					$data .= $sep . $id_item . "=" . $item_realid;
					$sep = "|";
					}
				}	
            }
        return array(1, $data, "UPDATE");
    }


    public function deleteMenu($ID){
        $sql = "DELETE FROM menu WHERE menu_categorie_id = '$ID'";
        pdo_exec($sql);

        $sql = "DELETE FROM menu_categorie WHERE id = '$ID'";
        pdo_exec($sql);
        return 1;
    }
      
        
}

?>
