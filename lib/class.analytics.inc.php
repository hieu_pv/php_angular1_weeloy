<?php

class WY_log {

   var $object; 
   var $iplong;
   var $frontend;
   var $month;
   var $count;
   var $msg;
   
	function __construct($where) {
		switch ($where) {
			case "website":
				$this->frontend = 1;
				break;
			case "api":
				$this->frontend = 2;
				break;
			case "backoffice":
				$this->frontend = 3;
				break;
			case "admin":
				$this->frontend = 4;
				break;
			 case "tracking":
				$this->frontend = 5;
				break;
			}
		}
	
                
	function LogEvent($user_id, $action, $target, $object, $other, $theDate="", $source = NULL, $user_token = NULL) {

		$frontend = 0;

		$frontend = $this->frontend;

		$headers = apache_request_headers();
		$real_client_ip  = $headers["X-Forwarded-For"];

		if(isset($real_client_ip) && $real_client_ip !='')
				$adresse_ip = $real_client_ip;
		else $adresse_ip = $_SERVER['REMOTE_ADDR'];
		//var_dump($_SERVER['REMOTE_ADDR']);die;
		if($adresse_ip == '::1'){
			$adresse_ip = '127.0.0.1';
		}
		$adresse_ip_tmp = explode(',',$adresse_ip);
		$adresse_ip = $adresse_ip_tmp[0];

		$iplong = ip2long($adresse_ip);
		
		
		if($source == NULL){
			if(empty($_SESSION['tracking'])){
				$_SESSION['tracking'] = filter_input(INPUT_COOKIE, 'weelredir', FILTER_SANITIZE_STRING);
			}
			$source = $_SESSION['tracking'];
		}

		if($user_token == NULL){
			if(empty($_SESSION['user_token'])){
				$_SESSION['user_token'] = filter_input(INPUT_COOKIE, 'weelusertoken',FILTER_SANITIZE_STRING);
			}
			$user_token = $_SESSION['user_token'];
		}
															  
		$qDate = ($theDate == "") ? "NOW()" : "'$theDate'";
		pdo_exec("INSERT INTO log_users(user_id, log_date, action, ip, target, object, other, front_end, source, user_token) VALUES ('$user_id', " . $qDate . ", '$action', '$iplong', '$target', '$object','$other', $frontend, '$source', '$user_token')",'dwh');
	}
	
	function readLog($object, $sdate, $edate) {
	
		$data = pdo_multiple_select("SELECT MONTH(log_date), COUNT(log_date) from log_users WHERE object = '$object' and log_date > '$sdate' and log_date < '$edate' GROUP BY MONTH(log_date)", "dwh");	

		foreach($data as $row) {
			$this->month[] = $row[0];
			$this->count[] = $row[1];
			}
	}
}

?>
