<?php

$allow_langue  = array('en' , 'fr' , 'cn' , 'hk' , 'th' , 'my' , 'jp' , 'kr' , 'id' , 'es' , 'it' , 'de' , 'ru', 'pt', 'vi');

class WY_Translation {
    
    var $msg;
    var $content;
    var $result;

    function __construct() {
    	}
    	
    function readprofile($email) {
    
    	$cls = new WY_Cluster;
    	
    	$cls->read('SLAVE', $email, 'TRANSLATION', 'TRANSLATION', '', '');
    	if($cls->result < 0 || count($cls->clustcontent) < 1 || empty($cls->clustcontent[0])) {
    		$this->result = -1;
    		$this->msg = "unable to find " . $email;
    		return array("", "");
    		}
    		 
    	$langue = "";
    	$title = array();
    	$contentAr = explode(";", $cls->clustcontent[0]);
    	if(substr($contentAr[0], 0, 7) == "langue=")
    		$langue = substr($contentAr[0], 7);
    	if(substr($contentAr[1], 0, 6) == "topic=") {
    		$titleAr = explode("|", substr($contentAr[1], 6));
    		foreach($titleAr as $topic)
    			$title[]["name"] = trim($topic);
    		}
		$this->result = 1;
		return array($langue, $title);
    	}

    function readzone($zone, $lang) {
		$this->content = pdo_multiple_select("SELECT zone, elem_name, object_name, lang, content from translation WHERE zone = '$zone' and lang = '$lang'");	
    	$this->result = (count($data) > 0) ? 1 : -1;
    	return $this->result;
    	}
    	
 	function readcontent($zone, $lang) {
 		global $allow_langue;
 		
 		if(!in_array($lang, $allow_langue)) {
 			$this->msg = "Invalid language " . $lang;
 			return $this->result;
 			}
 			
 		$this->content = array();
		$firstpass = pdo_multiple_select("SELECT zone, elem_name, object_name, lang, content from translation WHERE zone = '$zone' and lang = 'en' order by content");
		if(count($firstpass) < 1)
			return $this->result = -1;
		$i = 0; 
		foreach($firstpass as $data) {
			$elem_name = $data['elem_name'];
			$object_name = $data['object_name'];
			$row = pdo_single_select("SELECT content from translation WHERE zone = '$zone' and lang = '$lang' and elem_name = '$elem_name' and object_name = '$object_name' limit 1");	
			$content[$i]['translated'] = (count($row) > 0) ? $row['content'] : "";	
			$content[$i]['zone'] = $data['zone'];
			$content[$i]['elem_name'] = $data['elem_name'];
			$content[$i]['object_name'] = $data['object_name'];
			$content[$i]['content'] = $data['content'];
			$content[$i]['langue'] = $lang;
			$i++;
			}
		$this->content = $content;
		return $this->result = 1;
 		} 	

 	function writecontent($content, $lang) {
  		global $allow_langue;
 		
		//error_log("TRANSLATION WRITE " . print_r($content, true));
 		foreach($content as $row) {
			$elem_name = $row['elem_name'];
			$object_name = $row['object_name'];
			$zone = $row['zone'];
			$value = $row['translated'];
			$lang = $row['langue'];
 			if(!in_array($lang, $allow_langue)) 
 				continue;
 			$sql = "INSERT INTO translation (zone, elem_name, object_name, lang, content) VALUES ('$zone', '$elem_name', '$object_name', '$lang', '$value') "
                . "ON DUPLICATE KEY UPDATE id=LAST_INSERT_ID(id), zone = '$zone', elem_name = '$elem_name', object_name = '$object_name', lang = '$lang', content = '$value' ";
			pdo_exec($sql);
 			}
 		$this->result = 1;
 		}
 		
 	function newelement($elem_name, $content, $zone) {
 		
 		$elem_name = clean_input($elem_name);
 		$content = clean_input($content);
 		$zone = strtoupper(clean_input($zone));
 		$lang = 'en'; // for the time being
 		$object_name = "";
 		
 		if(empty($elem_name) || empty($content)) {
 			$this->msg = "invalid element: [ " . $elem_name . " ] => " . $content;
 			return $this->result = -1;
 			}
 		
 		$row = pdo_single_select("SELECT content from translation WHERE zone = '$zone' and lang = '$lang' and elem_name = '$elem_name' and object_name = '$object_name' limit 1");	
		if(count($row) > 0) {
 			$this->msg = "element already exist: [ " . $elem_name . " ] => " . $content;
 			return $this->result = -1;
 			}
			
		$sql = "INSERT INTO translation (zone, elem_name, object_name, lang, content) VALUES ('$zone', '$elem_name', '$object_name', '$lang', '$content') ";
		pdo_exec($sql);
		$this->msg = "element has been created: [ " . $elem_name . " ] => " . $content;
		return $this->result = 1;
 		}
}
