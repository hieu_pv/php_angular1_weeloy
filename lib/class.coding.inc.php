<?php

/**
 *	Richard Kefs (c) 2014
 *
 * 	Contains all classes definition for login session.
 *
*/


class WY_Coding {

	static protected $mycodeAr = array("Anguille12345678", "Brochets12345678", "Carrelet12345678", "Dragonnet1234567", "Esturgeon1234567", "Fletan1234567890", "Grenadier1234567", "Hareng1234567890", "Inanga1234567890", "Julienne12345678", "Kokopu1234567890", "Lamproie12345678", "Maquereau1234567", "Napilus123456789", "Ouananiche123456", "Piranha123456789", "Quillback1234567", "Rascasse12345678", "Sardine123456789", "Turbots123456789", "Uranoscope123456", "Vivaneau12345678", "Warbonnet1234567", "Xenotoca12345678", "Yellowtail123456", "zombie7890123456");

	function myencode($plaintext)
	{
		$keysAr = self::$mycodeAr;
		
		$plaintext = strlen($plaintext) . "|" . $plaintext;
		$cle = rand(0, 25);
		$cle = 0;				// myencode should be the same for password -> use md5 is even better
		$key = $keysAr[$cle];
	
		// the key should be random binary, use scrypt, bcrypt or PBKDF2 to
		// convert a string into a key is specified using hexadecimal
		//$key = pack('H*', $code);
	
		// show key size use either 16, 24 or 32 byte keys for AES-128, 192 and 256 respectively
		$key_size =  strlen($key);    
		$iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
		$iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
	
		// creates a cipher text compatible with AES (Rijndael block size = 128)
		// to keep the text confidential 
		// only suitable for encoded input that never ends with value 00h
		// (because of default zero padding)
		$ciphertext = mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $key, $plaintext, MCRYPT_MODE_CBC, $iv);

		// prepend the IV for it to be available for decryption
		$ciphertext = $iv . $ciphertext;
	
		// encode the resulting cipher text so it can be represented by a string
		$ciphertext = chr(65 + $cle) . base64_encode($ciphertext);
		return $ciphertext;
		}

	function mydecode($ciphertext)
	{
		$keysAr = self::$mycodeAr;
		$cle = ord($ciphertext[0]) - 65;
		if($cle > 0) 
			error_log("DECRYPT " . $ciphertext);
		$cle = 0;
		$key = $keysAr[$cle];
		$ciphertext = substr($ciphertext, 1);
			
		// === WARNING ===

		// Resulting cipher text has no integrity or authenticity added
		// and is not protected against padding oracle attacks.
	
		// --- DECRYPTION ---
	
		//$key = pack('H*', $code);
		$key_size =  strlen($key);    
		$iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);

		$ciphertext_dec = base64_decode($ciphertext);
	
		// retrieves the IV, iv_size should be created using mcrypt_get_iv_size()
		$iv_dec = substr($ciphertext_dec, 0, $iv_size);
	
		// retrieves the cipher text (everything except the $iv_size in the front)
		$ciphertext_dec = substr($ciphertext_dec, $iv_size);

		// may remove 00h valued characters from end of plain text
		$plaintext = mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key, $ciphertext_dec, MCRYPT_MODE_CBC, $iv_dec);

		// get rid of the padding => length . | . plaintexte
		for($start = 0; $start < 8; $start++)
			if($plaintext[$start] < '0' || $plaintext[$start] > '9')
				break;
		if($start >= 8 || $start == 0) return "";
		$len = intval(substr($plaintext, 0, $start));

		return substr($plaintext, $start+1, $len);		
	}
	
}

define('ENCRYPTION_KEY', 'd0a7e7997baaafcd55f4b5c32fffb87c');

function simple_encrypt($text) {
	return strtr(trim(base64_encode(mcrypt_encrypt(MCRYPT_BLOWFISH, ENCRYPTION_KEY, $text, MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_BLOWFISH, MCRYPT_MODE_ECB), MCRYPT_RAND))), " ="), "+/", "._");
}

function simple_decrypt($text) {
	return trim(mcrypt_decrypt(MCRYPT_BLOWFISH, ENCRYPTION_KEY, base64_decode(strtr($text, "._", "+/") . "="), MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_BLOWFISH, MCRYPT_MODE_ECB), MCRYPT_RAND)));
}


$tokenKey = array("Barracuda", "Barracudina", "Barramundi", "Barreleye", "Beachsalmon", "Beardfish", "Bigscale", "Billfish", "Bitterling", "Blackchin", "Blackfish", "Blobfish", "Blowfish", "Bluefish", "Bluegill", "Boafish", "Boarfish", "Bocaccio", "Bonefish", "Bonnetmouth", "Bonytongue", "Boxfish", "Bristlemouth", "Brotula", "Bullhead", "Butterflyfish");

function tokenize($text) { 
 	global $tokenKey;
 	
 	$index = rand(0, 25);
 	$text = $text . $tokenKey[$index]; 	
 	$text = strrev($text);
 	return simple_encrypt($text) . chr(65 + $index); 
 }
 	
function detokenize($text) { 
 	global $tokenKey;
 	
 	$ch = substr($text, strlen($text) - 1, 1);
  	$index = ord($ch) - 65;
	$text = substr($text, 0, strlen($text) - 1);
 	$text = simple_decrypt($text);
 	$text = strrev($text);
	
 	$pat = $tokenKey[$index];
 	if(substr($text, strlen($text) - strlen($pat)) != $pat)
 		return "BAD " . substr($text, 0, strlen($text) - strlen($pat)) . "-" . $pat . "-";
 	return substr($text, 0, strlen($text) - strlen($pat));
 }
 	

?>