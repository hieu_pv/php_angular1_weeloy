<?php

require_once("imagelib.inc.php");
$GLOBALS['picture_typelist'] = array('restaurant', 'event', 'logo', 'chef', 'menu', 'promotion', 'sponsor', 'profile');


class WY_Media {

    private $id;
    private $name;
    private $object_type;
    private $object_id;
    private $restaurant;
    private $media_type;
    private $path;
    private $status;
    private $description;
    
    private $globalshow;
    private $dirname;
    private $smallPicSize = '500/';
    private $mediumPicSize = '1024/';
    private $largePicSize = '1440/';
    
    var $tmpdir;
    var $s3;
    var $bucket;
    
    
    var $msg;
    //var $oldimg;
    //var $logo;
    //var $chef;
    //var $rPictures;  // restaurant pictures
    //var $ePictures;  // event pictures
    //var $storedPictures;  // event pictures
    //var $msg;  // event pictures
    //var $s3;
    //var $bucket;
    //var $object_type;

    function __construct($theRestaurant = '', $object_id = '', $type='restaurant') {
        if (AWS) {
            $this->globalshow = __S3HOST__ . __S3DIR__;
        } else {
            $this->globalshow = 'http://localhost:8888' . __SHOWDIR__;
        }
        if (AWS) {
            $this->globalmedia = __S3HOST__;
        } else {
            $this->globalmedia = __MEDIADIR__;
        }
        if (AWS) {
            switch ($type){
                case 'restaurant': $this->dirname = __S3DIR__ . "$theRestaurant/";break;
                
                case 'user': 
                    $unique_folder_name = md5($theRestaurant);
                    
                    $this->object_id = $object_id;
                    $this->dirname = __S3DIRUSER__ . "$object_id/$unique_folder_name/";
                    
                    break;
            }
        } else {
            switch ($type){
                case 'restaurant': $this->dirname = __UPLOADDIR__ . "$theRestaurant/";break;
                case 'user': 
                    $unique_folder_name = md5($theRestaurant);
                    $this->object_id = $object_id;
                    $this->dirname = __UPLOADDIRUSER__ . "$object_id/$unique_folder_name/";
                    break;
            }
        }

        $this->tmpdir = __TMPDIR__;
        $this->restaurant = $theRestaurant;
        
    }

    public function getName(){
        return $this->name;
    }
    public function getDescription(){
        return $this->description;
    }
    public function getPath(){
        return $this->path;
    }    
    public function constructPath(){
        return $this->path;
    }   
    public function getPartialPath() {
    	return $this->globalshow . $this->restaurant . '/';
		}

    public function getFullPath($size = 'small', $file_name=''){
        if(!empty($file_name)){$this->name = $file_name;}
        $path = '';
        if (AWS) {
            $random = rand(1, 5);
            switch ($random){
                case 1:
                    $this->globalshow = __S3HOST1__ . __S3DIR__;
                    break;
                case 2:
                    $this->globalshow = __S3HOST2__ . __S3DIR__;
                    break;
                case 3:
                    $this->globalshow = __S3HOST3__ . __S3DIR__;
                    break;
                case 4:
                    $this->globalshow = __S3HOST4__ . __S3DIR__;
                    break;
                case 5:
                    $this->globalshow = __S3HOST5__ . __S3DIR__;
                    break;
                default :
                    $this->globalshow = __S3HOST__ . __S3DIR__;
                    break;
            }
        } else {
            $this->globalshow = 'http://localhost:8888' . __SHOWDIR__;
        }
        switch ($size){
            case '100':
                    $path = $this->globalshow.$this->restaurant.'/100/'.$this->name;
                    break;
            case '140':
                    $path = $this->globalshow.$this->restaurant.'/140/'.$this->name;
                    break;
            case '180':
                    $path = $this->globalshow.$this->restaurant.'/180/'.$this->name;
                    break;
            case '270':
                    $path = $this->globalshow.$this->restaurant.'/270/'.$this->name;
                    break;
            case '300':
                    $path = $this->globalshow.$this->restaurant.'/300/'.$this->name;
                    break;
            case '325':
                    $path = $this->globalshow.$this->restaurant.'/325/'.$this->name;
                    break;
            case '360':
                    $path = $this->globalshow.$this->restaurant.'/360/'.$this->name;
                    break;
            case '450':
                    $path = $this->globalshow.$this->restaurant.'/450/'.$this->name;
                    break;
            case '500':
                    $path = $this->globalshow.$this->restaurant.'/500/'.$this->name;
                    break;
            case '600':
                    $path = $this->globalshow.$this->restaurant.'/600/'.$this->name;
                    break;
            case '700':
                    $path = $this->globalshow.$this->restaurant.'/700/'.$this->name;
                    break;
            case 'small': 
                $path = $this->globalshow.$this->restaurant.'/'.$this->name;
                break;
            case 'medium':
                    $path = $this->globalshow.$this->restaurant.'/'.$this->mediumPicSize.$this->name;
                    break;
            case 'large':
                $path = $this->globalshow.$this->restaurant.'/'.$this->largePicSize.$this->name;
                break;
        }
        
        if($path == ''){
            $path = $this->globalshow.$this->restaurant.'/'.$this->name;
        }
        return $path;
    }  
   
   public function getFullPathWheelvalue($wheelvalue){
        
       $path = '';
        if (AWS) {
            $random = rand(1, 5);
            switch ($random){
                case 1:
                    $this->globalmedia = __S3HOST1__ ;
                    break;
                case 2:
                    $this->globalmedia = __S3HOST2__ ;
                    break;
                case 3:
                    $this->globalmedia = __S3HOST3__ ;
                    break;
                case 4:
                    $this->globalmedia = __S3HOST4__ ;
                    break;
                case 5:
                    $this->globalmedia = __S3HOST5__ ;
                    break;
                default :
                    $this->globalmedia = __S3HOST__ ;
                    break;
            }
        } else {
            $this->globalmedia = 'http://localhost:8888' . __MEDIADIR__;
        }
      
        
        if($path == ''){
            $path = $this->globalmedia . "upload/wheelvalues/wheelvalue_" . $wheelvalue . ".png";
        }
        return $path;
    }  
 
    function showPicture($subdir, $name, $size = '') {

        if (!empty($size)) {
            $size = $size . '/';
        }

        if ($subdir == "images")
            return "$subdir/" . $size . $name;

        return $this->globalshow . "$subdir/" . $size . $name;
    }  

    public function setMedia($data){
        $this->id = $data['id'];
        $this->name = $data['name'];
        $this->object_type = $data['object_type'];
        $this->restaurant = $data['restaurant'];
        $this->media_type = $data['media_type'];
        $this->path = $data['path'];
        $this->status = $data['status'];
        $this->description = $data['description'];
    }
    
    function insertMedia($name, $object_type,$restaurant, $media_type, $path){
        $sql = "INSERT INTO media ('name','object_type','restaurant',media_type','path','status') VALUES ('$name', '$object_type','$restaurant', '$media_type', '$path', 'active')";
        return pdo_exec($sql);
    }
    
    function deleteMedia($restaurant, $name, $category){
    	global $picture_typelist;

		$log = $this->deleteImg($restaurant, $name, $category);
		if($log > 0) 
       		pdo_exec("DELETE FROM media WHERE name = '$name' and object_type = '$category' and restaurant = '$restaurant' limit 1");
		return $log;
    }

   
    function updateMedia($restaurant, $name, $category, $type, $description, $status, $morder) {
        pdo_exec("update media set object_type = '$category', description = '$description', status = '$status', morder = '$morder' where restaurant = '$restaurant' and name = '$name' and media_type = 'picture' limit 1");
        return 1;
    }

    function getRestaurantPictures($restaurant){
        $sql = "SELECT id, name, object_type, restaurant, media_type, path, description, status FROM media WHERE object_type = 'restaurant' && restaurant = '$restaurant' && media_type = 'picture' && status = 'active' ORDER BY morder";
        $data = pdo_multiple_select($sql);
        $tmpAr = array();
        foreach ($data as $d){
            $obj = new WY_Media;
            $obj->setMedia($d);
            $tmpAr[] = $obj;
            unset($obj);
        }
        return $tmpAr;
    }
    
    function getRestaurantGallery($restaurant){
        $sql = "SELECT id, name, object_type, restaurant, media_type, path, description, status FROM media WHERE object_type = 'restaurant' && restaurant = '$restaurant' && media_type = 'picture' && status = 'active' ORDER BY morder ";
        $data = pdo_multiple_select($sql);
        $tmpAr = array();
        foreach ($data as $d){
            $obj = new WY_Media;
            $obj->setMedia($d);
            $tmpAr[] = $obj;
            unset($obj);
        }
        return $tmpAr;
    }
    
    
    
    function checkRestaurantMedia($restaurant, $name){
    
        $data = pdo_multiple_select("SELECT name FROM media WHERE restaurant = '$restaurant' && name = '$name' limit 1");
        return (count($data) > 0);
		}
		
    function getRestaurantMedia($restaurant, $type){
    
    	if($type != "picture") $type = "picture";  // we will add other type
        $sql = "SELECT name, object_type, restaurant, media_type, path, description, morder, status FROM media WHERE restaurant = '$restaurant' && media_type = '$type' ORDER BY object_type, morder,name";
        $data = pdo_multiple_select($sql);
        $tmpAr = array();
        $k = 0;
        foreach ($data as $d){
			$tmpAr[$k]['name'] = $d['name'];
			$tmpAr[$k]['morder'] = $d['morder'];
			$tmpAr[$k]['object_type'] = $d['object_type'];
			$tmpAr[$k]['restaurant'] = $d['restaurant'];
			$tmpAr[$k]['media_type'] = $d['media_type'];
			$tmpAr[$k]['path'] = $d['path'];
			$tmpAr[$k]['description'] = $d['description'];
			$tmpAr[$k]['status'] = $d['status'];
			$k++;
    		}
    	return $tmpAr;
    	}
    
    function setRestaurant($restaurant){
        $this->restaurant = $restaurant;
    	}
    
    function sponsorlist($restaurant) {
        $sql = "SELECT name FROM media WHERE object_type = 'sponsor' && restaurant = '$restaurant' && media_type = 'picture' && status = 'active' ";
        $data = pdo_multiple_select($sql);
        $tmpAr = array();

        foreach ($data as $d){
            $tmpAr[] = $d['name'];
        }
        return $tmpAr;
    }
    	
    function getRestaurantPictureNames($restaurant){
        $sql = "SELECT name FROM media WHERE object_type = 'restaurant' && restaurant = '$restaurant' && media_type = 'picture' && status = 'active' ";
        $data = pdo_multiple_select($sql);
        $tmpAr = array();

        foreach ($data as $d){
            $tmpAr[] = $d['name'];
        }
        return $tmpAr;
    }
    
    function getRestaurantPictureType($restaurant, $type){
        $sql = "SELECT name FROM media WHERE object_type = '$type' && restaurant = '$restaurant' && media_type = 'picture' && status = 'active' ";
        $data = pdo_multiple_select($sql);
        $tmpAr = array();

        foreach ($data as $d){
            $tmpAr[] = $d['name'];
        }
        return $tmpAr;
    }
       
    function getEventFullPath($restaurant, $pic_name, $size = 'small'){
        $path = $this->globalshow.$restaurant.'/'.$pic_name;
        return $path;
    } 
    function getEventPictureNames($restaurant){
        $sql = "SELECT name FROM media WHERE object_type = 'event' && restaurant = '$restaurant' && media_type = 'picture' && status = 'active' ";
        $data = pdo_multiple_select($sql);
        $tmpAr = array();

        foreach ($data as $d){
            $tmpAr[] = $d['name'];
        }
        return $tmpAr;
    }
    
    function getDefaultPicture($restaurant, $size = 'small'){
        $sql = "SELECT id, name, object_type, restaurant, media_type, path, status, description FROM media 
                WHERE object_type = 'restaurant' && restaurant = '$restaurant' && media_type = 'picture' && status = 'active' ORDER BY morder, name ASC LIMIT 1 ";
        
        $data = pdo_single_select($sql);
        $this->setMedia($data);
        return $this->getFullPath($size);
    }   
    function getRandomPicture($restaurant, $size = 'small'){
        //$sql = "SELECT id, name, object_type, restaurant, media_type, path, status, description FROM media 
        //        WHERE object_type = 'restaurant' && restaurant = '$restaurant' && media_type = 'picture' && status = 'active' ORDER BY morder, name ASC LIMIT 1 ";
        
        $sql = "SELECT id, name, object_type, restaurant, media_type, path, status, description FROM media 
                WHERE object_type = 'restaurant' && restaurant = '$restaurant' && media_type = 'picture' && status = 'active' ORDER BY RAND() LIMIT 1 ";
        $data = pdo_single_select($sql);
        $this->setMedia($data);
        return $this->getFullPath($size);
    }       
    function getEventDefaultPicture($event, $size = 'small'){
        $sql = "SELECT id, name, object_type, restaurant, media_type, path, status, description FROM media 
                WHERE object_type = 'restaurant' && restaurant = '$event' && media_type = 'picture' && status = 'active' ORDER BY morder, name ASC LIMIT 1 ";
        
        $data = pdo_single_select($sql);
        $this->setMedia($data);
        return $this->getFullPath($size);
    }   

    function getWidgetDefaultPicture($restaurant, $size = 'small'){
        $sql = "SELECT id, name, object_type, restaurant, media_type, path, status, description FROM media 
                WHERE object_type = 'restaurant' && restaurant = '$restaurant' && media_type = 'picture' && status = 'active' ORDER BY RAND() LIMIT 1 ";
        
        $data = pdo_single_select($sql);
        $this->setMedia($data);
        return $this->getFullPath($size);
    }  
    
    function getLogo($restaurant){
        $sql = "SELECT id, name, object_type, restaurant, media_type, path, status, description FROM media 
                WHERE object_type = 'logo' && restaurant = '$restaurant' && media_type = 'picture' && status = 'active' ORDER BY morder LIMIT 1 ";
        $data = pdo_single_select($sql);
        $this->setMedia($data);
        return $this->getFullPath('small');
    }
    
    function getChef($restaurant){
        $sql = "SELECT id, name, object_type, restaurant, media_type, path, status, description FROM media 
                WHERE object_type = 'chef' && restaurant = '$restaurant' && media_type = 'picture' && status = 'active' ORDER BY morder LIMIT 1 ";
        $data = pdo_single_select($sql);
        $this->setMedia($data);
        return $this->getFullPath('small');
    }
    function getWheel($restaurant){
        return $this->globalshow.$restaurant.'/wheel.png';
    }  
    function getWheelValue($value){
        return $this->globalshow.'../wheelvalues/wheelvalue_'.$value.'.png';
        
    }    

    function getUserProfilePicture($user){
        $sql = "SELECT id, name, object_type, restaurant, media_type, path, status, description FROM media 
                WHERE object_type = 'profile' && restaurant = '$user' && media_type = 'picture' && status = 'active' ORDER BY morder ASC, id DESC LIMIT 1 ";

        $data = pdo_single_select($sql);

        if(!$data){
            return NULL;
        }
        $this->setMedia($data);
        $path = $data['path'].$this->name;
        return $path;
    }

    function uploadImage($restaurant, $category, $type = 'restaurant') {
        
    error_log("uploadImage = " . $restaurant);

        $log = 0;
        $msg = "Unable to Upload image. Try again";
        
        if (isset($_POST["Stage"]) && $restaurant != "")
            if ($_POST["Stage"] == "Load") {
                
                list($log, $msg) = $this->modifyRecord($category);
                
                if (intval($log) <= 0)
                    return array("-1", $msg);

                $filename = $msg;
                
                $Ar[0] = $this->addImg($restaurant, $filename, $category, $type);
                $Ar[1] = $this->msg;
                
                return $Ar;
            }

        return array(-1, "Unknow Error");
    }

    
    function modifyRecord($category) {
				
    	$file = $_FILES['file'];
        
        $allowedExts = array("gif", "jpeg", "jpg", "png");
        $allowedTypes = array("image/gif", "image/jpeg", "image/jpg", "image/png");
        $extension = strtolower(pathinfo($_FILES["file"]["name"], PATHINFO_EXTENSION));
        
        if ((!in_array($file['type'], $allowedTypes)) || ($_FILES["file"]["size"] >= MAX_FILE_SIZE) || !in_array($extension, $allowedExts))
            return array("-1", "Invalid file " . $file['name'] . " type = " . $file['type'] . " size = " . $file['size'] . " MAXSIZE " . MAX_FILE_SIZE);

        
        $filename = str_replace(' ','_',$file['name']);
        
        if ($file["error"] > 0)
            return array("-1", "Return Code: " . $file['error'] . "<br>");

        if ($this->l_file_exists($filename))
            return array("-1", $filename . " already exists. ");

        $tmp_image = "l_" . $filename;
        
        
        $this->l_move_uploaded_file($file['tmp_name'], $tmp_image);
        $scope = ($category != "sponsor") ? NULL : "sponsor";
        
        error_log("CATEGORIE " . $category);
        if($category != "sponsor")
        	$this->l_resizecompress($tmp_image, $filename);
        else $this->l_rename($tmp_image, $filename, $scope);

        $cflg = "";
        
        if ($this->l_file_exists($filename)) {
            
            $this->l_unlink($tmp_image, 'tmp_image');
            $cflg = "(cr) ";
        } else{
            $this->l_rename($tmp_image, $filename);
        }

        return array("1", $filename);
    }
    
    function renameMedia($restaurant, $name, $newname, $type_media, $category) {
        
       
        
    	if($type_media != "picture")  	
	        return array(-1, "Can only rename pictures");    		

        $log = $this->renameImg($restaurant, $name, $newname, $category);
        if($log > 0) 
                pdo_exec("UPDATE media SET name = '$newname' WHERE object_type = '$category' && restaurant = '$restaurant' && media_type = '$type_media' && name = '$name' limit 1");
         return $log;
    	}
    	
    function renameImg($restaurant, $image, $newimage, $type) {
    	global $picture_typelist;
    	
        
     
        if(!in_array($type, $picture_typelist))	// do not forget !== as array search can return 0 as valid index, could be interpreted a false !
        return $this->retstatus(-1, "Invalid Category  image " . $type . " for " . $image);

        if (strtolower(pathinfo($image, PATHINFO_EXTENSION)) != strtolower(pathinfo($newimage, PATHINFO_EXTENSION)))
            return $this->retstatus(-1, 'Cannot rename file with different extension. Old ' . $image . ' new ' . $newimage . ' for ' . $this->restaurant);
         
        if ($this->checkRestaurantMedia($restaurant, $image) == false){
            return $this->retstatus(-1, 'Image does not exist ' . $image . ' for ' . $restaurant);
        }
        if ($this->checkRestaurantMedia($restaurant, $newimage) == true){
            return $this->retstatus(-1, 'Image already exist ' . $image . ' for ' . $restaurant);
        }

        if ($this->l_file_exists($image) == false || $this->l_file_exists($newimage) == true){
            return $this->retstatus(-1, 'File doest not exists or duplicate. Old ' . $image . ' new ' . $newimage . ' for ' . $restaurant);
        }

        $this->l_rename($image, $newimage);
        
        $sql = "UPDATE media SET name = '$newimage' WHERE object_type = '$type' && restaurant = '$restaurant' && media_type = 'picture' && name = '$image'";
        
        pdo_exec($sql);
        return $this->retstatus(1, "The image has been renamed " . $newimage);
        
    }

    function addImg($target_object, $image, $category, $type) {
    	global $picture_typelist;
    	
        if ($image == "" || !in_array($category, $picture_typelist))
            return $this->retstatus(-1, "Invalid Category  or empty image");

        if ($this->l_file_exists($image) == false)
            return $this->retstatus(-1, "The file has not been uploaded " . $image);
        
        if ($this->checkRestaurantMedia($target_object, $image) == true)
             return $this->retstatus(-1, "The image name already exists");        
    
        if($type == 'user'){
            $key = $this->object_id .'/'.md5($target_object);
            
            pdo_exec("INSERT INTO media (object_type, restaurant, media_type, name, path, status) VALUES ('$category', '$target_object', 'picture', '$image', '/upload/user/$key/','active') ");
        }else{
            pdo_exec("INSERT INTO media (object_type, restaurant, media_type, name, path, status) VALUES ('$category', '$target_object', 'picture', '$image', '/upload/restaurant/$target_object/','active') ");
        }
               
        return $this->retstatus(1, "The image '" . $image . "' has been uploaded ($category)");
    }

    function deleteImg($restaurant, $image, $category) {
    	global $picture_typelist;
    	    	
        if ($image == "" || !in_array($category, $picture_typelist))
            return $this->retstatus(-1, "Empty image or wrong Type " . $image);

		$arg = ($category != "sponsor") ? NULL : "sponsor";
        if ($this->l_file_exists($image) == true){
            $this->l_unlink($image, $arg);
        }

        if ($this->checkRestaurantMedia($restaurant, $image) == false)
             return $this->retstatus(-1, "The image does not exist");

        pdo_exec("DELETE FROM media WHERE object_type = '$category' && restaurant = '$restaurant' && media_type = 'picture' && name = '$image'");

        return $this->retstatus(1, "The image has been deleted " . $image);
    }
    function getWheelSource($wheelvalue) {
        return $this->getFullPathWheelvalue($wheelvalue);
    }
    
    function getVideoRestaurantDishes($restaurant){
            $link = '';
            if($restaurant == 'SG_SG_R_MarcoMarco'){
                 $link = 'https://player.vimeo.com/video/128461499';
             }
             if($restaurant == 'SG_SG_R_NorthBorder' || $restaurant == 'NorthBorder'){
                 $link = 'https://player.vimeo.com/video/128461553';
             }
             if($restaurant == 'SG_SG_R_31BarKitchen'){
                 $link = 'https://player.vimeo.com/video/128464692';
             }
             if($restaurant == 'SG_SG_R_TheBeast'){
                 $link = 'https://player.vimeo.com/video/128461554';
             }             
             if($restaurant == 'SG_SG_R_WindowOnThePark'){
                 $link = 'https://player.vimeo.com/video/128461580';
             }                 
             if($restaurant == 'SG_SG_R_SabioTapas'){
                 $link = 'https://player.vimeo.com/video/128461759';
             }
             if($restaurant == 'SG_SG_R_SabioByTheSea'){
                 $link = 'https://player.vimeo.com/video/128461098';
             }
             if($restaurant == 'SG_SG_R_Shutters'){
                 $link = 'https://player.vimeo.com/video/128461760';
             } 
             if($restaurant == 'SG_SG_R_BumboRumClub'){
                 $link = 'https://player.vimeo.com/video/128464688';
             }
             if($restaurant == 'SG_SG_R_JewelCoffee'){
                 $link = 'https://player.vimeo.com/video/128461093';
             } 
             if($restaurant == 'SG_SG_R_Forlino'){
                 $link = 'https://player.vimeo.com/video/128464689';
             }
             if($restaurant == 'SG_SG_R_MomoCafe'){
                 $link = 'https://player.vimeo.com/video/128461552';
             } 
            if($restaurant == 'SG_SG_R_DepartmentOfCaffeine'){
                 $link = 'https://player.vimeo.com/video/128464693';
             }
             if($restaurant == 'SG_SG_R_BBar'){
                 $link = 'https://player.vimeo.com/video/128464691';
             }     
             if($restaurant == 'SG_SG_R_JcCoffeeHouzz'){
                 $link = 'https://player.vimeo.com/video/128461500';
             }                
             if($restaurant == 'SG_SG_R_Pisco'){
                 $link = 'https://player.vimeo.com/video/128461097';
             }  
             if($restaurant == 'SG_SG_R_Match'){
                 $link = 'https://player.vimeo.com/video/128461095';
             }  
             if($restaurant == 'SG_SG_R_Leuphoriz'){
                 $link = 'https://player.vimeo.com/video/128461094';
             }  
             if($restaurant == 'SG_SG_R_TierBar'){
                 $link = 'https://player.vimeo.com/video/128456955';
             } 
             
             if($restaurant == 'TH_BK_R_ChiliHip'){
                 $link = 'https://player.vimeo.com/video/130830091';
             } 
             if($restaurant == 'TH_BK_R_Chyna'){
                 $link = 'https://player.vimeo.com/video/130830092';
             } 
             if($restaurant == 'TH_BK_R_LeDanang'){
                 $link = 'https://player.vimeo.com/video/130830093';
             } 
             if($restaurant == 'TH_BK_R_StationCafe'){
                 $link = 'https://player.vimeo.com/video/130830094';
             } 

             
             return $link;
    }
    
    function l_rename($oname, $fname, $scope = NULL, $object_type = '') {
        if (AWS) {
            require_once("conf/conf.s3.inc.php");
            if($object_type == 'wheel'){$cache_value_browser = "1";$cache_value_cdn = "1";}else{$cache_value_browser = "1296000";$cache_value_cdn = "86400";}
            if($scope == NULL){
				$this->getS3()->copyObject($this->bucket, $this->dirname . '1440/' . $oname, $this->bucket, $this->dirname . '1440/' . $fname, S3::ACL_PUBLIC_READ, array("Cache-Control" => "max-age=$cache_value_browser, s-maxage=$cache_value_cdn", "Content-Type" => "image/jpeg"), array("Cache-Control" => "max-age=$cache_value_browser, s-maxage=$cache_value_cdn", "Content-Type" => "image/jpeg"));
				//$this->l_unlink($oname);
				$this->getS3()->copyObject($this->bucket, $this->dirname . '1024/' . $oname, $this->bucket, $this->dirname . '1024/' . $fname, S3::ACL_PUBLIC_READ, array("Cache-Control" => "max-age=$cache_value_browser, s-maxage=$cache_value_cdn", "Content-Type" => "image/jpeg"), array("Cache-Control" => "max-age=$cache_value_browser, s-maxage=$cache_value_cdn", "Content-Type" => "image/jpeg"));
				//$this->l_unlink($oname);
				$this->getS3()->copyObject($this->bucket, $this->dirname . '500/' . $oname, $this->bucket, $this->dirname . '500/' . $fname, S3::ACL_PUBLIC_READ, array("Cache-Control" => "max-age=$cache_value_browser, s-maxage=$cache_value_cdn", "Content-Type" => "image/jpeg"), array("Cache-Control" => "max-age=$cache_value_browser, s-maxage=$cache_value_cdn", "Content-Type" => "image/jpeg"));
				}
            $this->getS3()->copyObject($this->bucket, $this->dirname . $oname, $this->bucket, $this->dirname . $fname, S3::ACL_PUBLIC_READ, array("Cache-Control" => "max-age=$cache_value_browser, s-maxage=$cache_value_cdn", "Content-Type" => "image/jpeg"), array("Cache-Control" => "max-age=$cache_value_browser, s-maxage=$cache_value_cdn", "Content-Type" => "image/jpeg"));
            return $this->l_unlink($oname);
        } else {

            if($scope == NULL){
				rename($this->dirname . '1440/' . $oname, $this->dirname . '1440/' . $fname);
				rename($this->dirname . '1024/' . $oname, $this->dirname . '1024/' . $fname);
				rename($this->dirname . '500/' . $oname, $this->dirname . '500/' . $fname);
				}
            return rename($this->dirname . $oname, $this->dirname . $fname);
        }
    }

    function l_file_exists($fname) {
        
        if (AWS) {
            require_once("conf/conf.s3.inc.php");
            return $this->getS3()->getObjectInfo($this->bucket, $this->dirname . $fname);
        } else {
            return file_exists($this->dirname . $fname);
        }
    }
    
    function l_unlink($fname, $scope = NULL) {
        if (AWS) {
            require_once("conf/conf.s3.inc.php");
            if($scope == NULL){
                $this->getS3()->deleteObject($this->bucket, $this->dirname . '1440/' . $fname);
                $this->getS3()->deleteObject($this->bucket, $this->dirname . '1024/' . $fname);
                $this->getS3()->deleteObject($this->bucket, $this->dirname . '500/' . $fname);
            }
            return $this->getS3()->deleteObject($this->bucket, $this->dirname . $fname);
        } else {
            if($scope == NULL){
                unlink($this->dirname . '1440/' . $fname);
                unlink($this->dirname . '1024/' . $fname);
                unlink($this->dirname . '500/' . $fname);
            }
            return unlink($this->dirname . $fname);
        }
    }

    
    function l_move_uploaded_file($name, $fname, $content_type = 'image/jpeg', $object_type = '') {
        if (AWS) {
            require_once 'conf/conf.s3.inc.php';
            if($object_type == 'wheel'){$cache_value_browser = "1";$cache_value_cdn = "1";}else{$cache_value_browser = "1296000";$cache_value_cdn = "86400";}
            $this->getS3()->putObjectFile($name, $this->bucket, $this->dirname . $fname, S3::ACL_PUBLIC_READ, array("Cache-Control" => "max-age=$cache_value_browser, s-maxage=$cache_value_cdn", "Content-Type" => "$content_type"), array("Cache-Control" => "max-age=$cache_value_browser, s-maxage=$cache_value_cdn", "Content-Type" => "$content_type"));
            return unlink($name);
        } else {
            return move_uploaded_file($name, $this->dirname . $fname);
        }
    }

    
    function l_resizecompress($oname, $fname) {
        if (AWS) {
            require_once("conf/conf.s3.inc.php");

            $onameTmp = $this->tmpdir . $this->restaurant . $oname;
            $fnameTmp = $this->tmpdir . $this->restaurant . $fname;

            if(strpos($onameTmp,'/api/') !== false){
                $onameTmp = str_replace('/api/', '/', $onameTmp);
            }
            if(strpos($fnameTmp,'/api/') !== false){
                $fnameTmp = str_replace('/api/', '/', $fnameTmp);
            }            
            if ($this->getS3()->getObject($this->bucket, $this->dirname . $oname, $onameTmp)) {
                if (resizecompress($onameTmp, $fnameTmp, 1440)) {
                    $this->l_move_uploaded_file($fnameTmp, '1440/' . $fname);
                }
                if (resizecompress($onameTmp, $fnameTmp, 1024)) {
                    $this->l_move_uploaded_file($fnameTmp, '1024/' . $fname);
                }
                if (resizecompress($onameTmp, $fnameTmp, 500)) {
                    $this->l_move_uploaded_file($fnameTmp, '500/' . $fname);
                }
                if (resizecompress($onameTmp, $fnameTmp)) {
                    $this->l_move_uploaded_file($fnameTmp, $fname);
                }
                if(file_exists($oname)){
                    $this->l_unlink($oname);
                }
                if(file_exists($onameTmp)){
                    unlink($onameTmp);
                }
                if(file_exists($fnameTmp)){
                    unlink($fnameTmp);
                }
                return true;
            }
            return false;
        } else {
            
            $this->l_folder_exist($this->dirname . '1440/');
            $this->l_folder_exist($this->dirname . '1024/');
            $this->l_folder_exist($this->dirname . '500/');
            resizecompress($this->dirname . $oname, $this->dirname . '1440/' . $fname, 1440);
            resizecompress($this->dirname . $oname, $this->dirname . '1024/' . $fname, 1024);
            resizecompress($this->dirname . $oname, $this->dirname . '500/' . $fname, 500);
            return resizecompress($this->dirname . $oname, $this->dirname . $fname);
        }
    }

    function l_resize($oname, $fname, $size) {
        if (AWS) {
            require_once("conf/conf.s3.inc.php");

            $onameTmp = $this->tmpdir . $this->restaurant . $oname;
            $fnameTmp = $this->tmpdir . $this->restaurant . $fname;

            if(strpos($onameTmp,'/api/') !== false){
                $onameTmp = str_replace('/api/', '/', $onameTmp);
            }
            if(strpos($fnameTmp,'/api/') !== false){
                $fnameTmp = str_replace('/api/', '/', $fnameTmp);
            }            
            if ($this->getS3()->getObject($this->bucket, $this->dirname . $oname, $onameTmp)) {
                if (resizecompress($onameTmp, $fnameTmp, $size, true)) {
                    $this->l_move_uploaded_file($fnameTmp, $size.'/' . $fname);
                }
                if(file_exists($oname)){
                    $this->l_unlink($oname);
                }
                if(file_exists($onameTmp)){
                    unlink($onameTmp);
                }
                if(file_exists($fnameTmp)){
                    unlink($fnameTmp);
                }
                return true;
            }
            return false;
        } else {
            $this->l_folder_exist($this->dirname . $size.'/');
            return resizecompress($this->dirname . $oname, $this->dirname . $size.'/' . $fname, $size);
        }
    }

    
    function l_folder_exist($path = NULL){
        if(empty($path)){$path = $this->dirname;};
        if (AWS) {
             return true;
         }else{
             $res = (file_exists($path)!== true);
              if($res){
                  mkdir($path, 0777, true);
              }
         }
    }
    
    function retstatus($log, $mess) {
        $this->msg = $mess;
        return $log;
    }
    
    function getS3() {

        if (!isset($this->s3)) {
            $this->bucket = awsBucket;
            $this->s3 = new S3(awsAccessKey, awsSecretKey);
            //$this->s3->putBucket($this->bucket, S3::ACL_PUBLIC_READ);
        }
        return $this->s3;
    }
}

?>