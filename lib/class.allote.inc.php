<?php

class WY_Allote {

   var $restaurant;
   var $mealduration;
   var $perpax;				// standard allotement is on table unit. Perpax set it on person
   var $openDay;   
   var $openLunch; 
   var $openDinner; 
   var $openDayHours;   
   var $openWeekHours; 
   var $aDay;
   var $result;   

   function __construct($theRestaurant) {
   		$this->restaurant = $theRestaurant;
   		$this->mealduration = 3;
   		$this->perpax = 0;
     }

	function getOpenHour($ohvalue, $weekday) {
				
	 	if(empty($ohvalue)) 
	 		return $this->result = -1;

	    $valAr = explode("|||", $ohvalue);
	    if(count($valAr) < 28) 
	 		return $this->result = -1;
		
		for($i = 0; $i < 7; $i++) {
			$k = ($i * 2);
			$this->openWeekHours[$i][0] = intval($valAr[$k]);
			$this->openWeekHours[$i][1] = intval($valAr[$k+1]);
			$this->openWeekHours[$i][2] = intval($valAr[$k+14]);
			$this->openWeekHours[$i][3] = intval($valAr[$k+15]);
			}
			
		$this->openDay = $this->openLunch = $this->openDinner = -1;
		$this->openDayHours[0] = $this->openDayHours[1] = $this->openDayHours[2] = $this->openDayHours[3] = -1;
		if($weekday >=0 && $weekday < 7) {
			for($i = 0; $i < 4; $i++)
				$this->openDayHours[$i] = $this->openWeekHours[$weekday][$i];
			$this->openLunch = ($this->openDayHours[0] < $this->openDayHours[1]) ? 1 : 0;
			$this->openDinner = ($this->openDayHours[2] < $this->openDayHours[3]) ? 1 : 0;
			$this->openDay = $this->openLunch + $this->openDinner;
			}
		return $this->result = 1;
		}
	
		
 	private function setUpRange($col, $a, $b) { $start = floor($a * 0.5) . ':' . ($a % 2) * 3 . '0'; $end = floor($b * 0.5); if($end >= 24) $end = "0" . ($end - 24); $end = $end . ':' . ($b % 2) * 3 . '0';  return $start . " - " . $end; } 
	
 	function templateAlloteOpenHour($ohvalue, $no_html = false) {
 
  	    $array_data = array();
 	    $tmp_car = array('<br>','&nbsp;', '&nbsp;', ' ');
		$weekday = array("Sunday", "Monday", "Tuesday", "Wednesday &nbsp;&nbsp;&nbsp;", "Thursday", "Friday", "Saturday");

		$this->getOpenHour($ohvalue, $weekday);
 		if($this->result < 0)
 			return ($no_html) ? $array_data : "";
 			
        $template = "<table class='table table-striped' style='font-family:Roboto;font-size:14px;font-weight:bold'>";
        $template .= "<th>Day</th><th>Lunch</th><th>Dinner</th>";
		for($kk = 0; $kk < 7; $kk++) {
			$col = $kk * 2;
        	$ll = $this->setUpRange($kk, $this->openWeekHours[$kk][0], $this->openWeekHours[$kk][1]);
        	$dd = $this->setUpRange($kk+7, $this->openWeekHours[$kk][2], $this->openWeekHours[$kk][3]);
        	if($this->openWeekHours[$kk][0] == $this->openWeekHours[$kk][1]) $ll = "closed<br>&nbsp;";        	
        	if($this->openWeekHours[$kk][2] == $this->openWeekHours[$kk][3]) $dd = "closed<br>&nbsp;";        	
			$template .= "<tr><td>" . $weekday[$kk] . "</td><td>$ll</td><td>$dd</td></tr>";                
			$array_data[$kk] = array('day'=>str_replace($tmp_car, '',$weekday[$kk]),'lunch'=>str_replace($tmp_car, '',$ll),'dinner'=>str_replace($tmp_car, '',$dd));
            }
        $template .= "</table>";
        
        return ($no_html) ? $array_data : $template;
 		}


	function consoBookingAllote($theRestaurant, $aDate) {
	
		$book = array();
		$booking = new WY_Booking();
		if($aDate == "") $data = $booking->getDateBooking($theRestaurant);
		else $data = $booking->getThatDayBooking($theRestaurant, $aDate);
		
		if(count($data) < 1)
			return $book;
			
		$perpax = $this->perpax;	
		$mealduration = $this->mealduration; //3 slots per repas
		foreach ($data as $row) {
            $bdata = intval($row['ndays']);
            $slottime = (intval(substr($row['rtime'], 0, 2)) * 2) + (intval(substr($row['rtime'], 3, 1)) / 3);
			for($i = 0; $i < $mealduration; $i++) {
				if(!isset($book[$bdata][$slottime + $i]))
					$book[$bdata][$slottime + $i] = 0;
				$inc = ($perpax) ? intval($row['cover']) : 1;
				$book[$bdata][$slottime + $i] += $inc;
				}
			}
		
		return $book;
	}
	
	function readAlloteRestaurant($theRestaurant, $type) {
	
		if($type != "lunch" && $type != "dinner")
			return "";
			
	 	$data = pdo_single_select("SELECT ID, restaurant, type, allotment from allotment where restaurant = '$theRestaurant' and type = '$type' limit 1");
		if(count($data) <= 0) {
			for($value = $sep = "", $i = 0; $i < 366; $i++, $sep="|") $value .= $sep . 5;
			pdo_exec("insert into allotment (restaurant, type, allotment) values ('$theRestaurant', '$type', '$value') ");
 			$data = pdo_single_select("SELECT ID, restaurant, type, allotment from allotment where restaurant = '$theRestaurant' and type = '$type' limit 1");
			if(count($data) <= 0) {
				error_log("saveALLOTE unable to create $type data");
				return -1;
				}
			}
		return $data['allotment'];
	}
	
 	function getAlloteRestaurant($theRestaurant) {

		$data['lunch'] = $this->readAlloteRestaurant($theRestaurant, 'lunch');
		$data['dinner']  = $this->readAlloteRestaurant($theRestaurant, 'dinner');
		
		return $data;
 	}
 
 	function saveAlloteRestaurant($theRestaurant, $start, $size, $newlunchdata, $newdinnerdata) {
 	
		$oldlunchdata = $this->readAlloteRestaurant($theRestaurant, 'lunch');
		if($oldlunchdata == "")
			return -1;
		$oldlunchAr = explode("|", $oldlunchdata);
		$newlunchAr = explode("|", $newlunchdata);
		for($i = 0, $k = $start; $i < $size; $i++, $k++)
			$oldlunchAr[$k] = $newlunchAr[$i];
		$dataStr = implode("|", $oldlunchAr);
        pdo_exec("Update allotment set allotment='$dataStr' where restaurant = '$theRestaurant' and type = 'lunch' limit 1");
		
		
		$olddinnerdata = $this->readAlloteRestaurant($theRestaurant, 'dinner');
		if($olddinnerdata == "")
			return -1;
		$olddinnerAr = explode("|", $olddinnerdata);
		$newdinnerAr = explode("|", $newdinnerdata);
		for($i = 0, $k = $start; $i < $size; $i++, $k++)
			$olddinnerAr[$k] = $newdinnerAr[$i];
		$dataStr = implode("|", $olddinnerAr);
        pdo_exec("Update allotment set allotment='$dataStr' where restaurant = '$theRestaurant' and type = 'dinner' limit 1");
		
		return 1;		
 	}
 
 	function ComputeOpenToday($ohvalue) {
 	
    	$openStr = "Open Today";
    	$closeStr = "Close Today";

		// get current day, and current day of the week
		// then check Open Hour, and availability
		
		$today = date('d-m-Y');
	    $ctime = strtotime($rdate);
 		$weekday = intval(date('w', $ctime));	// current day of week
		$yearday = intval(date('z', $ctime));	// current day of year

		$this->getOpenHour($ohvalue, $weekday);
		if($this->result < 0 || $this->openDay <= 0) {
			$this->result = 1;
			return $closeStr;
			}
				    	    	
    	$data = $this->getAlloteRestaurant($this->restaurant);	// configuration from the backoffice
    	if(count($data) < 1)
			return $closeStr;

		$this->result = 1;
		$lunchAr = explode("|", $data['lunch']);
		$dinnerAr = explode("|", $data['dinner']);
    	return ($lunchAr[$yearday] > 0 || $dinnerAr[$yearday] > 0) ? $openStr : $closeStr;
		}
	

	// check if we can approve this reservation by check the allotement for that day that time.
	// 1 check open hour if open
	// 2 check allotement availabele is bigger that the requested party
	// 3 check existing booking to check what's really left
		 	
	function getAllote1Slot($ohvalue, $rdate, $rtime, $rpers, $conf) {
	
		// rdate formt dd-mm-yyyy
		// rtime format hh:mm
		
	    $ctime = strtotime($rdate);
 		$weekday = intval(date('w', $ctime));	// current day of week
		$yearday = intval(date('z', $ctime));	// current day of year
		$this->getOpenHour($ohvalue, $weekday);
		if($this->result < 0 || $this->openDay <= 0)
			return -1;
			
	    //after 16:00, is dinner
	    $timeAr = explode(":", $rtime);
	    $whichmeal = (intval($timeAr[0]) >= 16) ? "dinner" : "lunch";
	    if( $whichmeal == "dinner" && $this->openDinner <= 0) return -1;
	    if( $whichmeal == "lunch" && $this->openLunch <= 0) return -1;

		$mealduration = $this->mealduration; //3 slots per repas or 5 if previously set
		$perpax = $this->perpax;
		$cover = ($perpax != 1) ? 1 : $rpers;
	    	
		// check the allotment for that day
    	$data = $this->getAlloteRestaurant($this->restaurant);	// configuration from the backoffice
    	if(count($data) < 1)
	 		return -1;

		$this->result = 0;
		// is request greater that configured availability ?
		$mealAr = ($whichmeal == "lunch") ? explode("|", $data['lunch']) : explode("|", $data['dinner']);
		$availPax = intval($mealAr[$yearday]);
		if($availPax < $cover)
	 		return $availPax;

		$booking = new WY_Booking();
		$data = $booking->getThatDayBooking($this->restaurant, $rdate);
		if(count($data) <= 0)	// no booking, so there are some availability
			return $this->result = 1;
			
		$book = array();
		$slottime = (intval(substr($rtime, 0, 2)) * 2) + (intval(substr($rtime, 3, 1)) / 3);
		for($i = 0; $i < $mealduration; $i++) {
			$book[$slottime + $i] = ($availPax - $cover);
			}

		foreach($data as $row) {
			if(!empty($conf) && $conf == $row['confirmation']) continue;
            $slottime = (intval(substr($row['rtime'], 0, 2)) * 2) + (intval(substr($row['rtime'], 3, 1)) / 3);
			$cover = ($perpax != 1) ? 1 : intval($row['cover']);
			for($i = 0; $i < $mealduration; $i++) {
				if(isset($book[$slottime + $i])) {
					$book[$slottime + $i] -= $cover;
					if($book[$slottime + $i] < 0)
						return -1;
					}
				}
			}		
		return $this->result = 1;
	}
	

		 	
	// 2 periods: lunch and dinner. If needed, we could add a third period for breakfast using same model
	// 4 Phases
	
	// 1) get open hours information for a week, "STARTING" the current day !important
	// 2) get availability for the restaurant for both lunch and dinner
	// 3) decrement existing availability with the bookings, each booking is a table for 1 and 1/2 hour -> 3 slots
	// 4) update the current day -> no walking => close the period 1 slot before (30mns). Else close previous slot of the day.
	 		
 	function AlloteComputeOpenHour($ohvalue, $maxdaybooking, $isnoWalking) {

		//date_default_timezone_set('Singapore'); 

		// 1) PHASE: first get open hours information for a week, "STARTING" the current day !important
		// and replicate the week for the next 2 months -> booking was limited to 2 month ahead (70days)
		// Now booking is limited to end of year
	 		
		$weekday =	date('w');	// current day of the week
		$this->getOpenHour($ohvalue, $weekday);
		if($this->result < 0)
			return "";
					
		$lunch = $dinner = array();

		$nchar_lunch = 4;
		$nchar_dinner = 4;
		$emptylunch = $emptydinner = "";
		for($i = 0; $i < $nchar_lunch; $i++) $emptylunch .= "0";	// "0000"
		for($i = 0; $i < $nchar_dinner; $i++) $emptydinner .= "0";
			
		$limit = $nchar_lunch * 4; 	// 4 is the number of bits
		$day = $weekday;
		for($k = 0; $k < 7; $k++, $day = ($day < 6) ? $day+1 : 0) {
			$Lstart = $this->openWeekHours[$day][0];
			$Lend = $this->openWeekHours[$day][1];
			
			$cd = 0;
			$startingReference = 18;		// 18 -> 9 * 2, 9h00 for lunch. Starting Reference for the OpenHours
			if($Lstart != $Lend) {
				for($i = 0, $pp = 1; $i < $limit; $i++, $pp <<= 1) {	// lunch, set the bit when the time is between start and end, for the 16 digits. Last possible is 16h30 -> slot 33 (everything is divided by 1/2 hour)
					$slot = $i + $startingReference;								
					if($slot >= $Lstart && $slot <= $Lend)
						$cd += $pp;
					}
				}

			$lunch[] = $cd;
			
			$Dstart = $this->openWeekHours[$day][2];
			$Dend = $this->openWeekHours[$day][3];

			$cd = 0;
			$startingReference = 32;		// 32 -> 16 * 2, 16h00 for dinner. Starting Reference for the OpenHours
			$limit = $nchar_dinner * 4; 	// 4 is the number of bits
			if($Dstart != $Dend) {
				for($i = 0, $pp = 1; $i < $limit; $i++, $pp <<= 1) {	// lunch, set the bit when the time is between start and end, for the 16 digits. Last possible is 16h30 -> slot 33 (everything is divided by 1/2 hour)
					$slot = $i + $startingReference;								
					if($slot >= $Dstart && $slot <= $Dend)
						$cd += $pp;
					}
				}
			$dinner[] = $cd;
			}
			
		$yearday = intval(date('z'));	// current day of year
		//$maxdaybooking = 365 - $yearday;	//
		//$maxdaybooking = 70;	// 70 days max

		// 1)a) replicate the week for the next 2 months -> booking is limited to 2 month ahead
	 				
		for($i = 7; $i < $maxdaybooking; $i++) {
			$lunch[$i] = $lunch[$i % 7];
			$dinner[$i] = $dinner[$i % 7];			
			}
		

  		// 2) PHASE: get availability for the restaurant for both lunch and dinner
	 		
  		$data = $this->getAlloteRestaurant($this->restaurant);	// configuration from the backoffice

		// 3) PHASE: decrement existing availability with the bookings, each booking is a table for 1 and 1/2 hour -> 3 slots
		// for lunch or dinner depending of the reservation time !!! Attention: no overlapping -> potential problem is 16h
	 		
		$lunchAr = explode("|", $data['lunch']);
		$dinnerAr = explode("|", $data['dinner']);

		$mealduration = $this->mealduration; //3 slots per repas or 5 if previously set
		$book = $this->consoBookingAllote($this->restaurant, "");	// information from the booking
		if(count($book) > 0) {										// is there any booking to substract from current availability ?
			$limit = $maxdaybooking;

			for($i = 0; $i < $limit; $i++) {

				$maxtable = intval($lunchAr[$i + $yearday]);
				$offset = 18; //starting of dinner slot: 18/2 = 9h
				if($maxtable <= 0) 
					$lunch[$i] = 0;
				else if(isset($book[$i])) {
					$row = $book[$i];
					while(list($slot, $val) = each($row)) 
						if($val >= $maxtable) {
							$lunch[$i] &= ~(1 << ($slot - $offset)); 	// unset corresponding bit for slot
							for($mm = 1; $mm < $mealduration && (($slot - $offset) - $mm) >= 0; $mm++) // close also the preceding slots for $mealduration - 1
								$lunch[$i] &= ~(1 << (($slot - $offset) - $mm));			
							}
					}
			
				$maxtable = intval($dinnerAr[$i + $yearday]);
				$offset = 32; //starting of dinner slot  32/2 = 16h
				if($maxtable <= 0) 
					$dinner[$i] = 0;
				else if(isset($book[$i])) {
					$row = $book[$i];
					while(list($slot, $val) = each($row)) 
						if($val >= $maxtable) {
							$dinner[$i] &= ~(1 << ($slot - $offset));	// unset corresponding bit for slot
							for($mm = 1; $mm < $mealduration && (($slot - $offset) - $mm) >= 0; $mm++) // close also the preceding slots for $mealduration - 1
								$dinner[$i] &= ~(1 << (($slot - $offset) - $mm));
							}
					}
				}
			}

  		// 4) PHASE: update today's information based upon the current time 	
		date_default_timezone_set('Asia/Singapore');
  			
		$slice = (intval(date('H')) * 2) + floor(intval(date('i')) / 30);
		if($slice >= $this->openDayHours[1]) $lunch[0] = $emptylunch;
		if($slice >= $this->openDayHours[3]) $dinner[0] = $emptydinner;

		for($slot = $this->openDayHours[0]; $slot <= $this->openDayHours[1] && $slot < $slice && $slice <= $this->openDayHours[1]; $slot++) {
			$lunch[0] &= ~(1 << ($slot - 18));
			}
					
		for($slot = $this->openDayHours[2]; $slot < $this->openDayHours[3] && $slot < $slice && $slice < $this->openDayHours[3]; $slot++) {
			$dinner[0] &= ~(1 << ($slot - 32));
			}
					
		if($isnoWalking) {
		error_log("NO WALKING " . $this->restaurant);
			if($slice >= $this->openDayHours[0] - 1) $lunch[0] = $emptylunch;
			if($slice >= $this->openDayHours[2] - 1) $dinner[0] = $emptydinner;
			}
			
		$lunchdata = $dinnerdata = "";
		for($i = 0; $i < $maxdaybooking; $i++) {
			$lunchdata .= sprintf("%04x", $lunch[$i]);
			$dinnerdata .= sprintf("%04x", $dinner[$i]);			
			}

		return array("lunchdata" => strtoupper($lunchdata), "dinnerdata" => strtoupper($dinnerdata));
 		}


 	    
	function getAlloteReportSlot($ohvalue, $rdate) {

		//date_default_timezone_set('Singapore'); 

		// 1) PHASE: first get open hours information for a day,
		// reformat the date format
	 		
	 	$tmpAr = explode("-", $rdate);
		$rdate = $tmpAr[2] . "-" . $tmpAr[1] . "-" . $tmpAr[0];
		$ctime = strtotime($rdate); // unix
		$todayflag = (date('d-m-Y') == date('d-m-Y', $ctime));
		$weekday = intval(date("w", $ctime));	// current day of the week
		$yearday = intval(date('z', $ctime));	// current day of year of specific date

		$this->getOpenHour($ohvalue, $weekday);
		if($this->result < 0)
			return -1;

  		$data = $this->getAlloteRestaurant($this->restaurant);	// configuration from the backoffice

		// set the allotement for the opening hours
		// in one array, 0-15 for lunch, 16 - 32 for dinner
	 		
		$lunchAr = explode("|", $data['lunch']);
		$dinnerAr = explode("|", $data['dinner']);
		
		$maxlunch = intval($lunchAr[$yearday]);
		$maxdinner = intval($dinnerAr[$yearday]);
		for($i = 0; $i < 32; $i++)
			$repasbits[] = $booked[] = 0;

// lunch, set the bit when the time is between start and end, for the 16 digits. Last possible is 16h30 -> slot 33 (everything is divided by 1/2 hour)

		$startingReference = 18;		// 18 -> 9 * 2, 9h00 for lunch. Starting Reference for the OpenHours
		$Lstart = $this->openDayHours[0] - $startingReference;
		$Lend = $this->openDayHours[1] - $startingReference;
		for($i = $Lstart; $i < $Lend; $i++) 
			$repasbits[$i] = $maxlunch;	

		$startingReference = 32;		// 32 -> 16 * 2, 16h00 for dinner. Starting Reference for the OpenHours
		$Dstart = $this->openDayHours[2] - $startingReference;
		$Dend = $this->openDayHours[3] - $startingReference;
		for($i = $Dstart; $i < $Dend; $i++) 
			$repasbits[$i+14] = $maxdinner;

		$book = $this->consoBookingAllote($this->restaurant, $rdate);	// information from the booking with mysql format date yyyy-mm-dd
			
		if(count($book) > 0 && isset($book[0])) {
			while(list($slot, $val) = each($book[0])) {
				$slot = (intval($slot) < 32) ? (intval($slot) - 18) : (intval($slot) - 32) + 14;
				$repasbits[$slot] -= $val;
				$booked[$slot] = $val;
				}
			}
			
  		// 4) PHASE: update today's information based upon the current time 	
		date_default_timezone_set('Asia/Singapore');
  			
		if($todayflag) { 
			for($slot = (intval(date('H')) * 2) + floor(intval(date('i')) / 30) - 18; $slot >= 0; $slot--)
				$repasbits[$slot] = 0;
			}

		//$this->printdebug($repasbits, $rdate);		
		for($i = 0; $i < 14; $i++)
			$lunchbits[] = $repasbits[$i];
		for($i = 14; $i < 32; $i++)
			$dinnerbits[] = $repasbits[$i];
			
		return array("avail" => implode(",", $repasbits), "booked" => implode(",", $booked), "pax" => strval($this->perpax));
		}

	function printdebug($repasbits, $rdate) {

		for($i = 0; $i < 32; $i++) {
			$mtype = ($i < 14) ? "LUNCH" : "DINNER";
			error_log($mtype . " " .  (floor($i/2)+9) . ":" . (($i % 2) * 3) . "0" . "   ->   " . $repasbits[$i]);
			}
		}
}

?>
