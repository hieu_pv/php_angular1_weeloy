<?php

class FB_Image {
   
   var $image;
   var $rd_file, $wr_file;
   var $tx_reduct;
   var $perm;
   var $img_type;
   var $img_info;
   var $img_width;
   var $img_height;
   var $img_status;
   var $img_exists;
   
   var $optimised;
   
 
   function init($rd_file, $wr_file, $tx_reduct, $perm, $optimised_val = 0)
		{
		$this->img_rdfile = trim($rd_file);
		$this->img_wrfile = $wr_file;
		$this->img_perm = $perm;
		$this->img_txreduct = $tx_reduct;
		$this->img_info = getimagesize($this->img_rdfile);
		$this->img_type = $this->img_info[2];
		$this->img_width = $this->img_info[0];
		$this->img_height = $this->img_info[1];
		$this->img_status = 1;
                $this->optimised = $optimised_val;

		return $this->img_status;
		}
    
   function read() 
		{
		$this->img_status = 1;
		
		switch($this->img_type)
			{
			case IMAGETYPE_GIF:
				$this->image = imagecreatefromgif($this->img_rdfile);
				break;
				
			case IMAGETYPE_JPEG:
				$this->image = imagecreatefromjpeg($this->img_rdfile);
				break;
				
			case IMAGETYPE_PNG:
				$this->image = imagecreatefrompng($this->img_rdfile);
				break;
			
			default:
				$this->img_status = -1;
			}
		}
		
   function write(){
		$this->img_status = 1;
                
		switch($this->img_type)
			{
			case IMAGETYPE_GIF:
				imagegif($this->image, $this->img_wrfile);         
				break;
				
			case IMAGETYPE_JPEG:
                            if($this->optimised > 0){
                                $image = new WY_Optimized_Images();
                                $image->init($this->image, $this->img_rdfile, $this->img_wrfile, $this->optimised);
                                $image->optimize();
                            }else{
                                imagejpeg($this->image, $this->img_wrfile, $this->img_txreduct);      
                                $compressed_jpg_content = shell_exec("/usr/local/bin/jpegoptim --strip-all --all-progressive - < ".escapeshellarg($this->img_wrfile));
                                //if(file_exists($this->tmpname)){
                                //    unlink($this->tmpname);
                                //}        
                                file_put_contents($this->img_wrfile, $compressed_jpg_content);
                            }
                            break;
				
			case IMAGETYPE_PNG:
				imagealphablending($this->image, false);
				imagesavealpha($this->image, true);
				imagepng($this->image, $this->img_wrfile);
				break;
			
			default:
				$this->img_status = -1;
			}

		if($this->img_status > 0)
			if($this->img_perm != null)
				chmod($this->img_wrfile, $this->img_perm);
		}
		
   function screen() 
		{
		$this->img_status = 1;
		switch($this->img_type)
			{
			case IMAGETYPE_GIF:
				imagegif($this->image);         
				break;
				
			case IMAGETYPE_JPEG:
				imagejpeg($this->image);
				break;
				
			case IMAGETYPE_PNG:
				imagepng($this->image);
				break;
			
			default:
				$this->img_status = -1;
			}

		}
		   
   function set_resizeheight($height)
		{
		if($height >= $this->img_height) return;
		$ratio = $height / $this->img_height;		
		$width = $this->img_width * $ratio;
		$this->resize($width, $height);
		}

   function set_resizewidth($width)
		{
		if($width >= $this->img_width) return;
		$ratio = $width / $this->img_width;
                
		$height = $this->img_height * $ratio;
                
		$this->resize($width, $height);
		}

   function set_scale($scale) 
		{
		$scale /= 100;
		$this->resize($this->img_width * $scale, $this->img_height * $scale);
		}

   function resize($width,$height)
		{
		$new_image = imagecreatetruecolor($width, $height);
		if($this->img_type == IMAGETYPE_PNG)
			imagecolortransparent($new_image, imagecolorallocate($new_image, 0, 0, 0));
		imagecopyresampled($new_image, $this->image, 0, 0, 0, 0, $width, $height, $this->img_width, $this->img_height);
		$this->image = $new_image;
		$this->img_width = $this->img_info[0] = imagesx($this->image);
		$this->img_height = $this->img_info[1] = imagesy($this->image);
		}
}


class WY_Optimized_Images {

    var $img;
    var $image_from_path;
    var $image_to_path;
    var $image_width;
    var $file_size;
    
    var $size_limit;
    var $rxcompress;

    function __construct() {
        

    }
    
    function init($img, $from_path, $to_path, $image_width = 500){
        $this->img = $img;
        switch ($image_width){
            case 1440:
                $this->size_limit = 200000;
                break;
            case 1024:
                $this->size_limit = 120000;
                break;
            case 700:
            case 600:    
            case 500:
                $this->size_limit = 100000;
                break;
            case 500:
            case 450:
                $this->size_limit = 70000;
                break;
            case 360:
            case 325:
                $this->size_limit = 55000;
                break;
            case 300:
            case 270:
                $this->size_limit = 50000;
                break;
            case 180:
            case 140:
                $this->size_limit = 25000;
                break;
            case 100:
                $this->size_limit = 15000;
                break;
        }
        
        $this->rxcompress = 90;
        $this->scaledownpercent = 5;
        
        $this->image_from_path = $from_path;
        $this->image_to_path = $to_path;
        $this->image_width = $image_width;
      
        $this->file_size = filesize($from_path);
        
        $this->tmpname = __TMPDIR__.MD5($this->image_to_path).'.jpg';
        
        
        
        }
    
    function optimize(){
            $this->compress();
            $this->rxcompress = $this->rxcompress - $this->scaledownpercent;
            $this->file_size = filesize($this->tmpname);
            while($this->file_size > $this->size_limit && $this->rxcompress >0){
                $this->compress();
                $this->rxcompress = $this->rxcompress - $this->scaledownpercent;
                $this->file_size = filesize($this->tmpname);
            }
            
            rename($this->tmpname, $this->image_to_path);
            
            if(file_exists($this->tmpname)){
                unlink($this->tmpname);
            }
    } 
    
    function compress(){
        if(file_exists($this->tmpname)){
            unlink($this->tmpname);
        }
        imagejpeg($this->img, $this->tmpname, $this->rxcompress);
        $compressed_jpg_content = shell_exec("/usr/local/bin/jpegoptim --strip-all --all-progressive - < ".escapeshellarg($this->tmpname));
        if(file_exists($this->tmpname)){
            unlink($this->tmpname);
        }        
        file_put_contents($this->tmpname, $compressed_jpg_content);
        return true;
    }
}

function resizecompress($oimage, $fimage, $size = 500, $optimised = true) {
    $img = new FB_Image;
        
        if($optimised){
            
            $img->init($oimage, $fimage, 100, null,$size);  
        }else{
            
            $img->init($oimage, $fimage, 100, null);
        }
	$img->read();
	//$img->set_scale(90);
	
    $img->set_resizewidth($size);
	
    $img->write();
        
    return true;
}

    function recurse_copy($src, $dst) {
        $dir = opendir($src);
        @mkdir($dst);
        while (false !== ( $file = readdir($dir))) {
            if (( $file != '.' ) && ( $file != '..' )) {
                if (is_dir($src . '/' . $file)) {
                    recurse_copy($src . '/' . $file, $dst . '/' . $file);
                } else {
                    copy($src . '/' . $file, $dst . '/' . $file);
                }
            }
        }
        closedir($dir);
    }
    
    function recurse_delete($dir) { 
        
        if (is_dir($dir)) { 
          $objects = scandir($dir); 
          foreach ($objects as $object) { 
            if ($object != "." && $object != "..") { 
              if (filetype($dir."/".$object) == "dir"){
                      recurse_delete($dir."/".$object);
              }
              else {
                  if(file_exists($dir."/".$object)){
                    unlink($dir."/".$object); 
                  }
              }
            } 
          } 
          reset($objects); 
          rmdir($dir); 
        } 
    }
?>