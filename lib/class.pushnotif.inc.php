<?php

class JM_Pushnotif {

    public $lang;
    protected $message;
    protected $badge;
    protected $sound;
    protected $development;

    function __construct() {
        
    }

    function sendPushnotif($recipient, $message, $pn_type, $options = NULL) {

        switch ($pn_type) {
            case 'ios':
                $this->initIosPushNotif($options);
                $this->sendIosNotif($recipient, $message);
                break;
            case 'android':
                $this->initAndoidPushNotif($options);
                $this->sendAndoidPushNotif($recipient, $message);
                break;
        }
    }

    function initIosPushNotif($options) {
        $this->badge = 3;
        $this->sound = 'default';
        $this->development = true;
    }

    public function sendIosNotif($recipient, $message) {

        $deviceToken = $recipient;

        // Put your private key's passphrase here:
        $scheme = 'prod';
        if ($scheme != 'dev') {
            $certifile = 'modules/mailspool/certificats/apn.pem';
            $passphrase = 'Lmdpdw31';
            $url = 'ssl://gateway.push.apple.com:2195';
        } else {
            $certifile = 'certificats/apn_dev.pem';
            $passphrase = 'Weeloy2014';
            $url = 'ssl://gateway.sandbox.push.apple.com:2195';
        }

// Put your alert message here:
        // $message = trim($message);
////////////////////////////////////////////////////////////////////////////////

        $ctx = stream_context_create();
        stream_context_set_option($ctx, 'ssl', 'local_cert', $certifile);
        stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

// Open a connection to the APNS server
        $fp = stream_socket_client($url, $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);

        if (!$fp)
            exit("Failed to connect: $err $errstr" . PHP_EOL);

        echo 'Connected to APNS' . PHP_EOL;

// Create the payload body
        $body['aps'] = array(
            'alert' => $message,
            'sound' => 'default'
        );

// Encode the payload as JSON
        $payload = json_encode($body);

// Build the binary notification
        $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

// Send it to the server
        $result = fwrite($fp, $msg, strlen($msg));

        if (!$result)
            echo 'Message not delivered' . PHP_EOL;
        else
            echo 'Message successfully delivered' . PHP_EOL;
        
// Close the connection to the server
        fclose($fp);
    }

    public function checkIosNotif() {

        $scheme = 'prod';
        if ($scheme != 'dev') {
            $certifile = 'modules/mailspool/certificats/apn.pem';
            $passphrase = 'Lmdpdw31';
            $url = 'ssl://feedback.push.apple.com:2196';
        } else {
            $certifile = 'modules/mailspool/certificats/apn_dev.pem';
            $passphrase = 'Weeloy2014';
            $url = 'ssl://feedback.sandbox.push.apple.com:2196';
        }
        
        $ctx = stream_context_create();
        stream_context_set_option($ctx, 'ssl', 'local_cert', $certifile);
        stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

        $apns = stream_socket_client($url, $errcode, $errstr, 60, STREAM_CLIENT_CONNECT, $ctx);
    
        if (!$apns) {
            echo "ERROR $err: $errstr\n";
            return;
        } else
            echo 'APNS FEEDBACK CONNECTION ESTABLISHED...<br/>';

        $feedback_tokens = array();
            //and read the data on the connection:
            while(!feof($apns)) {
                $data = fread($apns, 38);
                if(strlen($data)) {
                    $feedback_tokens[] = unpack("N1timestamp/n1length/H*devtoken", $data);
                }
            }
            fclose($apns);
            
            foreach ($feedback_tokens as $feedback_token){
                $this->deletePNDeviceByToken($feedback_token['devtoken']);
            }
            
            print_r($feedback_tokens);
            
            return $feedback_tokens;
        
    }
    
    
    function initAndoidPushNotif($options) {
        $this->badge = 3;
        $this->sound = 'default';
        $this->development = true;
    }

    public function sendAndoidPushNotif($recipient, $message) {
        define('API_ACCESS_KEY', 'AIzaSyC-l7EvSzWQEzbIl2umc9uM8-DCmap9EUo');

        $registrationIds = array($recipient);
        if (isset($_REQUEST['device_id'])) {
            $registrationIds = array($_REQUEST['device_id']);
        }
// prep the bundle
        $msg = array
            (
            'message' => $message,
            'title' => '',
            'subtitle' => '',
            'tickerText' => '',
            'vibrate' => 1,
            'sound' => 1
        );

        $fields = array
            (
            'registration_ids' => $registrationIds,
            'data' => $msg
        );

        $headers = array
            (
            'Authorization: key=' . API_ACCESS_KEY,
            'Content-Type: application/json'
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://android.googleapis.com/gcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        
        //var_dump($result);
        $result_array = json_decode($result);
        
        if($result_array->failure === 1){
            $res = $result_array->results[0];
            if($res->error === 'InvalidRegistration' || $res->error === 'NotRegistered'){
                $this->deletePNDeviceByToken($recipient);
            }
        }
        curl_close($ch);

        //echo $result;
    }
    
    static public function sendPushnotifMessage($recipient, $message, $pn_type, $options) {
        $opts = $subject = NULL;
        $spool = new WY_Spool;
        $spool->register_pushnotif($pn_type, $recipient, $subject, $message, $opts);
    }
    
    function deletePNDeviceByToken($token) {
    $sql = "UPDATE `mobile_devices` SET `status` = 'notregistered' WHERE `mobile_devices`.`mobile_id` = '$token';";
        try {
            $db = getConnection();
            $stmt = $db->prepare($sql);
            $obj = $stmt->execute();

        } catch (PDOException $e) {
            echo 'error';
        }
    }
}
