<?php


function insert_event($theRestaurant, $eventname) {

	if($eventname == "")
		return array(-1, "Event name is invalid -$eventname-");
		
	$sql_check = "SELECT name FROM event where name = '$eventname' and restaurant = '$theRestaurant'  limit 1";
	$sql_ins = "INSERT INTO  event (name, restaurant) VALUES ('$eventname', '$theRestaurant')";
	$result = pdo_insert_unique($sql_check, $sql_ins);
  	return ($result > 0) ? array(1, "Event $eventname has been created") : array(-1, "Event $eventname already exists. No new event created");
}

function save_event($theRestaurant, $eventname) {

	$adderror = "";
	
	$title = clean_input($_REQUEST['itemeventtitle']);
	$startAr = explode("/", $_REQUEST['itemeventstart']);
	$start = $startAr[2] . "-" . $startAr[1] . "-" . $startAr[0];
	$endAr = explode("/", $_REQUEST['itemeventend']);
	$end = $endAr[2] . "-" . $endAr[1] . "-" . $endAr[0];
	if($endAr[2] < $startAr[2] || ($endAr[2] == $startAr[2] && $endAr[1] < $startAr[1]) || ($endAr[2] == $startAr[2] && $endAr[1] == $startAr[1] && $endAr[0] < $startAr[0])) {
		$end = $start;
		$adderror = "Ending date starts before starting date. Resetting Ending Date";
		}
	$picture = clean_input($_REQUEST['itemeventpicture']);
	$morder = clean_input($_REQUEST['itemeventorder']);
	$description = clean_input($_REQUEST['itemeventdesc']);
	$city = clean_input($_REQUEST['itemeventcity']);
	$country = clean_input($_REQUEST['itemeventcountry']);

	//error_log("CITY COUNTRY " . $city . " " . $country);	
	$affected_rows = pdo_exec("Update event set title='$title', city='$city', country='$country', start='$start', end='$end', description='$description', picture='$picture', morder='$morder' where name='$eventname' and restaurant = '$theRestaurant' limit 1");
	return array(1, "Event $eventname has been updated. $adderror");
}

function delete_event($theRestaurant, $eventame) {

	if($eventame == "")
		return array(-1, "Event name is invalid -$eventame-");
	
	$affected_rows = pdo_exec("DELETE FROM event WHERE name = '$eventame' and restaurant = '$theRestaurant' LIMIT 1");
	return array(1, "Event $eventame has been deleted");
}

function readEventName($theRestaurant) {

	return pdo_column("SELECT name FROM event where restaurant = '$theRestaurant' ORDER BY morder");
}

function readAllEvents($theRestaurant) {

    return pdo_multiple_obj("SELECT name, title, morder, city, country, start, end, description, picture FROM event where restaurant = '$theRestaurant' ORDER BY morder");
}

function getEvents($theRestaurant) {

    return pdo_multiple_select("SELECT name, title, morder, city, country, start, end, description, picture FROM event WHERE restaurant ='$theRestaurant' ORDER by morder");
}

function timelineEvents($theRestaurant) {

	$res = new WY_restaurant;
	$res->getTimeline($theRestaurant);
	$data = $res->Timeline;
	if(count($data) <= 0)
		return "";
		
    foreach($data as $row) {
		$name[] = $row['name'];
		$title[] = $row['title'];
		$city[] = $row['city'];
		$country[] = $row['country'];
		$start[] = $row['start'];
		$end[] = $row['end'];
		$description[] = $row['description'];
		$picture[] = $res->globalshow  . $row['restaurant'] . "/" . $row['picture'];
		}

	$label = array("bg-success", "bg-info", "bg-secondary", "bg-warning", "bg-primary", "bg-danger");
	$EventInfo = "<h5><strong>EVENTS</strong></h5><hr style='border-top: dotted 1px;' /><div class='timeline-centered'>";
	$ll = -1;
	
	$limit = count($name);
	$divider = 3;
	$choice = getnchoice($divider, $limit);
		
	for($i = $m = 0; $i < $limit && $m < $divider; $i++) {			
		if(!in_array($i, $choice))
			 continue;
		$m++;
		if(++$ll >= count($label)) $ll = 0;		
		$Sdate = new DateTime($start[$i]); 
		$Edate = new DateTime($end[$i]); 
		$location = $city[$i];
		if($city[$i] != "Singapore") 
			$location .= "," . $country[$i];

        $EventInfo .= "<article class='timeline-entry'><div class='timeline-entry-inner'><div class='timeline-icon " . $label[$ll] . "'><i class='entypo-suitcase'></i></div>";
        $EventInfo .= "<div class='timeline-label'><h5 class='media-heading'><strong>";
        $EventInfo .= "<strong>" . $title[$i] . "</strong><br /><em><small>" . $Sdate->format('F d, Y') . " - " . $Edate->format('F d, Y') . "," . $location . "</em></small></h5>";
        $EventInfo .= "<img src='" . $picture[$i] . "' class='media-object imgshadow' alt='Sample Image' width='200' border='no'><h6><small>" . $description[$i] . "</small></h6></div></div></article>";
        }
	$EventInfo .= "</div>";
	return $EventInfo;
}

function insert_reviews($theRestaurant) {

	$dbLabel = array('itemreviewdesc', 'itemreviewguest', 'itemreviewgrade', 'itemreviewdtvisite', 'itemreviewdtcreate');	// 'DATE_FORMAT(datevisit,\'%M %d, %Y\')'
	for($i = 0; $i < count($dbLabel); $i++)
		${$dbLabel[$i]} = clean_input($_REQUEST[$dbLabel[$i]]);

	//error_log("REVIEW ($theRestaurant) = $itemreviewdtvisite $itemreviewguest $itemreviewgrade $itemreviewdesc");

	if($itemreviewdtvisite != "") 
		$tmpAr = explode('/', $itemreviewdtvisite);
	if(count($tmpAr) > 2)
		{
		$itemreviewdtvisite = $tmpAr[2] . "-" . $tmpAr[0] . "-" . $tmpAr[1];
		pdo_exec("INSERT INTO  review (reviewdesc, reviewguest, reviewgrade, reviewdtvisite, reviewdtcreate, restaurant ) VALUES ('$itemreviewdesc', '$itemreviewguest', '$itemreviewgrade', '$itemreviewdtvisite', 'NOW()', '$theRestaurant')");
		return array(1, "");
		}
	return array(-1, "Some error " . $reviewdtvisite);
}

?>