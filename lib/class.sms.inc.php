<?php

class JM_Sms {

    static public function sendMessage($recipient, $message, $expeditor = 'Weeloy', $reference = 'NULL', $source_test) {

        $recipient = str_replace(" ", "", $recipient);
        
        $country = JM_Sms::extractCountry($recipient);
        //$this->route_sms($recipient, $message, $expeditor, $reference);
        $res = '';
        switch ($country) {
            case 'singapore':
            case 'thailand':
                $res = JM_Sms::sendSmsClickatell($recipient, $message, $expeditor, $reference, $source_test);
                break;
            
            default:
                $res = JM_Sms::sendSmsSmsgateway($recipient, $message, $expeditor, $reference);
                break;
        }
        return $res;
    }

    static public function sendSmsMessage($recipient, $message, $expeditor = 'Weeloy') {

        //$sms_message = $this->restaurantinfo->title."\n".$date_sms."\n". "Confirmation: " .$this->confirmation."\nWeeloy Code: ".$this->membCode." (for rewards)\n". $this->restaurantinfo->address . ", \n". $this->restaurantinfo->city." ". $this->restaurantinfo->zip.", \n". str_replace(" " , "", $this->restaurantinfo->tel);
        if (strlen($message) > 160) {
            $message = substr($message, 0, 160);
        }

        $opts = $subject = NULL;
        $spool = new WY_Spool;

        $message = htmlspecialchars($message);

        $spool->register_sms($recipient, $subject, $message, $opts, $expeditor);
    }

    static public function sendSmsSmsgateway($recipient, $message, $expeditor, $reference) {
        $xml = new SimpleXMLElement('<MESSAGES/>');

        $authentication = $xml->addChild('AUTHENTICATION');
        $authentication->addChild('PRODUCTTOKEN', 'd192a5cb-ded9-4378-9883-7c076cca0ffb');

        $msg = $xml->addChild('MSG');

        $msg->addChild('FROM', $expeditor);
        $msg->addChild('TO', $recipient);
        $msg->addChild('BODY', $message);
        $msg->addChild('REFERENCE', $reference);

        $xml = $xml->asXML();

        //return $xml->asXML();
        //$xml = self::buildMessageXml($recipient, $message, $expeditor, $reference);

        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => 'https://secure.cm.nl/smssgateway/cm/gateway.ashx',
            CURLOPT_HTTPHEADER => array('Content-Type: application/xml'),
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $xml,
            CURLOPT_RETURNTRANSFER => true
                )
        );

        $response = curl_exec($ch);

        curl_close($ch);

        return $response;
    }
    
    static public function sendSmsCommzgate($recipient, $message, $expeditor, $reference) {
        
        $username = '62580002';
        $password = '250419';
        $mobile = str_replace('+','',$recipient);
        
        $postfields = "";
        $postfields .= "ID=$username";
        $postfields .= "&Password=$password";
        $postfields .= "&Mobile=$mobile";
        $postfields .= "&Type=A";
        $postfields .= "&Message=$message";
        $postfields .= "&Sender=T$expeditor";

            $ch = curl_init();
        curl_setopt_array($ch, array(
                CURLOPT_URL => 'https://www.commzgate.net/gateway/SendMsg',
                CURLOPT_POST => true,
                CURLOPT_POSTFIELDS => $postfields,
                CURLOPT_RETURNTRANSFER => true
        ));

        $response = curl_exec($ch);

        curl_close($ch);

        return $response;
    }
    
    static public function sendSmsClickatell($recipient, $message, $expeditor, $reference, $source_test) {
                    
        $username = 'weeloy';
        $password = 'ORdZEAQCcGdMeH';
        $app_id = '3546942';
        
        $mobile = str_replace('+','',$recipient);
        
        //$message = str_replace('&amp;', '%26', $message);
        $message = str_replace('&amp;', '&', $message);
        $message = urlencode($message);
        
        $postfields = "";
        $postfields .= "user=$username";
        $postfields .= "&password=$password";
        $postfields .= "&api_id=$app_id";
        $postfields .= "&to=$mobile";
        $postfields .= "&from=Weeloy";
        $postfields .= "&text=$message";

        $ch = curl_init();
        curl_setopt_array($ch, array(
                CURLOPT_URL => 'http://api.clickatell.com/http/sendmsg',
                CURLOPT_POST => true,
                CURLOPT_POSTFIELDS => $postfields,
                CURLOPT_RETURNTRANSFER => true
        ));
        
        $response = curl_exec($ch);

        curl_close($ch);

        return $response;
        
        
    }


    public function extractCountry($recipient){
        if (strpos($recipient,'+65') !== false || strpos($recipient,'0065') !== false) {
            return 'singapore';
        }
        if (strpos($recipient,'+66') !== false || strpos($recipient,'0066') !== false) {
            return 'thailand';
        }
        return 'others';
    }

}
