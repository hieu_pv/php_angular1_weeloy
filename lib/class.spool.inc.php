<?php

/**
 * 	Richard Kefs (c) 2014
 *
 * 	Contains all classes definition for login session.
 *
 */
require_once("lib/class.async.inc.php");
class WY_Spool {

    function __construct() {
        
    }

    function register_mail($recipient, $subject, $body, $opts) {

        if($this->is_test_env()){
            $recipient = "philippe.benedetti@weeloy.com";
        }
        
        $data[] = $recipient;
        $data[] = $subject;
        $data[] = $body;
        $data[] = urlencode(serialize($opts));
        $datastring = implode("||||", $data);
        $datastring = preg_replace("/\'|\"/", "’", $datastring); //\xc2\xb4
        //$random = rand(0, 9999999);
        $random = uniqid();
        
        pdo_exec("INSERT INTO spool (type, timestamp, status, data) VALUES ('MAIL', NOW(), '$random', '$datastring')");

        $url = "https://" . $_SERVER['HTTP_HOST'] . __ROOTDIR__ . "/modules/mailspool/mailspool.php?mailid=$random";

        if (strpos($_SERVER['HTTP_HOST'], 'api') !== false) {
            if (strpos($_SERVER['HTTP_HOST'], 'dev') !== false) {
                $url = "https://dev.weeloy.com/modules/mailspool/mailspool.php?mailid=$random";
            } else {
                $url = "https://www.weeloy.com/modules/mailspool/mailspool.php?mailid=$random";
            }
        }

        $async = new WY_Async;
        $async->async_curl($url);
    }

    function register_sms($recipient, $subject, $body, $opts, $expeditor) {

        if($this->is_test_env()){
            return true;
            $recipient = "philippe.benedetti@weeloy.com";
        }
        
        $data[] = $recipient;
        $data[] = $subject;
        $data[] = $body;
        $data[] = urlencode(serialize($opts));
        $data[] = $expeditor;
        $datastring = implode("||||", $data);
        $datastring = preg_replace("/\’|\"/", " ", $datastring); //\xc2\xb4
        //$datastring = urlencode($datastring);
        //$random = rand(0, 9999999);
        $random = uniqid();
        pdo_exec("INSERT INTO spool (type, timestamp, status, data) VALUES ('SMS', NOW(), '$random', '$datastring')");

        $url = "https://" . $_SERVER['HTTP_HOST'] . __ROOTDIR__ . "/modules/mailspool/smsspool.php?mailid=$random";

        if (strpos($_SERVER['HTTP_HOST'], 'api') !== false) {
            if (strpos($_SERVER['HTTP_HOST'], 'dev') !== false) {
                $url = "https://dev.weeloy.com/modules/mailspool/smsspool.php?mailid=$random";
            } else {
                $url = "https://www.weeloy.com/modules/mailspool/smsspool.php?mailid=$random";
            }
        }

        $async = new WY_Async;
        $async->async_curl($url);
    }

    function register_pushnotif($pn_type, $recipient, $subject, $body, $opts) {
        if($this->is_test_env()){
            return true;
            $recipient = "philippe.benedetti@weeloy.com";
        }
        $data[] = $pn_type;
        $data[] = $recipient;
        $data[] = $subject;
        $data[] = $body;
        $data[] = urlencode(serialize($opts));
        $datastring = implode("||||", $data);
        $datastring = preg_replace("/\'|\"/", "’", $datastring); //\xc2\xb4
        //$random = rand(0, 9999999);
        $random = uniqid();
        pdo_exec("INSERT INTO spool (type, timestamp, status, data) VALUES ('PUSHNOTIF', NOW(), '$random', '$datastring')");

        $url = "https://" . $_SERVER['HTTP_HOST'] . __ROOTDIR__ . "/modules/mailspool/pnspool.php?mailid=$random";

        if (strpos($_SERVER['HTTP_HOST'], 'api') !== false) {
            if (strpos($_SERVER['HTTP_HOST'], 'dev') !== false) {
                $url = "https://dev.weeloy.com/modules/mailspool/pnspool.php?mailid=$random";
            } else {
                $url = "https://www.weeloy.com/modules/mailspool/pnspool.php?mailid=$random";
            }
        }
        if (strpos($_SERVER['HTTP_HOST'], 'localhost') !== false) {
                $url = "http://localhost:8888/weeloy_ext1/modules/mailspool/pnspool.php?mailid=$random";
        }
        $async = new WY_Async;
        $async->async_curl($url);
    }

    function execute_mail($mailid) {

        $db_history = getConnection('dwh');
        $mailer = new JM_Mail;
        if ($mailid != "") {
            $data = pdo_single_select("select ID, type, data, timestamp from spool where type = 'MAIL' and status = '$mailid' limit 1");
            if (count($data) > 0) {
                $id = $this->senddata($mailer, $data);
                $db_history->exec("INSERT INTO spool_history (type, timestamp, status, data, sent_on) VALUES ('MAIL', '$data[timestamp]', '$mailid', '$data[data]', NOW())");
                pdo_exec("delete from spool where type = 'MAIL' and ID = '$id' limit 1");
            }
        }
    }

    function execute_mail_backlog() {
        $db_history = getConnection('dwh');
        $mailer = new JM_Mail;
        // search for mail that were not sent !!!
        $data = pdo_multiple_select("select ID, type, data, status, timestamp from spool where type = 'MAIL' and DATE_SUB(CURRENT_TIMESTAMP(), INTERVAL 5 MINUTE) > timestamp");
        if (count($data) > 0) {
            foreach ($data as $row) {
                $ID = $this->senddata($mailer, $row);
                if($ID){
                    $db_history->exec("INSERT INTO spool_history (type, timestamp, status, data, sent_on) VALUES ('MAIL', '$row[timestamp]', '$row[status]', '$row[data]', NOW())");
                    pdo_exec("delete from spool where type = 'MAIL' and ID = '" . $ID . "' limit 1");
                }
            }
        }
    }

    function execute_sms($mailid) {
        $db_history = getConnection('dwh');
        $sms_mailer = new JM_Sms();

        if ($mailid != "") {
            $data = pdo_single_select("select ID, type, data, timestamp from spool where type = 'SMS' and status = '$mailid' limit 1");
            if (count($data) > 0) {
                $db_history->exec("INSERT INTO sms_status_history (ID, sent, reference, received, recipient, statuscode, errorcode, errordescription, last_update) VALUES (NULL, '', '$mailid', '', '', '', '', '', CURRENT_TIMESTAMP);");
                $id = $this->sendsms($sms_mailer, $data, $mailid, 'direct');
                $db_history->exec("INSERT INTO spool_history (type, timestamp, status, data, sent_on) VALUES ('SMS', '$data[timestamp]', '$mailid', '$data[data]', NOW())");
                pdo_exec("delete from spool where type = 'SMS' and ID = '$id' limit 1");
            }
        }
    }

    function execute_sms_backlog() {
        $db_history = getConnection('dwh');
        $sms_mailer = new JM_Sms();
        // search for sms that were not sent !!!
        $data = pdo_multiple_select("select ID, type, data, status, timestamp from spool where type = 'SMS' and DATE_SUB(CURRENT_TIMESTAMP(), INTERVAL 5 MINUTE) > timestamp");
        if (count($data) > 0) {
            foreach ($data as $row) {
                
                $ID = $this->sendsms($sms_mailer, $row, '', 'backlog');

                if($ID){
                    $db_history->exec("INSERT INTO spool_history (type, timestamp, status, data, sent_on) VALUES ('SMS', '$row[timestamp]', '$row[status]', '$row[data]', NOW())");
                    pdo_exec("delete from spool where type = 'SMS' and ID = '" . $row['ID'] . "' limit 1");
                }
            }
        }
    }

    function execute_pushnotif($mailid) {

        $db_history = getConnection('dwh');

        $pn_mailer = new JM_Pushnotif();

        if ($mailid != "") {
            $data = pdo_single_select("select ID, type, data, timestamp from spool where type = 'PUSHNOTIF' and status = '$mailid' limit 1");

            if (count($data) > 0) {
                $id = $this->sendpushnotif($pn_mailer, $data);
                $db_history->exec("INSERT INTO spool_history (type, timestamp, status, data, sent_on) VALUES ('PUSHNOTIF', '$data[timestamp]', '$mailid', '$data[data]', NOW())");
                pdo_exec("delete from spool where type = 'PUSHNOTIF' and ID = '$id' limit 1");
            }
        }
    }
    
     function execute_pushnotif_back_log() {
        $db_history = getConnection('dwh');
        $pn_mailer = new JM_Pushnotif();
        // search for sms that were not sent !!!
        $data = pdo_multiple_select("select ID, type, data, status, timestamp from spool where type = 'PUSHNOTIF' and DATE_SUB(CURRENT_TIMESTAMP(), INTERVAL 5 MINUTE) > timestamp");
        if (count($data) > 0) {
            foreach ($data as $row) {
                $ID = $this->sendpushnotif($pn_mailer, $row);
                if($ID){
                    $db_history->exec("INSERT INTO spool_history (type, timestamp, status, data, sent_on) VALUES ('PUSHNOTIF', '$row[timestamp]', '$row[status]', '$row[data]', NOW())");
                    pdo_exec("delete from spool where type = 'PUSHNOTIF' and ID = '" . $ID . "' limit 1");
                }
            }
        }
    }
    
    function senddata($mailer, $row) {
        $id = $row['ID'];
        $datastring = $row['data'];

        $data = explode("||||", $datastring);
        $recipient = $data[0];

        $subject = $data[1];
        $body = $data[2];
        $opts = unserialize(urldecode($data[3]));

        $response = $mailer->send_fromswiftmailer($recipient, $subject, $body, $opts);
        if ($response) {
            if (isset($opts['guest_qrcode'])) {
                unlink($opts['guest_qrcode']);
            }
        } else {
            
        }
        return $id;
    }

    function sendsms($mailer, $row, $mailid = '', $source_test) {
        $id = $row['ID'];
        $datastring = $row['data'];
        $data = explode("||||", $datastring);
        $recipient = $data[0];
        $subject = $data[1];
        $body = $data[2];
        $expeditor = $data[4];
        if($mailer->sendMessage($recipient, $body, $expeditor, $mailid, $source_test)){
            return $id;    
        }
        return $id;
    }

    function sendpushnotif($mailer, $row) {
        $id = $row['ID'];
        $datastring = $row['data'];
        $data = explode("||||", $datastring);
        $pn_type = $data[0];
        $recipient = $data[1];
        $body = $data[3];
        $mailer->sendPushnotif($recipient, $body, $pn_type, NULL);
        return $id;
    }
    
    function is_test_env(){
        
        if (strpos($_SERVER['HTTP_HOST'], 'api') !== false) {
            return false;
        }
        
        if (strpos($_SERVER['HTTP_HOST'], 'www') !== false) {
            return false;
        }
        
        return true;
    }

}

?>