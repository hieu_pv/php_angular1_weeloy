-- phpMyAdmin SQL Dump
-- version 3.3.9.2
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Aug 07, 2015 at 09:35 AM
-- Server version: 5.5.34
-- PHP Version: 5.5.10

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `weeloy88_dwh`
--

-- --------------------------------------------------------

--
-- Table structure for table `app_login_history`
--

CREATE TABLE `app_login_history` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(64) NOT NULL,
  `pass` varchar(64) NOT NULL,
  `appname` varchar(32) DEFAULT NULL,
  `appversion` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `app_login_history`
--


-- --------------------------------------------------------

--
-- Table structure for table `log_users`
--

CREATE TABLE `log_users` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` mediumint(8) unsigned NOT NULL,
  `log_date` datetime NOT NULL,
  `action` mediumint(9) NOT NULL,
  `ip` int(11) NOT NULL,
  `target` mediumint(9) DEFAULT NULL,
  `object` varchar(64) NOT NULL,
  `other` varchar(16) NOT NULL,
  `front_end` varchar(8) NOT NULL,
  `source` varchar(128) NOT NULL,
  `user_token` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=209 ;

--
-- Dumping data for table `log_users`
--

INSERT INTO `log_users` VALUES(1, 0, '2015-07-31 14:18:28', 900, 2130706433, 0, 'w-gourmand_restaurant_20150702_0018', '', '5', 'w-gourmand', '55bb133414d71');
INSERT INTO `log_users` VALUES(2, 0, '2015-07-31 14:18:28', 901, 2130706433, 0, 'w-gourmand_restaurant_20150702_0018', '', '5', 'w-gourmand', '55bb133414d71');
INSERT INTO `log_users` VALUES(3, 0, '2015-07-31 14:18:28', 50, 2130706433, 0, 'SG_SG_R_TheFunKitchen', 'front_restaurant', '1', 'w-gourmand_restaurant_20150702_0018', '55bb1104f2aed');
INSERT INTO `log_users` VALUES(4, 0, '2015-07-31 14:18:32', 801, 2130706433, 0, 'SG_SG_R_TheFunKitchen', '', '1', 'w-gourmand_restaurant_20150702_0018', '55bb1104f2aed');
INSERT INTO `log_users` VALUES(5, 0, '2015-07-31 14:18:46', 802, 2130706433, 0, 'SG_SG_R_TheFunKitchen', '', '1', 'w-gourmand_restaurant_20150702_0018', '55bb1104f2aed');
INSERT INTO `log_users` VALUES(6, 0, '2015-07-31 14:18:47', 820, 2130706433, 0, 'SG_SG_R_TheFunKitchen', '', '1', 'w-gourmand_restaurant_20150702_0018', '55bb1104f2aed');
INSERT INTO `log_users` VALUES(7, 0, '2015-07-31 14:19:56', 900, 2130706433, 0, 'w-gourmand_restaurant_20150702_0018', '', '5', 'w-gourmand', '55bb138c58964');
INSERT INTO `log_users` VALUES(8, 0, '2015-07-31 14:19:56', 901, 2130706433, 0, 'w-gourmand_restaurant_20150702_0018', '', '5', 'w-gourmand', '55bb138c58964');
INSERT INTO `log_users` VALUES(9, 0, '2015-07-31 14:19:56', 50, 2130706433, 0, 'SG_SG_R_TheFunKitchen', 'front_restaurant', '1', 'w-gourmand_restaurant_20150702_0018', '55bb1104f2aed');
INSERT INTO `log_users` VALUES(10, 0, '2015-07-31 14:20:51', 801, 2130706433, 0, 'SG_SG_R_TheFunKitchen', '', '1', 'w-gourmand_restaurant_20150702_0018', '55bb1104f2aed');
INSERT INTO `log_users` VALUES(11, 0, '2015-07-31 14:21:04', 900, 2130706433, 0, 'w-gourmand_restaurant_20150702_0018', '', '5', 'w-gourmand', '55bb13d04879e');
INSERT INTO `log_users` VALUES(12, 0, '2015-07-31 14:21:04', 901, 2130706433, 0, 'w-gourmand_restaurant_20150702_0018', '', '5', 'w-gourmand', '55bb13d04879e');
INSERT INTO `log_users` VALUES(13, 0, '2015-07-31 14:21:04', 50, 2130706433, 0, 'SG_SG_R_TheFunKitchen', 'front_restaurant', '1', 'w-gourmand_restaurant_20150702_0018', '55bb1104f2aed');
INSERT INTO `log_users` VALUES(14, 0, '2015-07-31 14:21:07', 801, 2130706433, 0, 'SG_SG_R_TheFunKitchen', '', '1', 'w-gourmand_restaurant_20150702_0018', '55bb1104f2aed');
INSERT INTO `log_users` VALUES(15, 0, '2015-07-31 14:21:18', 802, 2130706433, 0, 'SG_SG_R_TheFunKitchen', '', '1', 'w-gourmand_restaurant_20150702_0018', '55bb1104f2aed');
INSERT INTO `log_users` VALUES(16, 0, '2015-07-31 14:21:20', 820, 2130706433, 0, 'SG_SG_R_TheFunKitchen', '', '1', 'w-gourmand_restaurant_20150702_0018', '55bb1104f2aed');
INSERT INTO `log_users` VALUES(17, 0, '2015-07-31 14:27:06', 900, 2130706433, 0, 'w-gourmand_restaurant_20150702_0018', '', '5', 'w-gourmand', '55bb153a95563');
INSERT INTO `log_users` VALUES(18, 0, '2015-07-31 14:27:06', 901, 2130706433, 0, 'w-gourmand_restaurant_20150702_0018', '', '5', 'w-gourmand', '55bb153a95563');
INSERT INTO `log_users` VALUES(19, 0, '2015-07-31 14:27:06', 50, 2130706433, 0, 'SG_SG_R_TheFunKitchen', 'front_restaurant', '1', 'w-gourmand_restaurant_20150702_0018', '55bb1104f2aed');
INSERT INTO `log_users` VALUES(20, 0, '2015-07-31 14:27:10', 801, 2130706433, 0, 'SG_SG_R_TheFunKitchen', '', '1', 'w-gourmand_restaurant_20150702_0018', '55bb1104f2aed');
INSERT INTO `log_users` VALUES(21, 0, '2015-07-31 14:27:22', 802, 2130706433, 0, 'SG_SG_R_TheFunKitchen', '', '1', 'w-gourmand_restaurant_20150702_0018', '55bb1104f2aed');
INSERT INTO `log_users` VALUES(22, 0, '2015-07-31 14:27:31', 820, 2130706433, 0, 'SG_SG_R_TheFunKitchen', '', '1', 'w-gourmand_restaurant_20150702_0018', '55bb1104f2aed');
INSERT INTO `log_users` VALUES(23, 0, '2015-07-31 14:29:57', 900, 2130706433, 0, 'w-gourmand_restaurant_20150702_0018', '', '5', 'w-gourmand', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(24, 0, '2015-07-31 14:29:57', 901, 2130706433, 0, 'w-gourmand_restaurant_20150702_0018', '', '5', 'w-gourmand', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(25, 0, '2015-07-31 14:29:57', 50, 2130706433, 0, 'SG_SG_R_TheFunKitchen', 'front_restaurant', '1', 'w-gourmand_restaurant_20150702_0018', '55bb1104f2aed');
INSERT INTO `log_users` VALUES(26, 0, '2015-07-31 14:30:00', 801, 2130706433, 0, 'SG_SG_R_TheFunKitchen', '', '1', 'w-gourmand_restaurant_20150702_0018', '55bb1104f2aed');
INSERT INTO `log_users` VALUES(27, 0, '2015-07-31 14:30:09', 802, 2130706433, 0, 'SG_SG_R_TheFunKitchen', '', '1', 'w-gourmand_restaurant_20150702_0018', '55bb1104f2aed');
INSERT INTO `log_users` VALUES(28, 0, '2015-07-31 14:30:10', 820, 2130706433, 0, 'SG_SG_R_TheFunKitchen', '', '1', 'w-gourmand_restaurant_20150702_0018', '55bb1104f2aed');
INSERT INTO `log_users` VALUES(29, 0, '2015-07-31 14:30:31', 50, 2130706433, 0, '', 'home', '1', 'w-gourmand_restaurant_20150702_0018', '55bb1104f2aed');
INSERT INTO `log_users` VALUES(30, 0, '2015-07-31 14:30:33', 50, 2130706433, 0, '', 'search_restauran', '1', 'w-gourmand_restaurant_20150702_0018', '55bb1104f2aed');
INSERT INTO `log_users` VALUES(31, 0, '2015-07-31 14:30:36', 50, 2130706433, 0, 'SG_SG_R_NorthBorder', 'front_restaurant', '1', 'w-gourmand_restaurant_20150702_0018', '55bb1104f2aed');
INSERT INTO `log_users` VALUES(32, 0, '2015-07-31 14:30:40', 50, 2130706433, 0, 'SG_SG_R_TheFunKitchen', 'front_restaurant', '1', 'w-gourmand_restaurant_20150702_0018', '55bb1104f2aed');
INSERT INTO `log_users` VALUES(33, 0, '2015-07-31 14:30:40', 50, 2130706433, 0, 'SG_SG_R_TheFunKitchen', 'front_restaurant', '1', 'w-gourmand_restaurant_20150702_0018', '55bb1104f2aed');
INSERT INTO `log_users` VALUES(34, 0, '2015-07-31 14:30:43', 801, 2130706433, 0, 'SG_SG_R_TheFunKitchen', '', '1', 'w-gourmand_restaurant_20150702_0018', '55bb1104f2aed');
INSERT INTO `log_users` VALUES(35, 0, '2015-07-31 14:30:53', 802, 2130706433, 0, 'SG_SG_R_TheFunKitchen', '', '1', 'w-gourmand_restaurant_20150702_0018', '55bb1104f2aed');
INSERT INTO `log_users` VALUES(36, 0, '2015-07-31 14:30:54', 820, 2130706433, 0, 'SG_SG_R_TheFunKitchen', '', '1', 'w-gourmand_restaurant_20150702_0018', '55bb1104f2aed');
INSERT INTO `log_users` VALUES(37, 0, '2015-07-31 14:40:22', 50, 2130706433, 0, 'SG_SG_R_TheFunKitchen', 'front_restaurant', '1', 'w-gourmand_restaurant_20150702_0018', '55bb1104f2aed');
INSERT INTO `log_users` VALUES(38, 0, '2015-07-31 16:12:00', 50, 2130706433, 0, '', 'home', '1', 'w-gourmand_restaurant_20150702_0018', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(39, 0, '2015-07-31 16:12:02', 50, 2130706433, 0, '', 'home', '1', 'w-gourmand_restaurant_20150702_0018', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(40, 0, '2015-07-31 16:12:07', 50, 2130706433, 0, '', '404', '1', 'w-gourmand_restaurant_20150702_0018', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(41, 0, '2015-07-31 16:12:07', 50, 2130706433, 0, '', '404', '1', 'w-gourmand_restaurant_20150702_0018', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(42, 0, '2015-07-31 16:12:07', 50, 2130706433, 0, '', '404', '1', 'w-gourmand_restaurant_20150702_0018', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(43, 0, '2015-07-31 16:12:07', 50, 2130706433, 0, '', '404', '1', 'w-gourmand_restaurant_20150702_0018', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(44, 0, '2015-07-31 16:12:07', 50, 2130706433, 0, '', '404', '1', 'w-gourmand_restaurant_20150702_0018', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(45, 0, '2015-07-31 16:12:07', 50, 2130706433, 0, '', '404', '1', 'w-gourmand_restaurant_20150702_0018', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(46, 0, '2015-07-31 16:13:31', 50, 2130706433, 0, '', '404', '1', 'w-gourmand_restaurant_20150702_0018', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(47, 0, '2015-07-31 16:13:31', 50, 2130706433, 0, '', '404', '1', 'w-gourmand_restaurant_20150702_0018', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(48, 0, '2015-07-31 16:13:31', 50, 2130706433, 0, '', '404', '1', 'w-gourmand_restaurant_20150702_0018', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(49, 0, '2015-07-31 16:13:31', 50, 2130706433, 0, '', '404', '1', 'w-gourmand_restaurant_20150702_0018', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(50, 0, '2015-07-31 16:13:31', 50, 2130706433, 0, '', '404', '1', 'w-gourmand_restaurant_20150702_0018', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(51, 0, '2015-07-31 16:13:31', 50, 2130706433, 0, '', '404', '1', 'w-gourmand_restaurant_20150702_0018', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(52, 0, '2015-07-31 16:13:43', 50, 2130706433, 0, '', '404', '1', 'w-gourmand_restaurant_20150702_0018', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(53, 0, '2015-07-31 16:13:43', 50, 2130706433, 0, '', '404', '1', 'w-gourmand_restaurant_20150702_0018', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(54, 0, '2015-07-31 16:13:43', 50, 2130706433, 0, '', '404', '1', 'w-gourmand_restaurant_20150702_0018', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(55, 0, '2015-07-31 16:13:43', 50, 2130706433, 0, '', '404', '1', 'w-gourmand_restaurant_20150702_0018', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(56, 0, '2015-07-31 16:13:43', 50, 2130706433, 0, '', '404', '1', 'w-gourmand_restaurant_20150702_0018', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(57, 0, '2015-07-31 16:13:43', 50, 2130706433, 0, '', '404', '1', 'w-gourmand_restaurant_20150702_0018', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(58, 0, '2015-07-31 16:13:53', 50, 2130706433, 0, '', 'home', '1', 'w-gourmand_restaurant_20150702_0018', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(59, 0, '2015-07-31 16:13:54', 50, 2130706433, 0, '', 'home', '1', 'w-gourmand_restaurant_20150702_0018', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(60, 0, '2015-07-31 16:13:55', 50, 2130706433, 0, '', 'front_restaurant', '1', 'w-gourmand_restaurant_20150702_0018', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(61, 0, '2015-07-31 16:13:59', 50, 2130706433, 0, '', 'front_restaurant', '1', 'w-gourmand_restaurant_20150702_0018', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(62, 0, '2015-07-31 16:14:04', 50, 2130706433, 0, '', '404', '1', 'w-gourmand_restaurant_20150702_0018', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(63, 0, '2015-07-31 16:14:04', 50, 2130706433, 0, '', '404', '1', 'w-gourmand_restaurant_20150702_0018', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(64, 0, '2015-07-31 16:14:04', 50, 2130706433, 0, '', '404', '1', 'w-gourmand_restaurant_20150702_0018', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(65, 0, '2015-07-31 16:14:04', 50, 2130706433, 0, '', '404', '1', 'w-gourmand_restaurant_20150702_0018', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(66, 0, '2015-07-31 16:14:04', 50, 2130706433, 0, '', '404', '1', 'w-gourmand_restaurant_20150702_0018', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(67, 0, '2015-07-31 16:14:04', 50, 2130706433, 0, '', '404', '1', 'w-gourmand_restaurant_20150702_0018', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(68, 0, '2015-07-31 16:15:48', 50, 2130706433, 0, '', '404', '1', '', '');
INSERT INTO `log_users` VALUES(69, 0, '2015-07-31 16:15:48', 50, 2130706433, 0, '', '404', '1', '', '');
INSERT INTO `log_users` VALUES(70, 0, '2015-07-31 16:15:48', 50, 2130706433, 0, '', '404', '1', '', '');
INSERT INTO `log_users` VALUES(71, 0, '2015-07-31 16:15:48', 50, 2130706433, 0, '', '404', '1', '', '');
INSERT INTO `log_users` VALUES(72, 0, '2015-07-31 16:15:48', 50, 2130706433, 0, '', '404', '1', '', '');
INSERT INTO `log_users` VALUES(73, 0, '2015-07-31 16:15:48', 50, 2130706433, 0, '', '404', '1', '', '');
INSERT INTO `log_users` VALUES(74, 0, '2015-07-31 16:15:48', 50, 2130706433, 0, '', '404', '1', '', '');
INSERT INTO `log_users` VALUES(75, 0, '2015-07-31 16:15:48', 50, 2130706433, 0, '', '404', '1', '', '');
INSERT INTO `log_users` VALUES(76, 0, '2015-07-31 16:15:53', 50, 2130706433, 0, '', '404', '1', '', '');
INSERT INTO `log_users` VALUES(77, 0, '2015-07-31 16:15:53', 50, 2130706433, 0, '', '404', '1', '', '');
INSERT INTO `log_users` VALUES(78, 0, '2015-07-31 16:15:53', 50, 2130706433, 0, '', '404', '1', '', '');
INSERT INTO `log_users` VALUES(79, 0, '2015-07-31 16:15:53', 50, 2130706433, 0, '', '404', '1', '', '');
INSERT INTO `log_users` VALUES(80, 0, '2015-07-31 16:15:53', 50, 2130706433, 0, '', '404', '1', '', '');
INSERT INTO `log_users` VALUES(81, 0, '2015-07-31 16:15:53', 50, 2130706433, 0, '', '404', '1', '', '');
INSERT INTO `log_users` VALUES(82, 0, '2015-07-31 16:15:53', 50, 2130706433, 0, '', '404', '1', '', '');
INSERT INTO `log_users` VALUES(83, 0, '2015-07-31 16:15:53', 50, 2130706433, 0, '', '404', '1', '', '');
INSERT INTO `log_users` VALUES(84, 0, '2015-07-31 16:16:36', 50, 2130706433, 0, '', '404', '1', '', '');
INSERT INTO `log_users` VALUES(85, 0, '2015-07-31 16:16:36', 50, 2130706433, 0, '', '404', '1', '', '');
INSERT INTO `log_users` VALUES(86, 0, '2015-07-31 16:16:36', 50, 2130706433, 0, '', '404', '1', '', '');
INSERT INTO `log_users` VALUES(87, 0, '2015-07-31 16:16:36', 50, 2130706433, 0, '', '404', '1', '', '');
INSERT INTO `log_users` VALUES(88, 0, '2015-07-31 16:16:36', 50, 2130706433, 0, '', '404', '1', '', '');
INSERT INTO `log_users` VALUES(89, 0, '2015-07-31 16:16:36', 50, 2130706433, 0, '', '404', '1', '', '');
INSERT INTO `log_users` VALUES(90, 0, '2015-07-31 16:16:36', 50, 2130706433, 0, '', '404', '1', '', '');
INSERT INTO `log_users` VALUES(91, 0, '2015-07-31 16:16:36', 50, 2130706433, 0, '', '404', '1', '', '');
INSERT INTO `log_users` VALUES(92, 0, '2015-07-31 16:16:39', 50, 2130706433, 0, '', '404', '1', '', '');
INSERT INTO `log_users` VALUES(93, 0, '2015-07-31 16:16:39', 50, 2130706433, 0, '', '404', '1', '', '');
INSERT INTO `log_users` VALUES(94, 0, '2015-07-31 16:16:39', 50, 2130706433, 0, '', '404', '1', '', '');
INSERT INTO `log_users` VALUES(95, 0, '2015-07-31 16:16:39', 50, 2130706433, 0, '', '404', '1', '', '');
INSERT INTO `log_users` VALUES(96, 0, '2015-07-31 16:16:39', 50, 2130706433, 0, '', '404', '1', '', '');
INSERT INTO `log_users` VALUES(97, 0, '2015-07-31 16:16:39', 50, 2130706433, 0, '', '404', '1', '', '');
INSERT INTO `log_users` VALUES(98, 0, '2015-07-31 16:16:39', 50, 2130706433, 0, '', '404', '1', '', '');
INSERT INTO `log_users` VALUES(99, 0, '2015-07-31 16:17:33', 50, 2130706433, 0, '', '', '1', 'w-gourmand_restaurant_20150702_0018', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(100, 0, '2015-07-31 16:18:56', 50, 2130706433, 0, '', '404', '1', '', '');
INSERT INTO `log_users` VALUES(101, 0, '2015-07-31 16:18:56', 50, 2130706433, 0, '', '404', '1', '', '');
INSERT INTO `log_users` VALUES(102, 0, '2015-07-31 16:18:56', 50, 2130706433, 0, '', '404', '1', '', '');
INSERT INTO `log_users` VALUES(103, 0, '2015-07-31 16:18:56', 50, 2130706433, 0, '', '404', '1', '', '');
INSERT INTO `log_users` VALUES(104, 0, '2015-07-31 16:18:56', 50, 2130706433, 0, '', '404', '1', '', '');
INSERT INTO `log_users` VALUES(105, 0, '2015-07-31 16:18:56', 50, 2130706433, 0, '', '404', '1', '', '');
INSERT INTO `log_users` VALUES(106, 0, '2015-07-31 16:18:56', 50, 2130706433, 0, '', '404', '1', '', '');
INSERT INTO `log_users` VALUES(107, 0, '2015-07-31 16:55:03', 50, 2130706433, 0, '', 'home', '1', 'w-gourmand_restaurant_20150702_0018', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(108, 0, '2015-07-31 17:09:05', 50, 2130706433, 0, '', 'home', '1', 'w-gourmand_restaurant_20150702_0018', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(109, 0, '2015-08-04 12:01:28', 50, 2130706433, 0, '', 'home', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(110, 0, '2015-08-04 12:01:32', 50, 2130706433, 0, '', 'search_restauran', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(111, 0, '2015-08-04 12:02:39', 50, 2130706433, 0, '', 'search_restauran', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(112, 0, '2015-08-04 12:02:44', 50, 2130706433, 0, '', 'home', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(113, 0, '2015-08-04 12:02:47', 50, 2130706433, 0, '', 'home', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(114, 0, '2015-08-04 12:07:17', 50, 2130706433, 0, '', 'search_restauran', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(115, 0, '2015-08-04 12:07:19', 50, 2130706433, 0, '', 'search_restauran', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(116, 0, '2015-08-04 12:09:33', 50, 2130706433, 0, '', '404', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(117, 0, '2015-08-04 12:09:33', 50, 2130706433, 0, '', '404', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(118, 0, '2015-08-04 12:09:33', 50, 2130706433, 0, '', '404', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(119, 0, '2015-08-04 12:09:33', 50, 2130706433, 0, '', '404', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(120, 0, '2015-08-04 12:09:33', 50, 2130706433, 0, '', '404', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(121, 0, '2015-08-04 12:09:35', 50, 2130706433, 0, '', '404', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(122, 0, '2015-08-04 12:09:35', 50, 2130706433, 0, '', '404', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(123, 0, '2015-08-04 12:09:35', 50, 2130706433, 0, '', '404', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(124, 0, '2015-08-04 12:09:35', 50, 2130706433, 0, '', '404', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(125, 0, '2015-08-04 12:09:35', 50, 2130706433, 0, '', '404', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(126, 0, '2015-08-04 12:16:33', 50, 2130706433, 0, '', '404', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(127, 0, '2015-08-04 12:16:33', 50, 2130706433, 0, '', '404', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(128, 0, '2015-08-04 12:16:33', 50, 2130706433, 0, '', '404', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(129, 0, '2015-08-04 12:16:33', 50, 2130706433, 0, '', '404', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(130, 0, '2015-08-04 12:16:33', 50, 2130706433, 0, '', '404', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(131, 0, '2015-08-04 12:16:35', 50, 2130706433, 0, '', '404', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(132, 0, '2015-08-04 12:16:35', 50, 2130706433, 0, '', '404', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(133, 0, '2015-08-04 12:16:35', 50, 2130706433, 0, '', '404', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(134, 0, '2015-08-04 12:16:35', 50, 2130706433, 0, '', '404', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(135, 0, '2015-08-04 12:16:35', 50, 2130706433, 0, '', '404', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(136, 0, '2015-08-04 12:16:51', 50, 2130706433, 0, '', '404', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(137, 0, '2015-08-04 12:16:51', 50, 2130706433, 0, '', '404', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(138, 0, '2015-08-04 12:16:51', 50, 2130706433, 0, '', '404', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(139, 0, '2015-08-04 12:16:51', 50, 2130706433, 0, '', '404', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(140, 0, '2015-08-04 12:16:51', 50, 2130706433, 0, '', '404', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(141, 0, '2015-08-04 12:19:37', 50, 2130706433, 0, '', '404', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(142, 0, '2015-08-04 12:19:37', 50, 2130706433, 0, '', '404', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(143, 0, '2015-08-04 12:19:37', 50, 2130706433, 0, '', '404', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(144, 0, '2015-08-04 12:19:37', 50, 2130706433, 0, '', '404', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(145, 0, '2015-08-04 12:19:37', 50, 2130706433, 0, '', '404', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(146, 0, '2015-08-04 12:19:40', 50, 2130706433, 0, '', 'home', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(147, 0, '2015-08-04 12:19:41', 50, 2130706433, 0, '', 'home', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(148, 0, '2015-08-04 12:20:11', 50, 2130706433, 0, '', 'home', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(149, 0, '2015-08-04 12:20:15', 50, 2130706433, 0, '', 'how-it-works', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(150, 0, '2015-08-04 12:20:17', 50, 2130706433, 0, '', 'how-it-works', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(151, 0, '2015-08-04 12:20:19', 50, 2130706433, 0, '', '404', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(152, 0, '2015-08-04 12:20:19', 50, 2130706433, 0, '', '404', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(153, 0, '2015-08-04 12:20:19', 50, 2130706433, 0, '', '404', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(154, 0, '2015-08-04 12:20:19', 50, 2130706433, 0, '', '404', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(155, 0, '2015-08-04 12:20:19', 50, 2130706433, 0, '', '404', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(156, 0, '2015-08-04 12:20:57', 50, 2130706433, 0, '', '404', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(157, 0, '2015-08-04 12:20:57', 50, 2130706433, 0, '', '404', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(158, 0, '2015-08-04 12:20:57', 50, 2130706433, 0, '', '404', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(159, 0, '2015-08-04 12:20:57', 50, 2130706433, 0, '', '404', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(160, 0, '2015-08-04 12:20:57', 50, 2130706433, 0, '', '404', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(161, 0, '2015-08-04 12:22:49', 50, 2130706433, 0, '', '404', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(162, 0, '2015-08-04 12:22:49', 50, 2130706433, 0, '', '404', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(163, 0, '2015-08-04 12:22:49', 50, 2130706433, 0, '', '404', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(164, 0, '2015-08-04 12:22:49', 50, 2130706433, 0, '', '404', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(165, 0, '2015-08-04 12:22:49', 50, 2130706433, 0, '', '404', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(166, 0, '2015-08-04 12:22:56', 50, 2130706433, 0, '', 'home', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(167, 0, '2015-08-04 12:22:56', 50, 2130706433, 0, '', 'home', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(168, 0, '2015-08-04 12:22:58', 50, 2130706433, 0, '', 'home', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(169, 0, '2015-08-04 12:23:05', 50, 2130706433, 0, '', 'search_restauran', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(170, 0, '2015-08-04 12:25:17', 50, 2130706433, 0, '', '404', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(171, 0, '2015-08-04 12:25:17', 50, 2130706433, 0, '', '404', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(172, 0, '2015-08-04 12:25:17', 50, 2130706433, 0, '', '404', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(173, 0, '2015-08-04 12:25:17', 50, 2130706433, 0, '', '404', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(174, 0, '2015-08-04 12:25:17', 50, 2130706433, 0, '', '404', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(175, 0, '2015-08-04 12:25:17', 50, 2130706433, 0, '', '404', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(176, 0, '2015-08-04 12:25:50', 50, 2130706433, 0, '', '404', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(177, 0, '2015-08-04 12:25:50', 50, 2130706433, 0, '', '404', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(178, 0, '2015-08-04 12:25:51', 50, 2130706433, 0, '', '404', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(179, 0, '2015-08-04 12:25:51', 50, 2130706433, 0, '', '404', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(180, 0, '2015-08-04 12:25:51', 50, 2130706433, 0, '', '404', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(181, 0, '2015-08-04 12:25:51', 50, 2130706433, 0, '', '404', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(182, 0, '2015-08-04 12:26:27', 50, 2130706433, 0, '', 'home', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(183, 0, '2015-08-04 12:26:27', 50, 2130706433, 0, '', 'home', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(184, 0, '2015-08-04 12:26:29', 50, 2130706433, 0, '', 'home', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(185, 0, '2015-08-04 12:31:04', 50, 2130706433, 0, '', 'home', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(186, 0, '2015-08-04 12:31:23', 50, 2130706433, 0, '', 'home', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(187, 0, '2015-08-04 12:31:26', 50, 2130706433, 0, '', 'search_restauran', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(188, 0, '2015-08-04 12:31:28', 50, 2130706433, 0, '', 'home', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(189, 0, '2015-08-04 12:31:38', 50, 2130706433, 0, '', 'home', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(190, 111, '2015-08-05 11:06:05', 801, 2130706433, 0, 'SG_SG_R_NorthBorder', '', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(191, 111, '2015-08-05 11:32:16', 801, 2130706433, 0, 'SG_SG_R_TheFunKitchen', '', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(192, 111, '2015-08-05 11:32:20', 802, 2130706433, 0, 'SG_SG_R_TheFunKitchen', '', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(193, 111, '2015-08-05 11:32:21', 820, 2130706433, 0, 'SG_SG_R_TheFunKitchen', '', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(194, 111, '2015-08-05 14:51:21', 801, 2130706433, 0, 'SG_SG_R_NorthBorder', '', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(195, 111, '2015-08-05 14:52:43', 801, 2130706433, 0, 'SG_SG_R_NorthBorder', '', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(196, 111, '2015-08-05 14:53:07', 801, 2130706433, 0, 'SG_SG_R_NorthBorder', '', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(197, 111, '2015-08-05 14:55:03', 801, 2130706433, 0, 'SG_SG_R_NorthBorder', '', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(198, 111, '2015-08-05 14:55:21', 801, 2130706433, 0, 'SG_SG_R_NorthBorder', '', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(199, 111, '2015-08-05 14:55:32', 801, 2130706433, 0, 'SG_SG_R_NorthBorder', '', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(200, 111, '2015-08-05 14:55:48', 801, 2130706433, 0, 'SG_SG_R_NorthBorder', '', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(201, 111, '2015-08-05 14:56:08', 801, 2130706433, 0, 'SG_SG_R_NorthBorder', '', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(202, 111, '2015-08-05 14:56:30', 801, 2130706433, 0, 'SG_SG_R_NorthBorder', '', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(203, 111, '2015-08-05 14:56:58', 801, 2130706433, 0, 'SG_SG_R_NorthBorder', '', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(204, 111, '2015-08-05 14:57:16', 801, 2130706433, 0, 'SG_SG_R_NorthBorder', '', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(205, 111, '2015-08-05 14:58:19', 801, 2130706433, 0, 'SG_SG_R_NorthBorder', '', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(206, 111, '2015-08-05 15:01:45', 801, 2130706433, 0, 'SG_SG_R_NorthBorder', '', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(207, 0, '2015-08-06 15:54:57', 801, 2130706433, 0, 'SG_SG_R_NorthBorder', '', '1', '', '55bb15e5c6b53');
INSERT INTO `log_users` VALUES(208, 111, '2015-08-06 15:55:09', 801, 2130706433, 0, 'SG_SG_R_NorthBorder', '', '1', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `log_users_actions`
--

CREATE TABLE `log_users_actions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `id_action` smallint(6) NOT NULL,
  `name_action` varchar(32) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=32 ;

--
-- Dumping data for table `log_users_actions`
--

INSERT INTO `log_users_actions` VALUES(1, 1, 'Connection attempt to');
INSERT INTO `log_users_actions` VALUES(2, 2, 'Connected to');
INSERT INTO `log_users_actions` VALUES(3, 3, 'Inscription');
INSERT INTO `log_users_actions` VALUES(4, 4, 'validation email');
INSERT INTO `log_users_actions` VALUES(5, 5, 'pw recovery request');
INSERT INTO `log_users_actions` VALUES(6, 6, 'pw change');
INSERT INTO `log_users_actions` VALUES(7, 7, 'suspend account');
INSERT INTO `log_users_actions` VALUES(8, 8, 'delete account');
INSERT INTO `log_users_actions` VALUES(9, 9, 'modification account');
INSERT INTO `log_users_actions` VALUES(10, 30, 'change profile info');
INSERT INTO `log_users_actions` VALUES(11, 31, 'upload photo');
INSERT INTO `log_users_actions` VALUES(12, 32, 'delete_picture');
INSERT INTO `log_users_actions` VALUES(13, 33, 'define profile pic');
INSERT INTO `log_users_actions` VALUES(14, 50, 'visit');
INSERT INTO `log_users_actions` VALUES(26, 100, 'new abo');
INSERT INTO `log_users_actions` VALUES(28, 150, 'BO_UPD_RESTO_config');
INSERT INTO `log_users_actions` VALUES(29, 151, 'BO_UPD_RESTO_media');
INSERT INTO `log_users_actions` VALUES(30, 152, 'BO_UPD_RESTO_wheel');
INSERT INTO `log_users_actions` VALUES(31, 153, 'BO_UPD_RESTO_event');

-- --------------------------------------------------------

--
-- Table structure for table `search_history`
--

CREATE TABLE `search_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value` varchar(256) NOT NULL,
  `count` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `search_history`
--

INSERT INTO `search_history` VALUES(1, 'wqewqeqwewq', 1);

-- --------------------------------------------------------

--
-- Table structure for table `sms_status_history`
--

CREATE TABLE `sms_status_history` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `sent` varchar(256) NOT NULL,
  `reference` varchar(256) NOT NULL,
  `received` varchar(256) NOT NULL,
  `recipient` varchar(256) NOT NULL,
  `statuscode` varchar(256) NOT NULL,
  `errorcode` varchar(256) NOT NULL,
  `errordescription` varchar(256) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `sms_status_history`
--


-- --------------------------------------------------------

--
-- Table structure for table `spool_history`
--

CREATE TABLE `spool_history` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `type` varchar(12) NOT NULL,
  `status` int(11) NOT NULL,
  `data` text NOT NULL,
  `sent_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `spool_history`
--

INSERT INTO `spool_history` VALUES(1, '2015-06-04 19:03:53', 'SMS', 557030992, '+6582452082||||||||The Fun Kitchen\n15/06/2015 19:30\nConfirmation: R_ThebephW8QUh\nWeeloy Code: 1122 (for the wheel), \n+6563397777\n10 Bras Basah Road 0124653, \nSingapore||||N%3B||||Weeloy', '2015-06-11 14:10:40');
INSERT INTO `spool_history` VALUES(2, '2015-06-09 16:17:22', 'SMS', 5576, '+65 82452082||||||||The Fun Kitchen\n9/06/2015 19:00\nConfirmation: R_TheBePhUytaM\nWeeloy Code: 1122 (for the wheel), \n+6563397777\n10 Bras Basah Road 0124653, \nSingapore||||N%3B||||Weeloy', '2015-06-11 14:10:40');
INSERT INTO `spool_history` VALUES(3, '2015-06-09 16:19:31', 'SMS', 5576, '+65 82452082||||||||The Fun Kitchen\n9/06/2015 19:00\nConfirmation: R_TheBePh1WDaH\nWeeloy Code: 1122 (for the wheel), \n+6563397777\n10 Bras Basah Road 0124653, \nSingapore||||N%3B||||Weeloy', '2015-06-11 14:10:40');
INSERT INTO `spool_history` VALUES(4, '2015-06-09 17:15:21', 'SMS', 5576, '+65 82452082||||||||The One Kitchen\n9/06/2015 19:00\nConfirmation: R_TheBePhhe49s, \n+6563397777\n20 Bras Basah Road 0124653, \nSingapore||||N%3B||||Weeloy', '2015-06-11 14:10:41');
INSERT INTO `spool_history` VALUES(5, '2015-06-09 18:12:42', 'SMS', 5576, '+65 82452082||||||||The Fun Kitchen\n9/06/2015 19:00\nConfirmation: R_TheBePhvauEK\nWeeloy Code: 1122 (for the wheel), \n+6563397777\n10 Bras Basah Road 0124653, \nSingapore||||N%3B||||Weeloy', '2015-06-11 14:10:41');
INSERT INTO `spool_history` VALUES(6, '2015-06-10 11:32:45', 'SMS', 5577, '+65 82452082||||||||The Fun Kitchen\n10/06/2015 21:30\nConfirmation: R_TheBePhnz6AS\nWeeloy Code: 1122 (for the wheel), \n+6563397777\n10 Bras Basah Road 0124653, \nSingapore||||N%3B||||Weeloy', '2015-06-11 14:10:41');
