/* dayroutine 0.1 (c) 2015 by richard kefs. All right reserved */


var aday = {
	init: function(aDate) {
	dateAr = aDate.split('-');
	this.date = new Date(dateAr[0], parseInt(dateAr[1]) - 1, dateAr[2], 0, 0, 0);
	this.tmp = new Date();
	},
	
	toDays: function(a2Date) {
	dateAr = a2Date.split('-');
	date2 = new Date(dateAr[0], parseInt(dateAr[1]) - 1, dateAr[2], 0, 0, 0);
	datediff = date2.getTime() - this.date.getTime();
	return (datediff / (24 * 60 * 60 * 1000));
	},
	
	toWeek: function(dd) {
	this.date = new Date(this.tmp.getFullYear(), 0, 1);
	return Math.ceil((this.toDays(dd) + 4) / 7);
	},
	
	dayofweek: function(a2Date) {
	dateAr = a2Date.split('-');
	date2 = new Date(dateAr[0], parseInt(dateAr[1]) - 1, dateAr[2], 0, 0, 0);
	return date2.getDay();
	},
	
	numberDay: function(aDate) {
	dateAr = aDate.split('/');
	this.date = new Date(dateAr[2], parseInt(dateAr[1]) - 1, dateAr[0], 23, 59, 59);
	datediff = this.date.getTime() - Date.now();
	return Math.floor((datediff / (24 * 60 * 60 * 1000)));
	},
	
	
};

