module.exports = function(grunt) {

    // Project configuration.
    grunt.initConfig({
        concat: {
            options: {
                separator: ';',
            },
            dist: {
                src: [
                    'app/shared/directives/*.js',
                    'app/shared/services/*.js',
                    'app/shared/filters/*.js',
                    'app/components/home/*.js',
                    'app/components/search_page/*.js',
                    'app/components/restaurant_info_page/*.js',
                    'app/components/mybookings_page/*.js',
                    'app/components/myaccount_page/*.js',
                    'app/components/myreviews_page/*.js',
                    'app/components/info_contact_page/*.js',
                    'app/components/info_partner_page/*.js',
                    'app/components/faq_page/*.js',
                    'app/components/how_it_work_page/*.js',
                    'app/components/all_rewards_page/*.js',
                    'app/shared/controllers/*.js',
                    'app/config.js',
                    'app/app.js',
                    'app/routes.js'
                ],
                dest: 'weeloy.js',
            },
        },
        watch: {
            scripts: {
                files: [
                    'app/shared/directives/*.js',
                    'app/shared/services/*.js',
                    'app/shared/filters/*.js',
                    'app/components/home/*.js',
                    'app/components/search_page/*.js',
                    'app/components/restaurant_info_page/*.js',
                    'app/components/mybookings_page/*.js',
                    'app/components/myaccount_page/*.js',
                    'app/components/myreviews_page/*.js',
                    'app/components/info_contact_page/*.js',
                    'app/components/faq_page/*.js',
                    'app/components/how_it_work_page/*.js',
                    'app/components/all_rewards_page/*.js',
                    'app/shared/controllers/*.js',
                    'app/config.js',
                    'app/app.js',
                    'app/routes.js'
                ],
                tasks: ['concat'],
                options: {
                    spawn: false,
                },
            },
        },
    });

    // Load the plugin that provides the "uglify" task.
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-watch');

    // Default task(s).
    grunt.registerTask('default', ['watch']);

};
