var MyBookingReview = angular.module('MyBookingReview', []);
MyBookingReview.directive('reviewSection', ['$rootScope', '$http', function($rootScope, $http) {
    return {
        restrict: 'E',
        scope: {
        	reviewItem: '=',
        },
        templateUrl: 'client/app/shared/partial/_my_booking_page_review_section.html',
        link: function(scope, element, attrs) {
            var review = new Object();
            scope.review = review;
            $(".sliders").jRating({
                step: true,
                length: 5,
                canRateAgain: true,
                showRateInfo: false,
                nbRates: 5,
                sendRequest: false,
                onClick: RateElement,
            });
            function RateElement(element, rate) {
                rate = Math.round(rate / 36 * 5);
                switch ($(element).attr('rate-model')) {
                    case 'review.food':
                        review.food = rate;
                        break;
                    case 'review.ambiance':
                        review.ambiance = rate;
                        break;
                    case 'review.service':
                        review.service = rate;
                        break;
                    case 'review.price':
                        review.price = rate;
                        break;
                    default:
                        break;
                };
                scope.review = review;
            };
            scope.ReviewNow = function() {
                console.log(scope.review);
                var data = {
                	confirmation_id: scope.reviewItem.confirmation,
                	user_id: $rootScope.user.email,
                	restaurant_id: scope.reviewItem.restaurantinfo.restaurant,
                	food_rate: scope.review.food,
                	ambiance_rate: scope.review.ambiance,
                	service_rate: scope.review.service,
                	price_rate: scope.review.price,
                	comment: scope.review.comment,
                };
                console.log(JSON.stringify(data));
                $http.post('api/review',data).success(function(response){
                	console.log(response);
                })
            };
        },
    };
}]);
;var NfSearch = angular.module('NfSearch', []);
NfSearch.run(['$rootScope', function($rootScope) {}]);
NfSearch.directive('nfSearch', ['$rootScope', '$http', '$location', function($rootScope, $http, $location) {
    return {
        restrict: 'AE',
        scope: {
            url: '@',
            cities: '=',
        },
        templateUrl: 'client/app/shared/partial/_search_form.html',
        link: function(scope, element, attrs) {
            if (scope.nfSearch == undefined || scope.nfSearch == '') {
                var API_URL = 'api/search/fulltext';
            } else {
                var API_URL = scope.nfSearch;
            };
            if ($location.$$search.query != undefined) {
                scope.free_search = $location.$$search.query;
            };

            scope.city = scope.cities[0];

            if ($location.$$search.city != undefined) {
                scope.cities.forEach(function(city) {
                    if (city.name == $location.$$search.city) {
                        scope.city = city;
                    };
                });
            };

            scope.SearchFormSelectCity = function(city) {
                $location.search({
                    city: city.name,
                    page: 1,
                });
            };

            var Input = $(element).find('.search-input');

            $rootScope.showAutoCorrect = false;
            var cuisinelist = CuisineListInit();
            localStorage.removeItem('__restaurants_bloodhood_cache_key__data');
            var restaurants = RestaurantResourceInit(scope.city);

            TypeaheadInit(Input, restaurants, cuisinelist);

            function RestaurantResourceInit(city) {
                var prefetch_url = API_URL + '?city=' + city.name;
                var remote_url = API_URL + "?city=" + city.name + "&query=%QUERY*";
                var restaurants = new Bloodhound({
                    datumTokenizer: function(d) {
                        var test = Bloodhound.tokenizers.whitespace(d.title);
                        $.each(test, function(k, v) {
                            i = 0;
                            while ((i + 1) < v.length) {
                                test.push(v.substr(i, v.length));
                                i++;
                            }
                        })
                        return test;
                    },
                    queryTokenizer: Bloodhound.tokenizers.whitespace,
                    prefetch: {
                        url: prefetch_url,
                        thumbprint: 'ver 1.4',
                        cacheKey: 'restaurants_bloodhood_cache_key',
                        transform: function(response) {
                            var data = response.data.restaurants;
                            data.forEach(function(value, key) {
                                data[key].cuisine = data[key].cuisine.replace("|", " ");
                            });
                            return data;
                        },
                    },
                    remote: {
                        url: remote_url,
                        wildcard: '%QUERY',
                        transform: function(response) {
                            var data = response.data.restaurants;
                            data.forEach(function(value, key) {
                                data[key].cuisine = data[key].cuisine.replace("|", " ");
                            })
                            return data;
                        },
                    },
                });
                return restaurants;
            };

            function CuisineListInit() {
                var cuisinelist = new Bloodhound({
                    datumTokenizer: function(d) {
                        var test = Bloodhound.tokenizers.whitespace(d.cuisine);
                        $.each(test, function(k, v) {
                            i = 0;
                            while ((i + 1) < v.length) {
                                test.push(v.substr(i, v.length));
                                i++;
                            }
                        })
                        return test;
                    },
                    queryTokenizer: Bloodhound.tokenizers.whitespace,
                    prefetch: {
                        url: "api/cuisinelist",
                        transform: function(response) {
                            var data = response.data.cuisine;
                            return data;
                        },
                    }
                });
                return cuisinelist;
            };

            function TypeaheadInit(Input, restaurants, cuisinelist) {
                Input.typeahead({
                    minLength: 1,
                    highlight: true,
                    hint: true,
                }, {
                    name: 'title',
                    display: 'title',
                    source: restaurants,
                    templates: {
                        header: '<p class="search-box-header"><strong>Restaurant</strong><p>',
                    }
                }, {
                    name: 'cuisine',
                    display: 'cuisine',
                    source: cuisinelist,
                    templates: {
                        header: '<p class="search-box-header"><strong>Cuisine</strong><p>',
                    }
                });
            };
            Input.bind('typeahead:render', function(evt, suggestions, flag, name) {
                if (suggestions == undefined) {
                    var length = $('.twitter-typeahead').find('.tt-suggestion').length;
                    if (length == 0) {
                        if ($('#auto_correct').attr('query') == Input.val() || element.val() == '') {
                            return;
                        }
                        var params = {
                            query: Input.val(),
                            city: scope.city.name,
                        };
                        $('#auto_correct').attr('query', Input.val());
                        $http.get('../api/search/fulltext', {
                            params: params
                        }).success(function(response) {
                            if (response.status == 1) {
                                $rootScope.showAutoCorrect = true;
                                $rootScope.suggestions = response.data.restaurants;
                            };
                        });
                    };
                } else {
                    $rootScope.showAutoCorrect = false;
                };
            });
            scope.SelectCity = function(city) {
                scope.city = city;
                scope.free_search = '';
                Input.typeahead('destroy');
                localStorage.removeItem('__restaurants_bloodhood_cache_key__data');
                var restaurants = RestaurantResourceInit(city);
                TypeaheadInit(Input, restaurants, cuisinelist);
                $rootScope.$broadcast('ChangeCity', {
                    city: city
                });
            };
            // change ng-model when typeahead:select
            Input.bind('typeahead:select', function(evt, data) {
                if (data.title != undefined) {
                    scope.free_search = data.title;
                } else {
                    scope.free_search = data.cuisine;
                }
            });
            scope.SearchInputKeyUp = function(query, city, evt) {
                if (evt.keyCode == 13) {
                    go_search(query, city);
                };
            };
            scope.Search = function(query, city) {
                go_search(query, city);
            };

            function go_search(query, city) {
                var data = {
                    query: query,
                    city: city.name,
                };
                $location.path('/angular-client/search');
                $location.search({
                    query: query,
                    city: city.name,
                });
            };
        },
    };
}]);
;var ResizeFilter = angular.module('ResizeFilter', []);
ResizeFilter.run(['$rootScope', function($rootScope) {
    $(window).resize(function() {
        ResizeFilter();
    });
    $rootScope.$on('MapSizeChanged', function(evt) {
        ResizeFilter();
    });

    function ResizeFilter() {
        setTimeout(function() {
            if ($('search-filter').width() < 800) {
                $('search-filter .cuisine').css('width', '50%');
            } else {
                $('search-filter .cuisine').css('width', '25%');
            }
            if ($('search-filter').width() < 600) {
                $('.filterButton').addClass('filterButtonRight');
            } else {
                $('.filterButton').removeClass('filterButtonRight');
            };
        }, 100);
    };
}]);
ResizeFilter.directive('resizeFilter', function() {
    return {
        restrict: 'A',
        scope: true,
        link: function(scope, element, attrs) {
            if ($('search-filter').width() < 800) {
                $(element).css('width', '50%');
            }
            if ($('search-filter').width() < 600) {
                $('.filterButton').addClass('filterButtonRight');
            };
        },
    };
});
;var RestaurantItem = angular.module('RestaurantItem', []);
RestaurantItem.directive('restaurantItem', ['$rootScope', function($rootScope) {
    return {
        restrict: 'E',
        templateUrl: 'client/app/shared/partial/_restaurant_item.html',
        scope: {
            restaurant: '=',
            openInNewTab: '@',
        },
        link: function(scope, element, attrs) {
            var img = document.createElement('img');
            var url = 'https://media.weeloy.com/upload/restaurant/' + scope.restaurant.restaurant + '/' + scope.restaurant.image;
            img.src = url;
            if (scope.openInNewTab == 'true') {
                scope.OpenRestaurantInNewTab = '_blank';
            };
            img.onload = function() {
                $(element).find('.img-transparent').css('background', 'url(' + url + ')');
            };
            scope.go = function(resto, title) {
                WindowOpen('modules/booking/book_form.php?bkrestaurant=' + resto + '&bktitle=' + title);
            };

            function WindowOpen(url) {
                window.open(url, '_blank', 'toolbar=no, scrollbars=yes, resizable=yes, top=30, left=30, width=800, height=900');
            }
        },
    };
}]);
;var SearchFilterModule = angular.module('SearchFilterModule', []);
SearchFilterModule.directive('searchFilter', ['$rootScope', '$location', function($rootScope, $location) {
    return {
        restrict: 'AE',
        scope: {
            cities: '=',
            cuisineList: '=',
            pricingArray: '=',
        },
        templateUrl: 'client/app/shared/partial/_search_filter.html',
        link: function(scope, element, attrs) {

            if ($location.$$search.city == undefined) {
                scope.city = scope.cities[0];
            } else {
                scope.cities.forEach(function(value, key) {
                    if (value.name == $location.$$search.city) {
                        scope.city = value;
                    };
                });
            };

            if ($location.$$search.pricing == undefined) {
                scope.pricing = scope.pricingArray[0];
            } else {
                scope.pricingArray.forEach(function(value, key) {
                    if (value.data == $location.$$search.pricing) {
                        scope.pricing = value;
                    };
                });
            };

            scope.expandCuisineFilter = false;


            $rootScope.$on('cuisineListLoaded', function(evt, data) {
                if ($location.$$search.cuisine != undefined) {
                    cuisine = $location.$$search.cuisine.split('|');
                    scope.cuisineList = data.cuisineList;
                    for (var key in scope.cuisineList) {
                        scope.cuisineList[key].forEach(function(value, k) {
                            if (cuisine.indexOf(value.cuisine) > -1) {
                                scope.cuisineList[key][k].selected = true;
                            };
                        });
                    };
                };
            });


            scope.changeCuisineFilter = function(cuisine, evt) {
                cuisine.selected = !cuisine.selected;
                evt.stopPropagation();
                return;
            };
            scope.SelectCity = function(city) {
                $location.search('city', city.name);
            };
            scope.filter = function(cuisineList, city, pricing) {
                scope.city = city.data;
                scope.fireZoomEvent = false;
                scope.mymap.setZoom(12);
                scope.fireZoomEvent = true;
                if (city.data != undefined && city.data != null) {
                    setMapCenter(city.data);
                } else {
                    setMapCenter('');
                };
                LoadRestaurantData(city.data, pricing.data, cuisineList);
            };
            scope.ApplyFilter = function(city, cuisineList, pricing) {
                var cuisine = [];
                for (var key in scope.cuisineList) {
                    scope.cuisineList[key].forEach(function(value, k) {
                        if (value.selected) {
                            cuisine.push(value.cuisine);
                        };
                    });
                };
                cuisine = cuisine.join('|');
                $location.search({
                    city: city.name,
                    cuisine: cuisine,
                    pricing: pricing.data,
                });
            };
            scope.ClearFilter = function() {
                scope.city = scope.cities[0];
                scope.pricing = scope.pricingArray[0];
                for (var key in scope.cuisineList) {
                    scope.cuisineList[key].forEach(function(value, k) {
                        scope.cuisineList[key][k].selected = false;
                    });
                };
                scope.ApplyFilter(scope.city, scope.cuisineList, scope.pricing);
            };
        },
    };
}]);
;var Map = angular.module('Map', ['SkipReload']);
Map.run(['$rootScope', function($rootScope) {
    $(window).scroll(function() {
        map_div = $('#google-map');
        fixedLimit = $(document).height() - $('footer').height() - 40;
        map_div.css('max-height', fixedLimit + 'px');
        windowScroll = $(window).scrollTop() + map_div.height();
        if (windowScroll > fixedLimit) {
            var top = '-' + (windowScroll - fixedLimit - 40);
        } else {
            top = 40;
        };
        map_div.css('top', top + 'px');
    });
}]);
Map.directive('map', ['$rootScope', '$location', '$window', 'location', function($rootScope, $location, $window, location) {
    return {
        restrict: 'A',
        scope: {
            map: '=',
            restaurantsPerPage: '=',
            restaurantsInMap: '=',
            fitBounds: '=',
            page: '=',
        },
        link: function(scope, element, attrs) {
            $(element).css('min-height', $(window).height() - 40);
            $('.search-section').css('min-height', $(window).height());
            var GoogleMap;
            scope.$watch(function() {
                return scope.map;
            }, function() {
                if (scope.map != undefined) {
                    if (document.getElementById('gmap-sdk') != undefined && document.getElementById('gmap-sdk') != null) {
                        $window.loadInfoBox();
                    } else {
                        var script = document.createElement("script");
                        script.type = "text/javascript";
                        script.id = 'gmap-sdk';
                        script.src = "http://maps.google.com/maps/api/js?sensor=false&callback=loadInfoBox";
                        document.body.appendChild(script);
                    }
                };
            });
            $rootScope.$on('MapSizeChanged', function(evt) {
                google.maps.event.trigger(GoogleMap, "resize");
            });
            $window.loadInfoBox = function() {
                LoadJsAsync('//google-maps-utility-library-v3.googlecode.com/svn/tags/infobox/1.1.9/src/infobox.js', 'infobox-js', CreateMap);
            };

            function LoadJsAsync(src, id, callback) {
                if (document.getElementById(id) != undefined) {
                    callback();
                    return;
                };
                var ScriptElement = document.createElement('script');
                ScriptElement.id = id;
                ScriptElement.src = src;
                document.head.appendChild(ScriptElement);
                ScriptElement.onload = function() {
                    callback();
                };
            };

            function LoadInfoBox() {
                LoadJsAsync('//google-maps-utility-library-v3.googlecode.com/svn/tags/infobox/1.1.9/src/infobox.js', 'infobox-js', CreateMap);
            };

            function CreateMap() {
                var EventFireByUser = false;
                var infoBox = CreateBox();
                if (scope.fitBounds == undefined) {
                    var bounds = new google.maps.LatLngBounds();
                } else {
                    var bounds = scope.fitBounds;
                };
                var loc;

                var mapOptions = {
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    scrollwheel: false,
                };
                GoogleMap = new google.maps.Map(document.getElementById(attrs.id), mapOptions);

                scope.map.forEach(function(restaurant) {
                    var LatLng = restaurant.GPS.split(',');
                    loc = new google.maps.LatLng(LatLng[0], LatLng[1]);
                    bounds.extend(loc);
                });
                GoogleMap.fitBounds(bounds);
                GoogleMap.panToBounds(bounds);
                CreateMarker(GoogleMap, infoBox);
                // add Click event - hidden infoBox
                google.maps.event.addListener(GoogleMap, 'click', function() {
                    infoBox.close();
                });
                google.maps.event.addListener(GoogleMap, 'resize', function() {
                    setTimeout(function() {
                        CreateMap();
                    }, 10);
                });
                // add zoom_change event - change marker
                google.maps.event.addListener(GoogleMap, 'zoom_changed', function() {
                    ChangeMapBounds(GoogleMap, infoBox);
                    $rootScope.$broadcast('google_map_zoom_changed', {
                        restaurantsInMap: scope.restaurantsInMap
                    });
                });
                google.maps.event.addListener(GoogleMap, 'dragend', function() {
                    ChangeMapBounds(GoogleMap, infoBox);
                    $rootScope.$broadcast('google_map_dragend', {
                        restaurantsInMap: scope.restaurantsInMap
                    });
                });
            };

            function CreateMarker(GoogleMap, infoBox) {
                var skip = (scope.page - 1) * scope.restaurantsPerPage;
                for (i = 0; i < scope.restaurantsPerPage; i++) {
                    var index = skip + i;
                    if (scope.restaurantsInMap[index] != undefined) {
                        var LatLng = scope.restaurantsInMap[index].GPS.split(',');
                        LatLng = new google.maps.LatLng(LatLng[0], LatLng[1]);
                        var title = scope.restaurantsInMap[index].title;
                        if (scope.restaurantsInMap[index].best_offer != undefined) {
                            var best_of = scope.restaurantsInMap[index].best_offer.offer;
                        } else {
                            best_of = '';
                        };
                        var zindex = 1;
                        var marker = new google.maps.Marker({
                            ID: scope.restaurantsInMap[index].ID,
                            position: LatLng,
                            map: GoogleMap,
                            title: title,
                            region: scope.restaurantsInMap[index].region,
                            cuisine: scope.restaurantsInMap[index].cuisine,
                            pricing: scope.restaurantsInMap[index].pricing,
                            rating: scope.restaurantsInMap[index].rating,
                            offer: best_of,
                            zIndex: zindex,
                            imgSrc: 'https://media.weeloy.com/upload/restaurant/' + scope.restaurantsInMap[index].restaurant + '/' + scope.restaurantsInMap[index].image,
                            wheelvalue: scope.restaurantsInMap[index].wheelvalue,
                            restaurant: scope.restaurantsInMap[index].restaurant,
                        });
                        marker.setIcon('images/gmap-marker.png');
                        if (scope.restaurantsInMap[index].is_wheelable == 1) {
                            marker.wheel = '<div class="wheel">' +
                                '<img class="wheel_value" src="https://media.weeloy.com/upload/wheelvalues/wheelvalue_' + marker.wheelvalue + '.png"/>' +
                                '</div>';
                        } else {
                            marker.wheel = '';
                        };
                        scope.restaurantsInMap[index].gmapMarker = marker;
                        scope.restaurantsInMap[index].LatLng = LatLng;
                        google.maps.event.addListener(marker, 'click', (function(marker, i) {
                            return function() {
                                var imgSrc = 'https://media.weeloy.com/upload/restaurant/' + scope.restaurantsInMap[i].restaurant + '/' + scope.restaurantsInMap[i].image;
                                //marker.setIcon(image_over);

                                infoBox.setContent('<div class="border">' +
                                    '<div class="custom-grid">' +
                                    '<article class="ribbon_holder">' +
                                    '<figure>' +
                                    '<img src="' + imgSrc + '"/>' +
                                    '<figcaption class="offer-left">' + marker.offer + '</figcaption>' +
                                    '<figcaption class="offer-restau-name ng-binding">' + marker.title + '</figcaption>' +
                                    '</figure>' +
                                    '</article>' +
                                    '</div>' +
                                    '<div class="item">' +
                                    '<div class="item-info">' +
                                    '<div class="location-icon">' +
                                    '<div class="location-text"><span class="fa fa-map-marker"></span>  ' + marker.region + ' - ' + marker.pricing + '<br/>' + marker.cuisine + '</div>' +
                                    '</div>' +
                                    '<div class="book">' +
                                    '<button id="btn_book" class="btn custom_button" onClick="go(\'' + scope.restaurantsInMap[i].restaurant + '\', \'' + scope.restaurantsInMap[i].title + '\');">BOOK NOW</button>' +
                                    '</div>' +
                                    '</div>' +
                                    marker.wheel +
                                    '</div>' +
                                    '</div>' +
                                    '<span class="arrow"></span>');
                                infoBox.open(GoogleMap, marker);

                                GoogleMap.setCenter(marker.getPosition());

                            }
                        })(marker, i));
                    };
                };
                $rootScope.$on('PageChanged', function(evt, page) {
                    scope.page = page;
                    ClearMarker();
                    CreateMarker(GoogleMap, infoBox);
                });
            };

            function ChangeMapBounds(GoogleMap, infoBox) {
                var Bounds = GoogleMap.getBounds();
                if (Bounds != undefined && Bounds != null) {
                    scope.restaurantsInMap = [];
                    scope.map.forEach(function(restaurant, key) {
                        var LatLng = restaurant.GPS.split(',');
                        LatLng = new google.maps.LatLng(LatLng[0], LatLng[1]);
                        if (Bounds.contains(LatLng)) {
                            scope.restaurantsInMap.push(restaurant);
                        };
                    });
                    if ($location.$$search.page != undefined && $location.$$search.page != 1) {
                        scope.currentBounds = GoogleMap.getBounds();
                        location.skipReload().search({
                            page: 1
                        });
                        scope.page = 1;
                        $rootScope.$broadcast('ChangePageToOne');
                        scrollToTop();
                    };
                    ClearMarker();
                    CreateMarker(GoogleMap, infoBox);
                    try {
                        scope.$apply();
                    } catch (e) {
                        console.log(e);
                    };
                };
            };

            function ClearMarker() {
                for (var k = 0; k < scope.map.length; k++) {
                    if (scope.map[k].gmapMarker != undefined) {
                        scope.map[k].gmapMarker.setMap(null);
                    };
                };
            };

            function CreateBox() {
                var box = new InfoBox({
                    disableAutoPan: true,
                    maxWidth: 0,
                    pixelOffset: new google.maps.Size(-115, -300),
                    closeBoxMargin: '50px 200px',
                    closeBoxURL: '',
                    isHidden: false,
                    pane: 'floatPane',
                    enableEventPropagation: true
                });
                return box;
            };

            function scrollToTop() {
                $("html, body").animate({
                    scrollTop: 0
                }, 500);
            };
        },
    };
}]);
;var SkipReload = angular.module('SkipReload', []);
SkipReload.factory('location', ['$rootScope', '$route', '$location',
    function($rootScope, $route, $location) {
        $location.skipReload = function() {
            var lastRoute = $route.current;

            var deregister = $rootScope.$on('$locationChangeSuccess',
                function(e, absUrl, oldUrl) {
                    $route.current = lastRoute;
                    deregister();
                });
            return $location;
        };
        return $location;
    }
]);
;var LoginService = angular.module('LoginService', []);
LoginService.service('loginService', ['$http', function($http) {

    this.login = function(email, passw) {
        return $http.get("api/rmloginc/" + email + "/" + passw + "/web/1.0").then(function(response) {
            return response.data;
        });
    };

    this.change = function(email, passw, npassw, token) {
        return $http.post("api/services.php/rmloginc/module/change/", {
                'email': email,
                'password': passw,
                'npassword': npassw,
                'token': token
            })
            .then(function(response) {
                return response.data;
            });
    };

    this.forgot = function(email) {
        return $http.post("api/services.php/rmloginc/module/forgot/", {
                'email': email
            })
            .then(function(response) {
                return response.data;
            });
    };

    this.logout = function(email) {
        return $http.get("api/rmlogout/" + email).then(function(response) {
            return response.data;
        })
    };

    this.register = function(user) {
        var url = 'api/addmember/' + user.email + '/' + user.mobile + '/' + user.password + '/' + user.last_name + '/' + user.first_name + '/web';
        return $http.get(url).then(function(response) {
            return response.data;
        });
    };

    this.checkstatus = function(email, token) {
        return $http.post("api/services.php/rmloginc/module/checkstatus/", {
                'email': email,
                'token': token
            })
            .then(function(response) {
                return response.data;
            });
    };

    this.loginfacebook = function(params) {
        return $http.post("api/rmloginc/module/loginfacebook/", params)
            .then(function(response) {
                return response.data;
            });
    };

}]);

LoginService.factory('DataService', function($http, URL) {
    var getData = function() {
        return $http.get(URL + 'content.json');
    };

    return {
        getData: getData
    };
});
;var GetReviewClass = angular.module('GetReviewClass', []);
GetReviewClass.filter('get_review_class', function() {
    'use strict';
    return function(input) {
        if (input === undefined) {
            return false;
        }

        var res = (Math.round(input * 2) * 0.5) * 10;

        return 'review-' + res;
    };
});
;var ExplodeCuisineFilter = angular.module('ExplodeCuisineFilter', []);
ExplodeCuisineFilter.filter('explode_cuisine', function() {
    'use strict';
    return function(input, separator, items) {
        if (input === undefined) {
            return false;
        }
        var arr = input.split(separator),
            cuisine = '',
            i;
        for (i = 0; i < arr.length; i = i + 1) {
            if (i === items) {
                cuisine += '...';
                return cuisine;
            }
            if (cuisine === '') {
                cuisine += arr[i];
            } else {
                cuisine += ', ' + arr[i];
            }
        }
        return cuisine;
    };
});
;var noResult = angular.module('noResult', []);
noResult.filter('noResult', function() {
    return function(array) {
        try {
            if (array.length == 0) {
                return true;
            } else {
                return false;
            };
        } catch (e) {
        	return true;
        };
    };
});
;var TrustAsResource = angular.module('TrustAsResource', []);
TrustAsResource.filter('trustAsResource', ['$sce', function($sce) {
	return function(src) {
		return $sce.trustAsResourceUrl(src);
	};
}]);;var HomeController = angular.module('HomeController', ['NfSearch', 'HomeService', 'RestaurantItem']);
HomeController.controller('HomeCtrl', ['$rootScope', '$scope', 'Home', function($rootScope, $scope, Home) {
    $scope.cities = [{
        id: 1,
        name: 'Singapore'
    }, {
        id: 2,
        name: 'Bangkok'
    }, {
        id: 3,
        name: 'Phuket'
    }, {
        id: 4,
        name: 'Hong Kong'
    }];

    var HomeService = Home.getBestRestaurants('singapore', 6);
    HomeService.then(function(response) {
        console.log(response);
        if (response.status == 1) {
            $scope.BestRestaurants = response.data;
        };
    });
}]);
;var HomeService = angular.module('HomeService', []);
HomeService.service('Home', ['$http', '$q', function($http, $q) {
    this.getBestRestaurants = function(city, limit) {
        var defferred = $q.defer();
        var API_URL = 'api/restaurant/getShowcase/' + city + '/' + limit;
        $http.get(API_URL, {
            cache: true
        }).success(function(response) {
            defferred.resolve(response);
        });
        return defferred.promise;
    };
}]);
;var SearchController = angular.module('SearchController', [
    'Map',
    'SearchService',
    'SearchFilterModule',
    'ExplodeCuisineFilter',
    'GetReviewClass',
    'noResult',
    'ResizeFilter',
    'SkipReload',
    'RestaurantItem'
]);
SearchController.controller('SearchCtrl', [
    '$rootScope',
    '$scope',
    '$location',
    '$http',
    'location',
    'Search',
    function($rootScope, $scope, $location, $http, location, Search) {
        var Markers = [];
        $scope.showPagination = false;
        $scope.SearchSectionClasses = ['col-lg-8', 'col-md-8', 'col-sm-8', 'col-xs-12'];
        $scope.GoogleMapSectionClasses = ['col-lg-4', 'col-md-4', 'col-sm-4', 'hidden-xs'];
        $scope.RestaurantItemClasses = ['col-lg-4', 'col-md-4', 'col-sm-6', 'col-xs-12'];

        Search.getBestRestaurants($location.$$search.city).then(function(response) {
            console.log(response);
            if (response.status == 1) {
                response.data[0].cuisine = response.data[0].cuisine.replace('|', ', ');
                $scope.restaurantShowCase = response.data[0];
            };
        });

        $scope.MapExpanded = false;
        $scope.pageNumbers = [];
        $scope.restaurantsPerPage = config.restaurantsPerPage;
        $scope.ExpandMap = false;
        if ($location.$$search.page != undefined) {
            $scope.page = parseInt($location.$$search.page);
        } else {
            $scope.page = 1;
        };
        $rootScope.$on('ChangePageToOne', function() {
            $scope.page = 1;
            $scope.skip = ($scope.page - 1) * $scope.restaurantsPerPage;
        });
        $scope.setPage = function(page) {
            location.skipReload().search({
                page: page
            });
            $scope.page = page;
            $scope.skip = ($scope.page - 1) * $scope.restaurantsPerPage;
            $("html, body").animate({
                scrollTop: 0
            }, 500);

            $rootScope.$broadcast('PageChanged', $scope.page);
        };
        $scope.cities = [{
            id: 1,
            name: 'Singapore'
        }, {
            id: 2,
            name: 'Bangkok'
        }, {
            id: 3,
            name: 'Phuket'
        }, {
            id: 4,
            name: 'Hong Kong'
        }];

        $scope.skip = ($scope.page - 1) * $scope.restaurantsPerPage;
        var SearchService = Search.getRestaurant('both', $location.$$search.city, $location.$$search.cuisine, $location.$$search.pricing, $location.$$search.query);
        SearchService.then(function(response) {
            if (response.status == 1) {
                if (response.data.no_result == false) {
                    $scope.restaurants = response.data.restaurant;
                    $scope.restaurantsInMap = response.data.restaurant;
                    Pagination();
                };
            };
        });

        $scope.HighLightMarker = function(restaurant) {
            $scope.restaurants.forEach(function(value, key) {
                if (value.ID == restaurant.ID && value.gmapMarker != undefined) {
                    value.gmapMarker.setIcon('images/highlight-marker.png');
                };
            });
        };
        $scope.notHighLightMarker = function(restaurant) {
            $scope.restaurants.forEach(function(value, key) {
                if (value.ID == restaurant.ID && value.gmapMarker != undefined) {
                    value.gmapMarker.setIcon('images/gmap-marker.png');
                };
            });
        };

        var cuisineListShow = [{
            cuisine: 'french',
        }, {
            cuisine: 'italian',
        }, {
            cuisine: 'international',
        }, {
            cuisine: 'asian',
        }, ];
        $http.get('api/cuisinelist', {
            cache: true,
        }).success(function(response) {
            if (response.status == 1) {
                $scope.cuisineList = {
                    cuisineListShow: cuisineListShow,
                    cuisineListCollapse: response.data.cuisine,
                }
                $rootScope.$broadcast('cuisineListLoaded', {
                    cuisineList: $scope.cuisineList
                });
            }
        });

        $scope.pricingArray = [{
            id: 1,
            name: 'all',
            data: '',
        }, {
            id: 2,
            name: '$',
            data: 1,
        }, {
            id: 3,
            name: '$$',
            data: 2,
        }, {
            id: 4,
            name: '$$$',
            data: 3,
        }, {
            id: 5,
            name: '$$$$',
            data: 4,
        }, ];


        $rootScope.$on('google_map_dragend', function(evt, data) {
            $scope.restaurantsInMap = data.restaurantsInMap;
            Pagination();
        });
        $rootScope.$on('google_map_zoom_changed', function(evt, data) {
            $scope.restaurantsInMap = data.restaurantsInMap;
            Pagination();
        });

        $scope.ExpandMap = function() {
            if ($scope.MapExpanded) {
                $scope.MapExpanded = false;
                $scope.SearchSectionClasses = ['col-lg-8', 'col-md-8', 'col-sm-8', 'col-xs-12'];
                $scope.GoogleMapSectionClasses = ['col-lg-4', 'col-md-4', 'col-sm-4', 'hidden-xs'];
                $scope.RestaurantItemClasses = ['col-lg-4', 'col-md-4', 'col-sm-4', 'col-xs-12'];
            } else {
                $scope.MapExpanded = true;
                $scope.SearchSectionClasses = ['col-lg-6', 'col-md-6', 'col-sm-6', 'col-xs-12'];
                $scope.GoogleMapSectionClasses = ['col-lg-6', 'col-md-6', 'col-sm-6', 'hidden-xs'];
                $scope.RestaurantItemClasses = ['col-lg-6', 'col-md-6', 'col-sm-6', 'col-xs-12'];
            }
            $rootScope.$broadcast('MapSizeChanged');
        };

        function Pagination() {
            $scope.pageNumbers = [];
            if ($scope.page == 1) {
                $scope.showPrevBtn = false;
            } else {
                $scope.showPrevBtn = true;
            };
            if ($scope.restaurantsInMap.length < $scope.restaurantsPerPage) {
                $scope.showPagination = false;
            } else {
                $scope.showPagination = true;
                var totalPages = Math.ceil($scope.restaurantsInMap.length / $scope.restaurantsPerPage);
                for (i = 1; i <= totalPages; i++) {
                    $scope.pageNumbers.push(i);
                };
                if ($scope.page == totalPages) {
                    $scope.showNextBtn = false;
                } else {
                    $scope.showNextBtn = true;
                };
            };
        };
    }
]);
;var SearchService = angular.module('SearchService', []);
SearchService.service('Search', ['$http', '$q', function($http, $q) {
    this.getRestaurant = function(status, city, cuisine, pricing, free_search, page) {
        if (status == undefined) {
            status = 'both';
        };
        if (city == undefined) {
            city = 'singapore';
        };
        if (cuisine != undefined) {
            if (Array.isArray(cuisine)) {
                cuisine = cuisine.join('|');
            };
        } else {
            cuisine = '';
        };
        if (free_search == undefined) {
            free_search = '';
        };
        if (page == undefined) {
            page = 1;
        }
        if (pricing == undefined) {
            pricing = '';
        }
        var defferred = $q.defer();
        var params = {
            status: status,
            city: city,
            cuisine: cuisine,
            free_search: free_search,
            pricing: pricing,
        };
        $http.get('api/search/restaurant', {
            params: params,
            cache: false,
        }).success(function(response) {
            defferred.resolve(response);
        });
        return defferred.promise;
    };
    this.getBestRestaurants = function(city, limit) {
        if (city == undefined) {
            city = 'singapore';
        };
        if (limit == undefined) {
            limit = 1;
        };
        var defferred = $q.defer();
        var API_URL = 'api/restaurant/getShowcase/' + city + '/' + limit;
        $http.get(API_URL, {
            cache: true
        }).success(function(response) {
            defferred.resolve(response);
        });
        return defferred.promise;
    };
}]);
;var RestaurantInfoController = angular.module('RestaurantInfoController', ['RestaurantInfoService', 'TrustAsResource']);
RestaurantInfoController.filter('day', function() {
    return function(str) {
        str = str.toLowerCase();
        switch (str) {
            case 'monday':
                return 'Mon';
                break;
            case 'tuesday':
                return 'Tue';
                break;
            case 'wednesday':
                return 'Wed'
                break;
            case 'thursday':
                return 'Thu';
                break;
            case 'friday':
                return 'Fri';
                break;
            case 'saturday':
                return 'Sat';
                break;
            case 'sunday':
                return 'Sun'
                break;
            default:
                return str;
                break;
        }
    };
});
RestaurantInfoController.controller('RestaurantInfoCtrl', [
    '$rootScope',
    '$scope',
    '$routeParams',
    '$window',
    'RestaurantInfo',
    function($rootScope, $scope, $routeParams, $window, RestaurantInfo) {
        $rootScope.LoadFacebookSDK();
        RestaurantInfo.getFullInfo($routeParams.restaurant).then(function(response) {
            console.log(response);
            if (response.status == 1) {
                response.data.banner = 'https://media1.weeloy.com/upload/restaurant/' + $routeParams.restaurant + '/1440/' + response.data.restaurantinfo.images[0];
                response.data.logo = 'https://media1.weeloy.com/upload/restaurant/' + $routeParams.restaurant + '/' + response.data.restaurantinfo.logo;
                response.data.cuisineArray = response.data.restaurantinfo.cuisine.split('|');
                response.data.wheelArray = response.data.restaurantinfo.wheel.split('|');
                response.data.wheelArray.splice(0, 1);
                var GPS = response.data.restaurantinfo.GPS;
                GPS = GPS.split(',');
                response.data.lat = GPS[0];
                response.data.lng = GPS[1];
                var RestaurantServices = [];
                response.data.restaurantservices.forEach(function(service, key) {
                    var include = false;
                    var key;
                    RestaurantServices.forEach(function(value, k) {
                        if (value.category == service.categorie) {
                            include = true;
                            key = k;
                        };
                    });
                    if (include == false) {
                        var newService = {
                            category: service.categorie,
                            category_id: service.categorie_id,
                            category_icon: service.pico_categorie,
                            services: [{
                                name: service.service,
                                icon: service.pico_service,
                            }],
                        };
                        RestaurantServices.push(newService);
                    } else {
                        var service = {
                            name: service.service,
                            icon: service.pico_service,
                        };
                        RestaurantServices[key].services.push(service);
                    }
                });
                response.data.RestaurantServices = RestaurantServices;
                $scope.restaurant = response.data;
            };
        });

        RestaurantInfo.getReviews($routeParams.restaurant).then(function(response) {
            console.log(response);
            if (response.status == 1) {
                if (response.data.reviews != undefined) {
                    response.data.reviews.forEach(function(value, key) {
                        var time_str = value.post_date.replace('-', ' ');
                        var time = new Date(time_str);
                        var month = new Array();
                        month[0] = "January";
                        month[1] = "February";
                        month[2] = "March";
                        month[3] = "April";
                        month[4] = "May";
                        month[5] = "June";
                        month[6] = "July";
                        month[7] = "August";
                        month[8] = "September";
                        month[9] = "October";
                        month[10] = "November";
                        month[11] = "December";
                        response.data.reviews[key].time = month[time.getMonth()] + ' ' + time.getFullYear();
                    });
                    $scope.reviews = response.data;
                };
            };
        });

        $scope.btnBookNow_click = function(restaurant) {
            var url = "modules/booking/book_form.php?bkrestaurant=" + restaurant.restaurantinfo.restaurant + "&amp;bktitle=" + restaurant.restaurantinfo.title;
            window.open(url, '_blank', 'toolbar=no, scrollbars=yes, resizable=yes, top=30, left=30, width=800, height=900');
        };

        $scope.fbshare = function(restaurant) {
            var pictureUrl = 'https://media1.weeloy.com/upload/restaurant/' + restaurant.restaurantinfo.restaurant + '/1440/' + restaurant.restaurantinfo.images[0],
                resTitle = restaurant.restaurantinfo.title,
                resDescription = restaurant.restaurantinfo.description[0].body[0];
            var obj = {
                method: 'feed',
                link: BASE_URL + '/restaurant/' + restaurant.restaurantinfo.restaurant,
                picture: pictureUrl,
                name: resTitle,
                caption: 'Book ' + resTitle + ' & win rewards with Weeloy',
                description: resDescription,
                display: 'popup'
            };

            FB.ui(obj, function(response) {

            });
        }
        $scope.showReview = function() {
            $scope.showReviewSection = true;
            setTimeout(function() {
                var offset = $('#review-section').offset();
                $("html, body").animate({
                    scrollTop: offset.top - 100,
                }, 500);
            }, 100);
        };
        var offset;
        $scope.Affix = function() {
            offset = $('#btn_affix').offset();
            var LastElement = $('.right .last');
            $(window).scroll(function() {
                if ($(window).scrollTop() > offset.top - 15) {
                    LastElement.addClass('fixedTop');
                    LastElement.css('margin-left', offset.left - 15);
                } else {
                    LastElement.removeClass('fixedTop');
                    LastElement.css('margin-left', 0);
                };
            });
        };
        $(window).resize(function() {
            offset = $('#btn_affix').offset();
        });


        $scope.loadmap = function(lat, lng) {
            if (document.getElementById('gmap-sdk') != undefined && document.getElementById('gmap-sdk') != null) {
                CreateMap(lat, lng);
            } else {
                var script = document.createElement("script");
                script.type = "text/javascript";
                script.id = 'gmap-sdk';
                script.src = "http://maps.google.com/maps/api/js?sensor=false&callback=CreateMap";
                document.body.appendChild(script);
            }
        };

        $window.CreateMap = function() {
            CreateMap($scope.restaurant.lat, $scope.restaurant.lng);
        };

        function CreateMap(lat, lng) {
            var myLatlng = new google.maps.LatLng(lat, lng);
            var mapOptions = {
                zoom: 14,
                center: myLatlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                scrollwheel: false,
                navigationControl: false,
                mapTypeControl: false,
            };
            if ($(window).width < 767) {
                mapOptions.scaleControl = false;
                mapOptions.draggable = false;
                mapOptions.zoomControl = false;
            }
            map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);
            var marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
            });
        };
    }
]);
;var RestaurantInfoService = angular.module('RestaurantInfoService', []);
RestaurantInfoService.service('RestaurantInfo', ['$http', '$q', function($http, $q) {
    this.getFullInfo = function(query) {
        var API_URL = 'api/restaurantfullinfo/' + query;
        var defferred = $q.defer();
        $http.get(API_URL, {
            cache: true,
        }).success(function(response) {
            getRestaurantService(response.data.restaurantinfo.restaurant).then(function(restaurant_service_response) {
                if (restaurant_service_response.status == 1) {
                    response.data.restaurantservices = restaurant_service_response.data.restaurantservices;
                }
                defferred.resolve(response);
            });
        });
        return defferred.promise;
    };
    this.getReviews = function(query) {
        var API_URL = 'api/getreviews/' + query + '/list/summary';
        var defferred = $q.defer();
        $http.get(API_URL).success(function(response) {
            defferred.resolve(response);
        });
        return defferred.promise;
    };

    function getRestaurantService(restaurant) {
        var API_URL = 'api/restaurant/service/restaurantservices/' + restaurant;
        var defferred = $q.defer();
        $http.get(API_URL, {
            cache: true,
        }).success(function(response) {
            defferred.resolve(response);
        });
        return defferred.promise;
    };
}]);
;var MyBookingController = angular.module('MyBookingController', ['MyBookingService', 'MyBookingReview']);
MyBookingController.controller('MyBookingCtrl', ['$rootScope', '$scope', '$location', 'Booking', function($rootScope, $scope, $location, Booking) {
    $rootScope.checkLoggedin();
    $rootScope.MenuUserSelected = 'mybookings';
    if ($location.$$search.f == undefined) {
        $location.search('f', 'today');
        return;
    };
    $scope.MyBookingPage = $location.$$search.f;
    var bookings = Booking.get($location.$$search.f);
    $rootScope.LoadFacebookSDK();
    bookings.then(function(response) {
        console.log(response);
        if (response.status == 1) {
            var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
            var weekday = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
            if (response.data.bookings != undefined || response.data.bookings != null) {
                $scope.showBookingList = true;
                response.data.bookings.forEach(function(value, key) {
                    var rdate = value.rdate.split('-');
                    var rtime = value.rtime.split(':');
                    var d = new Date(rdate[0], rdate[1], rdate[2], rtime[0], rtime[1], rtime[2]);
                    var currentTime = new Date();
                    response.data.bookings[key].time = d;
                    if (currentTime.getTime() - d.getTime() > 0) {
                        response.data.bookings[key].passed = true;
                    } else {
                        response.data.bookings[key].passed = false;
                    };
                    response.data.bookings[key].timeStr = monthNames[d.getMonth()] + ' ' + d.getDate() + ', ' + d.getUTCFullYear();
                    response.data.bookings[key].image = 'https://media3.weeloy.com/upload/restaurant/SG_SG_R_TheFunKitchen/2_The_fun_kitchen.jpg';
                });
                $scope.bookings = response.data.bookings;
            } else {
                $scope.showBookingList = false;
            };
        };
    });
    $scope.Review = function(item) {
        $scope.ReviewItem = item;
        $scope.showReviewSection = true;
        setTimeout(function() {
            var offset = $('review-section').offset();
            $("html, body").animate({
                scrollTop: offset.top - 100,
            }, 500);
        }, 100);
    };
    $scope.fbshare = function(item) {
        if (item.canceldate != null) {
            description = "I regret having to cancel my reservation" + item.wheelwin + " @ " + item.restaurantinfo.title + " #weeloy";
            caption = "Booking Cancel " + "@ " + item.restaurantinfo.title;
        } else {
            description = "I just enjoyed " + item.wheelwin + " @ " + item.restaurantinfo.title + " #weeloy";
            caption = item.wheelwin + " @ " + item.restaurantinfo.title;
        };
        var obj = {
            method: 'feed',
            link: BASE_URL + '/restaurant/' + item.restaurantinfo.restaurant,
            picture: "https://media1.weeloy.com/upload/restaurant/SG_SG_R_TheFunKitchen/2_The_fun_kitchen.jpg",
            name: item.restaurantinfo.title,
            caption: caption,
            description: description,
            display: 'popup'
        };
        FB.ui(obj, function(response) {
            console.log(response);
        });
    };

    $scope.tws_click = function(item) {
        var twtTitle = item.restaurantinfo.title,
            twtUrl = BASE_URL + '/restaurant/' + item.restaurantinfo.restaurant,
            maxLength = 140 - (twtUrl.length + 1),
            twtLink = 'http://twitter.com/home?status=' + encodeURIComponent(twtTitle + ' ' + twtUrl);
        window.open(twtLink, "_blank", "toolbar=yes, scrollbars=yes, resizable=no, width=600, height=400");
    };
}]);
;var MyBookingService = angular.module('MyBookingService', []);
MyBookingService.service('Booking', ['$http', '$q', function($http, $q) {
    this.get = function(period) {
        var defferred = $q.defer();
        var url = 'api/user/booking/period/' + period;
        $http.get(url, {
            cache: true,
        }).success(function(response) {
            defferred.resolve(response);
        });
        return defferred.promise;
    };
}]);
;var MyAccountController = angular.module('MyAccountController', ['MyAccountService', 'LoginService']);
MyAccountController.controller('MyAccountCtrl', ['$rootScope', '$scope', 'Account', 'loginService', function($rootScope, $scope, Account, loginService) {
    $rootScope.MenuUserSelected = 'myaccount';
    $rootScope.showAccountForm = 'myAccountForm';
    var genders = [{
        id: 1,
        name: 'Male'
    }, {
        id: 2,
        name: 'Female'
    }, ];
    $scope.genders = genders;
    $scope.UpdateUser = {
        email : $rootScope.user.email,
        firstname: $rootScope.user.data.firstname,
        lastname: $rootScope.user.data.lastname,
    }
    $scope.UpdateAccount = function(user) {
        console.log(user);
    };
}]);
;var MyAccountService = angular.module('MyAccountService', []);
MyAccountService.service('Account', function() {

});
;var MyReviewController = angular.module('MyReviewController', ['MyReviewService']);
MyReviewController.controller('MyReviewCtrl', ['$rootScope', '$scope', 'Review', function($rootScope, $scope, Review) {
    $rootScope.MenuUserSelected = 'myreviews';
    Review.getMyReviews().then(function(response) {
        console.log(response);
        if (response.status == 1) {
            var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
            var weekday = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
            response.data.reviews.forEach(function(value, key) {
            	var rtime = value.reviewdtcreate.split(/[-|:| ]/g);
            	var d = new Date(rtime[0], rtime[1], rtime[2], rtime[3], rtime[4], rtime[5]);
            	response.data.reviews[key].timeStr = monthNames[d.getMonth()] + ' ' + d.getDate() + ' ' + d.getFullYear() + ' at ' + d.getHours() + ':' + d.getMinutes(); 
            });
            $scope.ReviewsCount = response.count;
            $scope.myReviews = response.data.reviews;
        };
    });
}]);
;var MyReviewService = angular.module('MyReviewService', []);
MyReviewService.service('Review', ['$q', '$http', function($q, $http) {
    this.getMyReviews = function() {
        var defferred = $q.defer();
        $http.get('api/getReviewList').success(function(response) {
            defferred.resolve(response);
        });
        return defferred.promise;
    };
}]);
;var InfoContactController = angular.module('InfoContactController', ['InfoContactService']);
InfoContactController.controller('InfoContactCtrl', ['$rootScope', '$scope', 'Contact', function($rootScope, $scope, Contact) {
    $scope.cities = [{
        id: 1,
        name: 'Singapore'
    }, {
        id: 2,
        name: 'Hong Kong'
    }, {
        id: 3,
        name: 'Bangkok'
    }, ];
    $scope.contact = new Object();
    $scope.contact.city = $scope.cities[0];
    $scope.ContactSubmit = function(contact) {
        console.log(contact);
        Contact.sendContact(contact.firstname, contact.lastname, contact.email, contact.message).then(function(response) {
            console.log(response);
        });
    };
}]);

;var InfoContactService = angular.module('InfoContactService', []);
InfoContactService.service('Contact', ['$http', '$q', function($http, $q) {
    this.sendContact = function(firstname, lastname, email, message) {
        var API_URL = 'api/send-contact-email';
        var data = {
            firstname: firstname,
            lastname: lastname,
            email: email,
            message: message,
        };
        var defferred = $q.defer();
        $http.post(API_URL, data).success(function(response) {
            defferred.resolve(response);
        });
        return defferred.promise;
    };
}]);
;var InfoPartnerController = angular.module('InfoPartnerController', []);
InfoPartnerController.controller('InfoPartnerCtrl', ['$rootScope', '$scope', function($rootScope, $scope) {

}]);

;var FAQController = angular.module('FAQController', []);
FAQController.controller('FAQCtrl', ['$rootScope', '$scope', function($rootScope, $scope) {

}]);
;var HowItWorkController = angular.module('HowItWorkController', []);
HowItWorkController.controller('HowItWorkCtrl', ['$rootScope', '$scope', function($rootScope, $scope) {

}]);
;var AllRewardsController = angular.module('AllRewardsController', ['RestaurantInfoService', 'AllRewardsService']);
AllRewardsController.controller('AllRewardsCtrl', ['$rootScope', '$scope', '$routeParams', 'RestaurantInfo', 'Reward', function($rootScope, $scope, $routeParams, RestaurantInfo, Reward) {
    RestaurantInfo.getFullInfo($routeParams.restaurant).then(function(response) {
        console.log(response);
        if (response.status == 1) {
            $scope.restaurant = response.data;
        };
    });
    Reward.getReward($routeParams.restaurant).then(function(response) {
    	console.log(response);
        if (response.status == 1) {
            $scope.rewards = response.data;
        };
    });
}]);
;var AllRewardsService = angular.module('AllRewardsService', []);
AllRewardsService.service('Reward', ['$http', '$q', function($http, $q) {
    this.getReward = function(query) {
        var API_URL = 'api/wheeldescription/' + query;
        var defferred = $q.defer();
        $http.get(API_URL, {
            cache: true,
        }).success(function(response) {
            defferred.resolve(response);
        });
        return defferred.promise;
    };
}]);
;var RootController = angular.module('RootController', ['LoginService']);
RootController.controller('RootCtrl', [
    '$rootScope',
    '$scope',
    '$http',
    '$location',
    'loginService',
    function($rootScope, $scope, $http, $location, loginService) {
        $scope.showForm = 'loginForm';
        $scope.showLoggedinBox = false;
        $scope.showLoggedin = function(evt) {
            $scope.showLoggedinBox = !$scope.showLoggedinBox;
            evt.stopPropagation();
        };
        $('body').click(function(evt) {
            var parent = $(evt.target).parent();
            if (parent != undefined && parent.attr('id') != 'user-profile-btn') {
                $scope.showLoggedinBox = false;
                $scope.$apply();
            };
        });

        $scope.Login = function(user) {
            loginService.login(user.email, user.password).then(function(response) {
                console.log(response);
                if (response.status == 1) {
                    $rootScope.loggedin = true;
                    $rootScope.user = new Object();
                    $rootScope.user.email = user.email;
                    $rootScope.user.data = response.data;
                    $('#loginModal').modal('hide');
                    $rootScope.$broadcast('UserLogin', $rootScope.user);
                    $rootScope.showSystemMsg = true;
                    $rootScope.systemMsg = 'You have successfully logged in, ' + response.data.firstname;
                } else {
                    $rootScope.systemMsg = response.errors;
                    $rootScope.showSystemMsg = true;
                };
            });
        };
        $rootScope.$on('$routeChangeSuccess', function() {
            $rootScope.showSystemMsg = false;
        });
        $rootScope.hideSystemMsg = function() {
            $rootScope.showSystemMsg = false;
        };
        $scope.logout = function() {
            if ($rootScope.user == undefined || $rootScope.user == null) {
                return;
            };
            loginService.logout($rootScope.user.email).then(function(response) {
                console.log(response);
                if (response.status == 1) {
                    $rootScope.user = null;
                    $rootScope.loggedin = false;
                    $rootScope.systemMsg = 'You have successfully logged out';
                    $rootScope.showSystemMsg = true;
                    console.log($rootScope.showSystemMsg, $rootScope.systemMsg);
                };
            });
        };
        $('#loginModal').on('shown.bs.modal', function() {
            $rootScope.LoadFacebookSDK();
        });
        $rootScope.LoadFacebookSDK = function() {
            (function(d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) return;
                js = d.createElement(s);
                js.id = id;
                js.src = "//connect.facebook.net/en_US/sdk.js";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));
            window.fbAsyncInit = function() {
                FB.init({
                    appId: FB_ID,
                    cookie: true, // enable cookies to allow the server to access 
                    // the session
                    xfbml: true, // parse social plugins on this page
                    version: 'v2.2' // use version 2.2
                });
            };
        };
        $scope.LoginWithFacebook = function() {
            FB.login(function(response) {
                if (response.authResponse) {
                    console.log('Welcome!  Fetching your information.... ');
                    FB.api('/me', function(fb_response) {
                        console.log(fb_response);
                        var params = {
                            email: fb_response.email,
                            facebookid: fb_response.id,
                            facebooktoken: FB.getAuthResponse()['accessToken'],
                            platform: 'web',
                        };
                        var facebookLogin = loginService.loginfacebook(params);
                        facebookLogin.then(function(response) {
                            console.log(response);
                            if (response.status == 1) {
                                $rootScope.loggedin = true;
                                $rootScope.user = new Object();
                                $rootScope.user.email = params.email;
                                $rootScope.user.data = response.data;
                                $('#loginModal').modal('hide');
                                $rootScope.$broadcast('UserLogin', $rootScope.user);
                                $rootScope.showSystemMsg = true;
                                $rootScope.systemMsg = 'You have successfully logged in, ' + response.data.firstname;
                            } else {
                                if (response.status == 0) {
                                    $scope.showForm = 'signupForm';
                                    $scope.SignupUser = fb_response;
                                };
                            };
                        });
                    });
                } else {
                    console.log('User cancelled login or did not fully authorize.');
                };
            });
        };
        $scope.ChangePassword = function(email, old_password, new_password) {
            var changePassword = loginService.change(email, old_password, new_password, $rootScope.user.data.token);
            console.log(email, old_password, new_password, $rootScope.user.data.tokenx);
            changePassword.then(function(response) {
                console.log(response);
            });
        };
        $scope.SubmitNewletter = function(email) {
            $http.get('api/newsletter/addEmail/' + email).success(function(response) {
                if (response.status == 1) {
                    alert(response.data);
                } else {
                    alert(response.errors);
                };
            });
        };
        $scope.Signup = function(user) {
            loginService.register(user).then(function(response) {
                console.log(response);
                if (response.status == 1) {
                    $scope.Login(user);
                } else {
                    alert(response.errors);
                };
            });
        };
        $scope.ForgotPassword = function(email) {
            loginService.forgot(email).then(function(response) {
                if (response.status == 1) {
                    alert(response.errors);
                    $('#loginModal').modal('hide');
                } else {
                    alert(response.errors);
                };
            });
        };
    }
]);
RootController.filter('json', function() {
    return function(obj) {
        return JSON.stringify(obj);
    };
});
;var config = {
    lang: 'en',
    restaurantsPerPage: 12,
};
;var WeeloyApp = angular.module('WeeloyApp', [
    'ngRoute',
    'RootController',
    'HomeController',
    'SearchController',
    'RestaurantInfoController',
    'MyBookingController',
    'MyAccountController',
    'MyReviewController',
    'InfoPartnerController',
    'InfoContactController',
    'FAQController',
    'HowItWorkController',
    'AllRewardsController'
]);
WeeloyApp.run([
    '$rootScope',
    '$http',
    '$location',
    function($rootScope, $http, $location) {
        console.log('WeeloyApp is running...');

        $http.get('client/assets/language/' + config.lang + '.json', {
            cache: true,
        }).success(function(response) {
            $rootScope.Str = response.data.str;
        });
        $rootScope.loggedin = loggedin;
        $rootScope.user = user;
        $rootScope.$on('$routeChangeSuccess', function() {
            $("html, body").animate({
                scrollTop: 0
            }, 100);
        });
        if ($rootScope.user != null) {
            BasicAuthentication($rootScope.user);
        };
        $rootScope.$on('UserLogin', function(evt, user) {
            BasicAuthentication($rootScope.user);
        });

        function BasicAuthentication(user) {
            var Authorization = btoa(user.email + ':' + user.data.token);
            $http.defaults.headers.common.Authorization = 'Basic ' + Authorization;
        }
        $rootScope.checkLoggedin = function() {
            if ($rootScope.user == undefined || $rootScope.user == null) {
                $location.path('angular-client');
            };
        };
    }
]);
;WeeloyApp.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
    $routeProvider
        .when('/angular-client', {
            templateUrl: 'client/app/components/home/_home.html',
            controller: 'HomeCtrl',
        })
        .when('/angular-client/search', {
            templateUrl: 'client/app/components/search_page/_search_page.html',
            controller: 'SearchCtrl',
        })
        .when('/angular-client/restaurant/:restaurant', {
            templateUrl: 'client/app/components/restaurant_info_page/_restaurant_info_page.html',
            controller: 'RestaurantInfoCtrl',
        })
        .when('/angular-client/mybookings', {
            templateUrl: 'client/app/components/mybookings_page/_mybookings_page.html',
            controller: 'MyBookingCtrl',
        })
        .when('/angular-client/myaccount', {
            templateUrl: 'client/app/components/myaccount_page/_myaccount_page.html',
            controller: 'MyAccountCtrl',
        })
        .when('/angular-client/myreviews', {
            templateUrl: 'client/app/components/myreviews_page/_my_reviews_page.html',
            controller: 'MyReviewCtrl',
        })
        .when('/angular-client/info-contact', {
            templateUrl: 'client/app/components/info_contact_page/_info_contact_page.html',
            controller: 'InfoContactCtrl',
        })
        .when('/angular-client/info-partner', {
            templateUrl: 'client/app/components/info_partner_page/_info_partner.html',
            controller: 'InfoPartnerCtrl',
        })
        .when('/angular-client/how-it-works', {
            templateUrl: 'client/app/components/how_it_work_page/how_it_work.html',
            controller: 'HowItWorkCtrl',
        })
        .when('/angular-client/info-faq', {
            templateUrl: 'client/app/components/faq_page/faq.html',
            controller: 'FAQCtrl',
        })
        .when('/angular-client/all-rewards/:restaurant', {
            templateUrl: 'client/app/components/all_rewards_page/_all_rewards.html',
            controller: 'AllRewardsCtrl',
        });

    $locationProvider.html5Mode({
        enabled: true,
        requireBase: false
    });
}]);
