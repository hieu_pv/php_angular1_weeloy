var LoginService = angular.module('LoginService', []);
LoginService.service('loginService', ['$http', function($http) {

    this.login = function(email, passw) {
        return $http.get("api/rmloginc/" + email + "/" + passw + "/web/1.0").then(function(response) {
            return response.data;
        });
    };

    this.change = function(email, passw, npassw, token) {
        return $http.post("api/services.php/rmloginc/module/change/", {
                'email': email,
                'password': passw,
                'npassword': npassw,
                'token': token
            })
            .then(function(response) {
                return response.data;
            });
    };

    this.forgot = function(email) {
        return $http.post("api/services.php/rmloginc/module/forgot/", {
                'email': email
            })
            .then(function(response) {
                return response.data;
            });
    };

    this.logout = function(email) {
        return $http.get("api/rmlogout/" + email).then(function(response) {
            return response.data;
        })
    };

    this.register = function(user) {
        var url = 'api/addmember/' + user.email + '/' + user.mobile + '/' + user.password + '/' + user.last_name + '/' + user.first_name + '/web';
        return $http.get(url).then(function(response) {
            return response.data;
        });
    };

    this.checkstatus = function(email, token) {
        return $http.post("api/services.php/rmloginc/module/checkstatus/", {
                'email': email,
                'token': token
            })
            .then(function(response) {
                return response.data;
            });
    };

    this.loginfacebook = function(params) {
        return $http.post("api/rmloginc/module/loginfacebook/", params)
            .then(function(response) {
                return response.data;
            });
    };

}]);

LoginService.factory('DataService', function($http, URL) {
    var getData = function() {
        return $http.get(URL + 'content.json');
    };

    return {
        getData: getData
    };
});
