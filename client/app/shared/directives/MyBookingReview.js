var MyBookingReview = angular.module('MyBookingReview', []);
MyBookingReview.directive('reviewSection', ['$rootScope', '$http', function($rootScope, $http) {
    return {
        restrict: 'E',
        scope: {
        	reviewItem: '=',
        },
        templateUrl: 'client/app/shared/partial/_my_booking_page_review_section.html',
        link: function(scope, element, attrs) {
            var review = new Object();
            scope.review = review;
            $(".sliders").jRating({
                step: true,
                length: 5,
                canRateAgain: true,
                showRateInfo: false,
                nbRates: 5,
                sendRequest: false,
                onClick: RateElement,
            });
            function RateElement(element, rate) {
                rate = Math.round(rate / 36 * 5);
                switch ($(element).attr('rate-model')) {
                    case 'review.food':
                        review.food = rate;
                        break;
                    case 'review.ambiance':
                        review.ambiance = rate;
                        break;
                    case 'review.service':
                        review.service = rate;
                        break;
                    case 'review.price':
                        review.price = rate;
                        break;
                    default:
                        break;
                };
                scope.review = review;
            };
            scope.ReviewNow = function() {
                console.log(scope.review);
                var data = {
                	confirmation_id: scope.reviewItem.confirmation,
                	user_id: $rootScope.user.email,
                	restaurant_id: scope.reviewItem.restaurantinfo.restaurant,
                	food_rate: scope.review.food,
                	ambiance_rate: scope.review.ambiance,
                	service_rate: scope.review.service,
                	price_rate: scope.review.price,
                	comment: scope.review.comment,
                };
                console.log(JSON.stringify(data));
                $http.post('api/review',data).success(function(response){
                	console.log(response);
                })
            };
        },
    };
}]);
