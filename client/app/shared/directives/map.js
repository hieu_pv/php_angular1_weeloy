var Map = angular.module('Map', ['SkipReload']);
Map.run(['$rootScope', function($rootScope) {
    $(window).scroll(function() {
        map_div = $('#google-map');
        fixedLimit = $(document).height() - $('footer').height() - 40;
        map_div.css('max-height', fixedLimit + 'px');
        windowScroll = $(window).scrollTop() + map_div.height();
        if (windowScroll > fixedLimit) {
            var top = '-' + (windowScroll - fixedLimit - 40);
        } else {
            top = 40;
        };
        map_div.css('top', top + 'px');
    });
}]);
Map.directive('map', ['$rootScope', '$location', '$window', 'location', function($rootScope, $location, $window, location) {
    return {
        restrict: 'A',
        scope: {
            map: '=',
            restaurantsPerPage: '=',
            restaurantsInMap: '=',
            fitBounds: '=',
            page: '=',
        },
        link: function(scope, element, attrs) {
            $(element).css('min-height', $(window).height() - 40);
            $('.search-section').css('min-height', $(window).height());
            var GoogleMap;
            scope.$watch(function() {
                return scope.map;
            }, function() {
                if (scope.map != undefined) {
                    if (document.getElementById('gmap-sdk') != undefined && document.getElementById('gmap-sdk') != null) {
                        $window.loadInfoBox();
                    } else {
                        var script = document.createElement("script");
                        script.type = "text/javascript";
                        script.id = 'gmap-sdk';
                        script.src = "http://maps.google.com/maps/api/js?sensor=false&callback=loadInfoBox";
                        document.body.appendChild(script);
                    }
                };
            });
            $rootScope.$on('MapSizeChanged', function(evt) {
                google.maps.event.trigger(GoogleMap, "resize");
            });
            $window.loadInfoBox = function() {
                LoadJsAsync('//google-maps-utility-library-v3.googlecode.com/svn/tags/infobox/1.1.9/src/infobox.js', 'infobox-js', CreateMap);
            };

            function LoadJsAsync(src, id, callback) {
                if (document.getElementById(id) != undefined) {
                    callback();
                    return;
                };
                var ScriptElement = document.createElement('script');
                ScriptElement.id = id;
                ScriptElement.src = src;
                document.head.appendChild(ScriptElement);
                ScriptElement.onload = function() {
                    callback();
                };
            };

            function LoadInfoBox() {
                LoadJsAsync('//google-maps-utility-library-v3.googlecode.com/svn/tags/infobox/1.1.9/src/infobox.js', 'infobox-js', CreateMap);
            };

            function CreateMap() {
                var EventFireByUser = false;
                var infoBox = CreateBox();
                if (scope.fitBounds == undefined) {
                    var bounds = new google.maps.LatLngBounds();
                } else {
                    var bounds = scope.fitBounds;
                };
                var loc;

                var mapOptions = {
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    scrollwheel: false,
                };
                GoogleMap = new google.maps.Map(document.getElementById(attrs.id), mapOptions);

                scope.map.forEach(function(restaurant) {
                    var LatLng = restaurant.GPS.split(',');
                    loc = new google.maps.LatLng(LatLng[0], LatLng[1]);
                    bounds.extend(loc);
                });
                GoogleMap.fitBounds(bounds);
                GoogleMap.panToBounds(bounds);
                CreateMarker(GoogleMap, infoBox);
                // add Click event - hidden infoBox
                google.maps.event.addListener(GoogleMap, 'click', function() {
                    infoBox.close();
                });
                google.maps.event.addListener(GoogleMap, 'resize', function() {
                    setTimeout(function() {
                        CreateMap();
                    }, 10);
                });
                // add zoom_change event - change marker
                google.maps.event.addListener(GoogleMap, 'zoom_changed', function() {
                    ChangeMapBounds(GoogleMap, infoBox);
                    $rootScope.$broadcast('google_map_zoom_changed', {
                        restaurantsInMap: scope.restaurantsInMap
                    });
                });
                google.maps.event.addListener(GoogleMap, 'dragend', function() {
                    ChangeMapBounds(GoogleMap, infoBox);
                    $rootScope.$broadcast('google_map_dragend', {
                        restaurantsInMap: scope.restaurantsInMap
                    });
                });
            };

            function CreateMarker(GoogleMap, infoBox) {
                var skip = (scope.page - 1) * scope.restaurantsPerPage;
                for (i = 0; i < scope.restaurantsPerPage; i++) {
                    var index = skip + i;
                    if (scope.restaurantsInMap[index] != undefined) {
                        var LatLng = scope.restaurantsInMap[index].GPS.split(',');
                        LatLng = new google.maps.LatLng(LatLng[0], LatLng[1]);
                        var title = scope.restaurantsInMap[index].title;
                        if (scope.restaurantsInMap[index].best_offer != undefined) {
                            var best_of = scope.restaurantsInMap[index].best_offer.offer;
                        } else {
                            best_of = '';
                        };
                        var zindex = 1;
                        var marker = new google.maps.Marker({
                            ID: scope.restaurantsInMap[index].ID,
                            position: LatLng,
                            map: GoogleMap,
                            title: title,
                            region: scope.restaurantsInMap[index].region,
                            cuisine: scope.restaurantsInMap[index].cuisine,
                            pricing: scope.restaurantsInMap[index].pricing,
                            rating: scope.restaurantsInMap[index].rating,
                            offer: best_of,
                            zIndex: zindex,
                            imgSrc: 'https://media.weeloy.com/upload/restaurant/' + scope.restaurantsInMap[index].restaurant + '/' + scope.restaurantsInMap[index].image,
                            wheelvalue: scope.restaurantsInMap[index].wheelvalue,
                            restaurant: scope.restaurantsInMap[index].restaurant,
                        });
                        marker.setIcon('images/gmap-marker.png');
                        if (scope.restaurantsInMap[index].is_wheelable == 1) {
                            marker.wheel = '<div class="wheel">' +
                                '<img class="wheel_value" src="https://media.weeloy.com/upload/wheelvalues/wheelvalue_' + marker.wheelvalue + '.png"/>' +
                                '</div>';
                        } else {
                            marker.wheel = '';
                        };
                        scope.restaurantsInMap[index].gmapMarker = marker;
                        scope.restaurantsInMap[index].LatLng = LatLng;
                        google.maps.event.addListener(marker, 'click', (function(marker, i) {
                            return function() {
                                var imgSrc = 'https://media.weeloy.com/upload/restaurant/' + scope.restaurantsInMap[i].restaurant + '/' + scope.restaurantsInMap[i].image;
                                //marker.setIcon(image_over);

                                infoBox.setContent('<div class="border">' +
                                    '<div class="custom-grid">' +
                                    '<article class="ribbon_holder">' +
                                    '<figure>' +
                                    '<img src="' + imgSrc + '"/>' +
                                    '<figcaption class="offer-left">' + marker.offer + '</figcaption>' +
                                    '<figcaption class="offer-restau-name ng-binding">' + marker.title + '</figcaption>' +
                                    '</figure>' +
                                    '</article>' +
                                    '</div>' +
                                    '<div class="item">' +
                                    '<div class="item-info">' +
                                    '<div class="location-icon">' +
                                    '<div class="location-text"><span class="fa fa-map-marker"></span>  ' + marker.region + ' - ' + marker.pricing + '<br/>' + marker.cuisine + '</div>' +
                                    '</div>' +
                                    '<div class="book">' +
                                    '<button id="btn_book" class="btn custom_button" onClick="go(\'' + scope.restaurantsInMap[i].restaurant + '\', \'' + scope.restaurantsInMap[i].title + '\');">BOOK NOW</button>' +
                                    '</div>' +
                                    '</div>' +
                                    marker.wheel +
                                    '</div>' +
                                    '</div>' +
                                    '<span class="arrow"></span>');
                                infoBox.open(GoogleMap, marker);

                                GoogleMap.setCenter(marker.getPosition());

                            }
                        })(marker, i));
                    };
                };
                $rootScope.$on('PageChanged', function(evt, page) {
                    scope.page = page;
                    ClearMarker();
                    CreateMarker(GoogleMap, infoBox);
                });
            };

            function ChangeMapBounds(GoogleMap, infoBox) {
                var Bounds = GoogleMap.getBounds();
                if (Bounds != undefined && Bounds != null) {
                    scope.restaurantsInMap = [];
                    scope.map.forEach(function(restaurant, key) {
                        var LatLng = restaurant.GPS.split(',');
                        LatLng = new google.maps.LatLng(LatLng[0], LatLng[1]);
                        if (Bounds.contains(LatLng)) {
                            scope.restaurantsInMap.push(restaurant);
                        };
                    });
                    if ($location.$$search.page != undefined && $location.$$search.page != 1) {
                        scope.currentBounds = GoogleMap.getBounds();
                        location.skipReload().search({
                            page: 1
                        });
                        scope.page = 1;
                        $rootScope.$broadcast('ChangePageToOne');
                        scrollToTop();
                    };
                    ClearMarker();
                    CreateMarker(GoogleMap, infoBox);
                    try {
                        scope.$apply();
                    } catch (e) {
                        console.log(e);
                    };
                };
            };

            function ClearMarker() {
                for (var k = 0; k < scope.map.length; k++) {
                    if (scope.map[k].gmapMarker != undefined) {
                        scope.map[k].gmapMarker.setMap(null);
                    };
                };
            };

            function CreateBox() {
                var box = new InfoBox({
                    disableAutoPan: true,
                    maxWidth: 0,
                    pixelOffset: new google.maps.Size(-115, -300),
                    closeBoxMargin: '50px 200px',
                    closeBoxURL: '',
                    isHidden: false,
                    pane: 'floatPane',
                    enableEventPropagation: true
                });
                return box;
            };

            function scrollToTop() {
                $("html, body").animate({
                    scrollTop: 0
                }, 500);
            };
        },
    };
}]);
