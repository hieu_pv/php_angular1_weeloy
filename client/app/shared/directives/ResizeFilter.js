var ResizeFilter = angular.module('ResizeFilter', []);
ResizeFilter.run(['$rootScope', function($rootScope) {
    $(window).resize(function() {
        ResizeFilter();
    });
    $rootScope.$on('MapSizeChanged', function(evt) {
        ResizeFilter();
    });

    function ResizeFilter() {
        setTimeout(function() {
            if ($('search-filter').width() < 800) {
                $('search-filter .cuisine').css('width', '50%');
            } else {
                $('search-filter .cuisine').css('width', '25%');
            }
            if ($('search-filter').width() < 600) {
                $('.filterButton').addClass('filterButtonRight');
            } else {
                $('.filterButton').removeClass('filterButtonRight');
            };
        }, 100);
    };
}]);
ResizeFilter.directive('resizeFilter', function() {
    return {
        restrict: 'A',
        scope: true,
        link: function(scope, element, attrs) {
            if ($('search-filter').width() < 800) {
                $(element).css('width', '50%');
            }
            if ($('search-filter').width() < 600) {
                $('.filterButton').addClass('filterButtonRight');
            };
        },
    };
});
