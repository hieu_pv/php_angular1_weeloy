var SearchFilterModule = angular.module('SearchFilterModule', []);
SearchFilterModule.directive('searchFilter', ['$rootScope', '$location', function($rootScope, $location) {
    return {
        restrict: 'AE',
        scope: {
            cities: '=',
            cuisineList: '=',
            pricingArray: '=',
        },
        templateUrl: 'client/app/shared/partial/_search_filter.html',
        link: function(scope, element, attrs) {

            if ($location.$$search.city == undefined) {
                scope.city = scope.cities[0];
            } else {
                scope.cities.forEach(function(value, key) {
                    if (value.name == $location.$$search.city) {
                        scope.city = value;
                    };
                });
            };

            if ($location.$$search.pricing == undefined) {
                scope.pricing = scope.pricingArray[0];
            } else {
                scope.pricingArray.forEach(function(value, key) {
                    if (value.data == $location.$$search.pricing) {
                        scope.pricing = value;
                    };
                });
            };

            scope.expandCuisineFilter = false;


            $rootScope.$on('cuisineListLoaded', function(evt, data) {
                if ($location.$$search.cuisine != undefined) {
                    cuisine = $location.$$search.cuisine.split('|');
                    scope.cuisineList = data.cuisineList;
                    for (var key in scope.cuisineList) {
                        scope.cuisineList[key].forEach(function(value, k) {
                            if (cuisine.indexOf(value.cuisine) > -1) {
                                scope.cuisineList[key][k].selected = true;
                            };
                        });
                    };
                };
            });


            scope.changeCuisineFilter = function(cuisine, evt) {
                cuisine.selected = !cuisine.selected;
                evt.stopPropagation();
                return;
            };
            scope.SelectCity = function(city) {
                $location.search('city', city.name);
            };
            scope.filter = function(cuisineList, city, pricing) {
                scope.city = city.data;
                scope.fireZoomEvent = false;
                scope.mymap.setZoom(12);
                scope.fireZoomEvent = true;
                if (city.data != undefined && city.data != null) {
                    setMapCenter(city.data);
                } else {
                    setMapCenter('');
                };
                LoadRestaurantData(city.data, pricing.data, cuisineList);
            };
            scope.ApplyFilter = function(city, cuisineList, pricing) {
                var cuisine = [];
                for (var key in scope.cuisineList) {
                    scope.cuisineList[key].forEach(function(value, k) {
                        if (value.selected) {
                            cuisine.push(value.cuisine);
                        };
                    });
                };
                cuisine = cuisine.join('|');
                $location.search({
                    city: city.name,
                    cuisine: cuisine,
                    pricing: pricing.data,
                });
            };
            scope.ClearFilter = function() {
                scope.city = scope.cities[0];
                scope.pricing = scope.pricingArray[0];
                for (var key in scope.cuisineList) {
                    scope.cuisineList[key].forEach(function(value, k) {
                        scope.cuisineList[key][k].selected = false;
                    });
                };
                scope.ApplyFilter(scope.city, scope.cuisineList, scope.pricing);
            };
        },
    };
}]);
