var RestaurantItem = angular.module('RestaurantItem', []);
RestaurantItem.directive('restaurantItem', ['$rootScope', function($rootScope) {
    return {
        restrict: 'E',
        templateUrl: 'client/app/shared/partial/_restaurant_item.html',
        scope: {
            restaurant: '=',
            openInNewTab: '@',
        },
        link: function(scope, element, attrs) {
            var img = document.createElement('img');
            var url = 'https://media.weeloy.com/upload/restaurant/' + scope.restaurant.restaurant + '/' + scope.restaurant.image;
            img.src = url;
            if (scope.openInNewTab == 'true') {
                scope.OpenRestaurantInNewTab = '_blank';
            };
            img.onload = function() {
                $(element).find('.img-transparent').css('background', 'url(' + url + ')');
            };
            scope.go = function(resto, title) {
                WindowOpen('modules/booking/book_form.php?bkrestaurant=' + resto + '&bktitle=' + title);
            };

            function WindowOpen(url) {
                window.open(url, '_blank', 'toolbar=no, scrollbars=yes, resizable=yes, top=30, left=30, width=800, height=900');
            }
        },
    };
}]);
