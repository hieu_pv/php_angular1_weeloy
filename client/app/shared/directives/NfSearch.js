var NfSearch = angular.module('NfSearch', []);
NfSearch.run(['$rootScope', function($rootScope) {}]);
NfSearch.directive('nfSearch', ['$rootScope', '$http', '$location', function($rootScope, $http, $location) {
    return {
        restrict: 'AE',
        scope: {
            url: '@',
            cities: '=',
        },
        templateUrl: 'client/app/shared/partial/_search_form.html',
        link: function(scope, element, attrs) {
            if (scope.nfSearch == undefined || scope.nfSearch == '') {
                var API_URL = 'api/search/fulltext';
            } else {
                var API_URL = scope.nfSearch;
            };
            if ($location.$$search.query != undefined) {
                scope.free_search = $location.$$search.query;
            };

            scope.city = scope.cities[0];

            if ($location.$$search.city != undefined) {
                scope.cities.forEach(function(city) {
                    if (city.name == $location.$$search.city) {
                        scope.city = city;
                    };
                });
            };

            scope.SearchFormSelectCity = function(city) {
                $location.search({
                    city: city.name,
                    page: 1,
                });
            };

            var Input = $(element).find('.search-input');

            $rootScope.showAutoCorrect = false;
            var cuisinelist = CuisineListInit();
            localStorage.removeItem('__restaurants_bloodhood_cache_key__data');
            var restaurants = RestaurantResourceInit(scope.city);

            TypeaheadInit(Input, restaurants, cuisinelist);

            function RestaurantResourceInit(city) {
                var prefetch_url = API_URL + '?city=' + city.name;
                var remote_url = API_URL + "?city=" + city.name + "&query=%QUERY*";
                var restaurants = new Bloodhound({
                    datumTokenizer: function(d) {
                        var test = Bloodhound.tokenizers.whitespace(d.title);
                        $.each(test, function(k, v) {
                            i = 0;
                            while ((i + 1) < v.length) {
                                test.push(v.substr(i, v.length));
                                i++;
                            }
                        })
                        return test;
                    },
                    queryTokenizer: Bloodhound.tokenizers.whitespace,
                    prefetch: {
                        url: prefetch_url,
                        thumbprint: 'ver 1.4',
                        cacheKey: 'restaurants_bloodhood_cache_key',
                        transform: function(response) {
                            var data = response.data.restaurants;
                            data.forEach(function(value, key) {
                                data[key].cuisine = data[key].cuisine.replace("|", " ");
                            });
                            return data;
                        },
                    },
                    remote: {
                        url: remote_url,
                        wildcard: '%QUERY',
                        transform: function(response) {
                            var data = response.data.restaurants;
                            data.forEach(function(value, key) {
                                data[key].cuisine = data[key].cuisine.replace("|", " ");
                            })
                            return data;
                        },
                    },
                });
                return restaurants;
            };

            function CuisineListInit() {
                var cuisinelist = new Bloodhound({
                    datumTokenizer: function(d) {
                        var test = Bloodhound.tokenizers.whitespace(d.cuisine);
                        $.each(test, function(k, v) {
                            i = 0;
                            while ((i + 1) < v.length) {
                                test.push(v.substr(i, v.length));
                                i++;
                            }
                        })
                        return test;
                    },
                    queryTokenizer: Bloodhound.tokenizers.whitespace,
                    prefetch: {
                        url: "api/cuisinelist",
                        transform: function(response) {
                            var data = response.data.cuisine;
                            return data;
                        },
                    }
                });
                return cuisinelist;
            };

            function TypeaheadInit(Input, restaurants, cuisinelist) {
                Input.typeahead({
                    minLength: 1,
                    highlight: true,
                    hint: true,
                }, {
                    name: 'title',
                    display: 'title',
                    source: restaurants,
                    templates: {
                        header: '<p class="search-box-header"><strong>Restaurant</strong><p>',
                    }
                }, {
                    name: 'cuisine',
                    display: 'cuisine',
                    source: cuisinelist,
                    templates: {
                        header: '<p class="search-box-header"><strong>Cuisine</strong><p>',
                    }
                });
            };
            Input.bind('typeahead:render', function(evt, suggestions, flag, name) {
                if (suggestions == undefined) {
                    var length = $('.twitter-typeahead').find('.tt-suggestion').length;
                    if (length == 0) {
                        if ($('#auto_correct').attr('query') == Input.val() || element.val() == '') {
                            return;
                        }
                        var params = {
                            query: Input.val(),
                            city: scope.city.name,
                        };
                        $('#auto_correct').attr('query', Input.val());
                        $http.get('../api/search/fulltext', {
                            params: params
                        }).success(function(response) {
                            if (response.status == 1) {
                                $rootScope.showAutoCorrect = true;
                                $rootScope.suggestions = response.data.restaurants;
                            };
                        });
                    };
                } else {
                    $rootScope.showAutoCorrect = false;
                };
            });
            scope.SelectCity = function(city) {
                scope.city = city;
                scope.free_search = '';
                Input.typeahead('destroy');
                localStorage.removeItem('__restaurants_bloodhood_cache_key__data');
                var restaurants = RestaurantResourceInit(city);
                TypeaheadInit(Input, restaurants, cuisinelist);
                $rootScope.$broadcast('ChangeCity', {
                    city: city
                });
            };
            // change ng-model when typeahead:select
            Input.bind('typeahead:select', function(evt, data) {
                if (data.title != undefined) {
                    scope.free_search = data.title;
                } else {
                    scope.free_search = data.cuisine;
                }
            });
            scope.SearchInputKeyUp = function(query, city, evt) {
                if (evt.keyCode == 13) {
                    go_search(query, city);
                };
            };
            scope.Search = function(query, city) {
                go_search(query, city);
            };

            function go_search(query, city) {
                var data = {
                    query: query,
                    city: city.name,
                };
                $location.path('/angular-client/search');
                $location.search({
                    query: query,
                    city: city.name,
                });
            };
        },
    };
}]);
