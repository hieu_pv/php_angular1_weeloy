var ExplodeCuisineFilter = angular.module('ExplodeCuisineFilter', []);
ExplodeCuisineFilter.filter('explode_cuisine', function() {
    'use strict';
    return function(input, separator, items) {
        if (input === undefined) {
            return false;
        }
        var arr = input.split(separator),
            cuisine = '',
            i;
        for (i = 0; i < arr.length; i = i + 1) {
            if (i === items) {
                cuisine += '...';
                return cuisine;
            }
            if (cuisine === '') {
                cuisine += arr[i];
            } else {
                cuisine += ', ' + arr[i];
            }
        }
        return cuisine;
    };
});
