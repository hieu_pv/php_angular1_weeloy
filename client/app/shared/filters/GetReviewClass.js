var GetReviewClass = angular.module('GetReviewClass', []);
GetReviewClass.filter('get_review_class', function() {
    'use strict';
    return function(input) {
        if (input === undefined) {
            return false;
        }

        var res = (Math.round(input * 2) * 0.5) * 10;

        return 'review-' + res;
    };
});
