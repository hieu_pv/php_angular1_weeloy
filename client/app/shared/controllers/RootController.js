var RootController = angular.module('RootController', ['LoginService']);
RootController.controller('RootCtrl', [
    '$rootScope',
    '$scope',
    '$http',
    '$location',
    'loginService',
    function($rootScope, $scope, $http, $location, loginService) {
        $scope.showForm = 'loginForm';
        $scope.showLoggedinBox = false;
        $scope.showLoggedin = function(evt) {
            $scope.showLoggedinBox = !$scope.showLoggedinBox;
            evt.stopPropagation();
        };
        $('body').click(function(evt) {
            var parent = $(evt.target).parent();
            if (parent != undefined && parent.attr('id') != 'user-profile-btn') {
                $scope.showLoggedinBox = false;
                $scope.$apply();
            };
        });

        $scope.Login = function(user) {
            loginService.login(user.email, user.password).then(function(response) {
                console.log(response);
                if (response.status == 1) {
                    $rootScope.loggedin = true;
                    $rootScope.user = new Object();
                    $rootScope.user.email = user.email;
                    $rootScope.user.data = response.data;
                    $('#loginModal').modal('hide');
                    $rootScope.$broadcast('UserLogin', $rootScope.user);
                    $rootScope.showSystemMsg = true;
                    $rootScope.systemMsg = 'You have successfully logged in, ' + response.data.firstname;
                } else {
                    $rootScope.systemMsg = response.errors;
                    $rootScope.showSystemMsg = true;
                };
            });
        };
        $rootScope.$on('$routeChangeSuccess', function() {
            $rootScope.showSystemMsg = false;
        });
        $rootScope.hideSystemMsg = function() {
            $rootScope.showSystemMsg = false;
        };
        $scope.logout = function() {
            if ($rootScope.user == undefined || $rootScope.user == null) {
                return;
            };
            loginService.logout($rootScope.user.email).then(function(response) {
                console.log(response);
                if (response.status == 1) {
                    $rootScope.user = null;
                    $rootScope.loggedin = false;
                    $rootScope.systemMsg = 'You have successfully logged out';
                    $rootScope.showSystemMsg = true;
                    console.log($rootScope.showSystemMsg, $rootScope.systemMsg);
                };
            });
        };
        $('#loginModal').on('shown.bs.modal', function() {
            $rootScope.LoadFacebookSDK();
        });
        $rootScope.LoadFacebookSDK = function() {
            (function(d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) return;
                js = d.createElement(s);
                js.id = id;
                js.src = "//connect.facebook.net/en_US/sdk.js";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));
            window.fbAsyncInit = function() {
                FB.init({
                    appId: FB_ID,
                    cookie: true, // enable cookies to allow the server to access 
                    // the session
                    xfbml: true, // parse social plugins on this page
                    version: 'v2.2' // use version 2.2
                });
            };
        };
        $scope.LoginWithFacebook = function() {
            FB.login(function(response) {
                if (response.authResponse) {
                    console.log('Welcome!  Fetching your information.... ');
                    FB.api('/me', function(fb_response) {
                        console.log(fb_response);
                        var params = {
                            email: fb_response.email,
                            facebookid: fb_response.id,
                            facebooktoken: FB.getAuthResponse()['accessToken'],
                            platform: 'web',
                        };
                        var facebookLogin = loginService.loginfacebook(params);
                        facebookLogin.then(function(response) {
                            console.log(response);
                            if (response.status == 1) {
                                $rootScope.loggedin = true;
                                $rootScope.user = new Object();
                                $rootScope.user.email = params.email;
                                $rootScope.user.data = response.data;
                                $('#loginModal').modal('hide');
                                $rootScope.$broadcast('UserLogin', $rootScope.user);
                                $rootScope.showSystemMsg = true;
                                $rootScope.systemMsg = 'You have successfully logged in, ' + response.data.firstname;
                            } else {
                                if (response.status == 0) {
                                    $scope.showForm = 'signupForm';
                                    $scope.SignupUser = fb_response;
                                };
                            };
                        });
                    });
                } else {
                    console.log('User cancelled login or did not fully authorize.');
                };
            });
        };
        $scope.ChangePassword = function(email, old_password, new_password) {
            var changePassword = loginService.change(email, old_password, new_password, $rootScope.user.data.token);
            console.log(email, old_password, new_password, $rootScope.user.data.tokenx);
            changePassword.then(function(response) {
                console.log(response);
            });
        };
        $scope.SubmitNewletter = function(email) {
            $http.get('api/newsletter/addEmail/' + email).success(function(response) {
                if (response.status == 1) {
                    alert(response.data);
                } else {
                    alert(response.errors);
                };
            });
        };
        $scope.Signup = function(user) {
            loginService.register(user).then(function(response) {
                console.log(response);
                if (response.status == 1) {
                    $scope.Login(user);
                } else {
                    alert(response.errors);
                };
            });
        };
        $scope.ForgotPassword = function(email) {
            loginService.forgot(email).then(function(response) {
                if (response.status == 1) {
                    alert(response.errors);
                    $('#loginModal').modal('hide');
                } else {
                    alert(response.errors);
                };
            });
        };
    }
]);
RootController.filter('json', function() {
    return function(obj) {
        return JSON.stringify(obj);
    };
});
