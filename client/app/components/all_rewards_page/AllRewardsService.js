var AllRewardsService = angular.module('AllRewardsService', []);
AllRewardsService.service('Reward', ['$http', '$q', function($http, $q) {
    this.getReward = function(query) {
        var API_URL = 'api/wheeldescription/' + query;
        var defferred = $q.defer();
        $http.get(API_URL, {
            cache: true,
        }).success(function(response) {
            defferred.resolve(response);
        });
        return defferred.promise;
    };
}]);
