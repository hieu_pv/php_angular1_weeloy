var AllRewardsController = angular.module('AllRewardsController', ['RestaurantInfoService', 'AllRewardsService']);
AllRewardsController.controller('AllRewardsCtrl', ['$rootScope', '$scope', '$routeParams', 'RestaurantInfo', 'Reward', function($rootScope, $scope, $routeParams, RestaurantInfo, Reward) {
    RestaurantInfo.getFullInfo($routeParams.restaurant).then(function(response) {
        console.log(response);
        if (response.status == 1) {
            $scope.restaurant = response.data;
        };
    });
    Reward.getReward($routeParams.restaurant).then(function(response) {
    	console.log(response);
        if (response.status == 1) {
            $scope.rewards = response.data;
        };
    });
}]);
