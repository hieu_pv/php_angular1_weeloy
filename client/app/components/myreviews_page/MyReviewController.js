var MyReviewController = angular.module('MyReviewController', ['MyReviewService']);
MyReviewController.controller('MyReviewCtrl', ['$rootScope', '$scope', 'Review', function($rootScope, $scope, Review) {
    $rootScope.MenuUserSelected = 'myreviews';
    Review.getMyReviews().then(function(response) {
        console.log(response);
        if (response.status == 1) {
            var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
            var weekday = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
            response.data.reviews.forEach(function(value, key) {
            	var rtime = value.reviewdtcreate.split(/[-|:| ]/g);
            	var d = new Date(rtime[0], rtime[1], rtime[2], rtime[3], rtime[4], rtime[5]);
            	response.data.reviews[key].timeStr = monthNames[d.getMonth()] + ' ' + d.getDate() + ' ' + d.getFullYear() + ' at ' + d.getHours() + ':' + d.getMinutes(); 
            });
            $scope.ReviewsCount = response.count;
            $scope.myReviews = response.data.reviews;
        };
    });
}]);
