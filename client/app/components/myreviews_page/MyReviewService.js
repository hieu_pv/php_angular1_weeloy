var MyReviewService = angular.module('MyReviewService', []);
MyReviewService.service('Review', ['$q', '$http', function($q, $http) {
    this.getMyReviews = function() {
        var defferred = $q.defer();
        $http.get('api/getReviewList').success(function(response) {
            defferred.resolve(response);
        });
        return defferred.promise;
    };
}]);
