var RestaurantInfoService = angular.module('RestaurantInfoService', []);
RestaurantInfoService.service('RestaurantInfo', ['$http', '$q', function($http, $q) {
    this.getFullInfo = function(query) {
        var API_URL = 'api/restaurantfullinfo/' + query;
        var defferred = $q.defer();
        $http.get(API_URL, {
            cache: true,
        }).success(function(response) {
            getRestaurantService(response.data.restaurantinfo.restaurant).then(function(restaurant_service_response) {
                if (restaurant_service_response.status == 1) {
                    response.data.restaurantservices = restaurant_service_response.data.restaurantservices;
                }
                defferred.resolve(response);
            });
        });
        return defferred.promise;
    };
    this.getReviews = function(query) {
        var API_URL = 'api/getreviews/' + query + '/list/summary';
        var defferred = $q.defer();
        $http.get(API_URL).success(function(response) {
            defferred.resolve(response);
        });
        return defferred.promise;
    };

    function getRestaurantService(restaurant) {
        var API_URL = 'api/restaurant/service/restaurantservices/' + restaurant;
        var defferred = $q.defer();
        $http.get(API_URL, {
            cache: true,
        }).success(function(response) {
            defferred.resolve(response);
        });
        return defferred.promise;
    };
}]);
