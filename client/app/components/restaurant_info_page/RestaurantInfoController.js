var RestaurantInfoController = angular.module('RestaurantInfoController', ['RestaurantInfoService', 'TrustAsResource']);
RestaurantInfoController.filter('day', function() {
    return function(str) {
        str = str.toLowerCase();
        switch (str) {
            case 'monday':
                return 'Mon';
                break;
            case 'tuesday':
                return 'Tue';
                break;
            case 'wednesday':
                return 'Wed'
                break;
            case 'thursday':
                return 'Thu';
                break;
            case 'friday':
                return 'Fri';
                break;
            case 'saturday':
                return 'Sat';
                break;
            case 'sunday':
                return 'Sun'
                break;
            default:
                return str;
                break;
        }
    };
});
RestaurantInfoController.controller('RestaurantInfoCtrl', [
    '$rootScope',
    '$scope',
    '$routeParams',
    '$window',
    'RestaurantInfo',
    function($rootScope, $scope, $routeParams, $window, RestaurantInfo) {
        $rootScope.LoadFacebookSDK();
        RestaurantInfo.getFullInfo($routeParams.restaurant).then(function(response) {
            console.log(response);
            if (response.status == 1) {
                response.data.banner = 'https://media1.weeloy.com/upload/restaurant/' + $routeParams.restaurant + '/1440/' + response.data.restaurantinfo.images[0];
                response.data.logo = 'https://media1.weeloy.com/upload/restaurant/' + $routeParams.restaurant + '/' + response.data.restaurantinfo.logo;
                response.data.cuisineArray = response.data.restaurantinfo.cuisine.split('|');
                response.data.wheelArray = response.data.restaurantinfo.wheel.split('|');
                response.data.wheelArray.splice(0, 1);
                var GPS = response.data.restaurantinfo.GPS;
                GPS = GPS.split(',');
                response.data.lat = GPS[0];
                response.data.lng = GPS[1];
                var RestaurantServices = [];
                response.data.restaurantservices.forEach(function(service, key) {
                    var include = false;
                    var key;
                    RestaurantServices.forEach(function(value, k) {
                        if (value.category == service.categorie) {
                            include = true;
                            key = k;
                        };
                    });
                    if (include == false) {
                        var newService = {
                            category: service.categorie,
                            category_id: service.categorie_id,
                            category_icon: service.pico_categorie,
                            services: [{
                                name: service.service,
                                icon: service.pico_service,
                            }],
                        };
                        RestaurantServices.push(newService);
                    } else {
                        var service = {
                            name: service.service,
                            icon: service.pico_service,
                        };
                        RestaurantServices[key].services.push(service);
                    }
                });
                response.data.RestaurantServices = RestaurantServices;
                $scope.restaurant = response.data;
            };
        });

        RestaurantInfo.getReviews($routeParams.restaurant).then(function(response) {
            console.log(response);
            if (response.status == 1) {
                if (response.data.reviews != undefined) {
                    response.data.reviews.forEach(function(value, key) {
                        var time_str = value.post_date.replace('-', ' ');
                        var time = new Date(time_str);
                        var month = new Array();
                        month[0] = "January";
                        month[1] = "February";
                        month[2] = "March";
                        month[3] = "April";
                        month[4] = "May";
                        month[5] = "June";
                        month[6] = "July";
                        month[7] = "August";
                        month[8] = "September";
                        month[9] = "October";
                        month[10] = "November";
                        month[11] = "December";
                        response.data.reviews[key].time = month[time.getMonth()] + ' ' + time.getFullYear();
                    });
                    $scope.reviews = response.data;
                };
            };
        });

        $scope.btnBookNow_click = function(restaurant) {
            var url = "modules/booking/book_form.php?bkrestaurant=" + restaurant.restaurantinfo.restaurant + "&amp;bktitle=" + restaurant.restaurantinfo.title;
            window.open(url, '_blank', 'toolbar=no, scrollbars=yes, resizable=yes, top=30, left=30, width=800, height=900');
        };

        $scope.fbshare = function(restaurant) {
            var pictureUrl = 'https://media1.weeloy.com/upload/restaurant/' + restaurant.restaurantinfo.restaurant + '/1440/' + restaurant.restaurantinfo.images[0],
                resTitle = restaurant.restaurantinfo.title,
                resDescription = restaurant.restaurantinfo.description[0].body[0];
            var obj = {
                method: 'feed',
                link: BASE_URL + '/restaurant/' + restaurant.restaurantinfo.restaurant,
                picture: pictureUrl,
                name: resTitle,
                caption: 'Book ' + resTitle + ' & win rewards with Weeloy',
                description: resDescription,
                display: 'popup'
            };

            FB.ui(obj, function(response) {

            });
        }
        $scope.showReview = function() {
            $scope.showReviewSection = true;
            setTimeout(function() {
                var offset = $('#review-section').offset();
                $("html, body").animate({
                    scrollTop: offset.top - 100,
                }, 500);
            }, 100);
        };
        var offset;
        $scope.Affix = function() {
            offset = $('#btn_affix').offset();
            var LastElement = $('.right .last');
            $(window).scroll(function() {
                if ($(window).scrollTop() > offset.top - 15) {
                    LastElement.addClass('fixedTop');
                    LastElement.css('margin-left', offset.left - 15);
                } else {
                    LastElement.removeClass('fixedTop');
                    LastElement.css('margin-left', 0);
                };
            });
        };
        $(window).resize(function() {
            offset = $('#btn_affix').offset();
        });


        $scope.loadmap = function(lat, lng) {
            if (document.getElementById('gmap-sdk') != undefined && document.getElementById('gmap-sdk') != null) {
                CreateMap(lat, lng);
            } else {
                var script = document.createElement("script");
                script.type = "text/javascript";
                script.id = 'gmap-sdk';
                script.src = "http://maps.google.com/maps/api/js?sensor=false&callback=CreateMap";
                document.body.appendChild(script);
            }
        };

        $window.CreateMap = function() {
            CreateMap($scope.restaurant.lat, $scope.restaurant.lng);
        };

        function CreateMap(lat, lng) {
            var myLatlng = new google.maps.LatLng(lat, lng);
            var mapOptions = {
                zoom: 14,
                center: myLatlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                scrollwheel: false,
                navigationControl: false,
                mapTypeControl: false,
            };
            if ($(window).width < 767) {
                mapOptions.scaleControl = false;
                mapOptions.draggable = false;
                mapOptions.zoomControl = false;
            }
            map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);
            var marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
            });
        };
    }
]);
