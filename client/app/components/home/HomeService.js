var HomeService = angular.module('HomeService', []);
HomeService.service('Home', ['$http', '$q', function($http, $q) {
    this.getBestRestaurants = function(city, limit) {
        var defferred = $q.defer();
        var API_URL = 'api/restaurant/getShowcase/' + city + '/' + limit;
        $http.get(API_URL, {
            cache: true
        }).success(function(response) {
            defferred.resolve(response);
        });
        return defferred.promise;
    };
}]);
