var HomeController = angular.module('HomeController', ['NfSearch', 'HomeService', 'RestaurantItem']);
HomeController.controller('HomeCtrl', ['$rootScope', '$scope', 'Home', function($rootScope, $scope, Home) {
    $scope.cities = [{
        id: 1,
        name: 'Singapore'
    }, {
        id: 2,
        name: 'Bangkok'
    }, {
        id: 3,
        name: 'Phuket'
    }, {
        id: 4,
        name: 'Hong Kong'
    }];

    var HomeService = Home.getBestRestaurants('singapore', 6);
    HomeService.then(function(response) {
        console.log(response);
        if (response.status == 1) {
            $scope.BestRestaurants = response.data;
        };
    });
}]);
