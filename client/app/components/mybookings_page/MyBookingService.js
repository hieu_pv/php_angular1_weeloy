var MyBookingService = angular.module('MyBookingService', []);
MyBookingService.service('Booking', ['$http', '$q', function($http, $q) {
    this.get = function(period) {
        var defferred = $q.defer();
        var url = 'api/user/booking/period/' + period;
        $http.get(url, {
            cache: true,
        }).success(function(response) {
            defferred.resolve(response);
        });
        return defferred.promise;
    };
}]);
