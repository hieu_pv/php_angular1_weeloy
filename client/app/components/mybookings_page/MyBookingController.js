var MyBookingController = angular.module('MyBookingController', ['MyBookingService', 'MyBookingReview']);
MyBookingController.controller('MyBookingCtrl', ['$rootScope', '$scope', '$location', 'Booking', function($rootScope, $scope, $location, Booking) {
    $rootScope.checkLoggedin();
    $rootScope.MenuUserSelected = 'mybookings';
    if ($location.$$search.f == undefined) {
        $location.search('f', 'today');
        return;
    };
    $scope.MyBookingPage = $location.$$search.f;
    var bookings = Booking.get($location.$$search.f);
    $rootScope.LoadFacebookSDK();
    bookings.then(function(response) {
        console.log(response);
        if (response.status == 1) {
            var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
            var weekday = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
            if (response.data.bookings != undefined || response.data.bookings != null) {
                $scope.showBookingList = true;
                response.data.bookings.forEach(function(value, key) {
                    var rdate = value.rdate.split('-');
                    var rtime = value.rtime.split(':');
                    var d = new Date(rdate[0], rdate[1], rdate[2], rtime[0], rtime[1], rtime[2]);
                    var currentTime = new Date();
                    response.data.bookings[key].time = d;
                    if (currentTime.getTime() - d.getTime() > 0) {
                        response.data.bookings[key].passed = true;
                    } else {
                        response.data.bookings[key].passed = false;
                    };
                    response.data.bookings[key].timeStr = monthNames[d.getMonth()] + ' ' + d.getDate() + ', ' + d.getUTCFullYear();
                    response.data.bookings[key].image = 'https://media3.weeloy.com/upload/restaurant/SG_SG_R_TheFunKitchen/2_The_fun_kitchen.jpg';
                });
                $scope.bookings = response.data.bookings;
            } else {
                $scope.showBookingList = false;
            };
        };
    });
    $scope.Review = function(item) {
        $scope.ReviewItem = item;
        $scope.showReviewSection = true;
        setTimeout(function() {
            var offset = $('review-section').offset();
            $("html, body").animate({
                scrollTop: offset.top - 100,
            }, 500);
        }, 100);
    };
    $scope.fbshare = function(item) {
        if (item.canceldate != null) {
            description = "I regret having to cancel my reservation" + item.wheelwin + " @ " + item.restaurantinfo.title + " #weeloy";
            caption = "Booking Cancel " + "@ " + item.restaurantinfo.title;
        } else {
            description = "I just enjoyed " + item.wheelwin + " @ " + item.restaurantinfo.title + " #weeloy";
            caption = item.wheelwin + " @ " + item.restaurantinfo.title;
        };
        var obj = {
            method: 'feed',
            link: BASE_URL + '/restaurant/' + item.restaurantinfo.restaurant,
            picture: "https://media1.weeloy.com/upload/restaurant/SG_SG_R_TheFunKitchen/2_The_fun_kitchen.jpg",
            name: item.restaurantinfo.title,
            caption: caption,
            description: description,
            display: 'popup'
        };
        FB.ui(obj, function(response) {
            console.log(response);
        });
    };

    $scope.tws_click = function(item) {
        var twtTitle = item.restaurantinfo.title,
            twtUrl = BASE_URL + '/restaurant/' + item.restaurantinfo.restaurant,
            maxLength = 140 - (twtUrl.length + 1),
            twtLink = 'http://twitter.com/home?status=' + encodeURIComponent(twtTitle + ' ' + twtUrl);
        window.open(twtLink, "_blank", "toolbar=yes, scrollbars=yes, resizable=no, width=600, height=400");
    };
}]);
