var SearchController = angular.module('SearchController', [
    'Map',
    'SearchService',
    'SearchFilterModule',
    'ExplodeCuisineFilter',
    'GetReviewClass',
    'noResult',
    'ResizeFilter',
    'SkipReload',
    'RestaurantItem'
]);
SearchController.controller('SearchCtrl', [
    '$rootScope',
    '$scope',
    '$location',
    '$http',
    'location',
    'Search',
    function($rootScope, $scope, $location, $http, location, Search) {
        var Markers = [];
        $scope.showPagination = false;
        $scope.SearchSectionClasses = ['col-lg-8', 'col-md-8', 'col-sm-8', 'col-xs-12'];
        $scope.GoogleMapSectionClasses = ['col-lg-4', 'col-md-4', 'col-sm-4', 'hidden-xs'];
        $scope.RestaurantItemClasses = ['col-lg-4', 'col-md-4', 'col-sm-6', 'col-xs-12'];

        Search.getBestRestaurants($location.$$search.city).then(function(response) {
            console.log(response);
            if (response.status == 1) {
                response.data[0].cuisine = response.data[0].cuisine.replace('|', ', ');
                $scope.restaurantShowCase = response.data[0];
            };
        });

        $scope.MapExpanded = false;
        $scope.pageNumbers = [];
        $scope.restaurantsPerPage = config.restaurantsPerPage;
        $scope.ExpandMap = false;
        if ($location.$$search.page != undefined) {
            $scope.page = parseInt($location.$$search.page);
        } else {
            $scope.page = 1;
        };
        $rootScope.$on('ChangePageToOne', function() {
            $scope.page = 1;
            $scope.skip = ($scope.page - 1) * $scope.restaurantsPerPage;
        });
        $scope.setPage = function(page) {
            location.skipReload().search({
                page: page
            });
            $scope.page = page;
            $scope.skip = ($scope.page - 1) * $scope.restaurantsPerPage;
            $("html, body").animate({
                scrollTop: 0
            }, 500);

            $rootScope.$broadcast('PageChanged', $scope.page);
        };
        $scope.cities = [{
            id: 1,
            name: 'Singapore'
        }, {
            id: 2,
            name: 'Bangkok'
        }, {
            id: 3,
            name: 'Phuket'
        }, {
            id: 4,
            name: 'Hong Kong'
        }];

        $scope.skip = ($scope.page - 1) * $scope.restaurantsPerPage;
        var SearchService = Search.getRestaurant('both', $location.$$search.city, $location.$$search.cuisine, $location.$$search.pricing, $location.$$search.query);
        SearchService.then(function(response) {
            if (response.status == 1) {
                if (response.data.no_result == false) {
                    $scope.restaurants = response.data.restaurant;
                    $scope.restaurantsInMap = response.data.restaurant;
                    Pagination();
                };
            };
        });

        $scope.HighLightMarker = function(restaurant) {
            $scope.restaurants.forEach(function(value, key) {
                if (value.ID == restaurant.ID && value.gmapMarker != undefined) {
                    value.gmapMarker.setIcon('images/highlight-marker.png');
                };
            });
        };
        $scope.notHighLightMarker = function(restaurant) {
            $scope.restaurants.forEach(function(value, key) {
                if (value.ID == restaurant.ID && value.gmapMarker != undefined) {
                    value.gmapMarker.setIcon('images/gmap-marker.png');
                };
            });
        };

        var cuisineListShow = [{
            cuisine: 'french',
        }, {
            cuisine: 'italian',
        }, {
            cuisine: 'international',
        }, {
            cuisine: 'asian',
        }, ];
        $http.get('api/cuisinelist', {
            cache: true,
        }).success(function(response) {
            if (response.status == 1) {
                $scope.cuisineList = {
                    cuisineListShow: cuisineListShow,
                    cuisineListCollapse: response.data.cuisine,
                }
                $rootScope.$broadcast('cuisineListLoaded', {
                    cuisineList: $scope.cuisineList
                });
            }
        });

        $scope.pricingArray = [{
            id: 1,
            name: 'all',
            data: '',
        }, {
            id: 2,
            name: '$',
            data: 1,
        }, {
            id: 3,
            name: '$$',
            data: 2,
        }, {
            id: 4,
            name: '$$$',
            data: 3,
        }, {
            id: 5,
            name: '$$$$',
            data: 4,
        }, ];


        $rootScope.$on('google_map_dragend', function(evt, data) {
            $scope.restaurantsInMap = data.restaurantsInMap;
            Pagination();
        });
        $rootScope.$on('google_map_zoom_changed', function(evt, data) {
            $scope.restaurantsInMap = data.restaurantsInMap;
            Pagination();
        });

        $scope.ExpandMap = function() {
            if ($scope.MapExpanded) {
                $scope.MapExpanded = false;
                $scope.SearchSectionClasses = ['col-lg-8', 'col-md-8', 'col-sm-8', 'col-xs-12'];
                $scope.GoogleMapSectionClasses = ['col-lg-4', 'col-md-4', 'col-sm-4', 'hidden-xs'];
                $scope.RestaurantItemClasses = ['col-lg-4', 'col-md-4', 'col-sm-4', 'col-xs-12'];
            } else {
                $scope.MapExpanded = true;
                $scope.SearchSectionClasses = ['col-lg-6', 'col-md-6', 'col-sm-6', 'col-xs-12'];
                $scope.GoogleMapSectionClasses = ['col-lg-6', 'col-md-6', 'col-sm-6', 'hidden-xs'];
                $scope.RestaurantItemClasses = ['col-lg-6', 'col-md-6', 'col-sm-6', 'col-xs-12'];
            }
            $rootScope.$broadcast('MapSizeChanged');
        };

        function Pagination() {
            $scope.pageNumbers = [];
            if ($scope.page == 1) {
                $scope.showPrevBtn = false;
            } else {
                $scope.showPrevBtn = true;
            };
            if ($scope.restaurantsInMap.length < $scope.restaurantsPerPage) {
                $scope.showPagination = false;
            } else {
                $scope.showPagination = true;
                var totalPages = Math.ceil($scope.restaurantsInMap.length / $scope.restaurantsPerPage);
                for (i = 1; i <= totalPages; i++) {
                    $scope.pageNumbers.push(i);
                };
                if ($scope.page == totalPages) {
                    $scope.showNextBtn = false;
                } else {
                    $scope.showNextBtn = true;
                };
            };
        };
    }
]);
