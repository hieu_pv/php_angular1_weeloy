var SearchService = angular.module('SearchService', []);
SearchService.service('Search', ['$http', '$q', function($http, $q) {
    this.getRestaurant = function(status, city, cuisine, pricing, free_search, page) {
        if (status == undefined) {
            status = 'both';
        };
        if (city == undefined) {
            city = 'singapore';
        };
        if (cuisine != undefined) {
            if (Array.isArray(cuisine)) {
                cuisine = cuisine.join('|');
            };
        } else {
            cuisine = '';
        };
        if (free_search == undefined) {
            free_search = '';
        };
        if (page == undefined) {
            page = 1;
        }
        if (pricing == undefined) {
            pricing = '';
        }
        var defferred = $q.defer();
        var params = {
            status: status,
            city: city,
            cuisine: cuisine,
            free_search: free_search,
            pricing: pricing,
        };
        $http.get('api/search/restaurant', {
            params: params,
            cache: false,
        }).success(function(response) {
            defferred.resolve(response);
        });
        return defferred.promise;
    };
    this.getBestRestaurants = function(city, limit) {
        if (city == undefined) {
            city = 'singapore';
        };
        if (limit == undefined) {
            limit = 1;
        };
        var defferred = $q.defer();
        var API_URL = 'api/restaurant/getShowcase/' + city + '/' + limit;
        $http.get(API_URL, {
            cache: true
        }).success(function(response) {
            defferred.resolve(response);
        });
        return defferred.promise;
    };
}]);
