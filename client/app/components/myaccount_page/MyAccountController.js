var MyAccountController = angular.module('MyAccountController', ['MyAccountService', 'LoginService']);
MyAccountController.controller('MyAccountCtrl', ['$rootScope', '$scope', 'Account', 'loginService', function($rootScope, $scope, Account, loginService) {
    $rootScope.MenuUserSelected = 'myaccount';
    $rootScope.showAccountForm = 'myAccountForm';
    var genders = [{
        id: 1,
        name: 'Male'
    }, {
        id: 2,
        name: 'Female'
    }, ];
    $scope.genders = genders;
    $scope.UpdateUser = {
        email : $rootScope.user.email,
        firstname: $rootScope.user.data.firstname,
        lastname: $rootScope.user.data.lastname,
    }
    $scope.UpdateAccount = function(user) {
        console.log(user);
    };
}]);
