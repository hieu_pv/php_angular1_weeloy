var InfoContactService = angular.module('InfoContactService', []);
InfoContactService.service('Contact', ['$http', '$q', function($http, $q) {
    this.sendContact = function(firstname, lastname, email, message) {
        var API_URL = 'api/send-contact-email';
        var data = {
            firstname: firstname,
            lastname: lastname,
            email: email,
            message: message,
        };
        var defferred = $q.defer();
        $http.post(API_URL, data).success(function(response) {
            defferred.resolve(response);
        });
        return defferred.promise;
    };
}]);
