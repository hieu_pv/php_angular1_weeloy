var InfoContactController = angular.module('InfoContactController', ['InfoContactService']);
InfoContactController.controller('InfoContactCtrl', ['$rootScope', '$scope', 'Contact', function($rootScope, $scope, Contact) {
    $scope.cities = [{
        id: 1,
        name: 'Singapore'
    }, {
        id: 2,
        name: 'Hong Kong'
    }, {
        id: 3,
        name: 'Bangkok'
    }, ];
    $scope.contact = new Object();
    $scope.contact.city = $scope.cities[0];
    $scope.ContactSubmit = function(contact) {
        console.log(contact);
        Contact.sendContact(contact.firstname, contact.lastname, contact.email, contact.message).then(function(response) {
            console.log(response);
        });
    };
}]);

