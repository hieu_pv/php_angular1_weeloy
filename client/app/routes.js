WeeloyApp.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
    $routeProvider
        .when('/angular-client', {
            templateUrl: 'client/app/components/home/_home.html',
            controller: 'HomeCtrl',
        })
        .when('/angular-client/search', {
            templateUrl: 'client/app/components/search_page/_search_page.html',
            controller: 'SearchCtrl',
        })
        .when('/angular-client/restaurant/:restaurant', {
            templateUrl: 'client/app/components/restaurant_info_page/_restaurant_info_page.html',
            controller: 'RestaurantInfoCtrl',
        })
        .when('/angular-client/mybookings', {
            templateUrl: 'client/app/components/mybookings_page/_mybookings_page.html',
            controller: 'MyBookingCtrl',
        })
        .when('/angular-client/myaccount', {
            templateUrl: 'client/app/components/myaccount_page/_myaccount_page.html',
            controller: 'MyAccountCtrl',
        })
        .when('/angular-client/myreviews', {
            templateUrl: 'client/app/components/myreviews_page/_my_reviews_page.html',
            controller: 'MyReviewCtrl',
        })
        .when('/angular-client/info-contact', {
            templateUrl: 'client/app/components/info_contact_page/_info_contact_page.html',
            controller: 'InfoContactCtrl',
        })
        .when('/angular-client/info-partner', {
            templateUrl: 'client/app/components/info_partner_page/_info_partner.html',
            controller: 'InfoPartnerCtrl',
        })
        .when('/angular-client/how-it-works', {
            templateUrl: 'client/app/components/how_it_work_page/how_it_work.html',
            controller: 'HowItWorkCtrl',
        })
        .when('/angular-client/info-faq', {
            templateUrl: 'client/app/components/faq_page/faq.html',
            controller: 'FAQCtrl',
        })
        .when('/angular-client/all-rewards/:restaurant', {
            templateUrl: 'client/app/components/all_rewards_page/_all_rewards.html',
            controller: 'AllRewardsCtrl',
        });

    $locationProvider.html5Mode({
        enabled: true,
        requireBase: false
    });
}]);
