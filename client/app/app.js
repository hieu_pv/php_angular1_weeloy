var WeeloyApp = angular.module('WeeloyApp', [
    'ngRoute',
    'RootController',
    'HomeController',
    'SearchController',
    'RestaurantInfoController',
    'MyBookingController',
    'MyAccountController',
    'MyReviewController',
    'InfoPartnerController',
    'InfoContactController',
    'FAQController',
    'HowItWorkController',
    'AllRewardsController'
]);
WeeloyApp.run([
    '$rootScope',
    '$http',
    '$location',
    function($rootScope, $http, $location) {
        console.log('WeeloyApp is running...');

        $http.get('client/assets/language/' + config.lang + '.json', {
            cache: true,
        }).success(function(response) {
            $rootScope.Str = response.data.str;
        });
        $rootScope.loggedin = loggedin;
        $rootScope.user = user;
        $rootScope.$on('$routeChangeSuccess', function() {
            $("html, body").animate({
                scrollTop: 0
            }, 100);
        });
        if ($rootScope.user != null) {
            BasicAuthentication($rootScope.user);
        };
        $rootScope.$on('UserLogin', function(evt, user) {
            BasicAuthentication($rootScope.user);
        });

        function BasicAuthentication(user) {
            var Authorization = btoa(user.email + ':' + user.data.token);
            $http.defaults.headers.common.Authorization = 'Basic ' + Authorization;
        }
        $rootScope.checkLoggedin = function() {
            if ($rootScope.user == undefined || $rootScope.user == null) {
                $location.path('angular-client');
            };
        };
    }
]);
