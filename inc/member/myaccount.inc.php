<?php

require_once("lib/class.member.inc.php");

$member = new WY_Member();
if(isset($_POST) && !empty($_POST)){
    
    //var_dump($_POST);
    $member->email = $_POST['email'];
    $member->gender = $_POST['gender'];
    $member->firstname = $_POST['fname'];
    $member->name = $_POST['lname'];
    $member->mobile = $_POST['mobile'];
    //var_dump($member->updateMember());die;
    if($member->updateMember()){
        $included_data['info_message'] = array('type'=>'success', 'message'=>'Informations updated');
    }else{
        $included_data['info_message'] = array('type'=>'warning', 'message'=>$member->updateMember().'An error occured, please try again later');
    }
}

$member->getMember($_SESSION['user']['email']);

$included_data['member'] = $member;