var ResizeFilter = angular.module('ResizeFilter', []);
ResizeFilter.run(function() {
    $(window).resize(function() {
        $('#expand-map-content').css('right', $('#map-content').width() - 60);
        if ($('#search-filter').width() < 720) {
            $('#search-filter .cuisine').css('width', '50%');
        } else {
            $('#search-filter .cuisine').css('width', '25%');
        }
        if ($('#search-filter').width() < 600) {
            $('.filterButton').addClass('filterButtonRight');
        } else {
            $('.filterButton').removeClass('filterButtonRight');
        };
    });
});
ResizeFilter.directive('resizeFilter', function() {
    return {
        restrict: 'A',
        scope: true,
        link: function(scope, element, attrs) {
            if ($('#search-filter').width() < 720) {
                $(element).css('width', '50%');
            }
            if ($('#search-filter').width() < 600) {
                $('.filterButton').addClass('filterButtonRight');
            };
        },
    };
});