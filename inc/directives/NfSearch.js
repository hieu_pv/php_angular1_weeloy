var NfSearch = angular.module('NfSearch', []);
NfSearch.directive('nfSearch', ['$rootScope', '$http', function($rootScope, $http) {
    return {
        restrict: 'A',
        scope: {
            nfSearch: '@',
            ngModel: '=',
        },
        link: function(scope, element, attrs) {
            $rootScope.showAutoCorrect = false;
            var cuisinelist = CuisineListInit();
            localStorage.removeItem('__restaurants_bloodhood_cache_key__data');
            var restaurants = RestaurantResourceInit();
            TypeaheadInit(element, restaurants, cuisinelist);

            function RestaurantResourceInit(city) {
                if (city == undefined) {
                    city = $('#dropdown_title2').text();
                };
                if (scope.nfSearch == undefined || scope.nfSearch == '') {
                    var url = '../../api/search/fulltext?city=' + city;
                } else {
                    var url = scope.nfSearch + '?city=' + city;
                };
                var restaurants = new Bloodhound({
                    datumTokenizer: function(d) {
                        var test = Bloodhound.tokenizers.whitespace(d.title);
                        $.each(test, function(k, v) {
                            i = 0;
                            while ((i + 1) < v.length) {
                                test.push(v.substr(i, v.length));
                                i++;
                            }
                        })
                        return test;
                    },
                    queryTokenizer: Bloodhound.tokenizers.whitespace,
                    prefetch: {
                        url: url,
                        thumbprint: 'ver 1.4',
                        cacheKey: 'restaurants_bloodhood_cache_key',
                        transform: function(response) {
                            var data = response.data.restaurants;
                            data.forEach(function(value, key) {
                                data[key].cuisine = data[key].cuisine.replace("|", " ");
                            });
                            console.log(data);
                            return data;
                        },
                    },
                    remote: {
                        url: url + "?city=" + city + "query=%QUERY*",
                        wildcard: '%QUERY',
                        transform: function(response) {
                            var data = response.data.restaurants;
                            data.forEach(function(value, key) {
                                data[key].cuisine = data[key].cuisine.replace("|", " ");
                            })
                            return data;
                        },
                    },
                });
                return restaurants;
            };

            function CuisineListInit() {
                var cuisinelist = new Bloodhound({
                    datumTokenizer: function(d) {
                        var test = Bloodhound.tokenizers.whitespace(d.cuisine);
                        $.each(test, function(k, v) {
                            i = 0;
                            while ((i + 1) < v.length) {
                                test.push(v.substr(i, v.length));
                                i++;
                            }
                        })
                        return test;
                    },
                    queryTokenizer: Bloodhound.tokenizers.whitespace,
                    prefetch: {
                        url: "../../api/cuisinelist",
                        transform: function(response) {
                            var data = response.data.cuisine;
                            return data;
                        },
                    }
                });
                return cuisinelist;
            };

            function TypeaheadInit(element, restaurants, cuisinelist) {
                $(element).typeahead({
                    minLength: 1,
                    highlight: true,
                    hint: true,
                }, {
                    name: 'title',
                    display: 'title',
                    source: restaurants,
                    templates: {
                        header: '<p class="search-box-header"><strong>Restaurant</strong><p>',
                    }
                }, {
                    name: 'cuisine',
                    display: 'cuisine',
                    source: cuisinelist,
                    templates: {
                        header: '<p class="search-box-header"><strong>Cuisine</strong><p>',
                    }
                });
            };
            $(element).bind('typeahead:render', function(evt, suggestions, flag, name) {
                if (suggestions == undefined) {
                    var length = $('.twitter-typeahead').find('.tt-suggestion').length;
                    if (length == 0) {
                        if ($('#auto_correct').attr('query') == $(element).val() || element.val() == '') {
                            return;
                        }
                        $('#auto_correct').attr('query', $(element).val());
                        if (scope.nfSearch == undefined || scope.nfSearch == '') {
                            var url = '../../api/search/fulltext' + '?query=' + $(element).val() + '&city=' + $('#dropdown_title2').text();
                        } else {
                            var url = scope.nfSearch + '?query=' + $(element).val() + '&city=' + $('#dropdown_title2').text();
                        };
                        $http.get(url, {
                            cache: true
                        }).success(function(response) {
                            console.log(url);
                            if (response.status == 1) {
                                $rootScope.suggestions = response.data.restaurants;
                                if (response.data.restaurants.length > 0) {
                                    $rootScope.showAutoCorrect = true;
                                };
                            };
                        });
                    };
                } else {
                    $rootScope.showAutoCorrect = false;
                };
            });
            $rootScope.$on('ChangeCity', function(evt, city) {
                $(element).typeahead('destroy');
                localStorage.removeItem('__restaurants_bloodhood_cache_key__data');
                console.log(city);
                var restaurants = RestaurantResourceInit(city.city);
                TypeaheadInit(element, restaurants, cuisinelist);
            });
        },
    };
}]);
