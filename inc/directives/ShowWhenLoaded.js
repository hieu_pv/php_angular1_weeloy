var ShowWhenLoaded = angular.module('ShowWhenLoaded', []);
ShowWhenLoaded.run(['$rootScope', function($rootScope) {
    $(window).resize(function() {
        $('.item-image').css('height', $('.item-image').parent().width() / 1.5);
    });
}]);
ShowWhenLoaded.directive('loadElement', ['$rootScope', function($rootScope) {
    return {
        restrict: 'A',
        scope: {
            loadElement: '=',
        },
        link: function(scope, element, attrs) {
            if (!scope.loadElement) {
                $(element).removeClass('col-lg-4');
                $(element).removeClass('col-md-4');
                $(element).removeClass('col-sm-4');
                $(element).addClass('col-lg-6');
                $(element).addClass('col-md-6');
                $(element).addClass('col-sm-6');
            };
            var img = $(element).find('.item-image');
            var ImgBg = $(element).find('.item-image-bg');
            var imageHeight = img.parent().width() / 1.5;
            ImgBg.css('height', imageHeight);
            img[0].onload = function() {
                img.css('height', imageHeight);
                ImgBg.remove();
                img.parent().css('position', 'relative');
            };
        },
    };
}]);
