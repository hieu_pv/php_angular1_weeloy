var CaculateMapContentMaxheight = angular.module('CaculateMapContentMaxheight', []);
CaculateMapContentMaxheight.run(['$rootScope', function($rootScope) {
    $(window).scroll(function() {
        map_div = $('#map-content');
        fixedLimit = $(document).height() - $('footer').height() - 40;
        map_div.css('max-height', fixedLimit + 'px');
        windowScroll = $(window).scrollTop() + map_div.height();
        if (windowScroll > fixedLimit) {
            var top = '-' + (windowScroll - fixedLimit - 40);
        } else {
            top = 40;
        }
        $('#expand-map-content').css('top', 'calc(60% + ' + top + 'px)');
        map_div.css('top', top + 'px');
    });
}]);
CaculateMapContentMaxheight.directive('caculateMapContentMaxheight', ['$rootScope', function($rootScope) {
    return {
        restrict: 'A',
        scope: true,
        link: function(scope, element, attrs) {
            $('#expand-map-content').css('right', $('#map-content').width() - 60);
            $(element).css('min-height', $(window).height());
        },
    };
}]);
