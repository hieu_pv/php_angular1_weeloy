<?php

require_once 'lib/class.event.inc.php';
require_once 'lib/class.promotion.inc.php';


//input
//$restaurant = filter_input(INPUT_GET, 'restaurantname', FILTER_SANITIZE_SPECIAL_CHARS);
$country = filter_input(INPUT_GET, 'country', FILTER_SANITIZE_STRING);
$city = filter_input(INPUT_GET, 'city', FILTER_SANITIZE_STRING);
$restaurant_name = filter_input(INPUT_GET, 'restaurantname', FILTER_SANITIZE_STRING);

$path_adaptor = '../../../';
if($country == $city){
    $path_adaptor = '../../';    
}
$browser = new Browser;
$included_data['is_mobile'] = $browser->isMobile();
$included_data['facebook_id'] = WEBSITE_FB_APP_ID;



if (!empty($country) && !empty($city)) {

    $restaurant_id = '';
    switch ($country) {
        case 'singapore':
            $restaurant_id .= 'SG';
            break;
        case 'hong-kong':
            $restaurant_id .= 'HK';
            break;
        case 'thailand':
            $restaurant_id .= 'TH';
            break;
        case 'malaysia':
            $restaurant_id .= 'MY';
            break;
        default:
            $restaurant_id .= 'SG';
            break;
    }

    $restaurant_id .= '_';

    switch ($city) {
        case 'singapore':
            $restaurant_id .= 'SG';
            break;
        case 'hong-kong':
            $restaurant_id .= 'HK';
            break;
        case 'bangkok':
            $restaurant_id .= 'BK';
            break;
        case 'phuket':
            $restaurant_id .= 'PK';
            break;
        case 'kuala-lumpur':
            $restaurant_id .= 'KL';
            break;
        default:
            $restaurant_id .= 'SG';
            break;
    }

    $restaurant_id .= '_R_';

    $restaurant_name = ucfirst($restaurant_name);
    $restaurant_name = preg_replace('/-[a-z]/e', 'strtoupper("$0")', $restaurant_name);
    $restaurant_name = str_replace('-', '', $restaurant_name);
    $restaurant_id .= $restaurant_name;
    
    //hack old method
    $_REQUEST['restaurantname'] = $restaurant_id;
    
    $restaurant = $restaurant_id;
} else {
    $restaurant = filter_input(INPUT_GET, 'restaurantname', FILTER_SANITIZE_STRING);
}

//restaurant information
$res = new WY_restaurant();
$res->getRestaurant($restaurant);
$description = $res->description;
$description = str_replace('||||', '<br>', $description);
$res->description = $description;

$included_data['restaurant'] = $res;
$included_data['opening_hours'] = $res->getOpeningHoursListing($restaurant, true);
if(count($included_data['opening_hours']) == 1){
    $included_data['opening_hours_type'] = '_24';
}else{$included_data['opening_hours_type'] = '';}




if ($res->is_wheelable == '0') {
    $promo = new WY_Promotion($res->restaurant);
    $tmp = $promo->getDefaultPromotionDetails();
    $included_data['best_promotion'] = $tmp['offer'];
}else{
    $included_data['best_offers'] = $res->getBestOffer(NULL,NULL,NULL,3);
}
//wheel
$included_data['wheel_link'] = $res->getWheelButtonLink($restaurant);

// media
$media = new WY_Media($restaurant);
$included_data['picture']['logo'] = $media->getLogo($restaurant);
$included_data['picture']['banner'] = str_replace(' ', '%20', $media->getDefaultPicture($restaurant, 'large'));
$included_data['picture']['chef'] = $media->getChef($restaurant);
$included_data['picture']['wheelvalue'] = $media->getWheelValue($res->wheelvalue);
$included_data['video']['restaurant_dishes'] = $media->getVideoRestaurantDishes($restaurant);

//chef
$included_data['chef']['name'] = $res->chef;
$included_data['chef']['believe'] = $res->chef_believe;
$included_data['chef']['description'] = $res->chef_description;
$description = $res->chef_description;
if (strlen($description) > 150) {
    $description_array = split_string_phrase($description, 150);
    $included_data['chef']['description'] = $description_array[0];
    $included_data['chef']['description2'] = $description_array[1];
}

$included_data['chef']['origin'] = $res->chef_origin;
$included_data['chef']['award'] = $res->chef_award;
$included_data['chef']['gender'] = (!empty($res->chef_type) && preg_match('/Female/', $res->chef_type)) ? "Her" : "His";
$included_data['chef']['label'] = (!empty($res->chef_type)) ? preg_replace('/ - .*|Male|Female/', '', $res->chef_type) : "Chef";

//maps
$map_details = explode('@', $res->map);
$coordonnees = explode(',', $map_details[1]);
$included_data['maps']['lat'] = $coordonnees[0];
$included_data['maps']['lon'] = $coordonnees[1];
$included_data['maps']['add'] = $map_details[0];

//events
$events = new WY_Event($restaurant);
$included_data['events'] = $events->getComingEvents();
foreach ($included_data['events'] as $key => $event) {
    $included_data['events'][$key]['default_picture'] = $media->getEventFullPath($restaurant, $event['picture']);
}

// pictures image Gallery
$pic = $media->getRestaurantGallery($restaurant);
//var_dump($pic);die;

$tmp = array();
if (count($pic) >= 5) {
    $included_data['gallery']['nb_pic'] = 5;
} else if (count($pic) >= 2) {
    $included_data['gallery']['nb_pic'] = 2;
} else {
    $included_data['gallery']['nb_pic'] = 0;
}
for ($i = 0; $i < count($pic); $i++) {
    $p = $pic[$i];
    $tmp[] = array('thumb' => $p->getFullPath('small'), 'full' => $p->getFullPath('large'), 'description'=> $p->getDescription());
}
$included_data['gallery']['pictures'] = $tmp;

$included_data['gallery']['total_pic'] = count($pic);

// menu
$menus = new WY_Menu($restaurant);
$menu_data = $menus->getMenuCategories();
$included_data['menu']['categorie'] = $menu_data;
$menu_data_item = $menus->getMenus();
$included_data['menu']['item'] = $menu_data_item;

//book button
$booktitle = "BOOK SOON";
$custombutton = "custom_button_book_soon";
if (($res->status == 'active' && $res->is_bookable) || $res->status == 'demo_reference') {
    $booktitle = "BOOK NOW";
    $custombutton = "";
}

if ($res->is_bookable == false && $res->status == 'active') {
    $booktitle = "REQUEST NOW";
    $custombutton = "btn-green";
}
$button = $res->getBookButtonActionLink($restaurant, $res->title, $path_adaptor);
$included_data['bookbutton']['booktitle'] = $booktitle;
$included_data['bookbutton']['customstyle'] = $custombutton;
$included_data['bookbutton']['actionlink'] = $button;

// services
$service = new WY_Service();
$included_data['service']['services_list'] = $service_list = $service->getRestaurantServices($res->restaurant, 'active_only');

//var_dump($included_data['service']['services_list']);

//REVIEWS
$reviews = new WY_Review($res->restaurant);
$count_reviews = $reviews->getReviewsCount();
$reviews->getReviewsList($limit);

$count = 0;


foreach ($reviews->data as $review){
    $count = $count + 1;
    $review['user_picture'] = $media->getUserProfilePicture($review['email']);
    unset($review['email']);
    $review_res[] = $review;
}
    
$included_data['review']['count']=$count_reviews;
$included_data['review']['details']=$review_res;

function split_string_phrase($longString, $size_first_p) {

    $arrayWords = explode('.', $longString);

    $currentLength = 0;
    $index = 0;
    $first_p_complete = false;

    foreach ($arrayWords as $word) {
        if (!$first_p_complete) {
            $wordLength = strlen($word) + 1;
            if (( $currentLength + $wordLength ) <= $size_first_p) {
                $arrayOutput[$index] .= $word . '. ';
                $currentLength += $wordLength;
            } else {
                $first_p_complete = true;
                $index ++;
                $arrayOutput[$index] .= $word . '.';
            }
        } else {
            if (trim($word) != '') {
                $arrayOutput[$index] .= $word . '.';
            }
        }
    }
    return $arrayOutput;
}
