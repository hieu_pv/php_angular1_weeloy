<?php

require_once 'lib/class.event.inc.php';
require_once 'lib/class.promotion.inc.php';


//input
//$restaurant = filter_input(INPUT_GET, 'restaurantname', FILTER_SANITIZE_SPECIAL_CHARS);
$country = filter_input(INPUT_GET, 'country', FILTER_SANITIZE_STRING);
$city = filter_input(INPUT_GET, 'city', FILTER_SANITIZE_STRING);
$restaurant_name = filter_input(INPUT_GET, 'restaurantname', FILTER_SANITIZE_STRING);

$path_adaptor = '../../../';
if($country == $city){
    $path_adaptor = '../../';    
}
$browser = new Browser;
$included_data['is_mobile'] = $browser->isMobile();



if (!empty($country) && !empty($city)) {

    $restaurant_id = '';
    switch ($country) {
        case 'singapore':
            $restaurant_id .= 'SG';
            break;
        case 'hong-kong':
            $restaurant_id .= 'HK';
            break;
        case 'thailand':
            $restaurant_id .= 'TH';
            break;
        case 'malaysia':
            $restaurant_id .= 'MY';
            break;
        default:
            $restaurant_id .= 'SG';
            break;
    }

    $restaurant_id .= '_';

    switch ($city) {
        case 'singapore':
            $restaurant_id .= 'SG';
            break;
        case 'hong-kong':
            $restaurant_id .= 'HK';
            break;
        case 'bangkok':
            $restaurant_id .= 'BK';
            break;
        case 'phuket':
            $restaurant_id .= 'PK';
            break;
        case 'kuala-lumpur':
            $restaurant_id .= 'KL';
            break;
        default:
            $restaurant_id .= 'SG';
            break;
    }

    $restaurant_id .= '_R_';

    $restaurant_name = ucfirst($restaurant_name);
    $restaurant_name = preg_replace('/-[a-z]/e', 'strtoupper("$0")', $restaurant_name);
    $restaurant_name = str_replace('-', '', $restaurant_name);
    $restaurant_id .= $restaurant_name;
    
    //hack old method
    $_REQUEST['restaurantname'] = $restaurant_id;
    
    $restaurant = $restaurant_id;
} else {
    $restaurant = filter_input(INPUT_GET, 'restaurantname', FILTER_SANITIZE_STRING);
}

//restaurant information
$res = new WY_restaurant();
$res->getRestaurant($restaurant);
$included_data['restaurant'] = $res;

// media
$media = new WY_Media($restaurant);
$included_data['picture']['logo'] = $media->getLogo($restaurant);
$included_data['picture']['wheel'] = $media->getWheel($restaurant).'?time='.uniqid();

//book button
$booktitle = "BOOK SOON";
$custombutton = "custom_button_book_soon";
if (($res->status == 'active' && $res->is_bookable) || $res->status == 'demo_reference') {
    $booktitle = "BOOK NOW";
    $custombutton = "";
}

if ($res->is_bookable == false && $res->status == 'active') {
    $booktitle = "REQUEST NOW";
    $custombutton = "btn-green";
}
$button = $res->getBookButtonActionLink($restaurant, $res->title, $path_adaptor);
$included_data['bookbutton']['booktitle'] = $booktitle;
$included_data['bookbutton']['customstyle'] = $custombutton;
$included_data['bookbutton']['actionlink'] = $button;

$included_data['best_offers'] = $res->getBestOffer(NULL,NULL,NULL,24);