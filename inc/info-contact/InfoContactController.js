var InfoContactController = angular.module('InfoContactController', []);
InfoContactController.controller('InfoContactCtrl', ['$rootScope', '$scope', function($rootScope, $scope) {
    $scope.cities = [{
        id: 1,
        name: 'Singapore'
    }, {
        id: 2,
        name: 'Hong Kong'
    }, {
        id: 3,
        name: 'Bangkok'
    }, ];
    $scope.contact = new Object();
    $scope.contact.city = $scope.cities[0];
    $scope.ContactSubmit = function(contact) {
        alert('submitted');
        console.log(contact);
    };
}]);
