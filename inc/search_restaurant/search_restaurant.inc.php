<?php

//$cuisinerestaurant = (isset($_REQUEST['cuisinerestaurant'])) ? $_REQUEST['cuisinerestaurant'] : "";  // this is a multi select -> this is an array
//$cityrestaurant = (isset($_REQUEST['cityrestaurant'])) ? $_REQUEST['cityrestaurant'] : "";
//$restitle = (isset($_REQUEST['restitle'])) ? $_REQUEST['restitle'] : "";
//$dinesfree = (isset($_REQUEST['dinesfree'])) ? "checked" : "";
//$wheelselect = (isset($_REQUEST['wheelselect'])) ? intval($_REQUEST['wheelselect']) : -1;

$cuisinerestaurant = '';
$dinesfree = '';
$wheelselect = -1;


$res = new WY_restaurant;
$browser = new Browser;
$included_data['is_mobile'] = $browser->isMobile();




$cityrestaurant = filter_input(INPUT_GET, 'city', FILTER_SANITIZE_STRING);
$restitle = filter_input(INPUT_GET, 'restitle', FILTER_SANITIZE_STRING);

$path_adaptor = '../../';

if (!empty($cityrestaurant)) {
    $_SESSION['user']['forced_city'] = $cityrestaurant;
    switch ($cityrestaurant) {
        case 'Phuket' :
        case 'Bangkok' :
        case 'phuket' :
        case 'bangkok' :
            $_SESSION['user']['forced_country'] = 'TH';
            break;

        case 'Singapore' :
        case 'singapore' :
            $_SESSION['user']['forced_country'] = 'SG';
            $_SESSION['user']['forced_city'] = 'Singapore';
            break;

        case 'Kuala Lumpur' :
        case 'kuala lumpur' :
            $_SESSION['user']['forced_country'] = 'MY';
            break;

        case 'Hong Kong' :
        case 'HongKong' :
        case 'Hongkong' :
        case 'hongkong' :
            $_SESSION['user']['forced_country'] = 'HK';
            $_SESSION['user']['forced_city'] = 'Hong Kong';
            break;
    }
} else {
    unset($cityrestaurant);
    unset($_REQUEST['cityrestaurant']);
    unset($_SESSION['user']['forced_country']);
    unset($_SESSION['user']['forced_city']);
}

//default value for search bar
$included_data['active_city'] = $_SESSION['user']['forced_city'];
$included_data['restitle'] = $restitle;
$included_data['userbase64'] = base64_encode($_SESSION['user']['email'].':qqqqqqqq');

