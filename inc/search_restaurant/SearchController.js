/* global angular */

var SearchController = angular.module('SearchController', [
    'ShowWhenLoaded',
    'CaculateMapContentMaxheight',
    'noResult',
    'ResizeFilter',
    'NfSearch'
]);
SearchController.controller('SearchCtrl', ['$rootScope', '$scope', '$http', '$location', function($rootScope, $scope, $http, $location) {
    'use strict';
    console.log('search controller loading');
    $scope.restaurantAr = [];
    $scope.markerAr = [];
    $scope.entryPerPage = 12; //number of resto per page
    $scope.currentPage = 1; //current page
    $scope.pageNumbers = [];
    //$scope.pageStartItem  = 1;
    //$scope.pageEndItem  = $scope.entryPerPage;
    $scope.nbPage = 3;
    $scope.showNoResultAlert = false;
    $scope.fireZoomEvent = true;
    $scope.ClearFilterWithoutLoadRestaurantData = false;
    $scope.hidePrev = true;
    $scope.hideNext = false;
    $scope.city = $("#dropdown_title2").html();
    $scope.free_search = $('#search-input-group input[name="restitle"]').val();

    $scope.user = $("#user").val();
    $scope.is_mobile = $("#is_mobile").val();

    $scope.no_result = false;
    $scope.fromsearch = false;
    var _this = this;

    var pattern = /(.*)?restitle=(.*)/;
    var matches = $location.$$absUrl.match(pattern);
    if (matches[2] != undefined && matches[2] != '') {
        $scope.free_search = decodeURIComponent(matches[2]);
    };


    var restaurant_showcase_api_call = '../../api/restaurantfullinfo/SG_SG_R_TheLatinQuarter';
    $http.get(restaurant_showcase_api_call, {
        headers: {
            'Authorization': 'Basic ' + $scope.user
        }
    }).success(function(d) {
        $scope.restaurantShowCase = d.data.restaurantinfo;
    });

    // DO NOT DELETE
    //$http.get('../../api/search/restaurant?status=both&c=1&i_p=1&p=' + $scope.currentPage + '&city=singapore&pricing=&cuisine=').success(function (d) {
    //    $scope.nbPage = 3;
    //});

    //$http.get('../../api/search/restaurant?status=both&i_p=1&p=' + $scope.currentPage + '&city=singapore&pricing=&cuisine=').success(function (d) {

    //$scope.restaurantAr = d.data.restaurant;
    //$scope.createMarker();
    //});

    var ApiUrl = '../../api/search/restaurant?status=both&i_p=1&p=1&city=' + $scope.city + '&pricing=&cuisine=&free_search=' + $scope.free_search;
    $http.get(ApiUrl, {
        headers: {
            'Authorization': 'Basic ' + $scope.user
        }
    }).success(function(d) {
        $scope.restaurantAr = d.data.restaurant;
        $scope.markerAr = d.data.restaurant;
        $scope.no_result = d.data.no_result;
        $scope.createMarker();
    });
    $http.get('../../api/search/restaurant?status=both&i_p=1&p=&city=' + $scope.city + '&pricing=&cuisine=&free_search=' + $scope.free_search, {
        headers: {
            'Authorization': 'Basic ' + $scope.user
        }
    }).success(function(d) {
        $scope.restaurantArCount = d.data.restaurant;
        $scope.nbPage = Math.ceil($scope.restaurantArCount.length / $scope.entryPerPage);
        $scope.pageNumbers = [1];
        $scope.hidePrev = ($scope.currentPage === 1) ? true : false;
        $scope.hideNext = ($scope.currentPage === $scope.nbPage) ? true : false;
        for (var i = 2; i < $scope.nbPage + 1; i++) {
            $scope.pageNumbers.push(i);
        }
        if ($scope.pageNumbers.length == 1) {
            $scope.hidePagination = true;
        } else {
            $scope.hidePagination = false;
        };
    });


    //$scope.path_adaptor = '../../';
    //$scope.orderProp = 'age';


    //######## MAP Creation ########

    $scope.beaches = [
        ['Sopra Cucina and Bar', 1.307607, 103.830121, 1, 319],
        ['North Border', 1.304714, 103.787529, 2, 164],
        ['Shutters', 1.251045, 103.823368, 3, 199],
        ['B-Bar', 1.292683, 103.848969, 4, 197],
        ['Escape Restaurant And Lounge', 1.31244, 103.854326, 5, 311],
        ['Absinthe', 1.288087, 103.849314, 6, 298],
        ['Window on the Park', 1.302375, 103.841014, 7, 102],
        ['Tier Bar', 1.25173, 103.822227, 8, 285],
        ['DOMVS, The Italian Restaurant', 1.311715, 103.836489, 9, 310],
        ['La Cantine', 1.278968, 103.851237, 10, 90],
        ['LÃ¢â‚¬â„¢EntrecÃƒÂ´te the French Brasserie', 1.294767, 103.858915, 11, 105],
        ['Forlino', 1.286057, 103.854132, 12, 106]
    ];
    $scope.mapCenterLatLng = [{
        location: ''
    }];
    $scope.infowindow = new google.maps.InfoWindow();
    $scope.myzoom = ($scope.beaches.length > 2) ? 11 : 12;
    $scope.delta = ($scope.beaches.length > 2) ? 0 : 0.05;
    $scope.beach = $scope.beaches[0];
    $scope.myLatlng = new google.maps.LatLng($scope.beach[1] - $scope.delta, $scope.beach[2]);
    $scope.Markers = [];
    $scope.image_over = 'images/google-marker.png';
    $scope.image_init = 'images/google-marker-grey.png';
    $scope.bounds;
    $scope.geocoder = new google.maps.Geocoder();

    $scope.InfoBox = new InfoBox({
        disableAutoPan: true,
        maxWidth: 0,
        pixelOffset: new google.maps.Size(-115, -300),
        closeBoxMargin: '50px 200px',
        closeBoxURL: '',
        isHidden: false,
        pane: 'floatPane',
        enableEventPropagation: true
    });;

    if (!$scope.is_mobile) {
        createMap();
        setMapCenter($scope.city.toLowerCase());
    }

    function setMapCenter(city) {
        city = city.toLowerCase();
        var lat;
        var lng;
        switch (city) {
            case 'bangkok':
                lat = 13.756411;
                lng = 100.500258;
                break;
            case 'phuket':
                lat = 7.956671;
                lng = 98.340016;
                break;
            case 'hongkong':
                lat = 22.281624;
                lng = 114.110707;
                break;
            case 'hong kong':
                lat = 22.281624;
                lng = 114.110707;
                break;
            default:
                lat = 1.307607;
                lng = 103.830121;
                break;
        }
        $scope.mymap.setCenter(new google.maps.LatLng(lat, lng));
    };

    function createMap() {
        $scope.mapOptions = {
            center: $scope.myLatlng,
            zoom: $scope.myzoom,
            //mapTypeId: google.maps.MapTypeId.TERRAIN,
            scrollwheel: false,
            navigationControl: false,
            mapTypeControl: false,
            scaleControl: false,
            draggable: true
        };
        $scope.mymap = new google.maps.Map(document.getElementById('map-canvas'), $scope.mapOptions);

        google.maps.event.addListener($scope.mymap, 'click', function() {
            $scope.InfoBox.close();
        });
        google.maps.event.addListener($scope.mymap, 'dragend', function() {
            var cuisines = [];
            for (var key in $scope.cuisineArray) {
                $scope.cuisineArray[key].forEach(function(value, k) {
                    if ($scope.cuisineArray[key][k].selected) {
                        cuisines.push($scope.cuisineArray[key][k].cuisine);
                    };
                });
            };
            if ($scope.cityFilter.data != null || cuisines.length > 0 || $scope.pricing.data != null) {
                LoadRestaurantData($scope.cityFilter.data, $scope.pricing.data, $scope.cuisineArray);
            } else {
                $scope.filterList();
                $scope.InfoBox.close();
            }
        });

        google.maps.event.addListener($scope.mymap, 'zoom_changed', function() {
            if ($scope.ZoomEventFalse == false) {
                return;
            };
            var cuisines = [];
            for (var key in $scope.cuisineArray) {
                $scope.cuisineArray[key].forEach(function(value, k) {
                    if ($scope.cuisineArray[key][k].selected) {
                        cuisines.push($scope.cuisineArray[key][k].cuisine);
                    };
                });
            };
            if ($scope.fireZoomEvent) {
                if ($scope.cityFilter.data != null || cuisines.length > 0 || $scope.pricing.data != null) {
                    LoadRestaurantData($scope.cityFilter.data, $scope.pricing.data, $scope.cuisineArray);
                } else {
                    $scope.filterList();
                    $scope.InfoBox.close();
                }
            };
        });
    };



    $scope.filterList = function() {
        $scope.bounds = $scope.mymap.getBounds();
        for (var i = 0; i < $scope.Markers.length; i++) {
            if ($scope.bounds.contains($scope.Markers[i].getPosition())) {
                $scope.Markers[i].setMap($scope.mymap);
            } else {
                $scope.Markers[i].setMap(null);
            };
        };
        $scope.currentPage = 1;
        LoadRestaurantData($scope.city, null, null);
    };
    //*******************************************************************
    //NIGHTFURY
    //*******************************************************************

    $scope.getRestaurant = function(SearchQuery) {
        return $http.get('../../api/search/fulltext', {
            params: {
                query: SearchQuery + '*'
            }
        }).then(function(response) {
            if (response.data.status == 1) {
                return response.data.data.restaurants.map(function(item) {
                    return item.title;
                });
            };
        });
    };

    $scope.citiesArray = [{
        id: 1,
        name: 'all',
        data: null,
    }, {
        id: 2,
        name: 'Bangkok',
        data: 'Bangkok',
    }, {
        id: 3,
        name: 'Singapore',
        data: 'Singapore',
    }, {
        id: 4,
        name: 'Hong Kong',
        data: 'Hong Kong',
    }, {
        id: 5,
        name: 'Phuket',
        data: 'Phuket',
    }, ];
    $scope.cityFilter = $scope.citiesArray[0];

    var cuisineListShow = [{
        cuisine: 'french',
    }, {
        cuisine: 'italian',
    }, {
        cuisine: 'international',
    }, {
        cuisine: 'asian',
    }, ];
    $http.get('../../api/cuisinelist').success(function(response) {
        if (response.status == 1) {
            $scope.cuisineArray = {
                cuisineListShow: cuisineListShow,
                cuisineListCollapse: response.data.cuisine,
            }
            $scope.expandCuisineFilter = false;
        }
    });
    $scope.pricingArray = [{
        id: 1,
        name: 'all',
        data: null,
    }, {
        id: 2,
        name: '$',
        data: 1,
    }, {
        id: 3,
        name: '$$',
        data: 2,
    }, {
        id: 4,
        name: '$$$',
        data: 3,
    }, {
        id: 5,
        name: '$$$$',
        data: 4,
    }, ];
    $scope.pricing = $scope.pricingArray[0];
    $scope.changeCuisineFilter = function(cuisine, evt) {
        cuisine.selected = !cuisine.selected;
        evt.stopPropagation();
        return;
    };
    $scope.SelectCity = function(city) {
        $scope.city = city;
        $rootScope.$broadcast('ChangeCity', {
            city: city
        });
        $('#search_bar_div input[name="restitle"]').val('');
        setMapCenter(city);
    };
    $scope.filter = function(cuisineArray, cityFilter, pricing) {
        $scope.city = cityFilter.data;
        $scope.fireZoomEvent = false;
        $scope.mymap.setZoom(12);
        $scope.fireZoomEvent = true;
        if (cityFilter.data != undefined && cityFilter.data != null) {
            setMapCenter(cityFilter.data);
        } else {
            setMapCenter('');
        };
        LoadRestaurantData(cityFilter.data, pricing.data, cuisineArray);
    };
    $scope.ApplyFilter = function() {
        LoadRestaurantData($scope.cityFilter.data, $scope.pricing.data, $scope.cuisineArray);
        $scope.expandCuisineFilter = false;
        $('#cuisineCollapse').collapse('hide');
    };
    $scope.ClearFilter = function() {
        for (var key in $scope.cuisineArray) {
            $scope.cuisineArray[key].forEach(function(value, k) {
                $scope.cuisineArray[key][k].selected = false;
            });
        };
        if ($scope.ClearFilterWithoutLoadRestaurantData) {
            return;
        } else {
            LoadRestaurantData($scope.cityFilter.data, $scope.pricing.data, $scope.cuisineArray);
        }
    };
    //*******************************************************************
    //NIGHTFURY
    //*******************************************************************
    function LoadRestaurantData(city, pricing, cuisineArray) {
        if (city == undefined || city == null) {
            city = '';
        };
        if (pricing == undefined || pricing == null) {
            pricing = '';
        };
        var cuisine = new Array();
        for (var key in cuisineArray) {
            cuisineArray[key].forEach(function(value, key) {
                if (value.selected) {
                    cuisine.push(value.cuisine);
                }
            });
        }
        if (cuisine.length > 0) {
            cuisine = cuisine.join('|');
        } else {
            cuisine = '';
        };

        $scope.bounds = $scope.mymap.getBounds();
        $http.get('../../api/search/restaurant?status=both&city=' + city + '&pricing=' + pricing + '&cuisine=' + cuisine + '&free_search=' + $scope.free_search).success(function(d) {
            $scope.restaurantAr = [];
            $scope.markerAr = [];
            $scope.ClearMarkers();
            for (var j = 0; j < d.data.restaurant.length; j++) {
                $scope.string = d.data.restaurant[j].GPS.split(',');
                $scope.latlng = new google.maps.LatLng($scope.string[0], $scope.string[1]);
                if ($scope.bounds.contains($scope.latlng)) {
                    $scope.markerAr.push(d.data.restaurant[j]);
                    $scope.restaurantAr.push(d.data.restaurant[j]);
                };
            };
            $scope.no_result = d.data.no_result;
            $scope.createMarker();

            $scope.nbPage = Math.ceil($scope.restaurantAr.length / $scope.entryPerPage);
            $scope.pageNumbers = [1];
            $scope.hidePrev = ($scope.currentPage === 1) ? true : false;
            $scope.hideNext = ($scope.currentPage === $scope.nbPage) ? true : false;
            for (var i = 2; i < $scope.nbPage + 1; i++) {
                $scope.pageNumbers.push(i);
            };
            if ($scope.pageNumbers.length == 1) {
                $scope.hidePagination = true;
            } else {
                $scope.hidePagination = false;
            };
            scrollToTop();
            if ($scope.restaurantAr.length == 0) {
                $scope.showNoResultAlert = true;
            } else {
                $scope.showNoResultAlert = false;
            };
        });
    };


    $scope.createMarker = function() {
        for (var i = 0; i < 12; i++) {
            $scope.beach = $scope.markerAr[i];
            if ($scope.beach) {
                $scope.string = $scope.beach.GPS.split(',');
                $scope.myLatLng = new google.maps.LatLng($scope.string[0], $scope.string[1]);
                //$scope.mytitle = ($scope.beaches.length > 2) ? $scope.beach[0] : $scope.beach[0] + '(' + $scope.beach[1] + ', ' + $scope.beach[2] + ')';
                $scope.mytitle = $scope.beach.title;
                $scope.zindex = 1;
                var best_of = '';
                if (typeof $scope.beach.best_offer != "undefined") {
                    best_of = $scope.beach.best_offer.offer;
                }
                var marker = new google.maps.Marker({
                    record_id: $scope.beach.ID,
                    position: $scope.myLatLng,
                    map: $scope.mymap,
                    title: $scope.mytitle,
                    region: $scope.beach.region,
                    cuisine: $scope.beach.cuisine,
                    pricing: $scope.beach.pricing,
                    rating: $scope.beach.rating,
                    offer: best_of,
                    //icon: image_init,
                    zIndex: $scope.zindex
                });
                marker.setIcon('images/gmap-marker.png');
                $scope.markerAr[i].gmapMarker = marker;
                if ($scope.beach.is_wheelable == 1) {
                    marker.wheel = '<div class="wheel">' +
                        '<img class="wheel_value" src="https://media.weeloy.com/upload/wheelvalues/wheelvalue_' + $scope.beach.wheelvalue + '.png"/>' +
                        '</div>';
                } else {
                    marker.wheel = '';
                }
                $scope.markerAr[i].gmapMarker = marker;
                $scope.Markers.push(marker);
                google.maps.event.addListener(marker, 'click', (function(marker, i) {
                    return function() {
                        var imgSrc = 'https://media.weeloy.com/upload/restaurant/' + $scope.markerAr[i].restaurant + '/' + $scope.markerAr[i].image;
                        //marker.setIcon(image_over);

                        $scope.InfoBox.setContent('<div class="border">' +
                            '<div class="custom-grid">' +
                            '<article class="ribbon_holder">' +
                            '<figure>' +
                            '<img src="' + imgSrc + '"/>' +
                            '<figcaption class="offer-left">' + marker.offer + '</figcaption>' +
                            '<figcaption class="offer-restau-name ng-binding">' + marker.title + '</figcaption>' +
                            '</figure>' +
                            '</article>' +
                            '</div>' +
                            '<div class="item">' +
                            '<div class="item-info">' +
                            '<div class="location-icon">' +
                            '<div class="location-text"><span class="fa fa-map-marker"></span>  ' + marker.region + ' - ' + marker.pricing + '<br/>' + marker.cuisine + '</div>' +
                            '</div>' +
                            '<div class="book">' +
                            '<button id="btn_book" class="btn custom_button" onClick="go(\'' + $scope.markerAr[i].restaurant + '\', \'' + $scope.markerAr[i].title + '\');">BOOK NOW</button>' +
                            '</div>' +
                            '</div>' +
                            marker.wheel +
                            '</div>' +
                            '</div>' +
                            '<span class="arrow"></span>');
                        $scope.InfoBox.open($scope.mymap, marker);

                        $scope.mymap.setCenter(marker.getPosition());

                    }
                })(marker, i));
                google.maps.event.addListener(marker, 'mouseout', (function(marker, i) {
                    return function() {
                        //marker.setIcon(image_init);
                        //$scope.InfoBox.close();
                    }
                })(marker, i));
            }
        }
    };
    $scope.showExpand = true;
    $scope.ExpandMap = function() {
        if (findBootstrapEnvironment() == 'xs') {
            return;
        };
        if ($scope.showExpand) {
            $('.search-section').find('.row-fixed').width('50%');
            $('.search-section').find('#map-content').width('50%');
            $scope.showExpand = false;
        } else {
            $('.search-section').find('.row-fixed').width('70%');
            $('.search-section').find('#map-content').width('30%');
            $scope.showExpand = true;
        }
        $('#expand-map-content').css('right', $('#map-content').width() - 50);
        $('.item-image').css('height', 'auto');
        setTimeout(function() {
            // change css filter checkbox
            $('.item-image').css('height', $('.item-image').parent().width() / 1.5);
            if ($('#search-filter').width() < 720) {
                $('#search-filter .cuisine').css('width', '50%');
            } else {
                $('#search-filter .cuisine').css('width', '25%');
            }
            // change css filter button
            if ($('#search-filter').width() < 600) {
                $('.filterButton').addClass('filterButtonRight');
            } else {
                $('.filterButton').removeClass('filterButtonRight');
            };
        }, 500);
        if (!$scope.is_mobile) {
            createMap();
            setMapCenter($scope.city);
        }
        $scope.createMarker();
        scrollToTop();
    };

    function findBootstrapEnvironment() {
        var envs = ['xs', 'sm', 'md', 'lg'];
        var $el = $('<div>');
        $el.appendTo($('body'));
        for (var i = envs.length - 1; i >= 0; i--) {
            var env = envs[i];
            $el.addClass('hidden-' + env);
            if ($el.is(':hidden')) {
                $el.remove();
                return env
            };
        };
    };

    $scope.HighLightMarker = function(restaurant) {
        $scope.markerAr.forEach(function(value, key) {
            if (value.ID == restaurant.ID) {
                value.gmapMarker.setIcon('images/highlight-marker.png');
            };
        });
    };
    $scope.notHighLightMarker = function(restaurant) {
        $scope.markerAr.forEach(function(value, key) {
            if (value.ID == restaurant.ID) {
                value.gmapMarker.setIcon('images/gmap-marker.png');
            };
        });
    };

    $scope.ClearMarkers = function() {
        for (var k = 0; k < $scope.Markers.length; k++) {
            $scope.Markers[k].setMap(null);
        }
        $scope.Markers = [];
    };

    $scope.go = function(resto, title) {
        window.open('../../modules/booking/book_form.php?bkrestaurant=' + resto + '&bktitle=' + title);
    };

    $scope.go_restaurant = function(resto) {
        if (!$scope.is_mobile) {
            window.open(
                '../../' + getRestaurantInternalPath(resto),
                '_blank' // <- This is what makes it open in a new window.
            );
        } else {
            window.location.href = '../../' + getRestaurantInternalPath(resto);
        }
    };
    $scope.Suggestion_click = function(suggestion) {
        $scope.free_search = suggestion.title;
        $rootScope.showAutoCorrect = false;
    };
    $scope.SearchInputKeyup = function(evt) {
        if (evt.keyCode === 13) {
            $scope.go_search();
            return false;
        };
    };
    $scope.AddTtCursorClass = function(evt) {
        $(evt.target).addClass('tt-cursor');
    }
    $scope.RemoveTtCursorClass = function(evt) {
        $(evt.target).removeClass('tt-cursor');
    }
    $scope.go_search = function() {
        //$('#search-input-group input[name="restitle"]').blur();
        $scope.InfoBox.close();
        //var free_text = $("#restitle").val();
        //var page = $(this).attr('rel');
        $scope.city = $("#dropdown_title2").html();
        $scope.free_search = $('#search-input-group input[name="restitle"]').val();

        $scope.ClearFilterWithoutLoadRestaurantData = true;
        $scope.ClearFilter();
        $scope.ClearFilterWithoutLoadRestaurantData = false;

        $scope.fireZoomEvent = false;
        $scope.mymap.setZoom(12);
        $scope.fireZoomEvent = true;

        var params = {
            status: 'both',
            city: $scope.city,
            pricing: '',
            cuisine: '',
            free_search: $scope.free_search,
        };
        $http.get('../../api/search/restaurant', {
            params: params
        }).success(function(d) {

            $scope.restaurantArCount = d.data.restaurant;
            $scope.nbPage = Math.ceil($scope.restaurantArCount.length / $scope.entryPerPage);
            $scope.pageNumbers = [1];
            $scope.hidePrev = ($scope.currentPage === 1) ? true : false;
            $scope.hideNext = ($scope.currentPage === $scope.nbPage) ? true : false;
            for (var i = 2; i < $scope.nbPage + 1; i++) {
                $scope.pageNumbers.push(i);
            }
            if ($scope.pageNumbers.length == 1) {
                $scope.hidePagination = true;
            } else {
                $scope.hidePagination = false;
            };
            if (!$scope.is_mobile) {
                $scope.bounds = $scope.mymap.getBounds();
                $scope.MapBounds = new google.maps.LatLngBounds();
                $scope.restaurantAr = [];
                $scope.markerAr = [];
                $scope.ClearMarkers();
                for (var j = 0; j < d.data.restaurant.length; j++) {
                    $scope.string = d.data.restaurant[j].GPS.split(',');
                    $scope.latlng = new google.maps.LatLng($scope.string[0], $scope.string[1]);
                    $scope.MapBounds.extend($scope.latlng);
                    if ($scope.bounds.contains($scope.latlng)) {
                        $scope.restaurantAr.push(d.data.restaurant[j]);
                        $scope.markerAr.push(d.data.restaurant[j]);
                    }
                }

                if (!$scope.free_search) {
                    $scope.fireZoomEvent = false;
                    $scope.mymap.fitBounds($scope.MapBounds);
                    $scope.fireZoomEvent = true;
                }
                $scope.createMarker();
            } else {
                $scope.restaurantAr = d.data.restaurant;
            }
            $scope.no_result = d.data.no_result;
        });

        /* $http.get('../../api/search/restaurant?status=both&i_p=1&p=&city=' + $scope.city + '&pricing=&cuisine=&free_search='+$scope.free_search).success(function (d) {
                                                                                                                                        });*/
    };

    //call this after updating the currentPage
    function updateContent() {
        $scope.InfoBox.close();
        //$scope.mymap.setCenter($scope.myLatlng);
        $scope.city = $("#dropdown_title2").html();
        $scope.free_search = $('#search-input-group input[name="restitle"]').val();
        if (!$scope.is_mobile) {
            $scope.bounds = $scope.mymap.getBounds();
        }
        $http.get('../../api/search/restaurant?status=both&i_p=1&p=' + $scope.currentPage + '&city=' + $scope.city + '&pricing=&cuisine=&free_search=' + $scope.free_search, {
            headers: {
                'Authorization': 'Basic ' + $scope.user
            }
        }).success(function(d) {
            $scope.restaurantAr = [];
            if (!$scope.is_mobile) {
                $scope.markerAr = [];
                $scope.ClearMarkers();
                for (var j = 0; j < d.data.restaurant.length; j++) {
                    $scope.string = d.data.restaurant[j].GPS.split(',');
                    $scope.latlng = new google.maps.LatLng($scope.string[0], $scope.string[1]);

                    if ($scope.bounds.contains($scope.latlng)) {
                        $scope.restaurantAr.push(d.data.restaurant[j]);
                        $scope.markerAr.push(d.data.restaurant[j]);

                    }
                }
                $scope.createMarker();
            } else {
                $scope.restaurantAr = d.data.restaurant;
            }
            $scope.no_result = d.data.no_result;
            scrollToTop();

        });

        $http.get('../../api/search/restaurant?status=both&i_p=1&p=&city=' + $scope.city + '&pricing=&cuisine=&free_search=' + $scope.free_search, {
            headers: {
                'Authorization': 'Basic ' + $scope.user
            }
        }).success(function(d) {
            $scope.restaurantArCount = d.data.restaurant;
            $scope.nbPage = Math.ceil($scope.restaurantArCount.length / $scope.entryPerPage);
            $scope.pageNumbers = [1];
            $scope.hidePrev = ($scope.currentPage === 1) ? true : false;
            $scope.hideNext = ($scope.currentPage === $scope.nbPage) ? true : false;
            for (var i = 2; i < $scope.nbPage + 1; i++) {
                $scope.pageNumbers.push(i);
            }
        });
    };

    function scrollToTop() {
        $("html, body").animate({
            scrollTop: 0
        }, 700);
    };
    $scope.getLatLng = function(address) {
        $scope.geocoder.geocode({
            'address': address
        }, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                $scope.mymap.setCenter(results[0].geometry.location);
            } else {
                alert('Geocode was not successful for the following reason: ' + status);
            }
        });
    };

    $scope.previous = function() {
        $scope.currentPage--;
        updateContent();
    };

    $scope.setPage = function(index) {
        $scope.currentPage = index + 1;
        updateContent();
    };

    $scope.next = function() {
        $scope.currentPage++;
        updateContent();
    };


    function getRestaurantInternalPath(restaurant) {


        var country = '',
            city = '',
            restaurant_name = '',
            restaurant_url = '';

        var restaurant_details = restaurant.split('_');


        var type = 'restaurant';

        switch (restaurant_details[0]) {
            case 'SG':
                country = 'singapore';
                break;
            case 'HK':
                country = 'hong-kong';
                break;
            case 'TH':
                country = 'thailand';
                break;
            case 'MY':
                country = 'malaysia';
                break;
            default:
                country = 'singapore';
                break;
        }


        switch (restaurant_details[1]) {
            case 'SG':
                city = 'singapore';
                break;
            case 'HK':
                city = 'hong-kong';
                break;
            case 'BK':
                city = 'bangkok';
                break;
            case 'PK':
                city = 'phuket';
                break;
            case 'KL':
                city = 'kuala-lumpur';
                break;
            default:
                city = 'singapore';
                break;
        }

        restaurant_name = restaurant.substring(8);
        restaurant_name = restaurant_name.replace('_', '');
        restaurant_name = restaurant_name.replace(/([A-Z])/g, "-$1");
        if (restaurant_name.substring(0, 1).match(/[a-z]/i)) {
            restaurant_name = restaurant_name.substring(1);
        }
        if (restaurant_name.substring(0, 1).match(/-/i)) {
            restaurant_name = restaurant_name.substring(1);
        }
        restaurant_name = restaurant_name.toLowerCase();

        if (country === city) {
            restaurant_url = type + '/' + country + '/' + restaurant_name;
        } else {
            restaurant_url = type + '/' + country + '/' + city + '/' + restaurant_name;
        }

        return restaurant_url;
    }



}]);

SearchController.filter('explode_cuisine', function() {
    'use strict';
    return function(input, separator, items) {
        if (input === undefined) {
            return false;
        }
        var arr = input.split(separator),
            cuisine = '',
            i;
        for (i = 0; i < arr.length; i = i + 1) {
            if (i === items) {
                cuisine += '...';
                return cuisine;
            }
            if (cuisine === '') {
                cuisine += arr[i];
            } else {
                cuisine += ', ' + arr[i];
            }
        }
        return cuisine;
    };
});

SearchController.filter('get_review_class', function() {
    'use strict';
    return function(input) {
        if (input === undefined) {
            return false;
        }

        var res = (Math.round(input * 2) * 0.5) * 10;

        return 'review-' + res;
    };
});

function go(resto, title) {
    window.open('../../modules/booking/book_form.php?bkrestaurant=' + resto + '&bktitle=' + title);
}
/*
 weeloyApp.directive('tbtitle', function () {
 'use strict';
 return {
 restrict: 'AE',
 replace: true,
 template: '<a href="javascript:;" id="{{y.a}}" ng-click="reorder(y.a);"><span id="{{y.a}}_span">{{ y.b }}<i class="fa fa-caret-down"></span></a>',
 link: function (scope, elem, attrs) {
 }
 };
 });
 
 weeloyApp.config(['$routeProvider', '$locationProvider',
 function($routeProvider, $locationProvider) {
 $routeProvider
 .when('/vignette_restaurant.ang', {
 templateUrl: 'partials/registration.html',
 controller: 'registrationController',
 })
 .when('/moderation', {
 templateUrl: 'partials/moderation.html',
 controller: 'moderationController',
 })
 .otherwise({
 redirectTo: '/registration'
 });
 
 //$locationProvider.html5Mode({enabled: true,requireBase: false});
 }]);*/
