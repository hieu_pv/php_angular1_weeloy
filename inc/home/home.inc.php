<?php

require_once 'lib/class.event.inc.php';
require_once("lib/class.images.inc.php");


$res = new WY_restaurant;
$mediadata = new WY_Media();
$imgdata = new WY_Images();
$review = new WY_Review();
//input
//$restaurant = filter_input(INPUT_GET, 'restaurantname', FILTER_SANITIZE_SPECIAL_CHARS);
//$country = filter_input(INPUT_GET, 'country', FILTER_SANITIZE_STRING);
//$city = filter_input(INPUT_GET, 'city', FILTER_SANITIZE_STRING);
//$restaurant_name = filter_input(INPUT_GET, 'restaurantname', FILTER_SANITIZE_STRING);
$path_adaptor = '';
//if($country == $city){
//    $path_adaptor = '../../';    
//}


$browser = new Browser;
$included_data['is_mobile'] = $browser->isMobile();
if(!empty($_SESSION['user']['forced_city'])){$current_city = $_SESSION['user']['forced_city'];}else{if(!empty($_SESSION['user']['search_city'])){$current_city = $_SESSION['user']['search_city'];;}else{$current_city = 'Singapore';}}
$included_data['active_city'] = $current_city;
// GET showcase 
$title = 'Best Restaurants in '.ucwords($current_city);
$sub_title = 'Reserve a table and win incredible rewards';

/*
 * //$res_showcase = $res->getGourmandRestaurants2($_SESSION['user']['search_city'], 6);

$res_showcase = $res->getBookWheelRestaurant($_SESSION['user']['search_city'], 6);
//var_dump($res_showcase);die;

foreach ($res_showcase as $resto){
        $tmp = $resto;
        //$tmp['default_picture'] = $mediadata->getDefaultPicture($resto['restaurant']);
        $tmp['wheelvalue'] = $mediadata->getWheelSource($resto['wheelvalue']);
       
        $showcase[] = $tmp;
}

 */
$res_showcase = $res->getBookWheelRestaurant($current_city, 6);
foreach ($res_showcase as $resto){
        $tmp = $res->getRestaurant($resto['restaurant']);
        
        
        
        $tmp['best_offer'] = $res->getBestOffer($resto['restaurant'], $res->wheel, $res->is_wheelable, 1);
        $tmp['default_picture'] = $mediadata->getRandomPicture($resto['restaurant']);
        $tmp['wheelvalue'] = $imgdata->getWheelSource($res->wheelvalue);
        $tmp['internal_path'] = $res->getRestaurantInternalPath();
        $tmp['pricerating'] = $res->pricerating;
        $review = new WY_Review($resto['restaurant']);
        $tmp['review'] = $review->getReviewsCount();
        
        $booktitle = "BOOK SOON";
        $custombutton = "custom_button_book_soon";
        if (($res->status == 'active' && $res->is_bookable) || $status == 'demo_reference') {
            $booktitle = "BOOK NOW";
            $custombutton = "";
        }
        if ($res->is_bookable == false && $res->status == 'active') {
            $booktitle = "REQUEST NOW";
            $custombutton = "btn-green";
        }
        $tmp['book_button'] =  $res->getBookButton($resto['restaurant'], $res->title, $booktitle, "", $custombutton);

        $showcase[] = $tmp;
}

$included_data['best_wheels']['title'] = $title;
$included_data['best_wheels']['sub_title'] = $sub_title;
$included_data['best_wheels']['restaurants'] = $showcase;
$included_data['current_city'] = $current_city;


$included_data['userbase64'] = base64_encode($_SESSION['user']['email']);