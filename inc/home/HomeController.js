var HomeController = angular.module('HomeController', ['NfSearch']);
HomeController.controller('HomeCtrl', ['$rootScope', '$scope', '$http', function($rootScope, $scope, $http) {
    $scope.SelectCity = function(city) {
        $rootScope.$broadcast('ChangeCity', {
            city: city
        });
        $('#search-input-group input[name="restitle"]').val('');
        console.log(city);
    };
    $scope.AddTtCursorClass = function(evt) {
        $(evt.target).addClass('tt-cursor');
    }
    $scope.RemoveTtCursorClass = function(evt) {
        $(evt.target).removeClass('tt-cursor');
    }
    $scope.Suggestion_click = function(suggestion) {
        $('#restitle').val(suggestion.title);
        $rootScope.showAutoCorrect = false;
    };
}]);
