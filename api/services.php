<?php

require_once "conf/conf.init.inc.php";
require_once "lib/wpdo.inc.php";
require_once "lib/wglobals.inc.php";
require_once "lib/class.coding.inc.php";
require_once "lib/class.login.inc.php";
require_once "lib/class.booking.inc.php";
require_once "lib/class.review.inc.php";
require_once "lib/class.restaurant.inc.php";
require_once "lib/class.member.inc.php";
require_once "lib/class.allote.inc.php";
require_once "lib/class.menu.inc.php";
require_once "lib/class.event.inc.php";
require_once "lib/class.wheel.inc.php";
require_once "lib/class.service.inc.php";
require_once "lib/class.images.inc.php";
require_once "lib/class.media.inc.php";
require_once "lib/imagelib.inc.php";
require_once "lib/class.mail.inc.php";
require_once "lib/class.sms.inc.php";
require_once "lib/class.pushnotif.inc.php";
require_once "lib/class.debug.inc.php";
require_once "lib/class.qrcode.inc.php";
require_once "lib/class.cluster.inc.php";
require_once "lib/class.translation.inc.php";
// require_once("lib/class.tms.inc.php");

require_once "lib/Browser.inc.php";

require_once "lib/class.spool.inc.php";
require_once "lib/class.async.inc.php";

define("__MEDIA__SERVER_NAME__", 'https://media.weeloy.com/upload/restaurant/');

require 'Slim/Slim.php';

\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim();

//$app->response()->header('Content-Type', 'application/json;charset=utf-8');
$app->contentType('application/json;charset=utf-8');

$app->post('/rmloginc/module/login/', function () use ($app) {
    $data = json_decode($app->request()->getBody(), true);
    rmloginc($data['email'], $data['passw'], 'translation');
});

$app->post('/rmloginc/module/logout/', function () use ($app) {
    $data = json_decode($app->request()->getBody(), true);
    rmlogoutc($data['email'], $data['token'], 'translation');
});

$app->post('/rmloginc/module/checkstatus/', function () use ($app) {
    $data = json_decode($app->request()->getBody(), true);
    rmcheckstatusc($data['email'], $data['token'], 'translation');
});

$app->post('/rmloginc/module/change/', function () use ($app) {
    $data = json_decode($app->request()->getBody(), true);
    rmchangepassc($data['email'], $data['password'], $data['npassword'], $data['token'], 'translation');
});

$app->post('/rmloginc/module/forgot/', function () use ($app) {
    $data = json_decode($app->request()->getBody(), true);
    rmforgotc($data['email'], 'translation');
});

$app->post('/rmloginc/module/register/', function () use ($app) {
    $data = json_decode($app->request()->getBody(), true);
    rmregisterc($data['first'], $data['last'], $data['mobile'], $data['email'], $data['password']);
});

$app->post('/rmloginc/module/loginfacebook/', function () use ($app) {
    $data = json_decode($app->request()->getBody(), true);
    rmloginfacebookc($data['email'], $data['facebookid'], $data['facebooktoken'], 'translation');
});

$app->post('/translation/gettranssection/', function () use ($app) {
    $data = json_decode($app->request()->getBody(), true);
    gettradSection($data['email'], $data['token'], 'translation');
});

$app->get('/translation/gettrans/:lang/:topic', 'gettrans');

$app->post('/translation/gettransreadcontent/', function () use ($app) {
    $data = json_decode($app->request()->getBody(), true);
    getttradreadContent($data['email'], $data['topic'], $data['langue'], $data['token'], 'translation');
});

$app->post('/translation/gettranswritecontent/', function () use ($app) {
    $data = json_decode($app->request()->getBody(), true);
    gettradwriteContent($data['email'], $data['content'], $data['langue'], $data['token'], 'translation');
});

$app->post('/translation/gettransnewelement/', function () use ($app) {
    $data = json_decode($app->request()->getBody(), true);
    gettradnewelement($data['email'], $data['element'], $data['content'], $data['zone'], $data['token'], 'translation');
});

$app->post('/tms/gettmssection/', function () use ($app) {
    $data = json_decode($app->request()->getBody(), true);
    gettmsSection($data['email'], $data['token'], 'translation');
});

$app->post('/tms/savetmslayout/', function () use ($app) {
    $data = json_decode($app->request()->getBody(), true);
    savetmslayout($data['email'], $data['token'], $data['obj'], 'translation');
});

$app->run();

function savetmslayout($email, $token, $objstr, $platform)
{
    $data = null;
    $status = $zdata = 0;
    $mode = 'api';
    $msg = "";
    $login_type = 'backoffice';
    if ($platform == 'translation') {
        $login_type = 'translation';
        $mode = 'api-ajax';
    }

    $log = new WY_Login(set_valid_login_type($login_type));
    $log->check_login_remote($email, $token, $mode);
    $msg = $log->msg;
    if ($log->result > 0) {
        $tms = new WY_Tms;
        $tms->saveLayout($objstr);
    }
    echo format_api($status = 1, $data = "ok", $zdata = 1, $msg);
}

function gettmsSection($email, $token, $platform)
{

    $data = null;
    $status = $zdata = 0;
    $mode = 'api';
    $msg = "";
    $login_type = 'backoffice';
    if ($platform == 'translation') {
        $login_type = 'translation';
        $mode = 'api-ajax';
    }

    $log = new WY_Login(set_valid_login_type($login_type));
    $log->check_login_remote($email, $token, $mode);
    $msg = $log->msg;
    if ($log->result > 0) {
        $tms = new WY_Tms;
        $profile = $tms->readprofile($email);
        $msg = $tms->msg;
        if ($tms->result > 0) {
            $data = array('restaurant' => $profile["restaurant"], 'section' => $profile["menu"], 'name' => $profile["name"], 'object' => $profile["object"]);
            $status = 1;
            $zdata = count($profile);
        }
    }
    echo format_api($status, $data, $zdata, $msg);
}

function gettradSection($email, $token, $platform)
{

    $data = null;
    $status = $sizedata = 0;
    $mode = 'api';
    $msg = "";
    $login_type = 'backoffice';
    if ($platform == 'translation') {
        $login_type = 'translation';
        $mode = 'api-ajax';
    }

    $log = new WY_Login(set_valid_login_type($login_type));
    $log->check_login_remote($email, $token, $mode);
    $msg = $log->msg;
    if ($log->result > 0) {
        $trad = new WY_Translation;
        list($langue, $title) = $trad->readprofile($email);
        $msg = $trad->msg;
        if ($trad->result > 0) {
            $data = array('section' => $title, 'langue' => $langue);
            $status = 1;
            $sizedata = count($title);
        }
    }
    echo format_api($status, $data, $sizedata, $msg);
}

function gettrans($langue, $topic)
{

    $data = null;
    $status = $sizedata = 0;
    $msg = "";
    $trad = new WY_Translation;
    $trad->readcontent($topic, $langue);
    $msg = $trad->msg;
    if ($trad->result > 0) {
        $content = $trad->content;
        $data = array('row' => $content);
        $status = 1;
        $sizedata = count($content);
    }
    echo format_api($status, $data, $sizedata, $msg);
}

function getttradreadContent($email, $topic, $langue, $token, $platform)
{

    $data = null;
    $status = $sizedata = 0;
    $mode = 'api';
    $msg = "";
    $login_type = 'backoffice';
    if ($platform == 'translation') {
        $login_type = 'translation';
        $mode = 'api-ajax';
    }

    $log = new WY_Login(set_valid_login_type($login_type));
    $log->check_login_remote($email, $token, $mode);
    $msg = $log->msg;
    if ($log->result > 0) {
        $trad = new WY_Translation;
        $trad->readcontent($topic, $langue);
        $msg = $trad->msg;
        if ($trad->result > 0) {
            $content = $trad->content;
            $data = array('row' => $content);
            $status = 1;
            $sizedata = count($content);
        }
    }
    echo format_api($status, $data, $sizedata, $msg);
}

function gettradwriteContent($email, $content, $langue, $token, $platform)
{

    $data = null;
    $status = $sizedata = 0;
    $mode = 'api';
    $msg = "";
    $login_type = 'backoffice';
    if ($platform == 'translation') {
        $login_type = 'translation';
        $mode = 'api-ajax';
    }

    $log = new WY_Login(set_valid_login_type($login_type));
    $log->check_login_remote($email, $token, $mode);
    $msg = $log->msg;
    if ($log->result > 0) {
        $trad = new WY_Translation;
        $trad->writecontent($content, $langue);
        $msg = $trad->msg;
        if ($trad->result > 0) {
            $data = "ok";
            $status = $sizedata = 1;
        }
    }
    echo format_api($status, $data, $sizedata, $msg);
}

function gettradnewelement($email, $element, $content, $zone, $token, $platform)
{

    $data = null;
    $status = $sizedata = 0;
    $mode = 'api';
    $msg = "";
    $login_type = 'backoffice';
    if ($platform == 'translation') {
        $login_type = 'translation';
        $mode = 'api-ajax';
    }

    $log = new WY_Login(set_valid_login_type($login_type));
    $log->check_login_remote($email, $token, $mode);
    $msg = $log->msg;
    if ($log->result > 0) {
        $trad = new WY_Translation;
        $trad->newelement($element, $content, $zone);
        $msg = $trad->msg;
        if ($trad->result > 0) {
            $data = "ok";
            $status = $sizedata = 1;
        }
    }
    echo format_api($status, $data, $sizedata, $msg);
}

function rmloginc($email, $pass, $platform)
{
//return state
    // 0  not login
    // 1 login
    // 2 login but need to update api
    $mode = 'api';
    $logintype = 'backoffice';
    if ($platform == 'translation') {
        $logintype = 'translation';
        $mode = 'api-ajax';
    }
    $log = new WY_Login(set_valid_login_type($logintype));

    $log->process_login_remote($email, $pass, $mode);

    $user_info = $log->signIn;

    $res = array(
        'result' => $log->msg,
        'token' => $log->token,
        'firstname' => $user_info['firstname'],
        'lastname' => $user_info['name'],
        'gender' => $user_info['gender'],
        'cookie' => $log->cookie,
        'duration' => $log->duration);

    echo ($log->result > 0) ? format_api(1, $res, 1, $log->msg) : format_api(0, null, 0, $log->msg);
}

function rmloginfacebookc($email, $facebookid, $facebooktoken, $platform)
{

    $mode = 'api';
    $login_type = 'backoffice';
    if ($platform == 'translation') {
        $login_type = 'translation';
        $mode = 'api-ajax';
    }

    $log = new WY_Login(set_valid_login_type($login_type));
    $res_login = $log->process_remote_facebook($email, $facebookid, $facebooktoken, $mode);

    $user_info = $log->signIn;


    $res = array(
        'result' => $log->msg,
        'token' => $log->token,
        'firstname' => $user_info['firstname'],
        'lastname' => $user_info['name'],
        'gender' => $user_info['gender'],
        'duration' => $log->duration,
        'cookie' => $log->cookie);
    $_SESSION['email'] = [
        "email" => $email,
        "data" => $res,
    ];
    echo ($log->result > 0) ? format_api(1, $res, 1, $log->msg) : format_api(0, null, 0, $log->msg);
}

function rmlogoutc($email, $token, $platform)
{

    $mode = ($platform != 'translation') ? "api" : "api-ajax";
    $log = new WY_Login(set_valid_login_type($platform));
    $log->check_login_remote($email, $token, $mode);
    if ($log->result > 0) {
        $log->logout_remote($email, $mode);
    }

    echo ($log->result > 0) ? format_api(1, "1", "1", $log->msg) : format_api(0, "0", "1", $log->msg);
}

function rmforgotc($email, $platform)
{
    $log = new WY_Login(set_valid_login_type($platform));
    $log->forgotPass($email);
    echo ($log->result > 0) ? format_api(1, "1", "1", $log->msg) : format_api(1, "0", "1", $log->msg);
}

function rmchangepassc($email, $password, $npassword, $token, $platform)
{

    $mode = ($platform != 'translation') ? "api" : "api-ajax";
    $log = new WY_Login(set_valid_login_type($platform));
    $log->check_login_remote($email, $token, $mode);
    if ($log->result < 0) {
        echo format_api(1, "0", "1", "");
        return;
    }

    $arg = new WY_Arg;
    $arg->email = $email;
    $arg->password = $password;
    $arg->npassword = $arg->rpassword = $npassword;
    $log->UpdatePassword($arg);
    echo ($log->result > 0) ? format_api(1, "1", "1", $log->msg) : format_api(1, "0", "1", $log->msg);
}

function rmcheckstatusc($email, $token, $platform)
{

    $mode = ($platform != 'translation') ? "api" : "api-ajax";
    $log = new WY_Login(set_valid_login_type($platform));
    $log->check_login_remote($email, $token, $mode);
    echo ($log->result > 0) ? format_api(1, "1", "1", $log->msg) : format_api(1, "0", "1", $log->msg);
}

function api_error($content, $funcname)
{

    $debug = new WY_debug;
    $debug->writeDebug("ERROR-API", strtoupper($funcname), $content);
    echo '{"error":{"text":' . $content . '}}';
}

function api_authentificate()
{

    $response = array();

    if (!isset($_SERVER['PHP_AUTH_USER']) || !isset($_SERVER['PHP_AUTH_PW'])) {
        $response["error"] = true;
        $response["message"] = "Credentials are misssing";
        set_header_status(401);
        echo format_api(1, '', 0, $response);
        exit; //$app->stop();
    }

    $email = $_SERVER['PHP_AUTH_USER'];
    $pass = $_SERVER['PHP_AUTH_PW']; //pass ou token

    $log = new WY_Login(set_valid_login_type('backoffice'));
    if ($log->check_login_remote($email, $pass, 'api') != '') {
        return true;
    } else if ($log->process_login_remote($email, $pass, 'api') != '') {
        return true;
    } else {
        $response["error"] = true;
        $response["message"] = "wrong login and password";
        set_header_status(401);
        echo format_api(1, '', 0, $response);
        exit;
    }

    return false;
}

/* add platform */

function format_api($status, $data, $count, $errors, $header_status = 200)
{

    set_header_status($header_status);

    $resultat['status'] = $status;
    $resultat['data'] = $data;
    $resultat['count'] = $count;
    $resultat['errors'] = $errors;
    return json_encode($resultat);
}

//// later on  ////

function authenticate(\Slim\Route $route)
{
    // Getting request headers
    $headers = apache_request_headers();
    $response = array();
    $app = Slim::getInstance();

    // Verifying Authorization Header
    if (isset($headers['Authorization'])) {
        $db = new DbHandler();

        // get the api key
        $api_key = $headers['Authorization'];
        // validating api key
        if (!$db->isValidApiKey($api_key)) {
            // api key is not present in users table
            $response["error"] = true;
            $response["message"] = "Access Denied. Invalid Api key";
            echoRespnse(401, $response);
            $app->stop();
        } else {
            global $user_id;
            // get user primary key id
            $user_id = $db->getUserId($api_key);
        }
    } else {
        // api key is missing in header
        $response["error"] = true;
        $response["message"] = "Api key is misssing";
        echoRespnse(400, $response);
        $app->stop();
    }

}

function is_email_valid($email)
{
    return !(filter_var($email, FILTER_VALIDATE_EMAIL) === false);
}

function set_header_status($code)
{
    $status = array(
        100 => 'Continue',
        101 => 'Switching Protocols',
        200 => 'OK',
        201 => 'Created',
        202 => 'Accepted',
        203 => 'Non-Authoritative Information',
        204 => 'No Content',
        205 => 'Reset Content',
        206 => 'Partial Content',
        300 => 'Multiple Choices',
        301 => 'Moved Permanently',
        302 => 'Found',
        303 => 'See Other',
        304 => 'Not Modified',
        305 => 'Use Proxy',
        306 => '(Unused)',
        307 => 'Temporary Redirect',
        400 => 'Bad Request',
        401 => 'Unauthorized',
        402 => 'Payment Required',
        403 => 'Forbidden',
        404 => 'Not Found',
        405 => 'Method Not Allowed',
        406 => 'Not Acceptable',
        407 => 'Proxy Authentication Required',
        408 => 'Request Timeout',
        409 => 'Conflict',
        410 => 'Gone',
        411 => 'Length Required',
        412 => 'Precondition Failed',
        413 => 'Request Entity Too Large',
        414 => 'Request-URI Too Long',
        415 => 'Unsupported Media Type',
        416 => 'Requested Range Not Satisfiable',
        417 => 'Expectation Failed',
        500 => 'Internal Server Error',
        501 => 'Not Implemented',
        502 => 'Bad Gateway',
        503 => 'Service Unavailable',
        504 => 'Gateway Timeout',
        505 => 'HTTP Version Not Supported');

    header("HTTP/1.1 $code " . $status[$code]);
    return true;
}
