<?php

require_once("conf/conf.init.inc.php");
require_once("lib/wpdo.inc.php");
require_once("lib/wglobals.inc.php");

require_once("conf/conf.session.inc.php");

require_once("lib/class.coding.inc.php");
require_once("lib/class.login.inc.php");
require_once("lib/class.booking.inc.php");
require_once("lib/class.review.inc.php");
require_once("lib/class.restaurant.inc.php");
require_once("lib/class.member.inc.php");
require_once("lib/class.allote.inc.php");
require_once("lib/class.menu.inc.php");
require_once("lib/class.event.inc.php");
require_once("lib/class.wheel.inc.php");
require_once("lib/class.service.inc.php");
require_once("lib/class.images.inc.php");
require_once("lib/class.media.inc.php");
require_once("lib/imagelib.inc.php");
require_once("lib/class.mail.inc.php");
require_once("lib/class.sms.inc.php");
require_once("lib/class.pushnotif.inc.php");
require_once("lib/class.debug.inc.php");
require_once("lib/class.qrcode.inc.php");
require_once("lib/class.cluster.inc.php");
require_once("lib/class.translation.inc.php");
require_once("lib/class.report.inc.php");
require_once("lib/class.session.inc.php");


require_once("lib/Browser.inc.php");

require_once("lib/class.spool.inc.php");
require_once("lib/class.async.inc.php");

define("__MEDIA__SERVER_NAME__", 'https://media.weeloy.com/upload/restaurant/');


require 'Slim/Slim.php';

\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim();



//$app->response()->header('Content-Type', 'application/json;charset=utf-8');
$app->contentType('application/json;charset=utf-8');

$app->get('/restaurant', 'getAllRestaurants');
$app->post('/restaurant', 'getAllRestaurants');

$app->get('/logo', 'getlogo');
$app->post('/logo', 'getlogo');

$app->get('/logourl/:restaurant', 'getlogourl');

$app->get('/restaurant/:query', 'getRestaurant');
$app->post('/restaurant/:query', 'getRestaurant');


$app->get('/wheel/:query', 'getWheel');
$app->post('/wheel/:query', 'getWheel');


$app->get('/wheeldescription/:query', 'getWheelDescription');

$app->post('/booking/email/', function () use ($app) {
  $data = json_decode($app->request()->getBody(),true);
  getBookingEmail($data['restaurant'], $data['email'], $data['token']);
});

$app->post('/booking/mobile/', function () use ($app) {
  $data = json_decode($app->request()->getBody(),true);
  getBookingPhone($data['restaurant'], $data['mobile'], $data['token']);
});

$app->get('/visit/:query', 'getVisit');
$app->post('/visit/:query', 'getVisit');

$app->get('/visit1/:conf', 'getVisit1');
$app->post('/visit1/:conf', 'getVisit1');

$app->get('/confirmation/:conf/:email', 'getConfirmation');
$app->post('/confirmation/:conf/:email', 'getConfirmation');

$app->post('/booking/cancelbackoffice/', function () use ($app) {
  $data = json_decode($app->request()->getBody(),true);
  canceltheBooking($data['restaurant'], $data['booking'], $data['email'], $data['token']);
});

$app->post('/booking/modifbackoffice/', function () use ($app) {
  $data = json_decode($app->request()->getBody(),true);
  modiftheBooking($data['restaurant'], $data['booking'], $data['email'], $data['cover'], $data['time'], $data['date'], $data['token']);
});

$app->get('/visit/cancel/:restaurant/:confirmation/:email', 'cancelVisit');
$app->post('/visit/cancel/:restaurant/:confirmation/:email', 'cancelVisit');

$app->post('/booking/state/', function () use ($app) {
    return setBookingState($app);
});

$app->post('/bookingservice/state/', function () use ($app) {
  $data = json_decode($app->request()->getBody(),true);
  setBookingService($data['restaurant'], $data['booking'], $data['state'], $data['token']);
});


$app->get('/visitMember/:query', 'getVisitMember');

//$app->post('/visitMember/:query', 'getVisitMember');


$app->post('/rmloginc/module/loginfacebook/', function () use ($app) {
    $data = json_decode($app->request()->getBody(), true);
    rmloginfacebookc($data['email'], $data['facebookid'], $data['facebooktoken'], 'web', '1.0');
});

//
$app->get('/restaurant/openhours/:query', 'getRestaurantOpenHour');


$app->get('/requestspin/:conf/:rcode/:mcode', 'requestSpin');
$app->post('/requestspin/:conf/:rcode/:mcode', 'requestSpin');

$app->get('/savespin/:win/:token', 'storeSpin');
$app->post('/savespin/:win/:token', 'storeSpin');

$app->get('/spinresult/:confirmation', 'getSpinResult');

$app->get('/savespin/:segment/:win/:desc/:source/:token', 'storeSpinComplete');
$app->get('/savespin/:segment/:win//:source/:token', 'storeSpinComplete_HOTFIX');

$app->get('/savespin/:wheelversion/:segment/:angle/:win/:desc/:source/:token', 'storeSpinCompleteAngle');

$app->get('/addmember/:email/:mobile/:password/:lastname/:firstname/:platform', 'addMember');
//$app->post('/addmember/:email/:mobile/:password', 'postaddMember');

$app->get('/rmlogin/:email/:pass', 'rmlogin');
$app->post('/rmlogin/:email/:pass', 'rmlogin');

$app->get('/rmlogin/:email/:pass/:name/:version', 'rmlogin');
$app->post('/rmlogin/:email/:pass/:name/:version', 'rmlogin');

$app->get('/rmloginc/:email/:pass/:name/:version', 'rmloginc');

$app->post('/rmloginc_facebook/:email/:facebookid/:facebooktoken/:name/:version', 'rmloginc_facebook');


$app->post('/rmloginc_facebook' ,function () use ($app) {
    return rmloginc_facebook($app);
});

$app->get('/facebookuser/:email/:facebookid/:facebooktoken', 'GetFacebookUser');
$app->post('/facebookuser/:email/:facebookid/:facebooktoken', 'GetFacebookUser');

$app->get('/rmlogout/:email', 'rmlogout');
$app->post('/rmlogout/:email', 'rmlogout');

$app->get('/rmreadlogin/:email/:token', 'rmreadlogin');
$app->post('/rmreadlogin/:email/:token', 'rmreadlogin');

$app->get('/wheelvalue/alert' ,function () {
    $data = array('message'=> 'The Higher the Score of the wheel, The Better the Rewards at the restaurant.');
     echo format_api(1, $data, 1, NULL);
});

$app->get('/updateProfile/:label/:value', 'api_authentificate' ,function ($label, $value) use ($app) {
    return updateProfile($label, $value);
});
        
                
$app->post('/updateProfile/:email/:label/:value', 'updateProfile');

$app->get('/updatePassword/:email/:oldpwd/:newpwd', 'updatePassword');
$app->post('/updatePassword/:email/:oldpwd/:newpwd', 'updatePassword');

$app->get('/forgotPassword/:email', 'forgotPassword');

$app->get('/restaurantfullinfo/:query', 'getRestaurantInfo');

$app->get('/restaurant/map/:query', 'getMap');


$app->get('/timestamp', 'getUpdateDate');

$app->get('/cuisinelist', 'getCuisineList');

$app->get('/restaurant/events/:query', 'getEventsRestaurant');
$app->post('/restaurant/events/:query', 'getEventsRestaurant');
$app->post('/setevent', function () use ($app) {
    return setEvent($app);
});

$app->get('/restaurant/events/active/:query', 'getActiveEventsRestaurant');

$app->post('/deleteevent', function () use ($app) {
    return deleteEvent($app);
});

$app->post('/updateevent', function () use ($app) {
    return updateEvent($app);
});


/////// MENU OLD

$app->get('/menulist/:query', 'getMenu');
$app->post('/menulist/:query', 'getMenu');


/////// MENU NEW
$app->post('/menu/create', function () use ($app) {
    return createMenu($app);
});

$app->post('/menu/update', function () use ($app) {
    return updateMenuTMP($app);
});

$app->post('/menu/delete', function () use ($app) {
    return deleteMenuTMP($app);
});


///////   PROMOTION   ///////

$app->get('/promotion/list/:query', function ($query){
    include "promotion.api.php";
    return getlist($query);
});
$app->post('/promotion/setdefault', function () use ($app) {
    include "promotion.api.php";
    return setdefault($app);
});
$app->post('/promotion/delete', function () use ($app) {
    include "promotion.api.php";
    return delete($app);
});
$app->post('/promotion/create', function () use ($app) {
    include "promotion.api.php";
    return create($app);
});


//// RESTAURANT SERVICES
$app->get('/restaurant/service/categories', 'getServiceCategories');
$app->get('/restaurant/service/services', 'getServices');
$app->get('/restaurant/service/restaurantservices/:restaurant', 'getRestaurantServices');
$app->post('/restaurant/service/save', function () use ($app) {
    return saveRestaurantServices($app);
});


//// TABLE MANAGEMENT

$app->get('/booking/savetable/:confimration/:table', 'saveBookingTable');




// WALKABLE
$app->post('/booking/walkable/create', function () use ($app) {
    require_once("lib/class.mail.inc.php");
            require_once("lib/class.sms.inc.php");
            require_once("lib/class.pushnotif.inc.php");
            require_once("lib/class.spool.inc.php");
            require_once("lib/class.async.inc.php");
            
    return addWalkableBooking($app);
});


// USER PICTURE
$app->get('/user/profilepicture', 'api_authentificate', function () {
    return getUserProfilePicture();
});

$app->post('/user/profilepicture/upload', 'api_authentificate', function () use ($app) {
    return addUserProfilePicture($app);
});

//user favorite

$app->get('/user/:restaurant/favorite/:is_favorite', 'api_authentificate', function ($restaurant, $is_favorite) use ($app)  {
    return setFavorite($app, $restaurant, $is_favorite);
});


$app->get('/restaurant/media/picture/:query', 'getMediaPictRestaurant');
$app->post('/restaurant/media/picture/:query', 'getMediaPictRestaurant');

$app->post('/renamemedia', function () use ($app) {
    return renameMedia($app);
});

$app->post('/deletemedia', function () use ($app) {
    return deleteMedia($app);
});

$app->post('/updatemedia', function () use ($app) {
    return updateMedia($app);
});


$app->get('/restaurant/allote/:query', 'allotmentData');
$app->post('/restaurant/allote/:query', 'allotmentData');

$app->get('/getallote/:query', 'getAlloteReponse');
$app->post('/getallote/:query', 'getAlloteReponse');


$app->post('/setallote', function () use ($app) {
    return setAlloteResponse($app);
});

// check if it is a valid request, i.e, this is enough availability
$app->post('/restaurant/availslot/', function () use ($app) {
  $data = json_decode($app->request()->getBody(),true);
  allotment1slotData($data['restaurant'], $data['date'], $data['time'], $data['pers'], '', $data['token']);
});

// same as above with the difference that this is a modification of an existing booking => 
// you would need to 'virtually' release the booking availability first, especially if modification is for same day and 'almost' same time

$app->post('/restaurant/availslotbkg/', function () use ($app) {
  $data = json_decode($app->request()->getBody(),true);
  allotment1slotData($data['restaurant'], $data['date'], $data['time'], $data['pers'], $data['booking'], $data['token']);
});


$app->get('/getreviews/:query', 'getReviews');
$app->get('/getreviews/:query/:page', 'getReviews');

$app->get('/getreviews/:query/list/summary', 'getReviewsList');
$app->get('/getreviews/:query/list/:page', 'getReviewsList');


$app->post('/reviewresponse', function () use ($app) {
    return setReviewResponse($app);
});

$app->post('/review', function () use ($app) {
    return postReview($app);
});

$app->get('/getReviewList', 'api_authentificate' ,'getUserPostedReviews');

$app->post('/addPNDevice', function () use ($app) {
    return addPNDevice($app);
});

$app->post('/deletePNDevice', function () use ($app) {
    return deletePNDevice($app);
});

$app->get('/restaurant/manager/:query', 'getRestaurantByManager');
$app->post('/restaurant/manager/:query', 'getRestaurantByManager');


$app->get('/restaurant/location/', 'getCityList');
$app->post('/restaurant/location/', 'getCityList');

$app->post('/send-contact-email', function () use ($app) {
  try {
    $request = $app->request->post();
    
    $email = $request['email'];
    $message = $request['message'];
    $firstname = $request['firstname'];
    $lastname = $request['lastname'];

    $member = new WY_Member();
    
    $result = $member->sendContactEmail($email, $message, $firstname, $lastname);
    if($result){
        echo format_api(1, 'OK', 1, NULL);
    }
  } catch (PDOException $e) {
    api_error($e, "showCaseRestaurants"); 
    var_dump($e);
  }
});



$app->get('/search/restaurant', function () use ($app) {
    return find($app);
});

$app->get('/search/fulltext', function () use ($app) {
	$request = $app->request->get();
        
	if (isset($request['query']) && $request['query'] != '') {
		$query = str_replace("*", "", $request['query']);
		$explode_array = explode(" ", $query);
		$soundex = [soundex($query)];
		foreach ($explode_array as $word) {
			$soundex[] = soundex($word);
		}
		$soundex = implode(" ", $soundex);
		$sql = "SELECT id, title, cuisine, ROUND(MATCH(title, cuisine, city, soundex_index) AGAINST('{$request['query']}' IN BOOLEAN MODE), 2) as score FROM restaurant WHERE city = '{$request['city']}' AND (MATCH(title, cuisine) AGAINST('{$request['query']}' IN BOOLEAN MODE) OR MATCH(soundex_index) AGAINST('{$soundex}')) AND is_displayed = 1 AND (status = 'active' OR status =  'comingsoon') ORDER BY score DESC LIMIT 5 OFFSET 0";
	} else {
		if(isset($request['city']) && $request['city'] != '') {
			$sql = "SELECT id, title, city, cuisine FROM restaurant WHERE city = '{$request['city']}' AND is_displayed = 1 AND (status = 'active' OR status =  'comingsoon') LIMIT 200 OFFSET 0";
		} else {
			$sql = "SELECT id, title, city, cuisine FROM restaurant WHERE is_displayed = 1 AND (status = 'active' OR status =  'comingsoon') LIMIT 200 OFFSET 0";
		}
	}
	try {
		$db = getConnection();
		$stmt = $db->prepare($sql);
		$stmt->execute();
                
		$restaurants = $stmt->fetchAll(PDO::FETCH_OBJ);
                
		$db = null;
		$errors = null;
		$data = array("restaurants" => $restaurants);
		echo format_api(1, $data, count($restaurants), $errors);
                
	} catch (PDOException $e) {
		apiError($e, "getCuisineList");
	}
});



$app->get('/restaurant/search/:query', 'findByCity');
$app->post('/restaurant/search/:query', 'findByCity');

$app->get('/restaurant/food/:query', 'findByCuisine');
$app->post('/restaurant/food/:query', 'findByCuisine');

$app->get('/restaurant/picture/:query', 'getPictRestaurant');
$app->post('/restaurant/picture/:query', 'getPictRestaurant');

$app->get('/restaurant/dispoday/:restaurant/:adate', 'getdayDispo');

$app->get('/restaurant/bookingdayreport/:query/:type', 'getBookingDayReport');
$app->post('/restaurant/bookingdayreport/:days/:type', 'getBookingDayReport');

$app->get('/user/booking', 'api_authentificate', function () use ($app) {
    return getBookingsUser();
});

$app->get('/user/booking/period/:period', 'api_authentificate', function ($period) {
    return getBookingsUserPeriod($period);
});

$app->get('/user/booking/:date1/:date2', 'api_authentificate', function ($date1, $date2) use ($app) {
    return getBookingsUser($date1, $date2);
});
        

//$app->get('/user/booking/:query/:filter/:date1/:date2', 'getBookingsUserFilter');

$app->get('/user/booking/:query/:filter/:date1/:date2', function ($query,$filter,$date1,$date2) use ($app) {
    return getBookingsUserFilter($query,$filter,$date1,$date2);
});

$app->get('/user/booking/:query/:filter/:date1/:date2', 'api_authentificate', function ($query,$filter,$date1,$date2) use ($app) {
    return getBookingsUserFilter($query,$filter,$date1,$date2);
});

$app->get('/walkin/:query/:filter/:date1/:date2', 'api_authentificate', function ($query,$filter,$date1,$date2) use ($app) {
    return getWalkinsUserFilter($query,$filter,$date1,$date2);
});

$app->post('/user/addbooking/add', 'addBooking');
$app->get('/user/cancelbooking/cancel/:confirmation', 'api_authentificate', function ($confirmation)  {
    return cancelUserBooking($confirmation);
});

$app->get('/error', 'getErrorExample');

$app->get('/testencrypt', 'testEncryptExample');
$app->get('/testdecrypt', 'testDecryptExample');



//// REPORTS ////

$app->get('/report/reworld/widget', 'getReworldReport');

//// REPORTS ////




$app->get('/readmlogintest', 'api_authentificate');
//$app->post('/restaurant', 'addRestaurant');
//$app->put('/restaurant/:id', 'updateRestaurant');
//$app->delete('/restaurant/:id', 'deleteRestaurant');


////////DEV and test only
$app->post('/user/deregister', 'api_authentificate', function () use ($app) {
    return removeUser($app);
});

$app->get('/add_index', function () {
	try {
		$sql = "SELECT * FROM restaurant";
		$db = getConnection();
		$stmt = $db->prepare($sql);
		$stmt->execute();
		$restaurants = $stmt->fetchAll(PDO::FETCH_OBJ);
		foreach ($restaurants as $restaurant) {
			$words = array();
			$words[] = soundex($restaurant->title);
			$explode_str = explode(" ", $restaurant->title);
			foreach ($explode_str as $word) {
				$words[] = soundex($word);
			}
			$explode_str = explode("|", $restaurant->cuisine);
			foreach ($explode_str as $word) {
				$words[] = soundex($word);
			}
			$words = implode(" ", $words);
			$update_sql = "UPDATE restaurant set soundex_index='{$words}' WHERE ID = '{$restaurant->ID}'";
			$stmt = $db->prepare($update_sql);
			$stmt->execute();
			echo $update_sql . '</br>';
		}
	} catch (PDOException $e) {
		var_dump($e);
	}
});

//// FULL SEARCH  ////



$app->get('/restaurant/getShowcase/:current_city/:count', function ($current_city,$count) {
	try {
            $res = new WY_restaurant();
            
            $res_showcase = $res->getShowcaseRestaurant($current_city, $count);
            echo format_api(1, $res_showcase, 1, NULL);
	} catch (PDOException $e) {
                api_error($e, "showCaseRestaurants");	
                var_dump($e);
	}
});

$app->get('/newsletter/addEmail/:email', function ($email) {
	try {
            $member = new WY_Member();
            
            $result = $member->addEmailNewsletter($email);
            if($result > -1){
                echo format_api(1, 'OK', 1, NULL);
            }else{
                $error["error"] = true;
                $error["message"] = "wrong login and password";  
                echo format_api(1, NULL, 1, $error);
            }
	} catch (PDOException $e) {
                api_error($e, "showCaseRestaurants");	
                var_dump($e);
	}
});






$app->run();

function api_error($e, $funcname) {

	$debug = new WY_debug;
	$trace = preg_replace("/\'\"/", "`", $e->getTraceAsString());
	$traceAr = $e->getTrace();
	$file = preg_replace("/^.*\//", "", $traceAr[0]['file']);
	$msg = $e->getMessage() . " - " . $file . " - " . $traceAr[0]['line'];
	$debug->writeDebug("ERROR-API", "API-" . strtoupper($funcname), $msg . " \n " . $trace);
    echo format_api(0, 0, 0, $funcname . "->" . $msg);
}


function getUpdateDate() {
    $errors = null;
    $data = array("timestamp" => "2014-09-01-23:15");
    echo format_api(1, $data, 1, $errors);

    //echo "{\"timestamp\":\"2014-09-01-23:15\"}";
}

function getlogo() {

    $sql = "select restaurant, logo FROM restaurant ORDER BY restaurant";
    try {
        $db = getConnection();
        $stmt = $db->query($sql);
        $restaurant = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $db = null;
        foreach ($restaurant as $resto) {
            if (!empty($resto['logo'])) {
                $errors = null;
                $data = array("logo" => __MEDIA__SERVER_NAME__ . $resto['restaurant'] . "/" . $resto['logo']);
                echo format_api(1, $data, 1, $errors);
            }
        }
    } catch (PDOException $e) {
        api_error($e, "getlogo");
    }
}

function getlogourl($restaurant) {

	$restaurant = clean_text($restaurant);
    $sql = "select restaurant, logo FROM restaurant WHERE restaurant = '$restaurant' ORDER BY restaurant";
    try {
        $db = getConnection();
        $stmt = $db->query($sql);
        $data = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $db = null;
        foreach ($data as $resto) {
            if (!empty($resto['logo'])) {
                $errors = null;
                $data = array("logo" => __MEDIA__SERVER_NAME__ . $resto['restaurant'] . "/" . $resto['logo']);
                echo format_api(1, $data, 1, $errors);
            }
        }
    } catch (PDOException $e) {
        api_error($e, "getlogourl");
    }
}

function getAllRestaurants() {
    $sql = "select restaurant, title, city, cuisine, rating, logo, stars, GPS, openhours, images, wheelvalue, likes "
            . "FROM restaurant "
            . "WHERE restaurant.status IN ('active') AND is_displayed = 1 ORDER BY wheelvalue DESC, restaurant ASC LIMIT 50";
    try {
        $db = getConnection();
        $stmt = $db->query($sql);
        $restaurant = $stmt->fetchAll(PDO::FETCH_ASSOC);
        $db = null;
        foreach ($restaurant as $key => $resto) {
            $restaurant[$key]['media_domain'] = __MEDIA__SERVER_NAME__;
            if (!empty($resto['images'])) {
                $tmp = explode("|", $resto['images']);
                $restaurant[$key]['images'] = '';
                for ($i = 0; $i < count($tmp); $i++) {
                    if (strtolower(substr($tmp[$i], 0, 5)) != 'logo.' && strtolower(substr($tmp[$i], 0, 5)) != 'chef.' && strtolower(substr($tmp[$i], 0, 5)) != 'wheel') {
                        $restaurant[$key]['images'] = $tmp[$i];
                        break;
                    }
                }
            }
        }

        $errors = null;
        $data = array("restaurant" => $restaurant);

        echo format_api(1, $data, count($restaurant), $errors);
    } catch (PDOException $e) {
        api_error($e, "getAllRestaurants");
    }
}

function getRestaurantOpenHour($query) {
    $res = new WY_restaurant;
    $res->getRestaurant($query);
    $hours = $res->openhours;
    $hours = $res->getOpeningHoursListing($query, true);
    
    if($hours){
        set_header_status(200);	
        echo format_api(1, $hours, 1, NULL);
    }else{
        set_header_status(200);	
        $error= 'An error occured, please try again later';
        echo format_api(1, NULL, 1, $error);
    }
    
}


function getRestaurant($query) {
    $sql = "SELECT restaurant, title, address, zip, country, city, region, cuisine, rating, email, tel, stars, GPS, likes, images, pricing, creditcard, mealtype, stars, dinesfree, chef, openhours, description, wheelvalue "
            . "FROM restaurant "
            . "WHERE restaurant=:query AND is_displayed = 1 limit 1";
    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("query", $query);
        $stmt->execute();
        $restaurant = $stmt->fetchAll(PDO::FETCH_ASSOC);       
        
        foreach ($restaurant as $key => $resto) {
            //if(!empty($resto['logo'])){
            $restaurant[$key]['media_domain'] = __MEDIA__SERVER_NAME__;
            $logo = "SELECT name as logo FROM media WHERE restaurant=:query and object_type = 'Logo'";
            
            $stmt_logo = $db->prepare($logo);
        
            $stmt_logo->bindParam("query", $query);
            $stmt_logo->execute();
            
            $data_logo = $stmt_logo->fetch(PDO::FETCH_ASSOC);
            $restaurant[$key]['logo'] = $data_logo['logo'];
            //}
        }
        $db = null;
        $errors = null;
        $data = array("restaurant" => $restaurant);
        echo format_api(1, $data, count($restaurant), $errors);
    } catch (PDOException $e) {
        api_error($e, "getRestaurant");
    }
}

function setBookingService($restaurant, $confirmation, $state, $token) {

 	$login = new WY_Login(set_valid_login_type('backoffice'));
 	if($login->checktoken($token)) {
		$mobj = new WY_Booking();
        $mobj->setBooking($restaurant, $confirmation, $state, 'state');
                
		if($mobj->result > 0)
			echo format_api(1, '', 0, $mobj->msg);
		else echo format_api(-1, '', 0, $mobj->msg);
 		}
 	else echo format_api(-1, '', 0, 'wrong token');	
}

function setBookingState($app) {

    $restaurant = $app->request->post("restaurant");
    $confirmation = $app->request->post("confirmation");    
    $state = $app->request->post("state");
    $token = $app->request->post("token");

 	$login = new WY_Login(set_valid_login_type('backoffice'));
 	if($login->checktoken($token)) {
		$mobj = new WY_Booking();
        $mobj->setBooking($restaurant, $confirmation, $state, 'state');
                
		if($mobj->result > 0)
			echo format_api(1, '', 0, $mobj->msg);
		else echo format_api(-1, '', 0, $mobj->msg);
 		}
 	else echo format_api(-1, '', 0, 'wrong token');	 	
}

function getBookingEmail($restaurant, $email, $token) {

    $res = new WY_Booking();
    $data = $res->getBookingEmail($restaurant, $email);
    if(count($data) < 1) {
        echo format_api(0, "FAIL", 0, "Confirmation does not exist or wrong restaurant or email");
        return;
    	}

	$errors = null;
	echo format_api(1, $data, count($data), $errors);

}


function getBookingPhone($restaurant, $phone, $token) {

    $res = new WY_Booking();
    $data = $res->getBookingPhone($restaurant, $phone);
    if(count($data) < 1) {
        echo format_api(0, "FAIL", 0, "Confirmation does not exist or wrong restaurant or phone number");
        return;
    	}

	$errors = null;
	echo format_api(1, $data, count($data), $errors);
}


function getConfirmation($confirmation, $email) {

    $res = new WY_Booking();
    if ($res->getBooking($confirmation) == false || $res->email != $email) {
        echo format_api(0, "FAIL", 0, "Confirmation does not exist or wrong restaurant or email");
        return;
    }

    $data['resto'] = $res->restaurant;
    $data['title'] = $res->restaurantinfo->title;
    $data['logo'] = $res->restaurantinfo->logo;
    $data['booking'] = $res->confirmation;
    $data['bkstatus'] = $res->status;
    $data['salutation'] = $res->salutation;
    $data['first'] = $res->firstname;
    $data['date'] = $res->rdate;
    $data['time'] = $res->rtime;
    $data['pers'] = $res->cover;
    $data['state'] = $res->state;
    $data['phone'] = $res->mobile;
    $data['email'] = $res->email;
    $data['wheelwin'] = $res->wheelwin;
    $data['restCode'] = $res->restCode;
    $data['membCode'] = $res->membCode;
    $data['canceldate'] = $res->canceldate;
    $data['specialrequest'] = $res->specialrequest;
    
	$errors = null;

	//$data = array("visit" => $data);
	echo format_api(1, $data, count($data), $errors);
}


function modiftheBooking($restaurant, $booking, $email, $cover, $time, $date, $token) {

 	$login = new WY_Login(set_valid_login_type('backoffice'));
 	if($login->checktoken($token) == false) {
		echo format_api(-1, '', 0, 'wrong token');
		return;
		}
		 	
    $res = new WY_Booking();
    if ($res->modifBooking($booking, $email, $cover, $time, $date) < 0) {
        echo format_api(0, "FAIL", 0, $res->msg);
        return;
        }

    echo format_api(1, "OK", 0, "booking has been modified");
	}

function canceltheBooking($restaurant, $booking, $email, $token) {
    
 	$login = new WY_Login(set_valid_login_type('backoffice'));
 	if($login->checktoken($token))
		return cancelVisit($restaurant, $booking, $email);
 	else echo format_api(-1, '', 0, 'wrong token');	 	
	}

function cancelVisit($restaurant, $confirmation, $email) {

	$restaurant = clean_text($restaurant);
    $res = new WY_Booking();
    if ($res->getBooking($confirmation) == false) {
        echo format_api(0, "FAIL", 0, "Confirmation does not exist or wrong restaurant or email");
        return;
    }

    if ($res->restaurant != $restaurant || $res->email != $email) {
        echo format_api(0, "FAIL", 0, "Confirmation does exist for a different restaurant or email ");
        return;
    }

    if ($res->status == 'cancel') {
        echo format_api(0, "FAIL", 0, "Confirmation has already been canceled ");
        return;
    }


    if ($res->cancelBooking($confirmation) < 0) {
        echo format_api(0, "FAIL", 0, $res->msg);
        return;
    }

    echo format_api(1, "OK", 0, "Confirmation has been canceled");
}

function getVisit($query) {

	$year = Date('Y');
    $mapping = array("restaurant" => "resto", "email" => "email", "mobile" => "phone", "salutation" => "salutation", "lastname" => "last", "firstname" => "first", "rdate" => "date", "rtime" => "time", "cover" => "pers", "specialrequest" => "comment", "state" => "state", "confirmation" => "booking", "wheelwin" => "wheelwin", "restCode" => "restCode", "membCode" => "membCode", "status" => "bkstatus", "cdate" => "createdate", "tracking" => "tracking", "booker" => "booker", "canceldate" => "canceldate", "type" => "type", "company" => "company", "hotelguest" => "hotelguest");
    $sql = "SELECT restaurant, email, mobile, status, salutation, lastname, firstname, rdate, rtime, cover, state, specialrequest, confirmation, wheelwin, restCode, membCode, cdate, tracking, booker, canceldate, type, company, hotelguest FROM booking WHERE restaurant='$query' and year(rdate) >= '$year' order by rdate DESC, rtime DESC limit 800";
    try {
        $db = getConnection();
        $stmt = $db->query($sql);
        $restaurant = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;

        $data = json_encode($restaurant);
        while (list($label, $val) = each($mapping))
            $data = preg_replace("/$label/", "$val", $data);

        $data_dec = json_decode($data);
        $errors = null;
        $data = array("visit" => $data_dec);
        echo format_api(1, $data, count($data), $errors);
    } catch (PDOException $e) {
    	api_error($e, "getVisit");
    }
}

function getVisit1($query) {

    $mapping = array("restaurant" => "resto", "email" => "email", "mobile" => "phone", "salutation" => "salutation", "lastname" => "last", "firstname" => "first", "rdate" => "date", "rtime" => "time", "cover" => "pers", "specialrequest" => "comment", "confirmation" => "booking", "wheelwin" => "wheelwin", "restCode" => "restCode", "membCode" => "membCode", "status" => "bkstatus", "tracking" => "tracking", "booker" => "booker", "canceldate" => "canceldate", "type" => "type", "company" => "company", "hotelguest" => "hotelguest");
    $sql = "SELECT restaurant, email, mobile, status, salutation, lastname, firstname, rdate, rtime, cover, specialrequest, confirmation, wheelwin, restCode, membCode, tracking, booker, canceldate, type, company, hotelguest FROM booking WHERE confirmation='$query' limit 1";
    try {
        $db = getConnection();
        $stmt = $db->query($sql);
        $restaurant = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;

        $data = json_encode($restaurant);
        while (list($label, $val) = each($mapping))
            $data = preg_replace("/$label/", "$val", $data);

        $data_dec = json_decode($data);
        $errors = null;
        $data = array("visit" => $data_dec);
        echo format_api(1, $data, count($data), $errors);
    } catch (PDOException $e) {
        api_error($e, "getVisit1");
    }
}

function getVisitMember($email) {

    $mapping = array("restaurant" => "resto", "email" => "email", "mobile" => "phone", "salutation" => "salutation", "lastname" => "last", "firstname" => "first", "rdate" => "date", "rtime" => "time", "cover" => "pers", "confirmation" => "booking", "wheelwin" => "wheelwin", "restCode" => "restCode", "membCode" => "membCode");
    $sql = "SELECT restaurant, email, mobile, salutation, lastname, firstname, rdate, rtime, cover, confirmation, wheelwin, restCode, membCode FROM booking WHERE email='$email' order by rdate DESC, rtime DESC";
    try {
        $db = getConnection();
        $stmt = $db->query($sql);
        $restaurant = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;

        $data = json_encode($restaurant);
        while (list($label, $val) = each($mapping))
            $data = preg_replace("/$label/", "$val", $data);

        $data_dec = json_decode($data);
        $errors = null;
        $data = array("visit" => $data_dec);
        echo format_api(1, $data, count($data), $errors);
    } catch (PDOException $e) {
        api_error($e, "getVisitMember");
    }
}

function requestSpin($confirmation, $rcode, $mcode) {

    $bkg = new WY_Booking();
    $ret = $bkg->verifyBookingWin($confirmation, $rcode, $mcode);

    if ($ret == "ok") {
        $token = base64_encode($confirmation . "|" . $rcode . "|" . $mcode);
        echo format_api(1, $token, 1, "ok");
    } else
        echo format_api(0, NULL, 0, $ret);
}

function storeSpin($spin, $token) {
    $spin = urldecode($spin);

    $tokenAr = explode("|", base64_decode($token));
    $confirmation = $tokenAr[0];
    $rcode = $tokenAr[1];
    $mcode = $tokenAr[2];

    $bkg = new WY_Booking();
    $ret = $bkg->submitBookingWin($confirmation, $rcode, $mcode, $spin);
    error_log("API res " . $confirmation . '  ' . $ret);
    echo ($ret == "ok") ? format_api(1, NULL, 1, "ok") : format_api(0, NULL, 0, $ret);
}

function getSpinResult($confirmation) {

    $bkg = new WY_Booking();
    $ret = $bkg->getSpinResult($confirmation);
    $data = array('wheelsegment'=>$bkg->wheelsegment,'wheelwin'=>$bkg->wheelwin,'wheeldesc'=>$bkg->wheeldesc,'wheelwinangle'=>$bkg->wheelwinangle,'spinsource'=> $bkg->spinsource);
    echo ($ret == "ok") ? format_api(1, $data, 1, NULL) : format_api(0, NULL, 0, $ret);
}


function storeSpinComplete($segment, $spin, $desc, $source, $token) {

    $spin = urldecode($spin);

    $tokenAr = explode("|", base64_decode($token));
    $confirmation = $tokenAr[0];
    $rcode = $tokenAr[1];
    $mcode = $tokenAr[2];

    if(!isset($desc) || $desc == ''){
        $desc = 'empty';
    }
    
    $bkg = new WY_Booking();
    $ret = $bkg->submitBookingCompleteWin($confirmation, $rcode, $mcode, $segment, $spin, $desc, $source);
    error_log("API res " . $confirmation . '  ' . $ret);
    echo ($ret == "ok") ? format_api(1, NULL, 1, "ok") : format_api(0, NULL, 0, $ret);
}



function storeSpinComplete_HOTFIX($segment, $spin, $source, $token) {

    $spin = urldecode($spin);

    $tokenAr = explode("|", base64_decode($token));
    $confirmation = $tokenAr[0];
    $rcode = $tokenAr[1];
    $mcode = $tokenAr[2];

    if(!isset($desc) || $desc == ''){
        $desc = 'empty';
    }
    
    $bkg = new WY_Booking();
    $ret = $bkg->submitBookingCompleteWin($confirmation, $rcode, $mcode, $segment, $spin, $desc, $source);
    error_log("API res " . $confirmation . '  ' . $ret);
    echo ($ret == "ok") ? format_api(1, NULL, 1, "ok") : format_api(0, NULL, 0, $ret);
}


function storeSpinCompleteAngle($wheelversion, $segment,$angle, $spin, $desc, $source, $token) {

    $spin = urldecode($spin);
    if (strpos($desc,'%20') !== false) {$desc = urldecode($desc);}   

    $tokenAr = explode("|", base64_decode($token));
    $confirmation = $tokenAr[0];
    $rcode = $tokenAr[1];
    $mcode = $tokenAr[2];

    $bkg = new WY_Booking();
    $ret = $bkg->submitBookingCompleteWin($confirmation, $rcode, $mcode, $segment, $spin, $desc, $source, $wheelversion, $angle);
    error_log("API res " . $confirmation . '  ' . $ret);
    echo ($ret == "ok") ? format_api(1, 1, 1, "ok") : format_api(0, NULL, 0, $ret);
}

function updateProfile($label, $value) {
    $email = $_SERVER['PHP_AUTH_USER'];
    $validLabel = array('name', 'firstname', 'mobile', 'country', 'member', 'gender', 'address', 'address1', 'zip', 'region', 'state', 'fax');

    if (!in_array($label, $validLabel)) {
        echo format_api(0, NULL, 0, "Invalid Label $label");
        return;
    }

    $sql = "UPDATE member SET $label=:value WHERE email=:email limit 1";
    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("email", $email);
        $stmt->bindParam("value", $value);
        $obj = $stmt->execute();
        if ($obj) {
            echo format_api(1, "success", 1, NULL);
        }
        $db = null;
    } catch (PDOException $e) {
        echo format_api(0, NULL, 0, "Update $label failed");
    }
}

function getWheel($query) {

    $sql = "SELECT wheel, wheelversion FROM restaurant WHERE restaurant=:query limit 1";
    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("query", $query);
        $stmt->execute();
        $restaurant = $stmt->fetchObject();
        if(empty($restaurant)) {
        	echo format_api(0, NULL, 0, "no result");
        	return;
        	}
        $str = explode("|", $restaurant->wheel);
        $jsonwheel = "\"value\":\"" . $str[0] . "\"";
        for ($i = 1; $i < 25; $i++)
            $jsonwheel .= ", \"slice" . $i . "\":\"" . $str[$i] . "\"";
        //echo "{" . $jsonwheel . "}";

        $errors = null;
        $data = array("wheel" => $jsonwheel, "wheelversion" => $restaurant->wheelversion);
        echo format_api(1, $data, count($jsonwheel), $errors);

        $db = null;
        //echo json_encode($restaurant);
    } catch (PDOException $e) {
        api_error($e, "getWheel");
    }
}

function getWheelDescription($restaurant) {
    
	$restaurant = clean_text($restaurant);
    $wheel = new WY_wheel($restaurant, 'api');
    $wheeldata = $wheel->wheeldescription();
    $data = array("wheel" => $wheeldata['wheel'], "description" => $wheeldata['desc'], "wheelversion" => $wheeldata['wheelversion']);
    echo format_api(1, $data, count($wheeldata['wheel']), NULL);
}



function getRestaurantInfo($query) {

	$res = new WY_restaurant;
	$media = new WY_Media();
	$images = array();
	$menu = array();
    try {
        $sql = "SELECT restaurant, title, address, zip, country, city, region, cuisine, rating, email, tel, "
                . " stars, GPS, likes, images, pricing, creditcard, mealtype, stars, dinesfree, status,"
                . "chef, chef_award, chef_origin, chef_description, chef_believe, openhours, description, "
                . "wheel, wheelvalue, wheelversion, currency, is_bookable, is_wheelable "
                . "FROM restaurant WHERE restaurant=:query limit 1";

        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("query", $query);
        $stmt->execute();
        $restaurant = $stmt->fetch(PDO::FETCH_ASSOC);
        
        //description
        $restaurant['description'] = $res->restaurantDescriptionToArray($restaurant['description']);
        
        
        $logo = "SELECT name as logo FROM media WHERE restaurant=:query and object_type = 'Logo'";
        $stmt = $db->prepare($logo);
        $stmt->bindParam("query", $query);
        $stmt->execute();
        $data_logo = $stmt->fetch(PDO::FETCH_ASSOC);
        
        $chef = array('chef_name' => $restaurant['chef'],
            'chef_award' => $restaurant['chef_award'],
            'chef_origin' => $restaurant['chef_origin'],
            'chef_description' => $restaurant['chef_description'],
            'chef_believe' => $restaurant['chef_believe']);
        
        $chef_img = "SELECT name as chef FROM media WHERE restaurant=:query and object_type = 'Chef'";
        $stmt = $db->prepare($chef_img);
        $stmt->bindParam("query", $query);
        $stmt->execute();
        $data_chef = $stmt->fetch(PDO::FETCH_ASSOC);
        //chef
        $chef['chef_image'] = $data_chef['chef'];
        
        unset($restaurant['chef_award']);
        unset($restaurant['chef_origin']);
        unset($restaurant['chef_description']);
        unset($restaurant['chef_believe']);
        
        //images
        $img_array = $media->getRestaurantGallery($restaurant['restaurant']); 
        foreach ($img_array as $img){
           $images[] = $img->getName();
        }
        array_multisort($images);
        $restaurant['images'] = $images;

        //video
        $restaurant['video'] = $media->getVideoRestaurantDishes($restaurant['restaurant']);
        
        //pricing
        $res->setPriceSegment($restaurant['currency']);
        $restaurant['pricing']= array('lunch'=>$res->PricingDollars($res->PriceMeal($restaurant['pricing'], 2)),'dinner'=>$res->PricingDollars($res->PriceMeal($restaurant['pricing'], 1)) );
        
        //logo
        $restaurant['logo'] = $data_logo['logo'];
        
        //convert open hours//
        $restaurant['openhours'] = $res->getOpeningHoursListing($restaurant['restaurant'], true);
        
        // menu
        $mobj = new WY_Menu($restaurant['restaurant']);
		$categories = $mobj->getCategorieList();
        
        //categories
        $categorie = array();
        foreach ($categories as $categorie){
            $menus = $mobj->getCategorieItems($categorie['ID']);
            $menu[] = array('categorie' => $categorie, 'items'=>$menus);
        }
        
        //best offer
        
        $tmp = $res->getBestOffer($restaurant['restaurant'], $restaurant['wheel'], $restaurant['is_wheelable'], 1);
        if(!empty($tmp) && isset($tmp[0])){
            $restaurant['best_offer'] = $tmp[0];
        }
        

        //book button
        $button = array('color'=> '#ee1d23', 'text'=> 'Book Now');
        if($restaurant['status'] == 'comingsoon'){
            $button = array('color'=> '#ffffffff', 'text' => 'Booking coming soon');
        }else
        if($restaurant['is_wheelable'] && !$restaurant['is_bookable']){
            //$button = array('color'=> '#438c4f', 'text'=> 'Get Code Now');
            $button = array('color'=> '#ee1d23', 'text'=> 'Request Now');
        }

        $restaurant['book_button'] = $button;
        
        $review = array();
        
        $db = null;

        $errors = null;
        $data = array(
            "restaurantinfo" => $restaurant,
            'menu' => $menu,
            'review' => $review,
            'chef' => $chef);
        echo format_api(1, $data, count($restaurant), $errors);

        //echo '{"restaurantinfo": ' . json_encode($restaurant) . ', "menu": ' . json_encode($menu) . ', "review": ' . json_encode($review) . '}';
    } catch (PDOException $e) {
        api_error($e, "getRestaurantInfo");
    }
}

function getMap($query) {
    $sql = "SELECT restaurant, title, city, cuisine, rating, logo, stars, GPS FROM restaurant WHERE city=:query";
    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("query", $query);
        $stmt->execute();
        $restaurant = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;

        $errors = null;
        $data = array("restaurant" => $restaurant);
        echo format_api(1, $data, count($restaurant), $errors);

        //echo '{"restaurant": ' . json_encode($restaurant) . '}';
    } catch (PDOException $e) {
        api_error($e, "getMap");
    }
}


function getActiveEventsRestaurant($restaurant) {

	$restaurant = clean_text($restaurant);
	$eobj = new WY_Event($restaurant);
    try {
	$event = $eobj->getActiveEvents();
        $data = array("event" => $event);
        echo format_api(1, $data, count($event), "");

    } catch (PDOException $e) {
        api_error($e, "getActiveEventsRestaurant");
    }
}

function getEventsRestaurant($restaurant) {

	$restaurant = clean_text($restaurant);
	$eobj = new WY_Event($restaurant);
    try {
		$event = $eobj->getEvents();
        $data = array("event" => $event);
        echo format_api(1, $data, count($event), "");

    } catch (PDOException $e) {
        api_error($e, "getEventsRestaurant");
    }
}


function setEvent($app) {

    $restaurant = $app->request->post("restaurant");
    $name = $app->request->post("name");
    $title = $app->request->post("title");
	$city=$app->request->post("city");
	$country=$app->request->post("country");
	$start=$app->request->post("start");
	$end=$app->request->post("end");
	$description=$app->request->post("description");
	$picture=$app->request->post("picture");
    $morder = $app->request->post("morder");
    $token = $app->request->post("token");

 	$login = new WY_Login(set_valid_login_type('backoffice'));
 	if($login->checktoken($token)) {
		$mobj = new WY_Event($restaurant);
 		list($val, $msg) = $mobj->insertEvent($name, $title, $morder, $city, $country, $start, $end, $description, $picture);
		if($val > 0)
			echo format_api(1, '', 0, 'OK');
		else echo format_api(-1, '', 0, $msg);
 		}
 	else echo format_api(-1, '', 0, 'wrong token');	 	
}

function deleteEvent($app) {

    $restaurant = $app->request->post("restaurant");
    $name = $app->request->post("name");
    $token = $app->request->post("token");

 	$login = new WY_Login(set_valid_login_type('backoffice'));
 	if($login->checktoken($token)) {
		$mobj = new WY_Event($restaurant);
 		list($val, $msg) = $mobj->delete($name);
		if($val > 0)
			echo format_api(1, '', 0, '');
		else echo format_api(-1, '', 0, $msg);
 		}
 	else echo format_api(-1, '', 0, 'wrong token');	
}

function updateEvent($app) {

    $restaurant = $app->request->post("restaurant");
    $name = $app->request->post("name");
    $title = $app->request->post("title");
	$city=$app->request->post("city");
	$country=$app->request->post("country");
	$start=$app->request->post("start");
	$end=$app->request->post("end");
	$description=$app->request->post("description");
	$picture=$app->request->post("picture");
    $morder = $app->request->post("morder");
    $token = $app->request->post("token");

 	$login = new WY_Login(set_valid_login_type('backoffice'));
 	if($login->checktoken($token)) {
		$mobj = new WY_Event($restaurant);
 		list($val, $msg) = $mobj->updateEvent($name, $title, $morder, $city, $country, $start, $end, $description, $picture);
		if($val > 0)
			echo format_api(1, '', 0, 'OK');
		else echo format_api(-1, '', 0, $msg);
 		}
 	else echo format_api(-1, '', 0, 'wrong token');	
 	
}


function getMenu($restaurant) {

	$restaurant = clean_text($restaurant);
	$mobj = new WY_Menu($restaurant);
	$menu = "";
    try {
		$categories = $mobj->getCategorieList();
        
		//categories
		$categorie = array();
		foreach ($categories as $categorie){
			$menus = $mobj->getCategorieItems($categorie['ID']);
			$menu[] = array('categorie' => $categorie, 'items'=>$menus);
			}
	
	$data = array("menu" => $menu);
	$cn = (is_array($menu)) ? count($menu) : 0;
	echo format_api(1, $data, $cn, "");

    } catch (PDOException $e) {
        api_error($e, "getMenu");
    }
}

function createMenu($app) {

    $restaurant = $app->request->post("restaurant");
    //$name = $app->request->post("name");
    $title = $app->request->post("title");
    $morder = $app->request->post("morder");
    $description = $app->request->post("description");
    $items = $app->request->post("items");

    $token = $app->request->post("token");
 	$login = new WY_Login(set_valid_login_type('backoffice'));
 	if($login->checktoken($token)) {
		$mobj = new WY_Menu($restaurant);
        list($val, $newid, $msg) = $mobj->createMenu($title, $morder, $description, $items);
                 
		if($val > 0)
			echo format_api(1, $newid, 0, $msg);
		else echo format_api(-1, '', 0, $msg);
 		}
 	else echo format_api(-1, '', 0, 'wrong token');	 	
}



function updateMenuTMP($app) {

    $restaurant = $app->request->post("restaurant");
    
    $id = $app->request->post("id");    
    $title = $app->request->post("title");
    $description = $app->request->post("description");
    $morder = $app->request->post("morder");
    $items = $app->request->post("items");

    $token = $app->request->post("token");
 	$login = new WY_Login(set_valid_login_type('backoffice'));
 	if($login->checktoken($token)) {
		$mobj = new WY_Menu($restaurant);
        list($val, $newid, $msg) = $mobj->updateMenu($id, $title, $description, $morder, $items);
                
		if($val > 0)
			echo format_api(1, $newid, 0, $msg);
		else echo format_api(-1, '', 0, $msg);
 		}
 	else echo format_api(-1, '', 0, 'wrong token');	 	
}


function deleteMenuTMP($app) {

    $restaurant = $app->request->post("restaurant");
    $id = $app->request->post("id");
    $token = $app->request->post("token");

 	$login = new WY_Login(set_valid_login_type('backoffice'));
 	if($login->checktoken($token)) {
		$mobj = new WY_Menu($restaurant);
 		list($val, $msg) = $mobj->deleteMenu($id);
		if($val > 0)
			echo format_api(1, '', 0, '');
		else echo format_api(-1, '', 0, $msg);
 		}
 	else echo format_api(-1, '', 0, 'wrong token');	
}


function getReviews($query, $page = '') {
    
    if($page == 'summary'){
        return getReviewsList($query);
    }
    
    
    $item_per_page = 2;
    $limit = '';
    if(!empty($page)){
        $item = $page * $item_per_page;
        $limit = " LIMIT $item,$item_per_page";
    }
    
    
    $mapping = array("restaurant" => "resto", "confirmation" => "booking", "reviewdesc" => "description", "reviewdtvisite" => "date", "reviewdtcreate" => "cdate", "reviewguest" => "guestname", "user_id" => "userid", "reviewgrade" => "grade","foodgrade" => "food","servicegrade" => "service","ambiancegrade" => "ambiance","pricegrade" => "price", "comment" => "comment", "response_to" => "response");

    $sql = "SELECT restaurant, confirmation, reviewdesc, reviewdtvisite, reviewdtcreate, reviewguest, user_id, reviewgrade,foodgrade,servicegrade, ambiancegrade, pricegrade, comment, response_to FROM review WHERE restaurant=:query $limit";
   
    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("query", $query);
        $stmt->execute();
        $review = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;

        $data = json_encode($review);
        while (list($label, $val) = each($mapping))
            $data = preg_replace("/$label/", "$val", $data);

        $data_dec = json_decode($data);
        $errors = null;
        
        $data = array("review" => $data_dec);
        echo format_api(1, $data, count($data), $errors);
    } catch (PDOException $e) {
        api_error($e, "getReviews");
    }
}

function getReviewsList($query, $page = '') {
    $media = new WY_Media();
    $item_per_page = 4;
    
    $limit = " LIMIT 0,3";
    if(!empty($page)){
        $page = $page - 1;
        $item = $page * $item_per_page;
        $limit = " LIMIT $item,$item_per_page";
    }
    
    try {
    $reviews = new WY_Review($query);
    $count_reviews = $reviews->getReviewsCount();
    $reviews->getReviewsList($limit);
    
    $count = 0;

        
    foreach ($reviews->data as $review){
        $count = $count + 1;
        $review['user_picture'] = $media->getUserProfilePicture($review['email']);
        unset($review['email']);
        $review_res[] = $review;
    }
    
        if($count>0){
            $data = array(
                "restaurant" => $reviews->restaurant,
                "count" => $count_reviews['count'],
                "score" => $count_reviews['score'],
                "score_desc" => $count_reviews['review_desc'],
                "reviews" => $review_res);

            echo format_api(1, $data, count($reviews->data), NULL);
        }else{
            echo format_api(1, array(), 0, NULL);
        }
    } catch (PDOException $e) {
        api_error($e, "getReviews");
    }
}

function allotment1slotData($restaurant, $rdate, $rtime, $rpers, $conf, $token) {

    try {
		$restaurant = clean_text($restaurant);
		$res = new WY_restaurant;
		$res->getRestaurant($restaurant);
		$ret = $res->CheckAvailability($restaurant, $rdate, $rtime, $rpers, $conf);

		echo ($res->result > 0) ? format_api(1, "1", 1, "available") : format_api(1, "0", $ret, "not available");
    } catch (Exception $e) {
        api_error($e, "allotment1slotData");
    }
}

function allotmentData($restaurant) {

    try {
		$restaurant = clean_text($restaurant);
		$res = new WY_restaurant;
		$res->getRestaurant($restaurant);
                
		$data = $res->getAlloteOpenHours($restaurant);
                $data['lunchstart'] = 9;
                $data['dinnerstart'] = 16;
		echo stripslashes(format_api(1, $data, 1, "ok"));
    } catch (Exception $e) {
        api_error($e, "allotmentData");
    }
}

function getdayDispo($restaurant, $aDate) {

    try {
		$restaurant = clean_text($restaurant);
		$res = new WY_restaurant;
		$res->getRestaurant($restaurant);
		$data = $res->reportAvailability($restaurant, $aDate);

		echo stripslashes(format_api(1, $data, 1, "ok"));
    } catch (Exception $e) {
        api_error($e, "getdayDispo");
    }
}

function getAlloteReponse($restaurant) {

    try {
		$restaurant = clean_text($restaurant);
		$allote = new WY_Allote($restaurant);
		$data = $allote->getAlloteRestaurant($restaurant);

		$errors = null;
		echo format_api(1, $data, count($data), $errors);	
    } catch (Exception $e) {
        api_error($e, "getAlloteReponse");
    }
}


function setAlloteResponse($app) {

    try {
		$restaurant = $app->request->post("restaurant");
		$start = $app->request->post("start");
		$size = $app->request->post("size");
		$lunchdata = $app->request->post("ldata");
		$dinnerdata = $app->request->post("ddata");
		$token = $app->request->post("token");

		$login = new WY_Login(set_valid_login_type('backoffice'));
		if($login->checktoken($token)) {
			$allote = new WY_Allote($restaurant);
			$allote->saveAlloteRestaurant($restaurant, $start, $size, $lunchdata, $dinnerdata);
			}
	
		echo format_api(1, '', 0, '');
    } catch (Exception $e) {
        api_error($e, "setAlloteResponse");
    }
}


function getRestaurantByManager($manager) {

	$res = new WY_restaurant();
	$data = $res->getRestaurantByManager($manager);
        $errors = null;
	echo format_api(1, $data, count($data), $errors);	
}

function setReviewResponse($app) {

    $restaurant = $app->request->post("restaurant");
    $confirmation = $app->request->post("confirmation");
    $response = $app->request->post("response");
    $token = $app->request->post("token");

 	$login = new WY_Login(set_valid_login_type('backoffice'));
 	if($login->checktoken($token)) {
 		$review = new WY_Review($restaurant);
 		$review->saveResponse($confirmation, $response);
 		}
 	
    echo format_api(1, '', 0, '');
}


function postReview($app) {

    require_once("lib/class.review.inc.php");
    $params = json_decode($app->request()->getBody(), true);
    try {
        $review = new WY_Review();

        if ($review->postReview($params['confirmation_id'], $params['user_id'], $params['restaurant_id'], $params['food_rate'], $params['ambiance_rate'], $params['service_rate'], $params['price_rate'], $params['comment'])) {
            $errors = null;
            $data = array("review" => 'saved');
            echo format_api(1, $data, count($data), $errors);
        } else {
            echo '{"error":{"text":dtc}}';
        }
        //echo '{"review": ' . json_encode($review) . '}';
    } catch (PDOException $e) {
        api_error($e, "postReview");
    }
}

function getUserPostedReviews() {
    $email = '';
    if (!empty($_SERVER['PHP_AUTH_USER'])) {
        $email = $_SERVER['PHP_AUTH_USER'];
    }
    
    $options = array('include_reviews' => true);
    $reviews = new WY_Review();
    $reviews->getUserReviews($email, $options);


    foreach ($reviews->reviews as $review){
            $images = new WY_Media($review->restaurant);
            $review->image = $images->getDefaultPicture($review->restaurant);
    } 
    
    $errors = null;
    $data = array("reviews" => $reviews->reviews);
    echo format_api(1, $data, count($reviews->reviews), $errors);
    
}

function ChkLogin($query) {

    $queryArg = explode("_:::_", $query);
    $sql = "SELECT Email FROM login WHERE Email=:query";
    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("query", $query);
        $stmt->execute();
        $login = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;

        $errors = null;
        $data = array("login" => $login);
        echo format_api(1, $data, count($login), $errors);

        //echo '{"login": ' . json_encode($login) . '}';
    } catch (PDOException $e) {
        api_error($e, "ChkLogin");
    }
}

function getCuisineList() {

    $sql = "SELECT distinct cuisine FROM cuisine order by cuisine";
    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $cuisine = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;

        $errors = null;
        $data = array("cuisine" => $cuisine);
        echo format_api(1, $data, count($cuisine), $errors);

        //echo json_encode($cuisine);
    } catch (PDOException $e) {
        api_error($e, "getCuisineList");
    }
}

function getCityList() {

	try {
		$res = new WY_restaurant();
		$city = $res->getActiveCities();    
        $errors = null;
        $data = array("city" => $city);
        echo format_api(1, $data, count($city), $errors);

        //echo json_encode($city);
    } catch (PDOException $e) {
        api_error($e, "getCityList");
    }
}

function getMediaPictRestaurant($restaurant) {

	$restaurant = clean_text($restaurant);
	$media = new WY_Media($restaurant);
    try {
		$data = $media->getRestaurantMedia($restaurant, 'picture');
		
    	$errors = null;
		echo format_api(1, $data, count($data), $errors);	
    } catch (PDOException $e) {
        api_error($e, "getMediaPictRestaurant");
    }
}

function renameMedia($app) {

    $restaurant = $app->request->post("restaurant");
    $name = $app->request->post("name");
    $newname = $app->request->post("newname");
    $category = $app->request->post("category");
    $token = $app->request->post("token");

 	$login = new WY_Login(set_valid_login_type('backoffice'));
 	if($login->checktoken($token)) {
		$media = new WY_Media($restaurant);
 		$log = $media->renameMedia($restaurant, $name, $newname, 'picture', $category);
		echo format_api($log, '', 0, $media->msg);
 		}
 	else echo format_api(-1, '', 0, 'wrong token');	
 	
}


function deleteMedia($app) {

    $restaurant = $app->request->post("restaurant");
    $name = $app->request->post("name");
    $category = $app->request->post("category");
    $token = $app->request->post("token");

 	$login = new WY_Login(set_valid_login_type('backoffice'));
 	if($login->checktoken($token)) {
		$media = new WY_Media($restaurant);
 		$log = $media->deleteMedia($restaurant, $name, $category);
		echo format_api($log, '', 0, $media->msg);
 		}
 	else echo format_api(-1, '', 0, 'wrong token');	
 	
}


function updateMedia($app) {

    $restaurant = $app->request->post("restaurant");
    $name = $app->request->post("name");
    $category = $app->request->post("category");
    $type = $app->request->post("type");
    $description = $app->request->post("description");
    $status = $app->request->post("status");
    $morder = $app->request->post("morder");
    $token = $app->request->post("token");

 	$login = new WY_Login(set_valid_login_type('backoffice'));
 	if($login->checktoken($token)) {
		$media = new WY_Media($restaurant);
 		$log = $media->updateMedia($restaurant, $name, $category, $type, $description, $status, $morder);
		echo format_api($log, '', 0, $media->msg);
 		}
 	else echo format_api(-1, '', 0, 'wrong token');	
 	
}



function getPictRestaurant($query) {

    $query = preg_replace("/_/", " ", $query);  // for Kuala Lumpur
    $sql = "SELECT images FROM restaurant WHERE restaurant=:query ORDER BY images";
    $binlog = true;
    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        if ($binlog)
            $stmt->bindParam("query", $query);
        $stmt->execute();
        $pictures = $stmt->fetchAll(PDO::FETCH_OBJ);
        if (count($pictures) < 1) {
            $errors = null;
            $data = array("pictures" => '');
            echo format_api(1, $data, 0, $errors);
            //echo '{"pictures": }';
            return;
        }
        $db = null;
        $val = $pictures[0]->images;
        $valAr = explode("|", $val);
        for ($i = 0; $i < count($valAr); $i++) {
            if ($i > 0)
                $pictures[$i] = new stdClass();
            $pictures[$i]->images = $valAr[$i];
        }

        $errors = null;
        $data = array("pictures" => $pictures);
        echo format_api(1, $data, count($pictures), $errors);

        //echo '{"pictures": ' . json_encode($restaurant) . '}';
    } catch (PDOException $e) {
        api_error($e, "getPictRestaurant");
    }
}

function find($app) {
    $request = $app->request()->get();
   
	$res = new WY_restaurant();
	$errors = null;
    try {
        $data = $res->find($request);
        $restaurant = $data['restaurant'];
    	if($res->result < 0) {
    		echo '{"error":{"text":food is empty}}';
    		return;
    		}

        //$data = array("restaurant" => $restaurant);
        echo format_api(1, $data, count($restaurant), $errors);
    } catch (PDOException $e) {
        api_error($e, "find");
    }
}


function oldfind($app) {
    $request = $app->request()->get();
    
    if(!empty($_SERVER['PHP_AUTH_USER'])){
        $email = $_SERVER['PHP_AUTH_USER'];
    }
   
    $leftjoin = '';
    $where = " AND (r.status like  '%$request[status]%'";
    if($request['status'] == 'both' ){
        $where = " AND ((r.status like 'active' OR r.status like 'comingsoon') ";
    }
    
    // for tester only
    if(!empty($email) && ($email == 'chris.danguien@weeloy.com' || $email == 'qatester7822@gmail.com' 
            || $email == 'philippe.benedetti@weeloy.com' || $email == 'gaolinch@hotmail.com'
            || $email == 'shawn.chen@weeloy.com' || $email == 'joel.lai@com'
            || $email == 'charlotte.donahue@weeloy.com' || $email == 'nicolas.finck@weeloy.com'
            || $email == 'jerome.arbault@weeloy.com'
            || $email == 'richard@kefs.me')){
        
        $where .= " OR r.status like  '%reference%'";
    }
    $where .= ')';
    $limit = '';
    
    //limit calculation
    if (!empty($request['p'])) {
        $start = ($request['p'] - 1) * 12;
        $limit = " LIMIT $start,12";
    }
    /*if (isset($request['free_search']) && !empty($request['free_search'])) {
        $where .= " AND (title like '%$request[free_search]%' OR description like '%$request[free_search]%' "
                . "OR hotelname like '%$request[free_search]%' "
                . "OR city like '%$request[free_search]%' "
                . "OR country like '%$request[free_search]%') ";
    }*/

    if (!empty($request['free_search'])) {
        $where .= " AND title like '%$request[free_search]%'";
    }
    
    if (isset($request['city']) && !empty($request['city'])) {
        $where .= " AND city like '%$request[city]%'";
    }
    /* if($pricing < 20) $ratepricing = "$";
      else if($pricing < 50)  $ratepricing = "$$";
      else if($pricing < 80)  $ratepricing = "$$$";
      else if($pricing < 100)  $ratepricing = "$$$$";
      else if($pricing < 5000)  $ratepricing = "$$$$$";
     * 
     */
    if (isset($request['pricing']) && !empty($request['pricing'])) {
        switch ($request['pricing']) {
            case '1':
                $where .= " AND pricing < 20";
                break;
            case '2':
                $where .= " AND pricing >= 20  AND pricing < 50";
                break;
            case '3':
                $where .= " AND pricing >= 50  AND pricing < 80";
                break;
            case '4':
                $where .= " AND pricing >= 80 AND pricing < 100";
                break;
            case '5':
                $where .= " AND pricing >= 80";
                break;
        }
    }

    if (isset($request['cuisine']) && !empty($request['cuisine'])) {

        $queryAr = explode("|", $request['cuisine']);
        $limit_cuisine = count($queryAr);
        if ($limit_cuisine < 1) {
            echo '{"error":{"text":food is empty}}';
            return;
        }

        for ($i = 0, $sep = " AND ("; $i < $limit_cuisine; $i++, $sep = " or ") {
            $where .= $sep . " cuisine like '%" . $queryAr[$i] . "%'";
        }
        $where.=')';
    }
    
    $where = preg_replace("/_/", " ", $where);  // for Kuala Lumpur
    
    if(!empty($request['is_favorite'])){
        $where.= " AND is_favorite = '$request[is_favorite]'"; 
    }
    
    $selectjoin = " '0' as is_favorite ";
    if(!empty($email)){
        $selectjoin = "IF(is_favorite = '1', 1, 0) as is_favorite";
        $leftjoin = "  LEFT JOIN members_restaurants_favorite mrf  ON mrf.restaurant = r.restaurant AND mrf.member = '$email' ";
    }
    
    
    

    $sql = "SELECT distinct(r.restaurant), r.ID, r.title, r.hotelname, r.cuisine, r.city, r.status, m2.name as logo, r.address, r.address1, r.map, r.region, r.rating, r.zip, r.country, r.GPS, r.likes, r.currency,r.pricing, r.mealtype, r.rating, r.openhours, r.wheel, IF(is_wheelable = '0',10,r.wheelvalue) as wheelvalue, "
			. "is_displayed, is_wheelable,is_bookable, extraflag, m.path as image_path, m.name as image, $selectjoin
                    FROM restaurant r $leftjoin, media m , media m2

                    WHERE 1 

                    AND m.restaurant = r.restaurant 
                    AND m.status = 'active'
                    AND m.object_type = 'restaurant'
                    AND m.media_type = 'picture'
                    
                    AND m2.restaurant = r.restaurant 
                    AND m2.status = 'active'
                    AND m2.object_type = 'logo'
                    AND m2.media_type = 'picture'
                    
                    AND r.is_displayed = '1'
                    
                     " .$where. " 
                    GROUP BY r.id
                    ORDER BY status ASC, wheelvalue DESC, m.morder ASC $limit";
    
    $binlog = true;

    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);

        $stmt->execute();
        $restaurant = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;

        //TMP not performant
        $res = new WY_restaurant();
        foreach($restaurant as $key => $r){
            if(!empty($restaurant[$key]->restaurant) && !empty($restaurant[$key]->wheel) && $restaurant[$key]->is_wheelable){

                $tmp = $res->getBestOffer($restaurant[$key]->restaurant, $restaurant[$key]->wheel,  $restaurant[$key]->is_wheelable, 1);
                $restaurant[$key]->best_offer = $tmp[0];
                
                $res->setPriceSegment($restaurant[$key]->currency);
                $restaurant[$key]->pricing = $res->PricingDollars($res->PriceMeal($restaurant[$key]->pricing, 3));
                $restaurant[$key]->pricelunch = (preg_match('/Lunch/', $restaurant[$key]->mealtype)) ? $res->PricingDollars($res->PriceMeal($restaurant[$key]->pricing, 1)) : "";
                $restaurant[$key]->pricediner = (preg_match('/Dinner/', $restaurant[$key]->mealtype)) ? $res->PricingDollars($res->PriceMeal($restaurant[$key]->pricing, 1)) : "";
		
                //$restaurant[$key]->internal_path'] = $res->getRestaurantInternalPath();
                $booktitle = "BOOK SOON";
                $custombutton = "custom_button_book_soon";
                
                if (($r->status == 'active' && $restaurant[$key]->is_bookable) || $restaurant[$key]->status == 'demo_reference') {
                    $booktitle = "BOOK NOW";
                    $custombutton = "";
                }
                if ($restaurant[$key]->is_bookable == false && ($restaurant[$key]->status == 'active' || $restaurant[$key]->status == 'demo_reference') ) {
                    $booktitle = "REQUEST NOW";
                    $custombutton = "btn-green";
                }
                $book_btn_tmp = array( 'label'=> ''.$booktitle, 'style'=> $custombutton);
                $restaurant[$key]->bookbutton = $book_btn_tmp;
       

                
            }
        }
        
        $errors = null;
        $data = array("restaurant" => $restaurant);
        echo format_api(1, $data, count($restaurant), $errors);
        //echo '{"restaurant": ' . json_encode($restaurant) . '}';
    } catch (PDOException $e) {
        api_error($e, "find");
    }
}

function findByCity($query) {


    $query = preg_replace("/_/", " ", $query);  // for Kuala Lumpur
    $sql = "SELECT restaurant, title, city, cuisine, rating, logo, stars, GPS, openhours FROM restaurant WHERE city=:query ORDER BY restaurant";
    $binlog = true;
    if ($query == "ALL") {
        $sql = "SELECT restaurant, title, city, cuisine, rating, logo, stars, GPS, openhours FROM restaurant where logo != '' ORDER BY city, restaurant";
        $binlog = false;
    }
    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        if ($binlog)
            $stmt->bindParam("query", $query);
        $stmt->execute();
        $restaurant = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;

        $errors = null;
        $data = array("restaurant" => $restaurant);
        echo format_api(1, $data, count($restaurant), $errors);
        //echo '{"restaurant": ' . json_encode($restaurant) . '}';
    } catch (PDOException $e) {
        api_error($e, "findByCity");
    }
}

function findByCuisine($query) {

    $request = "";
    $queryAr = explode("|", $query);
    $limit = count($queryAr);
    if ($limit < 1) {
        echo '{"error":{"text":food is empty}}';
        return;
    }

    for ($i = 0, $sep = ""; $i < $limit; $i++, $sep = " or ")
        $request .= $sep . "cuisine like '%" . $queryAr[$i] . "%'";

    $sql = "SELECT restaurant, title, city, cuisine, rating, logo, stars, GPS, openhours FROM restaurant WHERE $request ORDER BY restaurant";
    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $restaurant = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;

        $errors = null;
        $data = array("restaurant" => $restaurant);
        echo format_api(1, $data, count($restaurant), $errors);
        //echo '{"restaurant": ' . json_encode($restaurant) . '}';
    } catch (PDOException $e) {
        api_error($e, "findByCuisine");
    }
}


function api_authentificate() {
    
    $response = array();
    
    if( !isset($_SERVER['PHP_AUTH_USER']) || !isset($_SERVER['PHP_AUTH_PW']) ){
        $response["error"] = true;
        $response["message"] = "Credentials are misssing";
        set_header_status(401);
        echo format_api(1, '', 0, $response); 
        exit;//$app->stop();
    }
  
    $email = $_SERVER['PHP_AUTH_USER'];
    $pass = $_SERVER['PHP_AUTH_PW']; //pass ou token
  
    $log = new WY_Login(set_valid_login_type('backoffice'));
    if($log->check_login_remote($email, $pass, 'api') != ''){
        return true;
    }else if($log->process_login_remote($email, $pass, 'api') != ''){
            return true;
    }else{
        $response["error"] = true;
        $response["message"] = "wrong login and password";  
        set_header_status(401);
        echo format_api(1, '', 0, $response);
        exit;
    }
    
    return false;
  }

/* add platform */

function addMember($email, $mobile, $password, $lastname, $firstname, $platfrom) {

    try {
        $login = new WY_Login(set_valid_login_type('member'));
        
        $arg = new WY_Arg;
        $arg->email = $email;
        $arg->name = $lastname;
        $arg->firstname = $firstname;
        
        $arg->password = $password;
        $arg->rpassword = $password;

        $arg->mobile = $mobile;
        $arg->application = '';
        $arg->platform = 'member';

        $res = $login->register_remote($arg);
        
        if($res > 0 ){
            echo format_api(1, "success", 0, NULL);
//            /return rmlogin($email, $password);
        }else{
            if($res == -3){
                echo format_api(0, NULL, 0, 'This account already exists. Please verify email and mobile.');
            }else{
                echo format_api(0, NULL, 0, 'Unable yo create this account. Please verify email and mobile.');
            }
        }
    } catch (PDOException $e) {
        api_error($e, "addMember");
    }
}

/* add platform */

function updatePassword($email, $oldpwd, $newpwd) {

    $arg = new WY_Arg();
    $arg->email = $email;
    $arg->password = $oldpwd;
    $arg->npassword = $arg->rpassword = $newpwd;
    $arg->application = '';
    $log = new WY_Login(set_valid_login_type('backoffice'));
    $log->updatePassword($arg);

    echo ($log->result > 0) ? format_api(1, "success", 1, NULL) : format_api(0, NULL, 0, $log->msg);
}

/* add platform */

function forgotPassword($email) {

    $arg = new WY_Arg();
    $arg->email = $email;
    $arg->application = '';
    $log = new WY_Login(set_valid_login_type('backoffice'));
    $log->process_login('LostPassword', $arg);

    echo ($log->result > 0) ? format_api(1, "success", 1, NULL) : format_api(0, NULL, 0, "Update Password Failed");
}

/* add platform */

function rmlogin($email, $pass, $appname = NULL , $appversion = NULL) {
//return state 
    // 0  not login
    // 1 login 
    // 2 login but need to update api 
    
    $sql_store_login = "INSERT INTO app_login_history (email, pass, appname, appversion) VALUES ('$email','$pass','$appname','$appversion')";
    try {
        $db = getConnection('dwh');
        $stmt = $db->prepare($sql_store_login);
        $stmt->execute();
    }catch (PDOException $e) {
        api_error($e, "app tracking rmlogin");
    }
    
    
    
    $restau = '';
    $login_message = '';
    $err = '';
    //check app version and return error if too old
    if($appversion == '0'){
        $login_message = 'login_failed_update';
        $res = array('result' => $login_message);
        echo format_api(0, $res, 0, "This version is outdated. Please update using the Application Store.");
        return;
    }
    
    
    if($appname == NULL && $appversion == NULL){
        $login_message = 'login_succeed_update';
        $err = "This version is outdated. Please update using the Application Store.";
    }
    
    if($appversion == '0.1'){
        $login_message = 'login_succeed_update';
        $err = "This version is outdated. Please update using the Application Store.";
    }    
    // if restaurant manager -> get restaurant name
    
    $sql = "SELECT restaurant_id FROM restaurant_app_managers WHERE user_id=:query LIMIT 1";
    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("query", $email);
        $stmt->execute();
        $data = $stmt->fetch(PDO::FETCH_OBJ);

    }catch (PDOException $e) {
        api_error($e, "rmlogin");
    }

    
    if(isset($data->restaurant_id)){
        $restau = $data->restaurant_id;
    }

    $log = new WY_Login(set_valid_login_type('backoffice'));
    $log->process_login_remote($email, $pass, 'api');
    if($login_message == ''){ $login_message = 'login_succeed';}
    $res = array('result' => $login_message, 'token' => $log->token, 'member_type' => 'restaurant', 'restaurant_id' => $restau);

    echo ($log->result > 0) ? format_api(1, $res, 1, $err) : format_api(0, NULL, 0, $log->msg);
}


/* add platform */

function rmloginc($email, $pass, $appname = NULL , $appversion = NULL) {
//return state 
    // 0  not login
    // 1 login 
    // 2 login but need to update api 
    
    
    $restau = '';
    $login_message = '';
    $err = '';
    //check app version and return error if too old
    if($appversion == '0'){
        $login_message = 'login_failed_update';
        $res = array('result' => $login_message);
        echo format_api(0, $res, 0, "This version is too old. Please update your App");
        return;
    }
    if($appversion == '0.1'){
        $login_message = 'login_succeed_update';
        $err = "This version is too old. Please update your App";
    }    
    // if restaurant manager -> get restaurant name

	$mode = 'api';
	$logintype = 'backoffice';
	if($appversion == 'translation') {
		$logintype = 'translation';
		$mode = 'api-ajax';
		}
    $log = new WY_Login(set_valid_login_type($logintype));
    $log->process_login_remote($email, $pass, $mode);
    if($login_message == ''){ $login_message = 'login_succeed';}
    
    $user_info = $log->signIn;
    
    $mobile_array['prefix'] = '';
    $mobile_array['mobile'] = $user_info['mobile'];
    
    $tmp = explode(' ', $user_info['mobile']);
    if(count($tmp) == 2 && substr($tmp[0], 0, 1) == '+'){
                        $mobile_array['prefix'] = trim($tmp[0]);
                $mobile_array['mobile'] = trim($tmp[1]);
    }else{    
        if(substr($user_info['mobile'], 0, 1) == '+' ){

            if(substr($user_info['mobile'], 1, 1) == '1' ){
                $mobile_array['prefix'] = trim(substr($user_info['mobile'], 0, 4));
                $mobile_array['mobile'] = trim(substr($user_info['mobile'], 4));
            }else{
                $mobile_array['prefix'] = trim(substr($user_info['mobile'], 0, 3));
                $mobile_array['mobile'] = trim(substr($user_info['mobile'], 3));
            }
        }
    }
    
    $res = array(
        'result' => $login_message, 
        'token' => $log->token, 
        'member_type' => 'member', 
        'firstname' => $user_info['firstname'], 
        'lastname' => $user_info['name'], 
        'gender' => $user_info['gender'], 
        'cookie' => $log->cookie, 
        'duration' => $log->duration, 
        'prefix' => $mobile_array['prefix'],
        'mobile' => $mobile_array['mobile']);
    
    if($log->result > 0) {
      $_SESSION['email'] = [
          "email" => $email,
          "data" => $res,
      ];
    }
    echo ($log->result > 0) ? format_api(1, $res, 1, $err) : format_api(0, NULL, 0, $log->msg);
}

function GetFacebookUser($email, $facebookid, $facebooktoken) {

    $log = new WY_Login(set_valid_login_type('member'));
    $data = $log->get_facebook_member($email, $facebookid, $facebooktoken);
    echo ($log->result > 0) ? format_api(1, $data, 1, $log->msg) : format_api(0, NULL, 0, $log->msg);
}


function rmloginc_facebook($app) {
//return state 
//
//
//$email, $facebookid, $facebooktoken
    // 0  not login
    // 1 login 
    // 2 login but need to update api 
        
    $email = $app->request->post("email");
    $facebookid = $app->request->post("facebookid");
    $facebooktoken = $app->request->post("facebooktoken");
    $appname = $app->request->post("name");
    $appversion = $app->request->post("version");

    rmloginfacebookc($email, $facebookid, $facebooktoken, $appname, $appversion);
}

function rmloginfacebookc($email, $facebookid, $facebooktoken, $appname, $appversion) {

    $restau = '';
    $login_message = '';
    $err = '';
    //check app version and return error if too old
    if($appversion == '0'){
        $login_message = 'login_failed_update';
        $res = array('result' => $login_message);
        echo format_api(0, $res, 0, "This version is too old. Please update your App");
        return;
    }
    if($appversion == '0.1'){
        $login_message = 'login_succeed_update';
        $err = "This version is too old. Please update your App";
    }    
    // if restaurant manager -> get restaurant name
	$mode = 'api';
	$login_type = 'backoffice';
	if($appversion == 'translation') {
		$login_type = 'translation';
		$mode = 'api-ajax';
		}
    if($appname == 'weeloy'){
        $login_type = 'member';
    }
    
    $log = new WY_Login(set_valid_login_type($login_type));
    $res_login = $log->process_remote_facebook($email, $facebookid, $facebooktoken, $mode);
    if($login_message == '' && $res_login != ''){ $login_message = 'login_succeed';}
    
    $user_info = $log->signIn;
    
    $mobile_array['prefix'] = '';
    $mobile_array['mobile'] = $user_info['mobile'];
    
    $tmp = explode(' ', $user_info['mobile']);
    if(count($tmp) == 2 && substr($tmp[0], 0, 1) == '+'){
                        $mobile_array['prefix'] = trim($tmp[0]);
                $mobile_array['mobile'] = trim($tmp[1]);
    }else{    
        if(substr($user_info['mobile'], 0, 1) == '+' ){

            if(substr($user_info['mobile'], 1, 1) == '1' ){
                $mobile_array['prefix'] = trim(substr($user_info['mobile'], 0, 4));
                $mobile_array['mobile'] = trim(substr($user_info['mobile'], 4));
            }else{
                $mobile_array['prefix'] = trim(substr($user_info['mobile'], 0, 3));
                $mobile_array['mobile'] = trim(substr($user_info['mobile'], 3));
            }
        }
    }
    
   
    
    $res = array(
    	'result' => $login_message, 
    	'token' => $log->token, 
    	'member_type' => 'member', 
      'firstname' => $user_info['firstname'], 
      'lastname' => $user_info['name'], 
      'gender' => $user_info['gender'], 
      'prefix' => $mobile_array['prefix'], 
      'duration' => $log->duration, 
      'cookie' => $log->cookie, 
      'mobile' => $mobile_array['mobile'] 
    );
    if($log->result > 0) {
      $_SESSION['email'] = [
          "email" => $email,
          "data" => $res,
      ];
    }
    echo ($log->result > 0) ? format_api(1, $res, 1, $err) : format_api(0, NULL, 0, $log->msg);
}


/* add platform */

function rmlogout($email) {
	$mode = $platform = "api";
    $log = new WY_Login(set_valid_login_type($platform));
    $log->logout_remote($email, $mode);
    unset($_SESSION['email']);
    echo ($log->result > 0) ? format_api(1, NULL, 1, "loggedout") : format_api(0, NULL, 0, "loggedout");
}


function rmlogoutc($email, $token, $platform) {
	
	$mode = ($platform != 'translation') ? "api" : "api-ajax";
    $log = new WY_Login(set_valid_login_type($platform));
 	$log->check_login_remote($email, $token, $mode);
    if($log->result > 0)
    	$log->logout_remote($email, $mode);
    echo ($log->result > 0) ? format_api(1, "1", "1", "") : format_api(0, "0", "1", "");
}

function rmforgotc($email, $platform) {
    $log = new WY_Login(set_valid_login_type($platform));
    $log->forgotPass($email);
    echo ($log->result > 0) ? format_api(1, "1", "1", "") : format_api(1, "0", "1", "");
}

function rmchangepassc($email, $password, $npassword, $token, $platform) {

	$mode = ($platform != 'translation') ? "api" : "api-ajax";
    $log = new WY_Login(set_valid_login_type($platform));
 	$log->check_login_remote($email, $token, $mode);
    if($log->result < 0) {
    	echo format_api(1, "0", "1", "");
    	return;
    	}
    	
	$arg = new WY_Arg;
	$arg->email = $email;
	$arg->password = $password;
	$arg->npassword = $arg->rpassword = $npassword;
	$log->UpdatePassword($arg);
    echo ($log->result > 0) ? format_api(1, "1", "1", "") : format_api(1, "0", "1", "");
 }

function rmcheckstatusc($email, $token, $platform) {

	$mode = ($platform != 'translation') ? "api" : "api-ajax";
    $log = new WY_Login(set_valid_login_type($platform));
 	$log->check_login_remote($email, $token, $mode);
    echo ($log->result > 0) ? format_api(1, "1", "1", "") : format_api(1, "0", "1", "");
}

/* add platform */

function rmreadlogin($email, $token) {

    $log = new WY_Login(set_valid_login_type('backoffice'));
    $log->check_login_remote($email, $token, 'api');
    echo ($log->result > 0) ? format_api(1, NULL, 1, "loggedin") : format_api(0, NULL, 0, "loggedout");
}

//TODO
function addRestaurant() {

    return;

}

function updateRestaurant($id) {

    return;

}

function deleteRestaurant($id) {

    return;

}

function getBookingDayReport($when, $type) {

	$when = intval($when);
	$today = date("Y-m-d");
	
	$date=date_create($today);
	date_sub($date,date_interval_create_from_date_string($when . " days"));
	$startday = date_format($date,"Y-m-d");

	if($type == "r")
		$sql = "SELECT * FROM booking WHERE rdate >= :query ORDER BY rdate ASC";
	else if($type == "c")
		$sql = "SELECT * FROM booking WHERE cdate >= :query ORDER BY rdate ASC";
	else {
		$sql = "SELECT * FROM booking WHERE rdate >= :query ORDER BY rdate ASC";
		$startday = $today;
		}
	
    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("query", $startday);
        $stmt->execute();
        $bookings = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;

        $errors = null;
        $data = array("bookings" => $bookings);
        echo format_api(1, $data, count($bookings), $errors);
        //echo json_encode($cuisine);
    } catch (PDOException $e) {
        api_error($e, "getBookingDayReport");
    }
}

function getBookingsUser($date1 = NULL, $date2 = NULL) {
    $email = $_SERVER['PHP_AUTH_USER'];

    try{
        $member = new WY_Member($email);
        $bookingsAr = $member->getBooking($date1, $date2, 'DESC');
        $errors = null;
        $data = array("bookings" => $bookingsAr);
        echo format_api(1, $data, count($bookingsAr), $errors);
        //echo json_encode($cuisine);
    } catch (PDOException $e) {
        api_error($e, "getBookingsUser");
    }
}

function getBookingsUserPeriod($period) {
    $email = $_SERVER['PHP_AUTH_USER'];

    try{
        $params = array('include_reviews' => true, 'period' => $period);
        $bookings = new WY_Booking($email);
        $bookings->getUserBookings($email, $params);
        $errors = null;
        $data = array("bookings" => $bookings->bookings);
        echo format_api(1, $data, count($bookings->bookings), $errors);
        //echo json_encode($cuisine);
    } catch (PDOException $e) {
        api_error($e, "getBookingsUser");
    }
}


function getWalkinsUserFilter($user, $filter, $date1, $date2) {
    $where = '';
    if ($date1 == '2013-12-01') {
        $date1 = date('Y-m-d', time() - 3600 * 24);
    }
    if ($date2 == '2015-12-01') {
        $date2 = date('Y-m-d', time() + 3600 * 24 * 1000);
    }

    if ($date1 == 'START_DATE') {
        $date1 = date('Y-m-d', time());
        $date2 = date('Y-m-d', time() + 3600 * 24 * 7);
    }

    if ($filter == 'all') {
        $where = '';
    } else {
        if ($filter == 'not_spun') {
            $where = ' AND (wheelwin IS NULL OR wheelwin LIKE "") ';
        } else {
            $where = ' AND (wheelwin IS NOT NULL && wheelwin NOT LIKE "") ';
        }
    }
    $selectresto = "b.restaurant='".$user."'";

    $sql = "SELECT b.ID, b.confirmation, b.type, b.restaurant, rest.title as title, rest.cuisine, b.restable, b.status, b.restCode, b.membCode, b.email, b.mobile, b.salutation, b.lastname, b.firstname, b.cdate, b.rdate, b.rtime, "
            . "b.cover, b.revenue, b.specialrequest,rest.mealtype, b.wheelsegment,wheelwin, b.wheeldesc, b.spinsource, b.browser, b.language, b.country, b.ip,"
            . "r.reviewgrade, r.foodgrade, r.ambiancegrade, r.servicegrade, r.pricegrade, r.comment, r.response_to, r.reviewdtcreate "
            . " FROM booking b "
            . "LEFT JOIN restaurant rest ON b.restaurant = rest.restaurant "
            . "LEFT JOIN review r ON b.confirmation = r.confirmation "
            . "WHERE type = 'walkin' AND ".$selectresto." AND rdate > :date1 AND rdate < :date2 $where ORDER BY rdate ASC ";

    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        //$stmt->bindParam("query", $user);
        $stmt->bindParam("date1", $date1);
        $stmt->bindParam("date2", $date2);
        $stmt->execute();
        $data_sql = $stmt->fetchAll(PDO::FETCH_BOTH);
        $db = null;

        $errors = null;
        $bookingsAr = array();
        foreach ($data_sql as $data){
            $bookings['ID'] = $data['ID'];
            $bookings['confirmation'] = $data['confirmation'];
            $bookings['restaurant'] = $data['restaurant'];
            $bookings['title'] = $data['title'];
            $bookings['cuisine'] = $data['cuisine'];
            $bookings['status'] = $data['status'];
            $bookings['restCode'] = $data['restCode'];
            $bookings['membCode'] = $data['membCode'];
            $bookings['table'] = $data['restable'];
            $bookings['email'] = $data['email'];
            $bookings['mobile'] = $data['mobile'];
            $bookings['salutation'] = $data['salutation'];
            $bookings['lastname'] = $data['lastname'];
            $bookings['firstname'] = $data['firstname'];
            $bookings['cdate'] = $data['cdate'];
            $bookings['rdate'] = $data['rdate'];
            $bookings['rtime'] = $data['rtime'];
            $bookings['cover'] = $data['cover'];
            $bookings['revenue'] = $data['revenue'];
            $bookings['specialrequest'] = $data['specialrequest'];
            $bookings['mealtype'] = $data['mealtype'];
            $bookings['wheelsegment'] = $data['wheelsegment'];
            $bookings['wheelwin'] = $data['wheelwin'];
            $bookings['wheeldesc'] = $data['wheeldesc'];
            $bookings['spinsource'] = $data['spinsource'];
            $bookings['browser'] = $data['browser'];
            $bookings['language'] = $data['language'];
            $bookings['country'] = $data['country'];
            $bookings['ip'] = $data['ip'];
            if($data['reviewgrade']!= NULL && !$data['response_to']){
                $bookings['review']['reviewgrade'] = $data['reviewgrade'];
                $bookings['review']['foodgrade'] = $data['foodgrade'];
                $bookings['review']['ambiancegrade'] = $data['ambiancegrade'];
                $bookings['review']['servicegrade'] = $data['servicegrade'];
                $bookings['review']['pricegrade'] = $data['pricegrade'];
                $bookings['review']['comment'] = $data['comment'];
                $bookings['review']['reviewdtcreate'] = $data['reviewdtcreate']; 
            }else{
                $bookings['review'] = NULL;
            }
            
            if($data['reviewgrade']!= NULL && $data['response_to']){
                $bookings['review_response']['reviewgrade'] = $data['reviewgrade'];
                $bookings['review_response']['foodgrade'] = $data['foodgrade'];
                $bookings['review_response']['ambiancegrade'] = $data['ambiancegrade'];
                $bookings['review_response']['servicegrade'] = $data['servicegrade'];
                $bookings['review_response']['pricegrade'] = $data['pricegrade'];
                $bookings['review_response']['comment'] = $data['comment'];
                $bookings['review_response']['reviewdtcreate'] = $data['reviewdtcreate'];   
            }else{
                $bookings['review_response'] = NULL;
            }
            
            $bookingsAr[] = $bookings;
            $bookings = array();
        }
        
        $data = array("bookings" => $bookingsAr);
        echo format_api(1, $data, count($data), $errors);
        //echo json_encode($cuisine);
    } catch (PDOException $e) {
        api_error($e, "getWalkinsUserFilter");
    }
}



function getBookingsUserFilter($user, $filter, $date1, $date2) {
    $member = new WY_Member($user);
    $errors = null;
    $bookingsAr = array();
    try {
        
        $bookingsAr = $member->getBookingsUserFilter($user, $filter, $date1, $date2, 'not_cancled');
        $data = array("bookings" => $bookingsAr);
        echo format_api(1, $data, count($data), $errors);
        //echo json_encode($cuisine);
    } catch (PDOException $e) {
        api_error($e, "getBookingsUserFilter");
    }
}


function addBooking() {
    try {
        if (isset($_REQUEST)) {
            $booking = new WY_Booking();
            $booking->createBooking($_REQUEST['restaurant'], $_REQUEST['rdate'], $_REQUEST['rtime'], $_REQUEST['email'], $_REQUEST['mobile'],  $_REQUEST['cover'], $_REQUEST['salutation'], $_REQUEST['firstname'], $_REQUEST['lastname'], $_REQUEST['country'], $_REQUEST['language'], $_REQUEST['specialrequest'], 'booking', '', '', '', 0);
			if($booking->result < 0) {
            	echo format_api(1, 0, 0, $booking->msg);
            	return;
				}
            $bkconfirmation = $booking->confirmation;
            $booking->getBooking($bkconfirmation);
            
            $path_tmp = explode(':',get_include_path());
            $qrcode_filename = $path_tmp[count($path_tmp)-1].'tmp/'.$bkconfirmation.'.png';
            $qrcode = new QRcode();
            $qrcode->png('{"membCode":"'.$booking->membCode.'"}', $qrcode_filename);
		//file_put_contents($qrcode_filename,$qrcode->image(8));

            $booking->notifyBooking($qrcode_filename);
            //    $booking->notifyBooking();
            $errors = null; 
            echo format_api(1, $booking, count($booking), $errors);
        }
    } catch (PDOException $e) {
        api_error($e, "addBooking");
    }
}

function cancelUserBooking($confirmation) {
    
    $user = $_SERVER['PHP_AUTH_USER'];
    try {
            $booking = new WY_Booking();
            
            $booking->getBooking($confirmation);

            if($booking->email == $user){
                $booking->cancelBooking($confirmation);
                $errors = null; 
                if($booking->status == 'cancel'){
                    echo format_api(1, $booking, 1, NULL);
                }else{
                    echo format_api(1, NULL, 1, $errors);
                }
                
            }
    } catch (PDOException $e) {
        api_error($e, "cancelUserBooking");
    }
}

function addWalkableBooking($app) {
    
    //define var
    
    
    $request = $app->request()->post();
    $restaurant = $request['restaurant'];
    $firstname = $request['firstname'];
    $lastname = $request['lastname'];
    $email = $request['email'];
    $mobile = $request['mobile'];
    $rdate = $request['date'];
    $rtime = $request['time'];    

    try {
        

        if ( is_restaurant_valid($restaurant)  
                &&  is_email_valid($email)
                && is_mobile_valid($mobile)) {
            $booking = new WY_Booking();
            
            //$rdate = date('Y-m-d');
            //$rtime = date("h:i:s");
            $booking->createBooking(
                    $restaurant, 
                    $rdate, 
                    $rtime, 
                    $email, 
                    $mobile,  
                    '', 
                    '', 
                    $firstname, 
                    $lastname, 
                    '', 
                    '', 
                    '', 'walkin', '', '', '', 0);
            
            
            //$booking->notifyBookingWalkable();
            if($booking->result < 0) {
            	echo format_api(1, 0, 0, $booking->msg);
            	return;
				}

            return requestspin($booking->confirmation,$booking->restCode, $booking->membCode);
                    
            //echo format_api(1, $booking, count($booking), $errors);
        }else{
            $errors["error"] = true;
            $errors["message"] = "wrong information";
            echo format_api(1, NULL, 0, $errors);
        }
    } catch (PDOException $e) {
        api_error($e, "addWalkableBooking");
    }
}
function getUserProfilePicture(){
    
    $user = $_SERVER['PHP_AUTH_USER'];
    $media = new WY_Media($user, '' ,'user'); 
    
    $path = $media->getUserProfilePicture($user);
    if (empty($path)) {
       echo format_api(0, NULL, 0, 'Image not found');
       return;
    }   
    echo format_api(1, $path, 0, NULL);
}

function addUserProfilePicture($app){
    $_POST["Stage"] = "Load";
    $category = 'profile';
    $user = $_SERVER['PHP_AUTH_USER'];
    $member = new WY_Member();
    
    $id = $member->getIdByEmail($user);
    $media = new WY_Media($user, $id ,'user');
    
    //CHECK FOLDER EXIST
    $media->l_folder_exist();
    
    //var_dump($_FILES['uploads']);
    if (!isset($_FILES['file'])) {
        echo "No files uploaded!!v";
        return;
    }
   
    $files = $_FILES['file'];

    if ($files['error']=== 0) {
        $name = uniqid('img-'.date('Ymd').'-');
        $res = $media->uploadImage($user, $category, 'user');
    }
   
    if ($res[0] < 0) {
       echo format_api(0, NULL, 0, "$res[1]");
       return;
    }  
        echo format_api(1, 1, 1, NULL);
    }

function setFavorite($app, $restaurant, $is_favorite){
   $user = $_SERVER['PHP_AUTH_USER']; 
   $member = new WY_Member($user);
   $result = $member->setFavoriteRestaurant($restaurant, $is_favorite);
    if($result < 0){
        echo format_api(0, NULL, 1, $result);
    }else{
        echo format_api(1, 1, 1, NULL);
    }
}
    
function addPNDevice($app) {

    $request = $app->request()->post();
    $email = $request['user_id'];

    $device_id = $request['device_id'];
    $type = $request['type'];

    $app_name = 'weeloy_pro';
    if(!empty($request['app_name'])){
        $app_name = $request['app_name'];
    }
    
    $sql = "INSERT INTO `mobile_devices` (`mobile_id`, `arn_id`, `user_id`, `mobile_type`, `status`, `app_name`) "
            . "VALUES ('$device_id', '', '$email', '$type', 'active', '$app_name') ON DUPLICATE KEY UPDATE user_id = '$email', status = 'active' ";
    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $obj = $stmt->execute();
        if ($obj) {
            echo format_api(1, "success", 1, NULL);
        }
        $db = null;
    } catch (PDOException $e) {
        echo format_api(0, NULL, 0, "Update $label failed");
    }
}

function deletePNDevice($app) {

    $request = $app->request()->post();
    $email = $request['user_id'];

    $device_id = $request['device_id'];
    $type = $request['type'];

    $sql = "INSERT INTO `mobile_devices` (`mobile_id`, `arn_id`, `user_id`, `mobile_type`, `status`) "
            . "VALUES ('$device_id', '', '$email', '$type', 'deleted') ON DUPLICATE KEY UPDATE user_id = '$email', status = 'deleted'";
    
    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $obj = $stmt->execute();
        if ($obj) {
            set_header_status(200);	
            echo format_api(1, "success", 1, NULL);
        }
        $db = null;
    } catch (PDOException $e) {
        echo format_api(0, NULL, 0, "Update $label failed");
    }
}

function saveBookingTable($confirmation, $table){
    $booking = new WY_Booking();
    $booking->setConfirmation($confirmation);
    $res = $booking->saveTable($table);
     if ($res === 1) {
        set_header_status(200);	
        echo format_api(1, "success", 1, NULL);
     }else{
        set_header_status(200);	
        
        $error= 'We couldn’t update the table number';
        echo format_api(1, NULL, 1, $error);
     }
}

//// RESTAURANT SERVICE

function getServiceCategories(){
    require_once 'lib/class.service.inc.php';
    
    $service = new WY_Service();
    $categories = $service->getCategories();
    
     if ($categories) {
        set_header_status(200);	
        echo format_api(1, array('categories'=>$categories), 1, NULL);
     }else{
        set_header_status(200);	
        $error= 'We couldn’t get service categories';
        echo format_api(1, NULL, 1, $error);
     }
}


function getServices(){
    require_once 'lib/class.service.inc.php';
    
    $service = new WY_Service();
    $services = $service->getServices();
    
     if ($services) {
        set_header_status(200);	
        echo format_api(1, array('services'=>$services), 1, NULL);
     }else{
        set_header_status(200);	
        $error= 'We couldn’t get service categories';
        echo format_api(1, NULL, 1, $error);
     }
}








function getRestaurantServices($restaurant){
    require_once 'lib/class.service.inc.php';
    $service = new WY_Service();
    $restaurantservices = $service->getRestaurantServices($restaurant);
    
     if ($restaurantservices) {
        set_header_status(200);	
        echo format_api(1, array('restaurantservices'=>$restaurantservices), 1, NULL);
     }else{
        set_header_status(200);	
        $error= 'We couldn’t get service categories';
        echo format_api(1, NULL, 1, $error);
     }
}

function saveRestaurantServices($app){
    require_once 'lib/class.service.inc.php';
    
    $restaurant = $app->request->post("restaurant");
    $active_services = $app->request->post("active_services");

    $service = new WY_Service();
    $resultat = $service->updateServices($restaurant, $active_services);

    if($resultat){
        set_header_status(200);	
        echo format_api(1, 'success', 1, NULL);
    }else{
        set_header_status(200);	
        $error= 'An error occured, please try again later';
        echo format_api(1, NULL, 1, $error);
    }
}



//// REPORTS ////

function getReworldReport(){
    $report = new WY_Report();
    $res = $report->getReworldReport();
    set_header_status(200);	
    echo format_api(1, $res, 1, NULL);
}

//// REPORTS ////








////error format must be 
//$errors = array('type'=>'','message'=>'');
function getErrorExample() {
    echo format_api(0, null, 0, array('type' => 'error_login', 'message' => 'Login failed please try again later'));
}

function testDecryptExample() {
    $query = $_SERVER['REDIRECT_QUERY_STRING'];

    $coding = new WY_Coding;
    var_dump($coding->mydecode($query));
    //echo format_api(0, null, 0, array('type'=>'error_login','message'=>$_RESQUEST));
}

function testEncryptExample() {
    $query = $_SERVER['REDIRECT_QUERY_STRING'];

    $coding = new WY_Coding;
    var_dump($coding->myencode($query));
    //echo format_api(0, null, 0, array('type'=>'error_login','message'=>$_RESQUEST));
}

function format_api($status, $data, $count, $errors, $header_status= 200) {
    
    set_header_status($header_status);
    
    $resultat['status'] = $status;
    $resultat['data'] = $data;
    $resultat['count'] = $count;
    $resultat['errors'] = $errors;
    return json_encode($resultat);
}

//// later on  ////

function authenticate(\Slim\Route $route) {
    // Getting request headers
    $headers = apache_request_headers();
    $response = array();
    $app = Slim::getInstance();

    // Verifying Authorization Header
    if (isset($headers['Authorization'])) {
        $db = new DbHandler();

        // get the api key
        $api_key = $headers['Authorization'];
        // validating api key
        if (!$db->isValidApiKey($api_key)) {
            // api key is not present in users table
            $response["error"] = true;
            $response["message"] = "Access Denied. Invalid Api key";
            echoRespnse(401, $response);
            $app->stop();
        } else {
            global $user_id;
            // get user primary key id
            $user_id = $db->getUserId($api_key);
        }
    } else {
        // api key is missing in header
        $response["error"] = true;
        $response["message"] = "Api key is misssing";
        echoRespnse(400, $response);
        $app->stop();
    }


}

///// DEV and TEST ONLY
function removeUser($app){
    $user = $_SERVER['PHP_AUTH_USER']; 
    try{
        $sql = "DELETE FROM member WHERE email = '$user'";
        
        //pdo_exec($sql);
        $sql = "DELETE FROM login WHERE Email = '$user'";
        //pdo_exec($sql);
        echo format_api(1, 1, 1, NULL);
    } catch (Exception $ex) {
        echo format_api(1, 0, 1, array('error'=>'error'));
    }
    
}


function is_restaurant_valid($restaurant){
    return !empty($restaurant);
}

function is_mobile_valid($mobile){
    return preg_match('/^(NA|[0-9+-]+)$/', str_replace(' ','',$mobile));
    
}

function is_email_valid($email){
    return !(filter_var($email, FILTER_VALIDATE_EMAIL) === false);
}

		 function set_header_status($code){
			$status = array(
			100 => 'Continue',
			101 => 'Switching Protocols',
			200 => 'OK',
			201 => 'Created',
			202 => 'Accepted',
			203 => 'Non-Authoritative Information',
			204 => 'No Content',
			205 => 'Reset Content',
			206 => 'Partial Content',
			300 => 'Multiple Choices',
			301 => 'Moved Permanently',
			302 => 'Found',
			303 => 'See Other',
			304 => 'Not Modified',
			305 => 'Use Proxy',
			306 => '(Unused)',
			307 => 'Temporary Redirect',
			400 => 'Bad Request',
			401 => 'Unauthorized',
			402 => 'Payment Required',
			403 => 'Forbidden',
			404 => 'Not Found',
			405 => 'Method Not Allowed',
			406 => 'Not Acceptable',
			407 => 'Proxy Authentication Required',
			408 => 'Request Timeout',
			409 => 'Conflict',
			410 => 'Gone',
			411 => 'Length Required',
			412 => 'Precondition Failed',
			413 => 'Request Entity Too Large',
			414 => 'Request-URI Too Long',
			415 => 'Unsupported Media Type',
			416 => 'Requested Range Not Satisfiable',
			417 => 'Expectation Failed',
			500 => 'Internal Server Error',
			501 => 'Not Implemented',
			502 => 'Bad Gateway',
			503 => 'Service Unavailable',
			504 => 'Gateway Timeout',
			505 => 'HTTP Version Not Supported');
			 
			header("HTTP/1.1 $code ".$status[$code]);
			return true;
		
                }
?>